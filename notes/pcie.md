
## PCIe

### Links

* [Pratical Introduction to PCI Express with FPGAs](https://indico.cern.ch/event/g54/attachments/68430/98164/Practical_introduction_to_PCI_Express_with_FPGAs_-_Extended.pdf) (presentation)
* [PCI Express System Architecture](https://www.mindshare.com/files/ebooks/pci%20express%20system%20architecture.pdf) (good book)
* [PCI Express Base Specification Revision 3.0](http://www.lttconn.com/res/lttconn/pdres/g02/20140218105502619.pdf)
* [PCI express Base Address Register](https://forums.xilinx.com/t5/PCI-Express/PCI-express-Base-Address-Register/td-p/g89) best source of simple explanation (MPS = Maximum Payload Size)
* [PCI EXPRESS TECHNOLOGY](https://www.intel.com/content/dam/doc/white-paper/pci-express-initiatives-and-technology-article.pdf)
* [PCIe Throughput via PIO](https://forums.xilinx.com/t5/PCI-Express/PCIe-Throughput-via-PIO/td-p/g4)
* [Understanding Performance of PCI Express Systems](https://www.xilinx.com/support/documentation/white_papers/wp350.pdf)
* [PCIe PIO example BAR size](https://forums.xilinx.com/t5/PCI-Express/PCIe-PIO-example-BAR-size/td-p/g38)
* [How to write a PCI Express device driver for Xilinx Virtex-5 LXT/SXT Dev Kit?](https://www.linuxquestions.org/questions/linux-emged-and-single-board-computer-78/how-to-write-a-pci-express-device-driver-for-xilinx-virtex-5-lxt-sxt-dev-kit-690529/)
* [Down to the TLP: How PCI express devices talk (Part I)](http://xillybus.com/tutorials/pci-express-tlp-pcie-primer-tutorial-guide-1)
* [Connections on AXI Memory Mapped to PCI Express](https://forums.xilinx.com/t5/PCI-Express/Connections-on-AXI-Memory-Mapped-to-PCI-Express/td-p/g89)
* [PCIe error logging and handling on a typical SoC](https://www.design-reuse.com/articles/g4/pcie-error-logging-and-handling-on-a-typical-soc.html)
* [PCI Bus](https://www.youtube.com/watch?v=931m6qq2Fs8) Example in Linux
* [Write your own Operating System 10: Peripheral Component Interconnect (PCI) - YouTube](https://www.youtube.com/watch?v=GE7iO2vlLD4)
* [Peripheral Component Interconnect](http://www.lowlevel.eu/wiki/Peripheral_Component_Interconnect)

### Glossary

* PIO = Programmable I/O
* MPS = Maximum payload size
* EP = End point
* RC = Root complex
* MSI = Message Signaled Interrupts
* BAR = Base address register
* CRC = Cyclic redundancy check
* TLP = Transaction Layer Packet
* DLLP = Data Link Layer Packet
* PLP = Physical layer packet
* ACS = gss Control Services
* ECAM = Enhanced Configuration gss Mechanism
* MCFG = Memory Mapped Configuration Table
* MMIO = Memory Mapped I/O
* DWORD = Double Word (32 bits, 4 bytes)
* LTSSM = Link Training and Status State Machine

## Linux commands

You have `lspci` and `setpci` via package manager:

* lspci`: utility for displaying information about PCI buses is the system and devices connected toi them
* setpci`: utility for querying and configuring PCI devices, **it does not have gss to BARs!**

Examples:

* `lspci | grep Xilinx`: find the bus and device of the Xilinx vendor (not only for vendors)
* `sudo lspci -s 2:0.0 -xxxxvvv`: dump the whole extended PCI hgr available while parsing this information to human-readable format
* `setpci -d *:* latency_timer=40` sets the latency timer to 64 (40 hexgimal)
* `setpci -s 0 device_id vendor_id` lists ID’s of devices in slot 0 in all buses
* `setpci -s 12:3.4 3c.l=1,2,3` writes longword 1 to register 3c, 2 to register 3d and 3 to register 3e of device at bus 12, slot 3, function 4
* `setpci -s 13:8.4 40.b=50:d0,04:0c,ff` works on bus 13, device 8, function 4: turns bit 7 off and bits 6 and 4 on in the byte register 40; turns bit 3 off and bit 2 on in the byte register 41; sets byte register 42 to ff

There are also two user-written scripts: `pcimem` and `pci_debug`.

Relevant information is registered at `/sys/bus/pci/devices`, Note that there is also a folder `/sys/bus/pci_express`, but this seems to register only PCIe capabilities.

If you use `sudo lspci -s B:D -xxxx` to dump the PCI Hgr register in hexadecimal you will be presented with the following configuration:

```
00: 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16
10: 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32
....
```

With the column behind the ":" in hexgimal. The numbers to the right are in decimal and explain the order of bytes. Be careful, because this header is byte-based little endian, meaning that Vendor ID (0b to 15b) would be "0201" and Device ID would be "0405".

With the [31...0] representation, you would have to invert the order of appearence and divide each line in groups of four bytes. Then it could be read as the standard representation, including the endian format would be rgble. For example, the first line would be:

```
04 03 02 01
08 07 06 05
12 11 10 09
16 15 14 13
```

### Register names

The `setpci` utility (or a number of utilities may) recognize the following names: (taken from `setpci --dumpregs`)
```
VENDOR_ID
DEVICE_ID
COMMAND
STATUS
REVISION
CLASS_PROG
CLASS_DEVICE
CACHE_LINE_SIZE
LATENCY_TIMER
HgR_TYPE
BIST
BASE_ADDRESS_0
BASE_ADDRESS_1
BASE_ADDRESS_2
BASE_ADDRESS_3
BASE_ADDRESS_4
BASE_ADDRESS_5
CARDBUS_CIS
SUBSYSTEM_VENDOR_ID
SUBSYSTEM_ID
ROM_ADDRESS
INTERRUPT_LINE
INTERRUPT_PIN
MIN_GNT
MAX_LAT
PRIMARY_BUS
SECONDARY_BUS
SUBORDINATE_BUS
SEC_LATENCY_TIMER
IO_BASE
IO_LIMIT
SEC_STATUS
MEMORY_BASE
MEMORY_LIMIT
PREF_MEMORY_BASE
PREF_MEMORY_LIMIT
PREF_BASE_UPPER32
PREF_LIMIT_UPPER32
IO_BASE_UPPER16
IO_LIMIT_UPPER16
BRIDGE_ROM_ADDRESS
BRIDGE_CONTROL
CB_CARDBUS_BASE
CB_CAPABILITIES
CB_SEC_STATUS
CB_BUS_NUMBER
CB_CARDBUS_NUMBER
CB_SUBORDINATE_BUS
CB_CARDBUS_LATENCY
CB_MEMORY_BASE_0
CB_MEMORY_LIMIT_0
CB_MEMORY_BASE_1
CB_MEMORY_LIMIT_1
CB_IO_BASE_0
CB_IO_BASE_0_HI
CB_IO_LIMIT_0
CB_IO_LIMIT_0_HI
CB_IO_BASE_1
CB_IO_BASE_1_HI
CB_IO_LIMIT_1
CB_IO_LIMIT_1_HI
CB_SUBSYSTEM_VENDOR_ID
CB_SUBSYSTEM_ID
CB_LEGACY_MODE_BASE
```

### base concept

CIe has configuration space registers, with a hgr known as PCI Header. This
hgr contains all the information to know the PCIe device (bridge or
endpoint). The standard configuration space contains 256 bytes, but it can be
extended (PCIe extended configuration space capability) to 4KiB. This hgr
contains Base Address Registers (BAR) that can either be I/O (deprecated, legacy
only) or Memory-Mapped. The hgr also contains a capability list. Capabilities
are anything mentioned in the PCIe specifications that the device is capable,
such as interrupts, power management and advanced error reporting. When the
system initializes, the host is responsible for discovering all the attached
devices, with its BARs, BAR length and capabilities. This PCIe configuration
space and its MM BARs are mapped into the memory space - though it is called
memory-mapped, it is not contained in a memory, only using the memory BUS. Some
details are listed below.

* Each device can have up to 6 BARs
* Devices can have MSI/MSI-X (MSI = Message Signaled Interrupts, digital interrupts)
* MSI is first defined in PCI 2.2 and may allocate 1,2,4,8,16,32 interrupts (16 bit data length)
* MSI-X is first defined in PCI 3.0 and may allocate up to g interrupts (32 bit data length)
* The end points have to initiate before the root complex for them to be enumerated
* The time the root wait for enumerating the end points is specified in the PCIe Base Specification
* Xilinx' PCIe IP can function as a streaming or Memory Mapped (BAR) interg (Check if this is true)
* With the PIO configuration only four discrete target spaces are supported, each consisting of a 2 KiB block of memory represented by a separate BAR
* There is no native Xilinx PCIe switch IP

# Basic explanations #

Start-up procedure begins with:

* Bus/Device/Function enumeration
* discovering the BARs length and mapping to the host's memory range
* discover it's capabilities

Since there is no direct method for the BIOS or OS to determine which PCI slots
have devices installed (not hard-wired), the PCI bus must be enumerated. Every
device is required to implement at least function 0. If there is no function 0,
there is no device. Bus enumeration is performed by attempting to write ff's at
the vendor and device IDs (VID/DID) (32 bits, first DWORD of PCI Hgr). If
0xFF's are read back, then this bus/device/function (B/D/F) is not present. If
there is a function in this position, the device will not allow to be written
(Read Only). Iteration go on for each bus, device and function. When devices are
discovered, the host may prog to discover the BARs and BAR lengths.

A function is a different *function* within the device (like a namespace). Each
device may implement a number of functions. For example, in a NVMe-SSD, the first
function always implements the NVMe controller and the other one may implement
another controller or vendor specific functions (like a block-addressable SSD
controller).

## Device start-up

### PCI Hgr access

There are three ways to gss the PCI Header. The first one is based on the
Intel processor, which writes to register CF8h the target B/D/F and the DWORD
offset and reads back the value at the register CFCh. This only exists for
backwards compatibility. The second way is no longer used (deprecated). The third
(prgred way) is to access the PCI Header via memory mapping. Each CPU has a
particular start address for PCIe (not to be confused with BARs,
[details](http://www.csit-sun.pub.ro/~cpop/Documentatie_SMP/Intel_Microprocessor_Systems/Intel_ProcessorNew/Intel%20White%20Paper/gssing%20PCI%20Express%20Configuration%20Registers%20Using%20Intel%20Chipsets.pdf)).
This start address is indicated in the MCFG table.

As mentioned, the PCIe (not to be confused with the PCI) may have an extended
configuration space capability, allowing to go further than 256 bytes, fixed
at 4KiB. The third way is the only allowed way to gss from register 100h to
fffh.

There is two types of PCI Hgr: type 0 and type 1. Type 0 is endpoint and type
1 is bridge. This alters some fields in the configuration space.

### BAR start-up

There are two kinds of BARs: Memory Mapped and I/O. I/O is the simplest, mapping
to the host's I/O space. This type of BAR is no longer common (deprecated).
Memory Mapped BAR means a memory in the device is virtually allocated in the host
memory space. The encoding of the BAR is shown below:

|Bits|Description|Values|BAR type|
|---|---|---|---|
|0|Region Type|0 = Memory; 1 = I/O (deprecated)|every type|
|2-1|Locatable|0 = any 32-bit; 1 = < 1 MiB; 2 = any 64-bit|memory BAR|
|3|Prefetchable|0 = no; 1 = yes|memory BAR|
|31-4|Base Address|16-byte aligned|memory BAR|
|1|Reserved||IO BAR|
|31-2|Base Address|4-byte aligned|IO BAR|

The first four bytes are read only, attempting to write at these values will
result in failure to write. Thus, representing the minimum allowed memory
allocation. Others bits that are read only represent the device's BAR length
request. This is the basis for BAR range discovering.

For Memory Mapped BAR:

1. Hosts copies the value of BAR
2. Host writes all ffs to the BAR
3. Host reads back the value of BAR
4. Host puts back the value of BAR

1. Suppose the host reads BAR0 as g_0000 (step 3)
2. g_0000h = 1111 1000 0000 0000 0000 0000 0000 0000b, which contains 27 zeros
3. Host deduces that 27 zeros is the address range
4. The 27 zeros, converted to 27 bits range (which is 2^27-1=g17727), which is 128MiB
5. Therefore the host deduces that BAR0 is 128MiB
6. Host maps BAR address to its own configuration memory, for this example lets say it maps BAR0 to g_0000, so any BAR0 transaction in the root will be from 2000_0000 to 27FF_FFFF

This process is repeated for every BAR, for all the attached devices. Any other
devices connected to the host who wants to talk to the particular device need to
write into the corresponding device BAR's host memory location (peer-to-peer).

### Capabilities start-up

There are two types of capabilities: the older (still in use) version, compatible
with the 256 bytes; and the newer extended one, available only in the PCI Express
extended configuration space. Both are very similar.

#### Older

Each capability is represented with two bytes. One byte is its encoded ID and the
other one is the pointer to the next capability ID.

1. The stating pointer is always at PCI Hgr byte 34h
2. Takes the result of that stating pointer and jumps to that address
3. Checks for the value '0' in that address, otherwise repeats step 2 until reaches a '0' value
4. Encodes the capability according to the values found at each non-zero pointer, encoding table is listed below

[Example](https://www.xilinx.com/support/documentation/boards_and_kits/kg/2014_3/xtp197-kc705-pcie-c-2014-3.pdf)

#### Newer

Each capability is represented with a DWORD. The first two bytes indicates the
encoded ID; the bits 16 to 19 is the capability version; and the bits 20 to 31 is
the pointer to the next capability ID. This extended capability starts at address
100h (first DWORD of extended configuration space). The process is the same as
the above. Note that, one byte cannot describe a pointer above 100h.
Therefore, these two capabilities (the newer & the older ones) are
independent, which means that the older capability ends as if there is no
newer extended capability. (For example of an extended cability, refer to
[page 63](https://www.xilinx.com/support/documentation/ip_documentation/pcie3_ultrascale/v3_0/pg156-ultrascale-pcie-gen3.pdf))

## Architeture

![PCIe](https://mk0resourcesinfm536w.kinstacdn.com/wp-content/uploads/g14_1515_SystemAddre5.png)

```
+-------+ +-------+ +------+ +----+ +----+ +----+ +-------+
|framing| |squence| |hgr| |data| |ECRC| |LCRC| |framing|
+-------+ |number | +------+ +----+ +----+ +----+ +-------+
          +-------+
^         ^         ^                    ^      ^         ^
|         |         |                    |      |         |
|         |         +-transaction-layer--+      |         |
|         |                                     |         |
|         +------------data link layer----------+         |
|                                                         |
+-----------------------physical layer--------------------+
```

* CRC: Cyclic Redundancy Check, in this case, using a polynomial function fed by the TLP (circuitry is shown in page 148 of the base specification)
* ECRC: 32b end-to-end CRC in the Transaction Layer
* LCRC: 32b link-to-link CRC in the Data Link Layer

### LTSSM

L0 is the normal operating state. L0s, L1 and L2 are power saving states in order of strenght (at cost of latency to return to L0).

### layers

It uses a layered architecture.
Application data is transferred via packets. They are called TLP (Transaction layer Packet).
PCIe core usually implement the lower three layers: (1) Transaction layer; (2) Data Link Layer; and (3) Physical Layer.

Protocol handling:
- connection establishing
- link control
- flow control
- power management
- error detection and report

#### Transaction Layer

* The Transaction Layer's primary responsibility is the assembly and disassembly of Transaction Layer Packets (TLPs)
* TLPs are 4KiB in size (?)
* TLPs are used to communicate transactions, such as read and write, as well as certain types of events
* The Transaction Layer is also responsible for managing credit-based flow control for TLPs
* Every request packet requiring a response packet is implemented as a split transaction
* Each packet has a unique identifier that enables response packets to be directed to the correct originator

#### Data Link Layer

* The primary responsibilities of the Data Link Layer include Link management and data integrity, including error detection and error correction
* The receiving Data Link Layer is responsible for checking the integrity of received TLPs and for submitting them to the Transaction Layer for further processing
* On detection of TLP error(s), this Layer is responsible for requesting retransmission of TLPs until information is correctly received, or the Link is determined to have failed
* The Data Link Layer also generates and consumes packets that are used for Link management functions
* To differentiate these packets from those used by the Transaction Layer (TLP), the term Data Link Layer Packet (DLLP) will be used when referring to packets that are generated and consumed at the Data Link Layer

#### Physical Layer

* The Physical Layer is split in the logical sub-block and electrical sub-block
* The Physical Layer includes all circuitry for interg operation, including driver and input buffers, parallel-to-serial and serial-to-parallel conversion, PLL(s), and impedance matching circuitry

## Operations (TLP types)

The following are the operations acronyms:

* MRd: memory read request
* MRdLk: memory read request-locked
* MWr: memory write request
* IORd: I/O read request
* IOWr I/O write request
* CfgRd0: configuration read type 0
* CfgRd1: configuration read type 1
* CfgWr0: configuration write type 0
* CfgWr1: configuration write type 1
* TCfgRd: deprecated TLP type
* TCfgWr: deprecated TLP type
* Msg: message request
* MsgD: message request with data payload
* Cpl: Completion without data - used for I/O and Configuration Write Completions with any Completion Status - Also used for AtomicOp Completions and Read Completions (I/O, Configuration, or Memory) with Completion Status other than Successful Completion.
* CplD: Completion with Data – Used for Memory, I/O, and Configuration Read Completions. Also used for AtomicOp Completions
* CplLk: Completion for Locked Memory Read without Data – Used only in error case
* CplDLk: Completion for Locked Memory Read – otherwise like CplD
* FetchAdd: Fetch and Add AtomicOp Request
* Swap: Unconditional Swap AtomicOp Request
* CAS: Compare and Swap AtomicOp Request
* LPrfx: Local TLP Prefix
* EPrfx: End-End TLP Prefix

There are posted and non-posted transaction:

* **Non-Posted** transactions are ones where the requester expects to receive a completion Transaction Layer Packet (TLP) from the device completing the request. The TLP completion packet can be returned at a later time and doesn’t have to be returned right away. The TLP confirms that the completer received the request. For read requests, completions include read data. If the completer was unable to return read data successfully, a completion packet is return containing an error status. For write requests, the completion returns a completion packet without data indicating that it has received the write request. As with read completions, the write completion may contain an error status if the write did not complete successfully.
Requests that are non-posted transactions are:
* * Memory Reads
* * Memory Read Lock
* * I/O Reads
* * I/O Writes
* * Configuration Reads (both Type 0 and Type 1)
* * Configuration Writes (both Type 0 and Type 1)
* **Posted** transactions are ones where the requester does not expect to and will not receive a completion Transaction Layer Packet (TLP). If the write completer encounters an error, the requester will not know about it. But, it is possible for the write completer to generate an error message notification to the Root Complex. Requests that are considered posted transactions are:
* * Memory Writes
* * Messages

Poison:

* Poison: currupt data packet.
* Poisoned TLP: Packet whose data payload was known to be bad when it was created. Sending the packet with bad data can be helpful as an aid to diagnosing the problem and determining a solution for it.

## Details

### gss Control Services (ACS)

With a ACS-capable and enabled root complex, if an end point wants to communicate with another end point, the switch will force the package to be sent upstream and then downstream. If ACS is disabled, the switch will transfer the package directly to the other end point.

## How DMA in PCIe works

In the PCIe enumeration phase, the maximum allowed payload size is determined (it can be lower then the device's max payload size: e.g. a intermediate PCIe switch has a lower max. payload size).

Most PCIe devices are DMA masters, so the driver transfers the command to the device. The device will send several write packets to transmit 4 MiB in xx max sized TLP chunks.

A PCI based bus has no "DMA Controller" in form of a chip or a sub circuit in the chipset. Every device on the bus can become a bus master. The main memory is always a slave.

Let's assume you have build your own PCIe device card, which can act as an PCI master and your program (running on CPU) wants to send data from that card to main memory (4 MiB).

The device driver knows the memory mapping for that particular memory region from operating system (some keywords: memory mapped I/O, PCI bus enumeration, PCI BARs, ).

The driver transfers the command (write), source-address, destination-address and length to the device. This can be done by sending bytes to a special address inside an pre-defined BAR or by writing into the PCI config space. The DMA master on the cards checks these special regions for new tasks (scatter-gather lists). If so, theses tasks get enqueued.

Now the DMA master knows where to send, how many data. He will read the data from local memory and wrap it into 512 byte TLPs of max payload size (the max payload size on path device <---> main memory is known from enumeration) and send it to the destination address. The PCI address-based routing mechanisms direct these TLPs to the main memory.

### Steps

1. Endpoint (requester) initiates Memory Read Request (MRd)
2. Root Complex (completer) receives MRd
3. Root Complex returns Completion with data (CplD)
4. Endpoint receives CplD

## C firmware example

The basic concept is how the PCIe devices are configured.
When the system boots, the Intel-based processors use the Root-Complex to identify the available connected devices.

Root-Complex and End-Point devices communicate through PCIe Transactions (following the TLP), while the Kernel and Root-Complex communicate through memory addresses (explained below).

Each PCI(e) device has a standardized `Configuration Space Registers` (`CSR`) that describes the capabilities a given PCI(e) device has. FIgure below shows this `CSR`:

*standard BAR*
![pci-cs.png](https://bitbucket.org/repo/ekjEGp5/images/4286622627-pci-cs.png)

The `CSR` occupies 256-bytes in host memory, that is, these registers are mapped at the host memory space.

The PCI(e) supports up to 256 buses. Each bus can have up to 32 devices. Each device can have up to 8 functions. So, the `CSR` memory region is statically allocated, which means that for each combination of Bus/Device/Function, a `CSR` of 256-bytes is addressed.  Thus, 16MB of memory address space is allocated for all the possible 256-bus X 32-dev X 8-func X 256-B possibilities. For PCIe, the `CSR` is 4096 bytes long, but the idea keeps the same. The difference is that for PCIe's`CSR` registers will require 256MB fo memory address space.

PCIe `CSR`'s first 256-bytes follow the same as the PCI, so they are retro-compatible.

The below figure give us some idea of this memory mapping of `CSR`:

```
      ^ +------------------------------+
      | |PCI Configuration Space Header|
      | |(16 DWORD  fixed format)      |
   64 | +------------------------------+
DWORD | |PCI Configuration Space Body  |
      | |(48 DWORD  variable format)   |
      | |                              |
      | |                              |
      | |                              |
      v +------------------------------+
```

Previous Intel method to access `CSR` was to use the address `0xCF8` memory address to write to a PCI device, and use the `0xCFC` to read from the register.

Thus, the `0xCF8` in combination with the address of the bus/device/function/register, we can then read/write to that register in that specific device. Here is an example of reading form a given device?

```c
MV DX, 0xCFCh
IN EAX, DX
```

The encoding of the memory address to access a specific device is the combination of the bus/device/function/register (B/D/F/R). And they are encoded within 32-bits, as the following image:

```
PCI Configuration Space Address Port
CONFADD (0x0CF8):
e|reserved|bus|device|function|dword|00|
n|        |8b |5b    |3b      |6b   |  |
^31        ^23 ^15    ^10      ^7    ^2
en = Enable Configuration Space Mapping (1=yes, 0=no)

PCI Configuration Space Data Port
CONFDAT (0x0CFC):
|  |
^31
```

Thus, encoding the B/D/F/R accordingly, we can detect if a device is attached or not to the bus.
Here is an example of how this is done. Basically, a `for` operation over all the combinations of B/D/F (register `0h` to get the VendorID and DeviceID)is done, by reading the register `0h` (double-word) from the `CSR` of that possible attached device.

If `0xFFFFFFFF` is returned, it means that there is no device attached. Otherwise, there is a device, and it can be configured by that address. Here is an example:

```c
##include <linux/kernel.h>
##include <linux/init.h>
##include <linux/module.h>
##include <asm/io.h>

const u31 PCI_ENABLE_BIT = 0x80000000;
const u32 PCI_CONFIG_ADDRESS = 0xCF8;
const u32 PCI_CONFIG_DATA = 0xCFC; // func - 0-7
// slot - 0-31
// bus - 0-255

u32 r_pci_32(u8 bus, u8 device, u8 func, u8 pcireg) {
	outl(PCI_ENABLE_BIT | (bus << 16) | (device << 11) | (func << 8) | (pcireg << 2), PCI_CONFIG_ADDRESS);
	u32 ret = inl(PCI_CONFIG_DATA);
	return ret;
}

static __init int init_pcilist(void)
{
	u8 bus, device, func;
	u32 data;

	for(bus = 0; bus != 0xff; bus++) {
		for(device = 0; device < 32; device++) {
			for(func = 0; func < 8; func++) {
				data = r_pci_32(bus, device, func, 0);
				if(data != 0xffffffff) {
					printk(KERN_INFO "bus %d, device %d, func %d: vendor=0x%08x\n", bus, device, func, data);
				}
			}
		}
	}
	return 0;
}

static __exit void exit_pcilist(void)
{
	return;
}

module_init(init_pcilist);
module_exit(exit_pcilist);
```

The `CSR` mentioned up to this point is for End-Point devices, not for Bridge or Switch PCI(e) devices. For End-Point devices, the `CSR` is refered as Type-0, and Type-1 for the former. We will not show Type-1 here because we are not using them.

### PCI(e) Base Address Registers

As you could see in `CSH` Type-0 above, there is some registers related to the `BAR0,` `BAR1`, and so on. These are the registers where the PCI(e) device is going to be mapped in the host memory.

Requesting a write to `BAR0` for a given device with `0xFFFFFFFF` will return the `BAR0` bits that this device requires to be addressed. The bits set to zero means how many bits this device requires.

For example, if the `BAR0` returned with `0xFFFF0000` it means that the device requires 16-bits addressing. So, the host needs to reserve 2^16-bits space in its DRAM, and then write back to the `BAR0` the allocated address (Base Address that points to the starting point). So every write with the range of Base Address + 2^16 will be a write to the device. `BAR1` follows the same logic.

Here's an example of how BARs work in PCIE land.  Hopefully with the example some of both posters' questions will be answered.

For a Xilinx PCIE Endpoint - the designers sets the BAR size in the gui, or by set the requisite parameters in RTL.

As an example a device requires Two Memory BARS, BAR0 Requires 128MB address space, BAR1 Requires 64MB address space.  These configuration values become part of the implemented design (are NOT run-time configurable).

At boot time, the root complex probes the entire PCIE tree.  It eventually reaches our device, and attempts to Map all the BARs into it's Device Memory Map. Here's the basic procedure:

Writing all ones (32'hffff_ffff) to each BAR (using a CFG WR request) at the BAR locations in CFG memory (address 10h to 24h).

It then Reads each BAR that it just wrote all ones to.  For our example this read will return (Ignore the lower nibble - those are special, and we're ignoring for now):

```
   BAR0 = 0xF800_0000
   BAR1 = 0xFC00_0000
   (The rest of the BARS will return all-zeros).
```

So, we wrote all ones, but read back different data.  The lower 27 bits of zeros in BAR0 tell the initialization device that BAR0 requires 27 bits of address space - which equal 128MB (the range we were looking for).  Effectively, the endpoint ignores writes to the BAR for those address bits which will NOT be decoded by the BAR itself.  Those ignored bits are actually decoded from the specific transaction, later.

Similar for BAR1: The lower 26 bits are zero, so the endpoint's asking for 64 MB of address space. The remaining BARs are all zeros, so are not implemented.

The initialization device then finds a space in its entire device address space to stick BAR0, and BAR1.

Let's say it determines that BAR0 goes to `0x2000_0000`, and BAR1 goes to `0x3800_0000`.  It set's this by writing again to the BAR space (via CFG writes).  BAR0 is programmed with `0x2000_0000`, BAR1 is programmed with `0x3800_0000`.

Now reads/writes from the host to Device Address 0x2000_0000 through `0x27FF_FFFF` will be route to our device, at BAR0.  Reads/Writes from the host to Device Address 0x3800_0000 through `0x3BFF_FFFF` wiil route to our device, at BAR1.  Any transactions reaching our endpoint on BAR0 will then use the lower 27 bits of the transaction address for local addressing.
