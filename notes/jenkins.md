
# Jenkins

A Jenkins pipeline is a combination of plugins that supports integration and
implementation of continuous delivery pipelines. A Jenkins pipeline can be
defined using a text file called `Jenkinsfile`.

- Pipeline: set of instructions for continuous delivery, contains instructions
  to the entire build process
- Node: machine which runs Jenkins
- Step: a single task
- Stage: named group of steps

## Jenkinsfile

Jenkinsfile can be defined by either Web UI or manually. There are two types of
Jenkins pipeline: declarative and scripted. Both declarative and scripted
syntax are different from each other and are defined totally differently. The
file is written using the Groovy Domain-Specific Language. Just like `Makefile`,
Jenkinsfile has environment variables.

### declarative

Contains a predefined hierarchy to create Jenkins pipelines, giving you the
ability to control all aspects of a pipeline execution in a simple manner

```groovy
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                echo 'Building..'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing..'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying....'
            }
        }
    }
}
```

### scripted

Runs on the Jenkins master with the help of a lightweight executor, using very
few resources to translate the pipeline into atomic commands Scripted is mostly
for advanced stuff and should be avoided.

```groovy
stage('Build') {
    /* .. snip .. */
}

stage('Test') {
    parallel linux: {
        node('linux') {
            checkout scm
            try {
                unstash 'app'
                sh 'make check'
            }
            finally {
                junit '**/target/*.xml'
            }
        }
    },
    windows: {
        node('windows') {
            /* .. snip .. */
        }
    }
}
```

### syntax

The syntax can be generated with Blue Ocean, i.g.

#### pipeline

Includes all the processes like create, check, deploy, etc. In a
Jenkinsfile, it’s a list of all the levels. All of the stages and steps within
this block are described. This is the fundamental block to the syntax of a
declarative pipeline.

```groovy
pipeline {

}
```

#### node

A system running a complete workflow.

```groovy
node {

}
```

#### agent

A directive that can run multiple builds using just one Jenkins instance.
It instructs Jenkins to assign the builds to an executor. Some of the most
commonly used Agent parameters are:

- Any: runs the stage pipeline on any available agent
- None: added to the root of the pipeline, meaning that there
  is no global agent for the entire pipeline, and each stage must define its own
  agent
- Label: Performs on the labeled agent the pipeline/stage.
- Docker: use a docker container as a pipeline execution
  environment or as a specific level. Example:

```groovy
pipeline {
    agent {
        docker {
            image  'ubuntu'
                }
            }
        }
```

#### stages

Includes all of the work that needs to be completed. The work is defined in the
form of stages. Each executing a particular task.

```groovy
pipeline {
agent any
    stages {
        stage ('Build') {

        }
        stage ('Test') {

        }
        stage ('QA') {

        }
        stage ('Deploy') {

        }
        stage ('Monitor') {

        }

    }
 }
```

#### Steps

Performed in sequence for the execution of a level. Within the guideline, there
must be at least one step.

```groovy
pipeline {
    agent any
        stages {
            stage ('Build') {
                steps {
                    echo
                    'Running build phase. '
                }
            }
        }
}
```

### variables

- accessing: `"${env.VARIABLE}"`
- setting:

```groovy
environment {
	VARIABLE = 'value'
}
```

They will be available inside the docker file - i.g. `printenv` inside Linux.

