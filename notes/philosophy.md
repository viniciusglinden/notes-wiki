<!-- vim: spelllang=pt_br
-->

# Filosofia

## Objetos

Também chamado de matéria.

Exemplo:

- Objeto de estudo: teologia
- Objeto material primário: Deus
- Objeto material secundário: tudo criado por Deus
- Objeto formal _quod_: o que é próprio de Deus
- Objeto formal _quo_: a razão iluminada ou guiada pela fé

### Objeto material

> Definição: é a realidade da que propriamente se ocupa o objeto de
  estudo

É o "o que".

Distingui-se entre:

- primário, principal: a matéria em si
- secundário: as diferentes formas que decorrem da matéria

### Objeto formal

> Definição: indica o ponto de vista

É o "porquê".

Também chamado de objeto de investigação ou motivo.

Distingui-se entre:

- _quod_ (por o qual): o que é próprio do objeto
- _quo_ (pelo qual): os meios, métodos e instrumentos a considerar o objeto

## A fortiori

"Com mais razão". Expressão usada para determinada conclusão deve ser aceita,
pois pela lógica é mais verdadeira que a anterior. Se a verdade anterior é
aceita, logo esta outra deve ser mais aceita.

## trívium quadrívium

artes liberais: conjunto de matérias ao qual se deveriam dedicar os homens
livres: trívium e quadrívium.

- trívium: gramática, retórica, lógica
- quadrívium: aritmética, geometria, música, astronomia

