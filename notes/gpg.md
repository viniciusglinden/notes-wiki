
# gpg

## generate key

```sh
gpg --full-gen-key
```

## encrypt file

```sh
gpg -c <file>
```

### with gpg key

```sh
gpg --recipient recipient@email.com --encrypt file
gpg -r recipient@email.com -e file
```

## decrypt file

```sh
gpg <file.gpg>
```

or

```sh
gpg -d <file.gpg>
```

### with gpg key

```sh
gpg --decrypt file
gpg -d file
```

Note: GPG password is cached for some minutes after use.

## authenticity check

```sh
gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-key "27DE B156 44C6 B3CF 3BD7  D291 300F 846B A25B AE09"
gpg --verify sha256sum.txt.gpg sha256sum.txt
```

## changing password

```sh
gpg --list-secret-keys # list available keys
gpg --edit-key KEY_HASH
gpg> passwd
gpg> save
```

## list secret keys

```sh
gpg --list-secret-keys --keyid-format=long
```

## export keys

- public: `gpg --output public.pgp --armor --export username@email`
- private: `gpg --output private.pgp --armor --export-secret-key username@email`
  or `gpg --output backupkeys.pgp --armor --export-secret-keys --export-options export-backup user@email`

## starting gpg agent manually

```sh
gpg-agent
```

## Error: File does not exist!

```sh
gpg: keyblock resource '/home/vln/.local/share/gnupg/pubring.kbx': No such file or directory
gpg: Fatal: /home/vln/.local/share/gnupg: directory does not exist!
```

Just create the directory and change access type to just read: `chmod 700`.

## WARNING: unsafe permissions on homedir

This error may when the gpg's home directory is at a custom place, or in control
of the user. The access permissions should ideally be `700` for the directory
itself and, for its contents:

```sh
ls -l .gnupg
drwx------ 2 linden linden 4,0K Jun 26 11:36 crls.d
drwx------ 2 linden linden 4,0K Jun 26 05:28 private-keys-v1.d
-rw-r--r-- 1 linden linden 6,4K Jun 26 11:42 pubring.kbx
-rw-r--r-- 1 linden linden 3,2K Jun 26 11:37 pubring.kbx~
srwx------ 1 root      root         0 Jun 26 11:38 S.dirmngr
-rw------- 1 linden linden 1,2K Jun 26 11:37 trustdb.gpg
```

The problem is the result of running gpg with sudo: gpg then runs as root, but
its home directory is still the user’s. This explains both the warning (gpg is
running as root but another user owns the configuration directory) and dirmngr’s
socket’s ownership.

1. `sudo gpgconf --kill dirmngr`
2. `sudo chown -R $USER .gnupg`

## scripting

```sh
gpg --no-tty -q -d "$XDG_CACHE_HOME/password_fox.gpg"
```
