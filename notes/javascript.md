# javascript

Javascript was always intended to run on browsers. Since then, it has been
extended with backend engines such as nodejs to run on backend and create native
applications with electron on computer and phone applications.

- [node](nodejs.md): javascript engine for backend applications
- yarn: **javascript** package manager (replaces npm, but is compatible with
  npm)

## `let` vs `var` vs `const`

Difference is the scope:

- `var`: group (function) scope, initialized to `undefined`
- `let`: global scope, uninitialized
- `const`: global scope

```javascript
var greeter = "hey hi";
var times = 4;

if (times > 3) {
    var greeter = "say Hello instead"; // note the var keyword again
}

console.log(greeter) // "say Hello instead"
```

```javascript
let greeting = "say Hi";
let times = 4;

if (times > 3) {
     let hello = "say Hello instead";
     console.log(hello);// "say Hello instead"
 }
console.log(hello) // hello is not defined
```

## debugging

### front-end

- `console` object: `console.log()`, `console.dir()`, `console.assert()`, `console.warn()`,
  `console.table()`, etc
- `debugger` to set breakpoints
- set a breakpoint in the browser debugger, equivalent to `debugger`

You may also debug with the integrated VScode debugger (access the browser debugger).

### back-end

The npm [debug](https://www.npmjs.com/package/debug) package is a prettified `console.log()`: it
does not contain breakpoints.

In addition to the above, with npm you may `npm inspect script` ~`npm debug script.js`~, which will
give you a gdb-like interface, stopping at the very first line. Inside of this, just type in `help`
and it will give you the commands that you may run. In this case, the line `debugger` will also set
a breakpoint.

If you wish to use a web-like breakpoint based debugging experience, take a look at
[node-inspector](https://www.npmjs.com/package/node-inspector).

