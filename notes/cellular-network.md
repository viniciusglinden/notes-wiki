
# Cellular Network Technology

## 5G

- [5G New Radio in Bullets](http://www.5g-bullets.com/)
- [ShareTechnote](https://www.sharetechnote.com/)

- Specified by the 3GPP group
- Separation of logical and physical definitions
- Uses both TDD and FDD

|LTE|NR
-|-|-
use cases|Mobile Broadband Access, MTC|eMBB, mMTC, URLLC
latency|~10 ms|< 1ms
band|< 6 GHz|< 60 GHz
SBS|fixed|variable
frequency allocation|UEs need to becode the whole BW|use of bandwidth parts
"always on" signals|CRS, PSS, SSS, PBCH|SSB

### Glossary

- [5G/NR - Acronyms](https://www.sharetechnote.com/html/5G/5G_Acronym.html)

- RAN: Radio Access Network: connects devices to various parts of the network
  through radio connections
- NR: New Radio: 5G RAN
- gNB / gNodeB: 5G base station
- LTE: Long Term Evolution: 4G RAN
- UE: User Equipment
- UL: Uplink: UE transmitting direction
- DL: Downlink: UE receiving direction
- SUP, DUP: Supplemental UL / DL: use of unpaired spectra
- BTS: Base Transceiver Station
- SCS: SubCarrier Spacing
- MSI: Minimum System Information
- RMSI: Remaining Minimum System Information: SIB1
- OSI: Other System Information: SI
- SI: System Information: SIB2...SIB9
- MIB: Master Information Block: critical system parameters needed for UE to
  access the cell: cell identity, system bandwidth and frame configuration
- SIB: System Information Block: SIB1, SIB2, etc. transmitted periodically or on
  demand
- RE: Resource Element: smallest time-frequency resource unit, representing one
  subcarrier in frequency domain and one symbol in time domain
- RG: Resource Grid: composed of the subcarrier frequencies and OFDM symbols
- RB: Resource Block: basic unit of resource allocation in frequency, 12
  congruent subcarriers
- PRB: Physical Resource Block
- VRB: Virtual Resource Block
- CRB: Common Resource Block: number of real RB for the entire cell bandwidth
- CC: Component Carrier: spectrum chunk that can transmit data independently
- CA: Carrier Aggregation: technique where UE is assigned multiple CCs
- SSB: Synchronization Signal Block
- PSS: Primary Synchronization Signal
- SSS: Secondary Synchronization Signal
- PCI: Physical layer Cell ID $= 3\cdot N_{SSS} + N_{PSS}$ (0 to 1007)
- DMRS: Demodulation Reference Signal
- DCI: Downlink Control Information
- UCI: Uplink Control Information
- CCE: Control Channel Elements
- PBCH: Physical Broadcast Channel: provides SFN
- RACH: Random Access Channel: initial access and synchronization
- PRACH: Physical RACH
- RAR: Random Access Response
- PDCCH: Physical Downlink Control Channel: DL control transmission channel
- PDSCH: Physical Downlink Shared Channel: DL data transmission channel
- PUCCH: Physical Uplink Control Channel UL control transmission channel
- PUSCH: Physical Uplink Shared Channel: UL data transmission channel
- SRS: Sounding Reference Signal: UL reference for channel estimation and
  feedback
- SR: Scheduling Request
- BSR: Buffer Status Report
- SFN: System Frame Number: subframe division ticking 10 ms from 0 to 1023
- HFN: Hyper (System) Frame Number: ticks once SFN reached maximum value, ranges
  0 to 1023
- RCC: Radio Resource Control: UR and RAN resource manager protocol
- CB: Code Block
- CBG: Code Block Group
- TB: Transport Block: payload passed betweeb MAC and physical layers
- SRB: Signaling Radio Bearer
- NUL: Normal Uplink
- GP: Gap
- ECP: Extended Cyclic Prefix
- ISI: Inter-Symbol Interference
- ICI: Inter-Carrier Interference
- eMBB: Enhanced Mobile Broadband: use case: data-driven high throughput
  requirements
- mMTC: Massive Machine Type Communications: use case: support for large number
  of devices that sporadically send data
- URLLC: Ultra-Reliable Low-Latency Communication: use case: low latency and
  high reliability
- HARQ: Hybrid Automatic Repeat Request
- TB: Transport Block
- O-RAN: Open RAM: architecture for building virtual RANs on hardware and cloud
- CSI: Channel State Information
- RI: Rank Indicator
- CQI: Channel Quality Indicator
- PMI: Precoding Matrix Information
- CBRA: Contention-Based Random Access
- CFRA: Contention-Free Random Access
- BWP: Bandwidth Part
- ARFCN: Absolute Reader Frequency Channel Number
- GSCN: Global Synchronization Channel Number
- CORESET: Control Resource Set
- RLC: Radio Link Control
- SCG: Secondary Cell Group
- RNTI: Radio Network Temporary Identifier

## Layers

- L1: Physical layer: physical channels and physical signals
- L2: MAC Layer: transport channels
- L3: RLC Layer: Logical channels: divided into control and traffic channels

### Units

- [5G Frequency and Time Domain
  Structure](https://www.youtube.com/watch?v=NCC0XGvlLSg)

- Slot: basic time unit, minimum scheduling unit
- Symbol: smallest unit of time that can carry digital data, defined by the
  numerology and SCS
- OFDM Symbol: time duration occupied by a symbol in an OFDM transmission
- RE: 1 subcarrier x 1 OFDM Symbol
  - Uniquely identified by $(k, l)_{p, \mu}$ where: $p$ is antenna port; $\mu$,
    SCS configuration; $k$, index in the frequency domain; $l$, symbol position
    in time domain

- RG: number of subcarriers in frequency axis and number of OFDM symbols in time
  axis
  - One set of RGs per transmission direction (UL, DL)
  - One set of RG assigned for each antenna port as per SCS configuration
- PRB: smallest allocation unit assigned to an user
  - 12 congruent subcarriers
  - No difference between RB and PRB in time domain
  - More than one RB in a RG
  - Grouped into PRB

### Frame structure

- [5G Frame Structure: Learn to Navigate the 5G Frame and
  Channels](https://www.youtube.com/watch?v=nyzHkIpWFjE)
- [RG vs RB vs RE | Difference Between RG RB RE In 5G
  NR](https://www.rfwireless-world.com/5G/5G-NR-Resource-Grid-vs-Resource-Block-vs-Resource-Element.html)

- radio frames: fixed 10 ms duration collection of subframes
- subframes: fixed 1 ms duration collection of slots
  - subframe index is noted by a µ
  - $\Delta f_{\text{SCS}} = 2^\mu \cdot 15 kHz$
  - slots in subframe: $2^\mu$
- Slot can have 14 (CP) or 12 (extended CP) OFDM Symbols

![RE, Symbol, subcarrier, subframe
relationship](https://www.rfwireless-world.com/images/5G-NR-RG-RB-RE.webp)

### Subframe

#### Types

- DL Control (Dc)
- DL Data (Dd)
- UL Control (Uc)
- UL Data (Ud)
- Gap (GP)

### Frequency ranges

- FR1: Frequency Range 1
- FR2: Frequency Range 2
- Separation hints different network implementation requirements

FR|minimum|maximum|supporting bandwidths|subcarriers spaces
-|-|-|-|-
FR1|450 MHz|6 GHz|5 to 100 MHz|15, 30, 60 kHz
FR2|25.25 GHz|52.5 GHz|50 to 400 MHz|60, 120, 240 kHz

### Initialization procedure / Entry Call Flow

Basic steps:

1. Cell search and cell selection
2. ...

- NR cell = same SSB information
- SSB blocks are bursted in time and at the same time in physical space
- PSS: 3 values for call ID section used for DL symbol sync
- SSS: 336 values for call ID group, DL frame sync
- PSS and SSS: modulated by BKSP

### Initial access / RACH procedure

- DL synchronization: UE detects the radio boundary (ie. timing when a radio
  frame strats) and OFDM symbol boundary
  - Process is done via SSB detection
- UL synchronization: UE detects timing when it can send UP data (ie. PUSCH \&
  PUCCH)
  - gNB is handling multiple UEs and has to ensure that UE is aligned with
    common receiver of each
  - this is called the RACH procedure

Non-specific (not only 5G) initial access procedure overview:

1. searches for synchronization signal
2. obtain DL synchronization and decode system information
3. perform random access and obtain UL synchronization
4. establish dedicated connection and obtain dedicated connection ID

More 5G specific overview:

1. DL synchronization: UE acquires RACH configuration parameters from gNB
2. RACH Preamble Transmission: UE selects a RACH preamble (predefined sequence)
   and transmit it back to via the PRACH to initialize the RACH procedure
3. RACH response: gNB responds with acknowledge and other essential information,
   including the initial UL grant
4. scheduled transmission: using the UL grant, UE transmits a connection request
   message with essential information
5. contention resolution: when multiple UE select the same preamble, gNB
   resolves this by sending a Contention Resolution message

RACH procedure can be categorized into two types:

- CBRA: UE randomly selects its preamble
- CFRA: gNB selects the UEs preamble

```plantuml
@startuml
actor "UE" as ue
actor gNB as gnb
ue -> gnb: RACH Preamble (MSG-1)
ue <- gnb: RAR (MSG-2)
ue -> gnb: RACH MSG-3
ue <- gnb: RACH MSG-4
@enduml
```

1. ask the preamble ("header") for the strongest beam signal (PCI)
2. receives the RAR, telling about where to find the frequency-time positions
   and the distance from the gNB
3. sends a RACH to synchronize the UE, telling it about how far away it is from
   the gNB
4. gNB confirms the RACH process by sending a final response

### Supporting LTE (NSA 5G NR)

- 5G B1 Threshold: physical LTE / 5G boundary

```plantuml
@startuml
actor "5G UE" as ue
actor "4G LTE eNB" as 4g
actor "5G NR gNB" as 5g
== Check boundary ==
ue <- 4g: RRC Reconfig (B1 NR Freq)
ue -> 4g: RCC Reconfig Complete
ue <- 5g: Measure RSPRP
ue -> 4g: B1 Measurement Report
4g -> 5g: SgNB Addition Request
note right
    5G Setup Attempt
end note
4g <- 5g: SgNB Addition Request ACK
ue <- 4g: RCC Reconfig (NR Bearer)
ue -> 4g: RCC Reconfig Complete
4g -> 5g: SgNB Reconfig Complete
note right
    5G Setup Success
end note
== Connect to 5G network ==
ue -> 5g: NR RACH Preamble MSG-1
note right
    RACH Attempt
end note
ue <- 5g: NR Random Access Response MSG-2 RAR
ue -> 5g: NR RACH MSG-3
note right
    RACH Success
end note
== UE is connected to the 5G NR ==
@enduml
```

### Synchronization Signal (Block)

- Synchronization signal is a predefined data sequence inside a specific OFDMA
  symbol in a specific subframe
- Synchronization signal also contains: Radio Frame Boundary, Subframe Boundary,
  Physical Cell ID, Hypercell ID, System ID, etc
- SBB is composed of PSS and SSS
- Time and frequency synchronization for UE
- The very first message upon power on or moves to another location
- Multiple SBBs can be transmitted together in a SBB burst to improve detection
- SSB burst is in 3D space with the help of multi-beam feature
- Accuracy needs to be within nanoseconds between network nodes as to avoid
  interference

### Channels

- [Different Channels in 5G
  NR](https://syedshan85.medium.com/different-channels-in-5g-nr-part-1-4e030b0c77f7)

The PxSCH carries all the user data, whereas the PxCCH carries specific control
information

![channels correlation](https://miro.medium.com/v2/format:webp/1*y8rwc8HfxnqStiMTqkaHng.png)

#### Downlink

##### PBCH

- Serves system information during initial cell search procedure
- Main task is to carry the MIB to all UEs
- Transmits along with the Synchronization Signal

- Uses a QPSK modulation

##### PDCCH

- Responsible for scheduling resources for transmissions on PDSCH and PUSCH, UL
  power control
- Only carries DCI
- The DCI format and RNTI types determine the type of information the this
  channel sends

- Uses a QPSK modulation

##### PDSCH

- Carries a variety of data: user application data as well as higher UE-specific
  control messages, SIB and paging
- Flexible modulation: QPSK, 16-QAM, 64-QAM, 256-QAM
- Flexible coding scheme

#### Uplink

##### PUCCH

- Carries UCI
  - In some cases, UCI may also be send on the PUSCH

##### PUSCH

- Same as PDSCH, but in the reverse direction
- Can also carry the UCI

##### PRACH

- Primary purpose is to facilitate the initial access process between the UE and
  gNB
- Ensures channel access
- Contains: Random Access Preamble, uplink timing adjustment, sequence generator

### Numerology

- [5G NR: Numerologies and Frame
  Structure](https://howltestuffworks.blogspot.com/2018/02/5g-nr-numerologies-and-frame-tructure.html)

- Defines the frequency domain SCS
- 5G specifies 15, 30, 60, 120 and 240 kHz SCS
  - 240 kHz is only applicable to the Synchronization Signal, not to transfer
    data
- RE duration is equal to one period plus the cyclic prefix
- Cyclic prefix duration is one period of the SCS frequency
- Numerology combinations may be used
- Numerology mixing is specified
  - eg. 15 and 30 kHz numerologies can be mixed while 15 and 120 KHz cannot
- Received sampling frequency depends upon numerology and FFT size

µ|SCS|1 PRB = 12 subcarriers|cyclic prefix|data support|PSS, SSS, PBCH support
-|-|-|-|-|-
0|15 kHz|180 kHz|normal|y|y
1|30 kHz|360 kHz|normal|y|y
2|60 kHz|720 kHz|normal, extended|y|n
3|120 kHz|1.44 MHz|normal|y|y
4|240 kHz|2.88 MHz|normal|n|y
5|480 kHz|5.76 MHz|normal

### Blind detection

Refers to the process of detecting DCI messages without knowing their exact
locations in the network.

### Antenna

Specification allows up to 256 antenna elements, enabling masive MIMO.

#### Antenna port

- "Logical antenna"
- Concept analog to the physical antenna
- Referred to as a number
  - Eg. PBCH is always at antenna port 4000
- Mapping to the physical antenna can be:
  - One to one: useful when in lower frequency band, which do not require
    beamforming)
  - One to many
- Eg. 2x2 MIMO: antenna ports 1000 and 1001 are mapped into different physical
  antennas

Channel/Signal|Antenna ports
-|-
PDSCH|starting with 1000
PDCCH|starting with 2000
CSI-RS|starting with 3000
SS/PBCH|starting with 4000
PUSCH/DMRS|starting with 0
SRS|starting with 1000
PUCCH|starting with 2000
PRACH|exact 4000

### Digital data hierarchy

- CB: Code Block: raw block of digital data pior to adding CRC
  - maximum size is 8448 bits
- CBG: a group of CBs with CRC added
- TB: a group of CBGs

## Cyclic prefix

- Provides a guard period to help prevent ISI baused by the propagation channel
  delay spread
- The received signal with the cyclic prefix is a circular convolution between
  the transmitted symbol and the propagation channel impulse response
    - Allows the impact of the propagation channel to be removed at the receiver
      using a simple multiplication without generating ICI

### Normal Cyclic Prefix

- used in smaller cell environments
  - in smaller cells, the delay between reflected signals is not as significant
- provides sufficient guard time to prevent ISI between OFDM symbols
- shorter, allowing for more useful OFDM symbols per slot

### Extended Cyclic Prefix

- used in large cell environments
  - in larger cells, the delay between reflected signals can be much longer
- provides a longer guard time to handle the increased multipath delay spread in larger cells
  - typically up to 5 km in size
- defined only for the 60 kHz SCS
  - this wider spacing is more suitable for large cell deployments
- fewer OFDM symbols per slot (12 instead of 14) due to the longer CP length

## Bandwidth Part

- BWP refers to a contiguous set of PRBs that are allocated from the overall
  carrier bandwidth for a specific UE at a given time
- CRB is used for numbering the RBs
- "Reference point A" is the center of the CRB0 block
- PRB is used only in BWP
- VRB is used to interliving
  - interliving is grouping VRBs and repositioning them into preferred PRBs
- One BWP has homogeneous numerology
- BWP is defined so that the UE don't have to search everywhere and to save
  power
- A UE has only one active BWP

## Frequency Raster

Frequency raster is twofold:

- Global Frequency Raster: defines raster from 0 to 100GHz
- Channel Raster: spacing between the allowed center transmission frequencies
  - FR1: 100kHz or 200kHz
  - FR2: 50MHz or 100MHz

- For NSA: UE receives RCC message with NR-ARFCN (SSB), which provides the
  raster number
- For SA: SSB follows GSCN raster
- Center frequency is known as "reference point A"

## System Information

The MIB and SIBs are transmitted from the base station to the UE. MIB is the UEs
first objective after turning on.

```
          RRC          RRC      Logical  Transport  Physical
        Message    Sub-Message  Channel   Channel   Channel
┌──────────────┐
│ MSI     MIB  ├───────────────► BCCH ────► BCH ───► PBCH
│┌────────────┐│
││RMSI    SIB1├┼───────────────► BCCH ──► DL-SCH ──► PDSCH
│└────────────┘│
└──────────────┘
┌──────────────┐
│ OSI      SI  ├─► SIB1..SIB9 ─► BCCH ──► DL-SCH ──► PDSCH
└──────────────┘
```

- MIB provides information regarding the CORESET and search space
  used by the PDCCH when making a resource allocation for SIB1
- SIB1 provides scheduling information for all OSI and what SIB
- Some SIB (2 to 9) may only be broadcasted upon UE request
  - SIB1 tells how the SIBs will be broadcasted
  - On-demand is configured with MSG1 or MSG3
