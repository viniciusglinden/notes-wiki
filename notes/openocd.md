
# OpenOCD

- [Open On-Chip Debugger](https://openocd.org/)
- [Project
  setup](https://openocd.org/doc-release/html/OpenOCD-Project-Setup.html)

Open On-Chip Debugger (OpenOCD) is a libre software to debug, program and boundary scan a target
microcontroller, being compatible with a range of hardware adapter (programmer).

To make OpenOCD run, you just need a correct configuration file (.cfg) supplied to OpenOCD.
Configuration files are written in Jim-Tcl. Configuration files inside the `scripts` folder:

- `interface`: hardware adapters
- `target`: MCU targets
- `board`: development boards, ready-made configurations

Say, you run `openocd -f board/stm32l4discovery.cfg`, means to use the OpenOCD configuration for
STM32L4 Discovery board.

To help the available commands, just type in `openocd -c command` or visit the official website.
Creating a new configuration steps are:

1. adapter: what adapter, adapter configuration
2. configure transfer protocol
3. configure target

For USB devices on GNU/Linux you should copy the `contrib/60-openocd.rules` file
to `/etc/udev/rules.d` and add yourself to the "plugdev" group.

## flashing with OpenOCD

- [Flashing and debugging STM32 microcontrollers under
  Linux](https://cycling-touring.net/2018/12/flashing-and-debugging-stm32-microcontrollers-under-linux)
- [STM OpenOCD fork](https://github.com/STMicroelectronics/OpenOCD): this may
  have been merged to the official later on.

- install telnet or gdb
- copy the udev rule inside `/usr/share/openocd/contrib` to the udev
- search for your config file under `/usr/share/openocd/script`, there are
  config files for MCU, programmers - and board, which combine both
- use `openocd -f config1.cfg -f ...`: this opens a telnet (gdb server command)
  and tcl (debugging) and gdb connections

### command line

- `openocd -f board/stm32l4discovery.cfg -c "program PROGRAMM.bin reset exit 0x08000000"`

### telnet

- `telnet localhost <port>`: to get the programming interface in another
  terminal
- type:

```
init
reset init
halt
flash write_image erase myprogram.elf
exit
```

Note: to exit: `^]`.

### gdb

- [Upload code to STM32L4 using linux and
  openocd](https://www.hackster.io/yusefkarim/upload-code-to-stm32l4-using-linux-gnu-make-and-openocd-a3d4de)

Typing `monitor` to gdb tells it to use telnet commands: monitor means, gdb
server commands.

- `arm-none-eabi-gdb ./PROGRAM.elf`[^gdb-1]: `.bin` file does not contain
  debugging information
- `(gdb) target remote localhost:3333` or `(gdb) target extended-remote
  localhost:3333`[^gdb-2]
- `(gdb) monitor reset init`: halt processor
- `(gdb) monitor targets`: find target and see stati
- `(gdb) monitor stm32l4x mass_erase 0`: erase stm32l4x board memory at bank 0
- `(gdb) monitor flash write_bank 0 PROGRAMM.bin 0`: flash programm at bank 0
  with offset 0x00[^gdb-3]
- `(gdb) monitor reset run`: run code on target (unhalt)

[^gdb-1]: use `gdb-multiarch` for different architectures
[^gdb-2]: may be abbreviated to `tar ext :3333`
[^gdb-3]: `PROGRAMM.bin` is relative to `$HOME`

The program should be running now.

Alternatively:

- `*-gdb ./PROGRAM.elf`
- `(gdb) tar ext :3333`
- `(gdb) load`: programs
- `(gdb) monitor reset init`: loads new firmware
- `(gdb) continue`: runs firmware

## debugging with OpenOCD

- [GDB and OpenOCD](https://openocd.org/doc/html/GDB-and-OpenOCD.html)

Do the above to open a connection and then:

- `arm-none-eabi-gdb myprogram.elf`
- `(gdb) target extended-remote localhost:3333` (assuming 3333 is the gdb port)
- `(gdb) tar ext :3333` (same as above)
- `(gdb) monitor reset halt`
- `(gdb) load`

To see all supported OpenOCD commands type `monitor help`.

