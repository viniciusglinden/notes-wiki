
# Printer

```sh
lpr unixtoolbox.ps                 # Print on default printer
export PRINTER=hp4600              # Change the default printer
lpr -Php4500 #2 unixtoolbox.ps     # Use printer hp4500 and print 2 copies
lpr -o Duplex=DuplexNoTumble ...   # Print duplex along the long side
lpr -o PageSize=A4,Duplex=DuplexNoTumble ...
lpq                                # Check the queue on default printer
lpq -l -Php4500                    # Queue on printer hp4500 with verbose
lprm -                             # Remove all users jobs on default printer
lprm -Php4500 3186                 # Remove job 3186. Find job nbr with lpq
lpc status                         # List all available printers
lpc status hp4500                  # Check if printer is online and queue length
```

