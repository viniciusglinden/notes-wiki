
# Commands (Unix)

## Regex

Regular expressions come in differently depending on the application, but should
basically conform to the following (GNU grep):

- `.` any character
- `*` match any number of previous character (including 0)
- `+` match any number of previous character[^regex-1]
- `$` end of the line
- `^` beginning of the line
- `\S` any non-white character
- `\s` any whitespace character
- `?` optional
- `[a-z]` any lowercase letter
- `[A-Z]` any uppercase letter
- `[A-Za-z]` any letter
- `|` or[^regex-1]
- `{N}` repeat least character exactly `N` times

[^regex-1]: has to be escaped with `\`

- html: `https\?://\S*\.[A-Za-z]\+\S*`
- e-mail: `\S\+@\s\+\.[A-Za-z]\+`

See `man 7 regex`. For the C library see `man 3 regex`.

- [Regex debugger](https://www.debuggex.com/)

## grep

- `grep`: basic regex: e.g. `'t\(a\|e\)st'`
- `egrep`: extended regex: = `grep -E`: e.g. `'t(a|e)st'`
- `fgrep`: fixed (no regex): = `grep -F`

## `ln` command confusion

Suppose we run the following command:

```sh
ln -s A B
```

* A is the **target** entity
* B is the **link** name

In other words

```sh
ln -s target link-name
```

Both A can be a directory. The verbose option is given, it will show the following:

`link name -> target entity`

(Note: this is **inverted** from above)

Or, if A a file at `/folder1/test1` and B is `/folder2`

`folder2/test1 -> /folder1/test1`

## `chmod` command

The meaning of each number in `chmod` is as follows (see file
[permissions](linux.md##permissions)):

* 7, `rwx`: read, write, and execute
* 6, `rw-`: read and write
* 5, `r-x`: read and execute
* 4, `r--`: read-only
* 3, `-wx`: write and execute
* 2, `-w-`: write only
* 1, `--x`: execute only
* 0, `---`: none

This means, in 8-based form (`chmod`, one bit per permission):

- r = 4
- w = 2
- x = 1

Additionally:

- `u`: the file owner
- `g`: the users who are members of the group
- `o`: all other users
- `a`: all users, identical to `ugo`

And:

- `-`: removes the specified permissions
- `+`: adds specified permissions
- `=`: changes the current permissions to the specified permissions
  - if not specified, command affects every user permissions, being equivalent
    to `u=`

Examples:

```sh
chmod g=r file # group members may read file, but neither write nor execute
chmod g=r filename # everyone is not allowed to execute

# user may read, write and execute
# group may read only
# others are completly disallowed
chmod u=rwx,g=r,o= file
```

Flags:

- `-R`: recursive
- `--reference=FILE` copy FILE permissions to file

## find something in the whole system

- `locate <file>`

## get link path

- `readlink`: gets the target file name, with `-f` flag, shows the full path
- `readpath`: there are several, it is a utility around `readlink`, does the
  same thing - the use of  `readlnk` is recommended

## Disk usage

For devices, use `df`:

```sh
df -h --total /dev/sda1 /dev/sda2
```

For folders and files, use `du`:

```sh
du -h ./folder
```

Or just use `ls`.

## check progress of coreutils

coreutils = cp, mv, dd, tar, bsdtar, cat, rsync, scp, grep, fgrep, egrep, cut,
sort, md5sum, sha1sum, sha224sum, sha25 , split, gpg

```sh
pacman -S progress
progress -w
```

## `sed`

### new line

`sed` was not make to recognize new line, however, there is this work around:

```sh
sed ':a;N;$!ba;s/\n/ /g' file
```

1. `:a` create a label 'a'
2. `N` append the next line to the pattern space
3. `$!` if not the last line, `ba` branch (go to) label 'a'
4. `s` substitute, `/\n/` regex for new line, `/ /` by a space, `/g` global match (as many times as it can)

`sed` will loop through step 1 to 3 until it reach the last line, getting all lines fit in the pattern space where `sed` will substitute all `\n` characters

## Check status of a file/folder (creation date)

Use `stat` command (example below). Note that the file system may or may not
save the file's creation date.

```sh
$ stat teste
# File: teste
# Size: 4096 Blocks: 8 IO Block: 4096 directory
#Device: 801h/2049d Inode: 9961813 Links: 3
#Access: (0755/drwxr-xr-x) Uid: ( 1000/vglinden) Gid: ( 998/ wheel)
#Access: 2020-06-23 14:39:28.891584037 -0300
#Modify: 2020-06-23 14:39:25.021584086 -0300
#Change: 2020-06-23 14:39:25.021584086 -0300
# Birth: 2020-06-23 14:39:20.141584139 -0300
```

## `man`

Whenever you see something like **see (7) git**, this means chapter 7 of git's
manual page. Then, if the page exists, use `man 7 git`.

## Repair Filesystem

`fsck` checks and File System errors. Run it _preferably_ unmounted.
Syntax is: `fsck -V /dev/sda1` (`V` option makes it verbose, `-f` forces run,
`p` automatically repairs).
Error codes are:

```
0	No errors
1	Filesystem errors corrected
2	System should be rebooted
4	Filesystem errors left uncorrected
8	Operational error
16	Usage or syntax error
32	Checking canceled by user request
128	Shared-library error
```

To force an `fsck` on the next boot, just do (file will be removed automatically):

```sh
touch /forcefsck
reboot
```

If you cannot unmount the partition, then run `tune2fs`, which tunes the file
system check.

```sh
tune2fs -c 1 /dev/sda # change settings to run every 1 boots
tune2fs -i 1w /dev/sda # run every week
```

### NTFS

Make sure you have the following packages:

- `ntfs-3g`
- `ntfs-config`
- `ntfsprogs`
- `testdisk`

(For Artix, I only had to install `ntfs-3g` and `testdisk`).

Then, make some symbolic links:

```sh
ln -s /usr/bin/ntfsfix /usr/sbin/fsck.ntfs
ln -s /usr/bin/ntfsfix /usr/sbin/fsck.ntfs-3g
```

Then run `fsck` as normal.

## `gpasswd`

Administer `/etc/group` and `/etc/gshadow`.
`-a` adds the user to the group. E.g.:

```sh
gpasswd -a mpd vglinden
```

Adds the user `vglinden` to the `mpd` group.

## Investigate sockets and listening ports

Use `ss` or `netstat`. For example `sudo netstat -tunlp` will display all ports
that are in current use, same as `sudo ss -tunlp`.

`netstat` is obsolete and replaced with `ss` and `ip`, but still it is of the
most used commands to check network connections.

## xargs

Hint: try using `cat -` for debugging.

`xargs` accepts `stdin` and puts it as the last argument, if no other flag is
given.

- `-p` prompt to confirm command
- `-I %` defines the `%` character as the place to which input the arguments
- `-r` does not run if empty

Example:

```sh
cat directories.txt | xargs -I % sh -c 'echo %; mkdir %'
```

Will read all the listed directory names inside `directories.txt`, echo them and
create them at the same time.

Another example, creating thumbnails from folder `pic`:

```sh
find pic/ -type f -iname '*.jpg' | \
	xargs -I '{}' convert '{}' -resize 150x150 -quality 100 '{}'.png
```

- `xargs` executes the command for each line of input
- `-I ‘{}’` replace every occurrence of `‘{}’` with the current line's input
- `‘{}’` say line input is `pic/a.jpg`, `xargs` would replace it as so
- `‘{}’.png` same thing as above, but would be `pic/a.jpg.png`

## find

- `find . -type f -iname "example*"`: every file under this folder, which
  matches (case insensitive) "example*"
- `find . -type d \( -name git -o -path dir1 \) -prune -false -o -type f -iname
  "example*"`: every file that matches the case-insensitive regex "example*"
  under this folder, excludig every folder named "git" and the path "./dir1"
  (`-o` is "or" and `-a` is "and")

## checksum verify

```sh
sha256sum --ignore-missing -c sha256.txt my_file
```

- `-c` check against a file (`sha256.txt` in this case)
- `--ignore-missing`: don't fail if a listed file is missing

## File compressors

### tar

If your tar file is compressed using a gZip compressor, use the command `tar
xvzf file.tar.gz`

* x: extact file
* v: list all files inside (stands for verbose)
* z: decompresses the gzip file
* f: tell that you are going to extract file.tar.gz

If you have `file.tar.tbz` or `file.tar.bz2` (another compressor), you should use `tar xvjf file.tar.bz2`.
If you are decompressing a .xz file, use tar xvJf file.tar.xz.

* j: decompresses the bzip2 file
* J: decompresses the xz file

### zip

- `unzip` to uncompress
- `zip -r compressed.zip path/files-or-folders` to compress
- `unzip -l <file.zip>` to list content

### gz

- `gunzip file.gz` to decompress
- `gzip -d file.gz` to decompress (alternative)

If file extension is `.tar.gz`, use the `tar` command above.

### rar

[open, extract and create rar
files](https://www.tecmint.com/how-to-open-extract-and-create-rar-files-in-linux/)

```sh
rar a files.tat files/
```

## export

The command `export` exports a variable to the environment (`env`). This means
that, if you export a variable in one terminal simulator and try to see it at
the other, you will have to use `env | grep MYVAR`. However, if you export it in
`.profile` you will be able to use it as normally.

## shredding files

When file contains sensitive information, one can `shred` it:

```sh
shred --remove file
```

## `wpa_supplicant`

### connecting to wifi from the command line

1. `ip link` get the name of the wifi device
2. `sudo vim /etc/wpa_supplicant/wpa_supplicant-WIFIDEVICENAME.conf`

```
ctrl_interface=/run/wpa_supplicant
update_config=1
```

3. `wpa_passphrase SSID PASSPHRASE >>
   /etc/wpa_supplicant/wpa_supplicant-WIFIDEVICENAME.conf`
4. You may edit this file to remove plain text passphrase
5. `wpa_supplicant -B -i WIFIDEVICENAME -c
   /etc/wpa_supplicant/wpa_supplicant-WIFIDEVICENAME.conf`

## discovering your own ip address and location

- install `iproute2`
- `ip address`

Alternatives:

```sh
curl ifconfig.me
curl https://ipinfo.io/
hostname -i # local IP
hostname -I # external IP
```

### check usage of shared objects/libraries by a binary

```sh
ldd binary
```

## mount file (`.iso` or `.img`)

`mount -o loop file.iso mount_point`

## word document to text file

```sh
pandoc -s file.doc -o file.txt
```

## directory stack

- `dirs` is the lists the directories in the stack
- `pushd directory` adds the directory to the stack and change to it
- `pushd +1` changes to the next directory in the stack
- `pushd -1` changes to the previous directory in the stack
- `pushd -0` changes to the bottom directory in the stack
- `popd #` pops the # directory from the stack

## minicom serial communication

[Getting started with
minicom](https://wiki.emacinc.com/wiki/Getting_Started_With_Minicom)

Note: minicom gave me problems once. Use picocom instead.

- to connect to a RS232 external device: `sudo minicom -D /dev/ttyXXXX`
- to set baudrate, use `-b` flag
- to configure: `sudo minicom -s`

## dump serial without installing anything

The following will configure `/dev/ttyUSB2` to have a baudrate of 500000 bps and
raw (no special processing or interpretation of input/output characters is
performed). Then the block device may be simply readout.

```sh
stty -F /dev/ttyUSB2 500000 raw
cat /dev/ttyUSB2 >> "serial.dump"
```

## generate pseudorandom word

```sh
number_of_chars=20
tr -dc 'a-zA-Z0-9%^&*()-=_+[]~{}|;:,./<>?' < /dev/urandom | head -c $number_of_chars
```

## analyse code vulnerability

- [snyk](https://snyk.io/)

## Self-extractable archives

- [makeself](https://makeself.io/)

## fzf fuzzy-finder

[Configure
fzf](https://stackoverflow.com/questions/66382994/how-to-start-fzf-from-a-different-directory-than-the-current-working-directory/67019648#67019648)

- [Four useful fzf
  tricks](https://pragmaticpineapple.com/four-useful-fzf-tricks-for-your-terminal/)
- [fzf as dmenu
  replacement](https://medium.com/njiuko/using-fzf-instead-of-dmenu-2780d184753f)

## QR Code

- `libqrencode` library
- `qrencode` for generating QR codes
- `zbarimg` for reading

