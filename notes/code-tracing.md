
# Code Tracing

## Common Tracer Format

CTF is a standard that lets the user describe the trace message and when the
application runs, generate trace files (as in unix files) automatically. CTF can
be used in the embedded context. Use the following tools:

- barectf: reads a configuration describing the tracing content and outputs
  header files. Additional standard library is given. User must then use these
  functions, acting "as a printf".
- LTTng: CTF tracing running in the Linux kernel, trace user programs
- Babeltrace / Trace Compass: trace files viewers
- Trace Compass: GUI trace files viewers
