
# Node.js (Javascript runtime environment)

- node.js: runs javascript without a browser
- npm: node package manager
- nvm: node version manager (just a shell function)
- npx: execute binaries from npm

## installing nvm

Alternative to nvm is [asdf](https://asdf-vm.com/), which can manage more than
node versions.

```sh
paru -S nvm
echo "source /usr/share/nvm/init-nvm.sh" >> ~/.bashrc
nvm list-remote # lists all available node versions
nvm list # or nvm ls # lists version names
nvm install <version>
nvm use <version>
```

Notes:

- `node --version > ./path/to/project/.nvmrc` specifies the node version for the
  project, thus you can just call `nvm use`.
- `nvm use` changes not only node version but also npm version

## basic npm operations

- `npm install <package>`: install a package in `./node_modules`
- `npm -g install <package>`: install a package in `./usr/lib/node_modules/npm`
- `npm update [-g] <package>`: update a package
- `npm update [-g]`: update all packages
- `npm [-g] uninstall <package>`: remove package
- `npm [-g] list`: tree view of installed packages
- `npm [-g] list --depth=0`: top level installed packages
- `npm outdated`: show obsolete packages that need update

Inside a project:

- `npm run <script-key>`: run a command listed in the `package.json`, whose key is `<script-key>`

## setting up a node project

`npm init` generates a `package.json` file, ex:

```json
{
  "name": "myapp",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC"
}
```

Then when using `npm install <package>`, it will automatically add it to this file. You can use the
`--save-dev` flag to specify a development dependency only.

## automatically refreshing html

Opening files with the file system _may_ result in weird behavior. Try using a
`live-server` or `http-server`.

`node install live-server` or `pacman -S nodejs-live-server`.

## set default version globally

```sh
nvm list
nvm alias default lts/*
```

Now `nvm use` takes effect only in the project itself.
