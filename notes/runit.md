
# runit

## reference

- [runit - gentoo wiki](https://wiki.gentoo.org/wiki/Runit)
- [runit - FAQ](http://smarden.org/runit/faq.html#create)
- [runit - Artix wiki](https://wiki.artixlinux.org/Main/Runit)
- [runit - quickstart](https://kchard.github.io/runit-quickstart/)
- [Use runit!](https://www.mikeperham.com/2014/07/07/use-runit/)
- [init
  rosetta](https://gitlab.com/Night_H4nter/init-rosetta/-/blob/master/init_rosetta.md)

## package installations

Lets take the example of `mpd`. To initialize it with `runit`, you have to do:

```sh
pacman -S mpd-runit
ln -s /etc/runit/sv/mpd /run/runit/service
```

## programs

The core runit utilities are runsvdir, runsv, chpst, svlogd, and sv.

- `sv`: used for controlling services, getting status of services, and dependency
checking
- `chpst`: control of a process environment, including memory caps, limits on
cores, data segments, environments, user/group privileges, and more
- `runsv`: supervises a process, and optionally a log service for that process
- `svlogd`: a simple but powerful logger, includes auto-rotation based on
different methods (time, size, etc), post-processing, pattern matching, and
socket (remote logging) options
- `runsvchdir`: changes service levels
- `runsvdir`: starts a supervision tree
- `runit-init`: PID 1, does almost nothing besides being the init

## files

- `/etc/runit/1`: stage 1, system’s one-time initialization tasks
- `/etc/runit/2`: stage 2, Normally runs `runsvdir,` should not return until the
system is going to halt or reboot.
- `/etc/runit/3`: stage 3, system’s shutdown tasks
- `/etc/runit/ctrlaltdel`: Runit will execute this when receiving a `SIGINT`
signal
- `/etc/runit/runsvdir/*`: Runlevels
- `/etc/runit/sv/*`: directory containing subdirectories of available service
files
- `/run/runit/service`: always symlinked to active runlevel, `sv` will search for
running service here

## basic usage

- Enable service (in runlevel default) `ln -s /etc/runit/sv/service_name
/run/runit/service`
  - you always have to enable the service before running the `sv` command
  - you can enable and create a `/run/runit/service/$service_name/down` file so
    that it does not starts on boot
- Disable service `unlink /run/runit/service/service_name`
- Stop immediately `sv down service_name` or `sv stop service_name`
- Start (if not running) `sv up service_name` or `sv restart service_name`
- Restart `sv restart service_name`
- Reload `sv restart service_name`
- Status check `sv status service_name`
- Switch runlevels (this will stop all services that are currently running and
will start all services in the new runlevel) `runsvchdir runlevel`

Note: `/run/runit/service` is symbolically linked to
`/etc/runit/runsvdir/current`, which in turn is symbolically linked to
`/etc/runit/runsvdir/default`.

## runlevel

By default, runit has 2 runlevels, `default` and `single.` You can make your own
runlevels just by creating a new folder in `/etc/runit/runsvdir/` and symlinking
your desired service to that runlevel.

```sh
ln -s /etc/runit/sv/service /etc/runit/runsvdir/runlevel@@
```

## check dependencies

Some services may depend on other services. For example, `NetworkManager`
depends on `dbus`. To ensure that required dependencies are satisfied, check the
service's run file. For example, for `NetworkManager`:

```sh
/etc/runit/sv/NetworkManager/run
sv check dbus >/dev/null || exit 1
```

This means you have to enable `dbus` for `NetworkManager` to start.

## system boot time

Command `uptime` or `/proc/uptime` start counting since the kernel is
initialized (just after the bootloader).

## custom command on boot

Make a directory in `/etc/runit/sv` called whatever you want

Put a file inside that called "run" that contains something like:

```sh
#!/bin/bash

<command-you-want-to-run> &

pause
```

Then make that executable with `chmod +x /etc/runit/sv/<thing>/run`

Now symlink the new directory to `/run/runit/service`:

```sh
ln - s /etc/runit/sv/<thing> /run/runit/service/
```

And reboot. It should get started just like any other service and you can
control its execution with `sv` (try man `sv` for more info).

Note: test `runut`

## cli tool

```sh
pacman -S rsm
```

## logging

[How do you configure runit
logging](https://serverfault.com/questions/626436/how-do-you-configure-runit-logging#665728)

Logging is optional.
Log file will be created if then service has a `log` directory with `run` script inside it.

Something similar to:

```
/etc/sv/TEST
- run
- finish
- log/
  |
  - run
  - config
```

```sh
#!/bin/sh
# run

touch /tmp/pid
echo $$ > /tmp/pid

while true; do
    date
    sleep 3
done
```

```sh
#!/bin/sh
# finish

kill `cat /tmp/pid`
```

```sh
#!/bin/sh
# log/run
exec svlogd -t /var/log/TEST
```

- test if the logging deamon is running: `sv s TEST`
- stop the logging deamon: `sv d TEST/log`
- reload a configuration: `sv down TEST/log && sv start TEST/log`

Log file is found where `svlogd` runs, this means it should normally be in `/var/log/TEST`
