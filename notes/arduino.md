
# Arduino

- [Visual Micro](https://www.visualmicro.com/): VSCode extension for Arduino
- [generate compile_commands.json](https://github.com/Softmotions/acdb)

## CLI

- [arduino-cli](https://github.com/arduino/arduino-cli): Arduino IDE for the
  command line
- [Using the Arduino Command
  Line](https://www.woolseyworkshop.com/2019/04/14/using-the-arduino-command-line/)

This CLI can generate a global and project configuration files.
The `arduino-language-server` depends on this.
An alternative to the CLI may be a
[Makefile](http://volker.top.geek.nz/arduino/Makefile-Arduino-v1.8)

Arduino has a concept of Fully Qualified Board Names (fqbn). Syntax is
`<package>:<architecture>:<board>`:

- `<package>`: vendor identifier, typically arduino
- `<architecture>`: architecture, e.g. avr, megaavr, renesas_uno, samd.
- `<board>`: board name defined by the core platform, e.g. uno, uno2018,
  unor4wifi, nano.

## local library

If you build a local library, keep in mind that:

- [Library has to be inside `./src/`
  folder](https://stackoverflow.com/a/43819733/12932725), if so, no additional
  option for the `arduino-cli` is required
- Libraries are written as normal C++ files (`.h`, `.cpp`)
- Use `#include <Arduino.h>`
- Include the local library as a relative path
