
# Shell

## ENV variable

If you want a script to run for every shell, you set the ENV environment variable
to the name of this shell, and the shell will execute it. You can name this file
whatever you want, .shinit and .shrc are common names.

## Setting a default shell

To find out the path, use `whereis zsh` and use `sh sudo usermod -s /usr/bin/zsh $(whoami)`. Or simply type `chsh -s $(which zsh)`.  You may need to add the output from `which zsh` to your `/etc/shells` file.

Note: A notification "Shell not changed" may appear. Try to reboot if so.

## Bash

If bash is set as the system shell, it will do the following:

bashrc is not read by any program, and it is the configuration file of interactive instances of bash. You should not define environment variables in bashrc. The right place to define environment variables such as PATH is ~/.profile (or bash\_profile if you don't care about shells other than bash). [See What's the difference between them and which one should I use?](https://superuser.com/questions/183870/difference-between-bashrc-and-bash-profile/183980#183980)

Don't put it in `/etc/environment` or `~/.pam_environment`: these are not shell files, you can't use substitutions like `$PATH` in there. In these files, you can only override a variable, not add to it.

Path: Put the line to modify PATH in profile, or in `bash_profile` if that's what you have.

## zsh

### configuration files

* Since `.zshenv` .is always sourced, it often contains exported variables that should be available to other programs. For example, `$PATH`, `$EDITOR`, and `$PAGER` are often set in `.zshenv`. Also, you can set `$ZDOTDIR` in `.zshenv` to specify an alternative location for the rest of your zsh configuration.
* `.zshrc` is for interactive shell configuration. You set options for the interactive shell there with the setopt and unsetopt commands. You can also load shell modules, set your history options, change your prompt, set up zle and completion, et cetera. You also set any variables that are only used in the interactive shell (e.g. `$LS_COLORS`).
* `.zlogin` is sourced on the start of a login shell. This file is often used tostart X using `startx`. Some systems start X on boot, so this file is not always very useful.
* `.zprofile` is basically the same as `.zlogin` except that it's sourced directly before .zshrc is sourced instead of directly after it. According to the zsh documentation, "`.zprofile` is meant as an alternative to `.zlogin` for ksh fans; the two are not intended to be used together, although this could certainly be done if desired."
* `.zlogout` is sometimes used to clear and reset the terminal.

So:

* `.zshenv`: read every time
* `.zprofile`: read at login
* `.zshrc`: read when interactive
* `.zlogin` read at login
* `.zlogout`: read at logout, whitin login shell

### options

- [Moving to zsh: Shell Options](https://scriptingosx.com/2019/06/moving-to-zsh-part-3-shell-options/)

Use one of the following to activate an option:

- `set +o OPTION`: toggles the OPTION
- `setopt OPTION`: sets the OPTION
- `unsetopt OPTION`: unsets the OPTION

Type `setopt` to see active options.
Type `emulate -lLR zsh` to see available options.
Use `man zshoptions` to see the available option through the manual page.
Type `emulate -LR zsh` to revert back to default settings.

#### CORRECT option

Meaning of "nyae":

- `n`: execute as typed
- `y`: accept and execute the suggestion
- `a`: abort and do nothing
- `e`: continue editing

## alternative screen

[Using the "alternate screen" in a bash
script](https://stackoverflow.com/questions/11023929/using-the-alternate-screen-in-a-bash-script)

The alternate screen is used by many "user-interactive" terminal applications like vim, htop,
screen, alsamixer, less, etc. It is like a different buffer of the terminal content, which
disappears when the application exits, so the whole terminal gets restored and it looks like the
application hasn't output anything.

- switch to alternate screen: `tput smcup`
- back from alternate screen: `tput rmcup`
- xterm switch to alternate screen: `echo -e "\e[?1049h"
- xterm back from alternate screen: `echo -e "\e[?1049l"


