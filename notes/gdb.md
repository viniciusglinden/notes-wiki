
# GDB

- [GDB Cheat Sheet](https://darkdust.net/files/GDB%20Cheat%20Sheet.pdf)
- [gdb commands](https://gist.github.com/shekkbuilder/8b52e2cf4142c35ba6de)

- [How does gdb work?](https://jvns.ca/blog/2016/08/10/how-does-gdb-work/)
- [How does gdb call
  functions?](https://jvns.ca/blog/2018/01/04/how-does-gdb-call-functions/)
- [Profiler adventures: resolving symbol addresses is
  hard!](https://jvns.ca/blog/2018/01/09/resolving-symbol-addresses/)

Alternatives to `gdb` are:

- `lldb` (clang)
- [`rr`](https://rr-project.org/) (undo ability)

## plugins

- [pwngdb](https://github.com/scwuaptx/Pwngdb) additional gdb commands
- [pwndbg](https://github.com/pwndbg/pwndbg#readme) is a gdb plug-in that makes
  debugging with gdb suck less, with focus on features needed by low-level
  software developers, hardware hackers, reverse-engineers and exploit
  developers
- [Splitmind](https://github.com/jerdna-regeiz/splitmind) will give you windows
  so it is easier to analyse
- [GEF](https://github.com/hugsy/gef) is a set of commands for x86/64, ARM,
  MIPS, PowerPC and SPARC to assist exploit developers and reverse-engineers
- [cgdb](https://cgdb.github.io/)

## compiling

1. compile with debugging options
2. run through gdb

```sh
gcc -g test.c
gdb a.out
```

- `-g`: same as `-g2`
- `-g0`: no debug information
- `-g1`: minimal
- `-g2`: default
- `-g3`: all debug information (prints macros)
- `-ggdb`: same as `-g3` and add gdb specific stuff

Note: do not use `Og`, it optimized some stuff out (even though it should not).

GDB will start halted. Then, using the following syntax, you can work through

## syntax

* [Use gdb on arm assembly
  program](https://jacobmossberg.se/posts/2017/01/17/use-gdb-on-arm-assembly-program.html)
* [GDB how to print the current line or find the current
line](https://stackoverflow.com/questions/14581837/gdb-how-to-print-the-current-line-or-find-the-current-line-number)

- continue until next breakpoint: `c`
- back-trace: `bt`, `where`
- step-into a function: `s`
- Runs the next N lines of program: `s` `N`
- force set a value in run-time: `set var=val`
- next: `n`
- (re)run: `r`
- quit: q, quit
- refresh: refresh the screen (alternative to CTRL+L)
- load/source/restore a gdb script: `source filename`
- the mode your program is running (looking which of these registers exist):
  `info line *$REGISTER`
- watch variable changes: `watch var` (are treated as a breakpoint)
- watch variable on line change: `display var`
- memory map: `info proc mappings` (`i proc m`)
- If you append `@entry` string to a function parameter name you get its value
  at the time the function got called: `p var@entry`
- `monitor <cmd>`: execute a gdb-server (not gdb) command[^monitor-command-1]
- `finish`: step out of function
- `disassemble`: disassemble code
- `set var variable = "true"`: set / overwrite `bool variable` to `true`
- `whatis variable`: get the `variable` type

[^monitor-command-1]: Example is openocd in embedded. In this case, `monitor
<cmd>` is the same as using the openocd's telnet interface.

### breakpoint

* [GDB breakpoints example](http://www.gdbtutorial.com/gdb-breakpoints-example)

- breakpoint: b function, b line
- list information: `info break` or `info b`
- at line of source file: `break filename:linenumber`
  - `b linenumber` if only one file is available
- on function: `break function-name`
- at specific offset from current line: `break [+|-]offset-number`
- on all functions matching regex: `rbreak reg-expression`
- on instruction address: `break *instruction-address`
- conditional breakpoint: `break breakpoint condition` eg: `b fun_sum if b == 2` (space is optional)
- one-time (temporary): `tbreak breakpoint`
- ignore for N number of hits: `ignore breakpoint-number COUNT`
- enable breakpoint: `enable breakpoint-number`
- disable breakpoint: `disable breakpoint-number`
- delete breakpoint: `delete <breakpoint-number>`
- enable for one hit: `enable once breakpoint-number`
- enable and delete when hit: `enable delete breakpoint-number`
- enable for N hits: `enable count COUNT breakpoint-number`
- save: `save breakpoints filename`, `save b filename`

After setting a breakpoint, GDB will inform:

- breakpoint number
- memory address
- file
- line

### print

- print variable: p variable
- print current line: f, frame
- print all registers available: info registers
- print local variables: info local
- print in hexadecimal: `print/x var`
- print in binary: `print/t var`
- p[rint] function(): evaluate a function
- `print array[i]@count`: artificial array - print array range
- `print $`: print last value
- `print *$->next`: print thru list
- `print $1`: print value 1 from value history
- `print ::gx`: force scope to be global

## layouts

Either call gdb with `-tui` or type `layout` inside or type ctrl+X+A.
In TUI mode, arrow keys will not work: use ctrl+p and ctrl+n instead. Printing
output from program may also force you to refresh the screen frequently, you may
want to redirect output.

- `layout`: `lay`, display options
- `tui disable`: close TUI
- `tui enable`: enable TUI

Available layouts are:

- `next`, `prev`: cycle through
- `asm`: assembly
- `regs`: register
- `src`: source code
- `split`: assembly + source

### TUI layout control

- [GDB Text User Interface](https://ftp.gnu.org/old-gnu/Manuals/gdb/html_chapter/gdb_19.html)

- ctrl+x o to select the bottom pannel

## executable with command arguments

```sh
gdb --args path/to/command first second third arguments
```

or

```gdb
r[un] arguments [redirection]
```

## start gdb with a command

```sh
gdb --eval-command="<gdb command>" ./programm
```

or use `.gdbinit`.

## gdbinit

- [simulating gdb](http://cs107e.github.io/guides/gdb/)

This file configures your gdb. It can be global (@ `~/.gdbinit` or `~/.config/gdb/gdbinit`) or for
each project (put a `.gdbinit` at loading folder, has to be the folder where you call gdb).

To make gdb auto-load local init scripts, use the following in your global gdb:

```gdb
set auto-load safe-path /
```

## remote server

### via ssh

- [remote debugging using gdbserver over
  ssh](https://9to5answer.com/remote-debugging-using-gdbserver-over-ssh)

```sh
ssh remote_username@remote_ip -L local_port:localhost:remote_port <remote gdb-server command>
```

Parallel to

```gdb
target remote localhost:local_port
```

For example, a OpenOCD gdb server running on the `iol` ssh alias for a ARM Cortex M architecture.
Client runs on a X86 machine:

```sh
ssh iol -L 3333:localhost:3333 openocd -f /tmp/openocd.cfg
# parallel to
arm-none-eabi-gdb firmware.elf
(gdb) tar ext :3333
```

### with valgrind

- [Using valgrind with
  gdb](https://indico.cern.ch/event/392796/contribution/4/attachments/1196707/1744649/vgdb.pdf)

Use `--vgdb=full|yes --vgdb-error=n`[^with-valgrind] being `n` the number of
valgrind
errors to halt and open a gdb session. Valgrind will then automatically throw
this message:

[^with-valgrind]: with `n = 0`, valgrind will immediately halt.

```
==PID== TO DEBUG THIS PROCESS USING GDB: start GDB like this
==PID== /path/to/gdb ./PROGRAMM
==PID== and then give GDB the following command
==PID== target remote | /usr/local/lib/valgrind/../../bin/vgdb --pid=PID
==PID== --pid is optional if only one valgrind process is running
```

Then (as suggested above):

```gdb
target remote | vgdb
```

Useful server commands are:

- `monitor leak_check`
- `monitor v.kill`: kills valgrind

## attaching to running process

[How do I get a handle on a running
program?](https://gjbex.github.io/DPD-online-book/BugsAtRuntime/Debuggers/Gdb/gdb_attach/)

Run the program as normal and get its PID with `pgrep <name of program>`. Then
attach with `gdb -p <PID>` (equivalently `gdb ./<name of running program>
<PID>`). `gdb -p $(pgrep -f <name of program>)`.

Because gdb need to have the programs backtrace, if ptrace is not enabled, you
may get the "ptrace: Operation not permitted" message. To solve this
_temporarily_ disable this **kernel protection**:

- disable `sysctl -w kernel.yama.ptrace_scope=0`
- enable `sysctl -w kernel.yama.ptrace_scope=1`

And then proceed as normal.

## debuginfod

- [debuginfod: get debugging data + sources easily - DevConf.CZ
  2021](https://www.youtube.com/watch?v=X4Eg7vfPNPY)
- [Introducing debuginfod, the elfutils debuginfo
  server](https://developers.redhat.com/blog/2019/10/14/introducing-debuginfod-the-elfutils-debuginfo-server#how_do_i_use_debuginfod_)

Debuginfod (debug info daemon?) allows you to:

- take a compiled file, such as python from the maintainers, that does not have
  debug information, run gdb and never the less get the debug information.

## disable paging

If you want to disable the message: `--Type <RET> for more, q to quit, c to continue without
paging--`, use the following command:

```gdb
set pagination off
```

## logging

```gdb
set logging file <FILE>.log # set logging file
set logging enabled on # enable logging
set logging redirect on # print output to gdb and log file.
show logging # logging configuration
```

To suppress `stdout` for going to the terminal use `r[un] > /dev/null`


## action on a breakpoint

```gdb
b <function, symbol, etc>
command
    <what to do>
end
```

For example, printing the backtrace when a function is hit and then continue:

```gdb
b function
command
    bt
    c
end
```

## define custom commands

```gdb
define <name>
<custom command>
end
```

You can also use a hook:

```gdb
define hook - <gdb command>
<custom commands>
end
```

## print variable from the stack

- use backtrace and get the number
- use the frame command to change the context
- use print

Example:

```gdb
(gdb) bt
#0  eeprom_calc_initial_check (param=0x200279fc) at eeprom.c:284
#1  0x08007ac4 in eeprom_calc (param=0x200279fc) at eeprom.c:355
#2  0x0800761c in eeprom_write (buffer=0x20027a18 "\"", addr=63902, size=1378) at eeprom.c:43
#3  0x0800a22c in buffer_log (mod=fifo_e) at buffer.c:232
#4  0x08008c94 in log () at interface.c:34
#5  0x08000786 in run_firmware () at entry.c:142
#6  0x080006e6 in main () at entry.c:75
(gdb) f 4
(gdb) p <variable in log()>
```

Note `up[-silently] [n]` and `down[-silently] [n]` move up and down the stack
frame.

## using code dump

```sh
% ulimit -c unlimited # enable core dump
% ./<executable>
Segmentation fault (core dumped)
% ls
<executable> core
gdb <executable> core
```

## pretty printing


For example, here is an C++ `std::string` without a pretty-printer:

```
(gdb) print s
$1 = {
  static npos = 4294967295,
  _M_dataplus = {
    <std::allocator<char>> = {
      <__gnu_cxx::new_allocator<char>> = {
        <No data fields>}, <No data fields>
      },
    members of std::basic_string<char, std::char_traits<char>,
      std::allocator<char> >::_Alloc_hider:
    _M_p = 0x804a014 "abcd"
  }
}
```

and here is with:

```
(gdb) print s
$2 = "abcd"
```

## System View Description (SVD)

https://wiki.st.com/stm32mpu/wiki/CMSIS-SVD_environment_and_scripts

## Curses program

http://www.dirac.org/linux/gdb/07-Debugging_Ncurses_Programs.php#ncurses

## Debug child processes

```gdb
set follow-fork-mode child
set follow-fork-mode parent
```

## Debug exec commands

```gdb
set follow-exec-mode new
set follow-exec-mode same
```

## Go back

Undo the next command. Only supported by some targets. Newer gdb version is
required.

```gdb
target record-full
```

Then try to use `reverse-<command>`. For example: `reverse-next` (`rn` for
short).

