
# PIP

- list packages: `pip list`
- show package details: `pip show <package>`
- install locally package from current folder: `pip install .`
- `install package from current foler, by symbolic link: `pip install -e .`
- `install package with extras: `pip install <package>[extra]`
