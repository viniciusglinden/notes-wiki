# Qt

Qt is a widget toolkit for creating cross-platform GUI applications. It is licensed both as
commercial and open-source licenses. Qt has lots of modules for different tasks.

- widget = graphical control element, eg. button, scrollbar
- widget toolkit = widget library = UX library = a collection of libraries containing a set of
  widgets

- Qt Creator: IDE
- Qt Designer: tool for designing and building GUIs with Qt Widgets
- qmake: build script generator tool
- QML: a user interface specification a programming language (that looks like JSON), that supports
  JavaScript imperative programming




