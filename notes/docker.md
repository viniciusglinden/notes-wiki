[toc]

# Docker Container service

- [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)

Please use an alternative (see below), Docker does not work well.

Docker has two main executables:

- `docker`: used when managing individual containers, cli to access docker
  daemon
- `docker-compose`: manage multiple containers. Moves many options from
  `docker run` into `docker-compose.yml` file for easier reuse. Works like a
  front-end script.
- `lazydocker`: pretty TUI application for managing images and containers

A container is a running image.

## basic commands

To run docker you first have to launch `dockerd` daemon.

Note: in my Artix system, the daemon was talking the whole partition.

- `docker run image`: run a container based on the image
- `docker run --name="container name" image`: run an image, while customizing
  the containers name
- `docker run -it image`:  run image in interactive mode
- `docker run -p 8080:8080 -p 50000:50000 image`: run the image with two port
  mappings[^1]
- `docker run -it centos /bin/sh`: run centos image , with bash shell
- `docker run --rm image`: automatically remove the container when it exits
- `docker run -v host_dir:target_dir image`: run an image with `host_dir`
  mounter in `target_dir`
- `docker pull jenkins`: pull `jenkins` image
- `docker images`: list existing images in the computer
  - TAG: used to logically tag images
  - Image ID: image unique identifier
  - Created: days since the image creation
  - Virtual Size: image size
- `docker inspect <hash>`: display information about an image, including PID
- `docker ps`: show currently running containers
- `docker ps -a`: show all containers
- `docker rm ContainerID`: removes a container
- `docker rmi ImageID`: removes an image
- `docker rm $(docker ps -aq)`: remove every container
- `docker rmi $(docker images -q)`: remove every image
- `docker history ImageID`: show all commands that were run against the image
- `docker top ContainerID`: display top processes of a container
- `docker stop ContainerID`: stops a container
- `docker stats ContainerID`: display CPU and memory  utilization of the
  container
- `docker attach ContainerID`: "go inside" the container
- `docker pause ContainerID`: pause a container (SIGSTOP)
- `docker unpause ContainerID`: unpause a container (SIGCONT)
- `docker kill ContainerID`: kills the process which is running a container
  (SIGTERM then SIGKILL)
- `docker start ContainerID`: starts the existing container
- `docker attach ContainerID`: attaches ('enters') the tty of the container
- `docker start -a ContainerID`: starts and attaches to container
- `docker system prune`: remove stopped containers, dangling images and caches
  - `docker system prune --all --force --volumes` to be really aggressive
- `docker volume ls`: show docker volumes [^2]
- `docker inspect <container>`: show information about container
- `docker run -it --entrypoint=/bin/bash <image>`: to overwrite whatever default
  entrypoint to bash
- `docker tag old-tag new-tag`: to retag (rename) image

[^1]: Port mapping in docker is always `Hostport:Containerport`
[^2]: In rootless mode, it is stored under `~/.local/share/containers`

- Typically, if you want to run interactive mode: `docker run -it --rm -v
  $(pwd):/my-dir <image>`

## Docker compose

- `up` to start
- `down` to take it down
- `-d` flag to run detached (daemon)
- `-f <file>` to use custom named file, used with `up` and `down`

Note: CLI syntax has been changed.

### Docker compose file syntax

Docker compile file is used to describe how to launch a container of container
system.

```yml
version: "3.9"  # optional since v1.27.0
services:
  web:
    image: my_image:latest
    container_name: my_container
    environment:
      LOL: value
    build: .
    ports:
      - "443:8043"
    volumes:
      - .:/code
      - logvolume01:/var/log
    links:
      - redis
    restart:
      unless-stopped
  redis:
    image: redis
volumes:
  logvolume01: {}
```

- `services`: describes each container to be created
- `image`: image name with tag
- `environment`: environment variable
- `volumes`: shared folder in `host:container` format
- `restart`: [what happens when the container
  closes](https://docs.docker.com/config/containers/start-containers-automatically/)

Docker compose V2 integrates compose functions into the docker command, you can
run it by running `docker compose`.

## DockerHub

Basically a gitlab for docker images. Native integration with docker through
`docker login`.

## creating an image (Dockerfile)

[Dockerfile reference](https://docs.docker.com/engine/reference/builder/)

Although it is not necessary, most images have a base installation. Creating a
"bare metal" image is cumbersome and reserved only for specific tasks.

Note: in debian based distributions, if you do `ENV
DEBIAN_FRONTEND=noninteractive`, change will be persistent. Do this instead:

```
RUN DEBIAN_FRONTEND=noninteractive apt ...
# or
ARG DEBIAN_FRONTEND=noninteractive
RUN apt ...
```

### from a base (Linux) installation

Consists basically in a repository with a Dockerfile.

- Create a new directory and enter it
- `vim Dockerfile`

```
FROM base_image

# optional command
MAINTAINER my name <kkk@kkk.de>

CMD ["shell command"]
```

- `docker build .`: this creates an unnamed image in the local folder
- `docker run --name ContainerName ImageID`: test the image

Notes

- inside the container, when PID 1 exits, docker container exits.
- `^C` will not work on docker, a work around is to use `docker stop ContainerID`
- `CMD` syntax separated executable from arguments. E.g. `CMD ["echo", "hello"]`
- if you don't want to use a base installation, `FROM scratch` is for you
- to name your image: `docker build -t image_name:optional_tag .`
- to use custom Dockerfile name: `docker build -f myDockerfile .`

### ENTRYPOINT and CMD

- ENTRYPOINT: the executable that will run inside the container and cannot be overridden when starting the container.
- CMD: sets default arguments for an ENTRYPOINT instruction or prepares the container to run an executable - can be overritten.

```dockerfile
FROM ubuntu
ENTRYPOINT ["/bin/bash"]
CMD ["-c", "echo 'Hello World'"]
```

Here ENTRYPOINT defines the executable as `/bin/bash`, and CMD provides the
default arguments `-c` and `echo 'Hello World'` to the ENTRYPOINT.

## creating an image without Dockerfile / tag container as image

Just launch a container and do whatever it takes. Then, outside the container:

```sh
docker commit <container id> <image name>
```

### SSH

[Start SSHD automatically with docker
container](https://stackoverflow.com/questions/22886470/start-sshd-automatically-with-docker-container)

Just add an user, expose a port and run the daemon.

```
# expose ports 22 2022 1234
EXPOSE 22 2022 1234
CMD ["/usr/sbin/sshd", "-D"]
```

### inputting a file into the image

- Create any file, i.g. a script
- use the `COPY file_name_host file_name_target` to copy the file name inside
  the target. I.g. `COPY script.sh /script.sh`

### dockerignore

Works just like `.gitignore`, will ignore the files inside when sending them to
the cloud. Common files to ignore are `.git`, `*.md`.

### CMD vs RUN

- RUN: image build step, state of container after RUN will be comitted to the
  container. Docker can have multiple RUN steps.
- CMD: default command to execute, overwritten by `docker run image command`
- ENTRYPOINT: same as CMD, but will not overwrite

```
RUN apt-get -y install firefox
CMD echo "GeeksforGeeks"
ENTRYPOINT echo "GeeksforGeeks"
```

### running foreign architecture images

If you run an image with another architecture as your computer, like `arm64v8/alpine` (ARM) in a x86
Linux machine, you have to install binfmt (bin format) - otherwise you may get the following error:
`exec user process caused "exec format error"`. The Linux kernel has a feature called `binfmt_misc`
which allows us to define our own handlers for arbitrary executable formats.

1. `pacman -S qemu`
2. then `paru -S qemu-user-static-bin` (installs the binfmt)
3. reboot your computer

You may have to install [Busybox](https://www.busybox.net/) or similar and put
it in your PATH.

### running as non root user

1. `sudo groupadd docker`: create docker group
2. `sudo usermod -aG docker $USER`: add yourself to the group
3. log in and log out to from user

## Alternative

- [Podman](https://podman.io/): exact same syntax
- [Podman: Installation](https://podman.io/getting-started/installation.html):
  contains essential information, including about the registries

The only difference is, when pulling something from docker hub, you first have
to specify the URL: `podman run docker.io/hello-world` instead of `podman run
hello-world`. You may alternatively indicate where to download the image, through
adding at `/etc/containers/registries.conf` the following:

```
[registries.search]
registries = ['docker.io']
```

Podman did not work out-of-the-box, I first had to install `crun` and then
everything went fine.

Podman also doesn't need a daemon to run.

Podman separates user and root images: if the root user downloaded an image, it
is not visible (`podman images`) to the user.

### running podman as non-root user

```sh
sysctl kernel.unprivileged_userns_clone # if this does not return 1, set it to 1
touch /etc/subuid /etc/subgid
usermod --add-subuids 100000-165535 --add-subgids 100000-165535 <USERNAME>
podman system migrate
```

You can verify that it worked by running `podman info` and grepping for
`rootless`.

Note: the following did not work.

If you get the message "WARN[0001] "/" is not a shared mount, this could cause
issues or missing mounts with rootless containers", run:

```sh
sudo chmod 4755 /usr/bin/newgidmap
sudo chmod 4755 /usr/bin/newuidmap
```

Note: this message only appears once.

If message `WARN[0000] Error validating CNI config file [...]` appears, install
`dnsname` for podman^[dnsname-podman].

[dnsname-podman]: `pacman -S podman-dnsname`

### login into docker hub with podman

Just do `podman login`, there is no difference.

## init system

When you do `docker run` with bash as the command, the init system (e.g.
SystemD) doesn't get started. You need to install the init system.

## Running docker daemon without root privileges

Running as root is seen as not secure. [Run docker daemon as
rootless](https://docs.docker.com/engine/security/rootless/)

## Reduce image size

[Best Practices to Reduce Docker Images
Size](https://www.ecloudcontrol.com/best-practices-to-reduce-docker-images-size/)

- Use smaller base images
- Don't install debug tools like curl and vim
- Minimize `RUN` statements in dockerfile: try to gather these with pipeline commands
- Use `--no-install-recommends` on `apt get install`
- Clear package manager cache after installation on the same line as the installation itself:
  - Debian: `rm -rf /var/lib/apt/lists/*`
  - RHE: `yum clean all`
- Use fromlatest.io to lint the Dockerfile
- Use multistage build:

```dockerfile
FROM ubuntu as stage1
RUN apt-get update && \
    apt-get -y install make curl && \
    curl https://xyz.com/abc.tar.gz -O && \
    tar zxf abc.tar.gz && cd abc && \
    make DESTDIR=/tmp install

FROM alpine
COPY --from=stage1 /tmp /abc
ENTRYPOINT ["/abc/app"]
```

## Alpine image

Alpine uses Busybox and musl instead of GNU utils and glibc. If you download a package that uses
glibc, it will say `./<program>: not found`. Solution is: `apk add gcompat` or compile it from
source.
