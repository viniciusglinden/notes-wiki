
# Microcontrollers

- [i2cdevlib: open-source device libraries](https://www.i2cdevlib.com/)
- [Serial Studio](https://serial-studio.github.io/): customizable serial data
  stream visualizer with FFT, etc
