
# PDF

## compiling

### from svg

Use [Inkscape](inkscape.md) built-in compiler or `librsvg` library.

### to svg

Use `pdf2svg`.

## shell manipulation

Use [`pdftk`](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) to manipulate PDFs.
The following is just some of the things `pdftk` can do.

### convert to text

This is useful for other applications (e.g. `ranger`), for displaying in a pager.

```sh
pdftotext -layout in.pdf out.txt
```

### concatenate

```sh
pdftk 1.pdf 2.pdf 3.pdf cat output out.pdf
```

### split

The `cat` will _select_ pages. That is to say, if we have `in.pdf` and we want
to select pages 1 to 9 and 26 to the end and put those in `out.pdf`, we will run:

```sh
pdftk in.pdf cat 1-9 26-end output out.pdf
```

Or, if we want to split one file for each page:

```sh
pdftk in.pdf burst
```

### decrypt

```sh
pdftk in.pdf input_pw password output out.pdf
```

### encrypt

```sh
pdftk in.pdf output.pdf user_pw password
```

### extract page range

```sh
pdftk in.pdf cat first-last output outfile.pdf
```

## data size optimization

```sh
ps2pdf -dPDFSETTINGS=/screen in.pdf out.pdf
gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 \
	-dPDFSETTINGS=/printer -sOutputFile=out.pdf in.pdf
```

Or

```sh
yay -S shrinkpdf
```

## dimensions resize

```sh
pdfjam --a4paper in-out.pdf # using latex's geometry package
pdfjam --papersize '{210mm,297mm}' in-out.pdf # a4 using custom geometry
```

## extract images

```sh
convert in.pdf out.<image extension>
```

Or

Suppose a document called "Bulldog.pdf" has three pages.

```sh
pdftoppm Bulldog.pdf Bulldog -png
```

This will yield: `Bulldog-1.png`, `Bulldog-2.png` and `Bulldog-3.png`.

Or

```sh
pdfimages file.pdf name-of-images
```

Will generate `image-0000.ppm`, `image-0001.ppm`, etc. `.ppm` can be converted
with `convert` command.

## rotate 90°

```sh
pdftk A=in.pdf shuffle AoddWest AevenEast output out.pdf
```

## Sign

[openpdfsign](https://www.openpdfsign.org/)
