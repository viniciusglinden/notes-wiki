
# Monero

## Running local node

- [How to run your own Monero node](https://www.coincashew.com/coins/overview-xmr/guide-or-how-to-run-a-full-node)

To check the blockchain status: `monerod status`

## Mine Monero

```sh
monerod start_mining <YOUR XMR ADDRESS> <NUMBER OF THREADS>
```
