
# Package manager

- ["Translations" between package managers](https://wiki.archlinux.org/title/Pacman/Rosetta)

## Debian-based

This means: `apt` and `apt-get`.

The `apt` command is more recent, and should be used instead of `apt-get`.
Syntax works the same, when referring to `apt-get` commands.

`apt`|`apt-get`|function
-|-|-
`apt install`|`apt-get install`|Installs a package
`apt remove`|`apt-get remove`|Removes a package
`apt purge`|`apt-get purge`|Removes package with configuration
`apt update`|`apt-get update`|Refreshes repository index
`apt upgrade`|`apt-get upgrade`|Upgrades all upgradable packages
`apt autoremove`|`apt-get autoremove`|Removes unwanted packages
`apt full-upgrade`|`apt-get dist-upgrade`|Upgrades packages with auto-handling of dependencies
`apt search`|`apt-cache search`|Searches for the program
`apt show`|`apt-cache show`|Shows package details

Furthermore:

- `apt list`: Lists packages with criteria (installed, upgradeable, etc)
  - `apt list --installed` to list installed packages
- `apt edit-sources`: Edits sources list
- `dpkg -l <package name>`: see if the package is installed
- `dpkg -i <package name>`: to install the package manually
- `dpkg -L <package name>`: list all of the packages file

Note:

- remember to `apt update` before searching for a package.

- [Ubuntu Package Search](https://packages.ubuntu.com/)

### Preventing automatic kernel version upgrade

Do not run apt update/upgrade! This may cause a version upgrade. Run the following commands in the terminal first:

* `apt-mark hold ubuntu-desktop`
* `apt-mark hold ubuntu-release-upgrader-core`
* `apt-mark hold $(uname -r)`
* `apt update`
* `apt upgrade`
* restart computer
* verify Ubuntu version

Observation: `apt-mark showhold` displays the packages forbidden to upgrade.

### List .deb package content

- local file: `dpkg -c ./package.deb`

## RHE-based

This means: `dnf` and `yum`.

The `dnf` command is more recent, and should be used instead of `yum`. Syntax is
the same.

- auto remove leaf package: `dnf autoremove`

### RPM package search website

[RPM Search](http://rpm.pbone.net/)


### Preventing automatic kernel version upgrade

* Open the terminal
* Type `vim /etc/yum.conf`
* Add `exclude=kernel* redhat-release* centos-release*` to file
* To be safe, restart the computer
* Run `sudo` `yum update` as normal
* If an error occurs, try sudo `yum update --skip-broken`
* Check the OS version

## alpine

- [apk packages](https://alpine.pkgs.org/): this is more trust worthy than the
  official
- [apk packages](https://pkgs.alpinelinux.org)
- [Package management](https://wiki.alpinelinux.org/wiki/Package_management)

- add: add or upgrade package
- del: delete packages
- fix: repair or upgrade an installed package
- update: update the index of available packages
- info: information about installed or available packages
- search: search for packages or descriptions with wildcard patterns
- upgrade: upgrade the currently installed packages
- cache: maintenance operations for locally cached package repository
- version: compare version differences between installed and available packages
- index: create a repository index from a list of packages
- fetch: download (but not install) packages
- audit: list changes to the file system from pristine package install state
- verify: verify a package signature
- dot: create a graphviz graph description for a given package
- policy: display the repository that updates a given package, plus repositories that also offer the
  package
- stats: display statistics, including number of packages installed and available, number of
  directories and files, etc
- manifest: display checksums for files contained in a given package

## Arch-based

This means: `pacman`. `pacman` always has a capital letter option, follow by some (if any) smaller case letters. The smaller case letters meanings depends on the capital case letter.
- [How to use
pacman](https://community.chakralinux.org/t/how-to-use-pacman-to-search-for-install-upgrade-and-uninstall-packages/7205)
- [Artix Repositories](https://wiki.artixlinux.org/Main/Repositories)

### `S` synchronize

* `u` update packages
* `y` synchronize mirrors (not updating anything)
* `w` download packages, but do not install them
* `s` search for a package (either in the title or the description) containing
  the followed string
* `c` removed cached packages (previous versions, no longer in use)
* `q` as in `-Ssq` will print all the available packages, one each line
* `i` display info, including the package dependencies, doing `ii` will "invert"
  from "package dependencies" to "required by"

Therefore,

- `Syu`: synchronize the mirrors and update from the synchronization
- `Syyu`: force it to double check if mirrors are in sync
- `Sc`: remove cached uninstalled packages
- `Scc`: remove cached uninstalled and installed packages
  - Note: `paru -Scca` will only affect paru packages

### `R` removes

* `s` removes a package with its child dependencies
* `n` removes a package with its system configuration files
* `dd` as in `pacman -Rdd package` will force remove a package (not recommended)
* `c` remove all target packages, including those with depend on it

Therefore,

- `Rns`: remove a package with its orphan dependencies and its system
  configuration files.

### `Q` list packages

* `e` what programs I have installed
* `q` only program names
* `n` only programs from main repository
* `m` only programs from AUR
* `dt` unneeded dependencies (orphans from now removed packages)
* `s` search in local repository
* `i` detailed information
* `o` tell which package owns a particular file

Therefore,

- `Qdt`: present the unneeded dependencies
- `Qo /etc/sshd_config`: "is owned by openssh"

### `U` install a local package

E.g.: `pacman -U gimp-2.10.10-1-x86_64.pkg.tar.xz`

In order to downgrade a package, if you have it cached, simply type:

```sh
pacman -U /var/cache/pacman/pkg/package-old-version.pkg.tar.xz
```

Then you have to blacklist it, either temporarily via `--ignore` flag or permanently via `IgnorePkg=` in `/etc/pacman.conf`.

Warning: in arch, partial updates are unsupported.

### `F` query files database

To query a file:

```sh
pacman -Fy
pacman -Fq /path/to/file
```

### list files owned by a package

```sh
pacman -Ql
```

### search package by file

- `pacman -S pkgfile`
- `pkgfile -u`
- `pkgfile file_name`: e.g. `pkgfile mkisofs` or `pkgfile /usr/bin/mkisofs`
  (gives you different results)

Note: use `pacman -Qo /path/to/file` if it is currently installed

### list package dependencies

Use `pactree` which is owned by `pacman-contrib` to display tendencies as a
tree or `pacman -Si`.

### manually installing AUR packages

1. search and download the package in [Arch Wiki](https://aur.archlinux.org/packages)
2. untar it
5. `makepkg -s` to generate a package
6. `packman -U name_of_package.pkg.tar.xz`

## ignoring dependencies

- `--assume-installed package[=version]` to ignore one package
- `--nodeps, -d` skip dependencies altogether

