
# image

- [ImageMagick](https://imagemagick.org/)

## SVG image viewer

- `inkview`, provided by `inkscape`
- `gpicview`

## compiling svg from shell

inkscape is also a shell command. Use: `inkscape file.svg --export-type=pdf` for
exporting to pdf.

## convert to grayscale

If you don't care about losing the original file: `mogrify -colorspace Gray
file`

Or use `convert` from `imagemagick` package.

```sh
convert source.jpg -colorspace Gray destination.jpg (true grayscale only)
convert source.jpg -monochrome destination.jpg (true black and white)
convert source.jpg -separate destination.jpg (separate into gray channels)
```

## convert to pdf

```sh
convert file.jpg file.pdf
```

## resizing (adding border/limits)

```sh
convert -border {SIZE} -bordercolor {COLOR_NAME} {input.file} {output.file}
```

E.g.:

```sh
convert -border 20 -bordercolor input.png output.png
```

To just resize:

```sh
convert -size 10x10 in.png out.png
convert -size x10 in.png out.png # this will automatically guess the hight
```


## gray scale

- ITU-R BT.709: `0.21*red + 0.72*green + 0.07*blue`
- ITU-R BT.601: `0.30*red + 0.59*green + 0.11*blue`

## remove extra transparent borders

```sh
convert -trim in.png out.pn
```

## remove extra white space

```sh
convert -transparent white in.png out.ong
convert -transparent white -fuzz 10% in.png out.ong
```

## put one image aside another

Use blank canvasses

```sh
convert -size 1920x1080 canvas:darkblue
```

Most basic version would be:

```sh
convert -size 1920x1080 canvas:black \
	-gravity west img1.png -composite \
	-gravity east img2.png -composite \
	out.png
```

Advanced:

```sh
convert -size 1920x1080 canvas:blue-purple -swirl 180 \
	-gravity west img1.png -geometry +150+0 -composite \
	-gravity east img2.png -composite \
	out.png
```

## rotate

```sh
convert in.jpg -rotate 90 out.jpg
```

