
# Assembly language

- Assembly = the language
- Assembler = the "compiler"

```sh
as file.s -o file.o
ld file.o -o file
```

To debug: `objdump -d file`

## ARM

- [Introduction to ARM Assembly
  Basics](https://azeria-labs.com/writing-arm-assembly-part-1/)

- decimal: `#16`
- hexadecimal:"`0x0F`

### registers

\#|alias|purpose|x86 equivalent
R0|-|General purpose|eax
R1|-|General purpose|ebx
R2|-|General purpose|ecx
R3|-|General purpose|edx
R4|-|General purpose|esi
R5|-|General purpose|edi
R6|-|General purpose|-
R7|-|Syscall number|-
R8|-|General purpose|-
R9|-|General purpose|-
R10|-|General purpose|-
R11|FP|Frame Pointer|EBP
R12|IP|Intra Procedural Call|-
R13|SP|Stack Pointer|ESP
R14|LR|Link Register|-
R15|PC|Program Counter|EIP
CPSR|-|Current Program Status Register|EFLAGS

- Stack Pointer: points to the top of the stack
- Link Register: points to memory address of the next caller instruction
- Program Counter:
  - arm mode: increased 4 bytes, hold current instruction plus 8
  - thumb mode: 2 bytes, holds current instruction plus 4
- Current Program Status Register: compiled flags (eg. thumb, fast, interrupt,
  overflow carry zero negative)
  - [N, Z, C, V] = [SF, ZF, CF, OF] in x86 EFLAG

The ARM convention specifies that a calling function parameters range from R0 to
R3.

#### flags

CPSR for ARM, EFLAGS for x86

symbol|name|description|x86 equivalent
-|-|-
N|negative|instruction yields a negative number|SF
Z|zero|instruction yields a zero|ZF
C|carry|instruction yields a carry bit|CF
V|overflow|instruction yield cannot be represented in two's complement|OF
E|endian|0 for little, 1 for big|
T|thumb|1 thumb, 1 arm|
M|mode|privilege mode (USR, SVC, etc)|
J|jazelle|third execution state that allows some ARM processors to execute Java bytecode in hardware

### instruction set

- [ARM and Thumb Instructions
  Set](https://developer.arm.com/documentation/dui0473/m/arm-and-thumb-instructions/arm-and-thumb-instruction-summary)

ARM thumb versions are not all the same, this is why enhanced (ThumbV2) has been
created.

```
MNEMONIC{S}{condition} {Rd}, operand1, operand2
```

- `MNEMONIC`: Short name (mnemonic) of the instruction
- `{S}`: An optional suffix. If S is specified, the condition flags are updated
  on the result of the operation
- `{condition}`: Condition that is needed to be met in order for the instruction
  to be executed
- `{Rd}`: Register (destination) for storing the result of the instruction
- `operand1`: First operand. Either a register or an immediate value
- `operand2`: Second (flexible) operand. Can be an immediate value (number) or a
  register with an optional shift

Examples:

```as
#123      - Immediate value (with limited set of values).
Rx, ASR n - Register x with arithmetic shift right by n bits (1 = n = 32)
Rx, LSL n - Register x with logical shift left by n bits (0 = n = 31)
Rx, LSR n - Register x with logical shift right by n bits (1 = n = 32)
Rx, ROR n - Register x with rotate right by n bits (1 = n = 31)
Rx, RRX   - Register x with rotate right by one bit, with extend

/* add contents of R1 and R2 and stores the result into R0 (Rd) */
ADD R0, R1, R2

/* add contents of R1 and the value 2 and store the result into R0 (Rd) */
ADD R0, R1, #2

/* move number 5 (operand2) to R0 (Rd) ONLY if the condition LE (<=) is
satisfied */
MOVLE R0, #5

/* move the contents of R1 (operand2) shifted left by one bit to R0. */
MOV R0, R1, LSL #1
```

instruction|description
-|-:
MOV|Move data
MVN|Move and negate
ADD|Addition
SUB|Subtraction
MUL|Multiplication
LSL|Logical Shift Left
LSR|Logical Shift Right
ASR|Arithmetic Shift Right
ROR|Rotate Right
CMP|Compare
AND|Bitwise and
ORR|Bitwise or
EOR|Bitwise XOR
LDR|Load
STR|Store
LDM|Load Multiple
STM|Store Multiple
PUSH|Push on Stack
POP|Pop off Stack
B|Branch
BL|Branch with Link
BX|Branch and eXchange
BLX|Branch with Link and eXchange
SWI/SVC|System Call

### move and store

While x86 allows you to directly change the memory, arm forces you to first load
(LDR) to a register, alter the register and then store (STR).

Syntax is:

```as
ldr|str Rx, [address, offset, shift]! ; pre-indent
ldr|str Rx, [address], offset, shift! ; post-indent
```

- brackets: the value found in the register between these brackets is a memory
  address we want to load/store something from/to
- offset [optional]: how many bytes to offset from address, can be a `#number`
  or another register
- ! [optional]: modify register
- shift [optinal]: how many bits to rotate

Example:

```as
ldr r0, adr_var1  ; load the memory address of var1 via label adr_var1 into
R0 ldr r1, adr_var2  ; load the memory address of var2 via label adr_var2 into R1
ldr r2, [r0] ; load the value (0x03) at memory address found in R0 to register R2
str r2, [r1] ; store the value found in R2 (0x03) to the memory address found in R1
```

## x86

[A brief Introduction to x86 calling
conventions](https://codearcana.com/posts/2013/05/21/a-brief-introduction-to-x86-calling-conventions.html)


