
# 66Suite (init system)

66 is a layer of software on top of s6 that makes s6 easier.

- The service directories for init scripts are located: `/etc/66/service`
- Modified init scripts, to avoid being overwritten by pkg updates, should be placed in: `/etc/suite66/service`

If, lets say, you wanted to create a tree named audio for your audio services

`66-tree -n audio`

Another example: If one wanted to create another tree that you may want to run upon next reboot, but not your current default

`66-tree -ncE slim-tree`

Now lets explain what the option flags are:

```
-n : new
-c is to set tree as current
-E is to enable
```

At anytime you can enable/disable/remove a tree with

```
66-tree OPTIONS TREE_NAME
-E: Enable
-D: Disable
-R: Remove
```

## Enable service in tree: default

`66-enable -t default sshd`

## Disable service in tree: default

`66-disable -t default sshd`

## Display contents of tree: default

`66-intree default`

## Start

`66-start -t default sshd`

## Stop

`66-stop -t default sshd`


66's boot config/boot-services are edited using:

`66-env -t boot -e $EDITOR boot@system`

This allows one to change timezone, console font, services, hostname and etc started during boot process,etc. If the config is changed the config must be activated using:

`66-enable -t boot -F boot@system`

## packages

Packages are usually called `PKG-suite66` or just `PKG-66`.

## scripts

Global scripts are located in one of two folders:

- `/etc/66/service`
- `/etc/suite66/service`

They look like this:

```
[main]
@type = classic
@version = 0.0.1
@description = "bitfmt for other architectures"
@user = ( root )
@options = ( log )

[start]
@execute = (
	/etc/66/script/binfmt.sh
)
```

## debugging

If a service during boot crashes and causes tty to not show, tty12 should be
available for diagnostic purposes and allow service(s) to be fixed.
