
# Gnome (window manager)

## Applications location

System shortcuts are located at `/usr/share/applications`. Put a shortcut in
there to make it searchable. Icons may be stored at `/usr/share/gnome-software`.

## Gnome shortcuts

Files ending with `.desktop` means shortcuts. Content examples:

```sh
[Desktop Entry]
Version=1
Name=Vivado
Exec=/opt/Typora-linux-x64/Typora
Icon=/opt/Typora-linux-x64/typora.png
Terminal=false
Type=Application
Name[en_US]=Typora
```

`Exec` is a shell command.

## Wallpapers

Usually under `/usr/share/backgrounds/`.
