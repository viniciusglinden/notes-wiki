
# tmux

- Tmux Package Manager (tpm)

- [A Quick and Easy Guide to
  TMUX](https://hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/)
- [Clipboard](https://github.com/tmux/tmux/wiki/Clipboard)

## TPM installation

- [Use Tmux Plugin
  Manager](https://jonasbn.github.io/til/tmux/use_tmux_plugin_manager.html)

- `git clone https://github.com/tmux-plugins/tpm ~/.config/tmux/plugins/tpm`
- `run '~/.tmux/plugins/tpm/tpm'` inside `tmux.conf`
- `tmux && tmux source ~/.config/tmux/tmux.conf`
- then prefix + I to install tpm packages

## Configuration

- `bind` binds a key to a command with the prefix key
- `bind -n` binds a key to a command without the prefix key
- `bind-key` alias to `bind`
- `set` set options
  - `set -g` set global options
  - `set -a` append options
- `set-option` alias to `set`
