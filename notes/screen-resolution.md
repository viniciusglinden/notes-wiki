
# Screen Resolution

Use `xrandr` as a back-end and `arandr` as a front-end.

To add a new resolution (2560 by 1440 with 60Hz):

```sh
cvt 2560 1440 60
xrandr --newmode "2560x1440_60.00" 312.25 2560 2752 3024 3488 1440 1443 1448 1493 -hsync +vsync
xrandr --addmode HDMI-1 2560x1440_60.00
xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal \
	--output HDMI-1 --mode 2560x1440_60.00 --pos 1932x0 --rotate normal
```

- Use `xrandr -q` to display possible screen resolutions.
- `cvt`: calculates VESA CVT mode lines
- for some LCD screens, the command `cvt -r` (reduced blanking) is to be used.
- if you find that the screen goes blank when the modeline is applied, try
 lowering the refresh rate (e.g. 30 instead of 60).
- `xrandr`'s `--auto` option selects the to the preferred (maximum available)
 resolution
- 1920x1080 resolution has a 16:9 proportion (1,777 ratio)

