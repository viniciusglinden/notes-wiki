
# Code testing

Strategies are:

- unit tests
- integration tests
- regression tests
- fuzzing
- format/style tests
- test coverage

C++ testing frameworks

- [Gtest](gtest.md)
- [cmocka](https://cmocka.org/)
- [Catch2](catch2.md)

C testing frameworks

- [µnit](https://nemequ.github.io/munit/) for embedded devices

Other testing tools

- manually: assert/longjmp
- GNU autotools (portable qemu/valgrind)
- cmake/ctest (`enable_testing()`, by return code only)
- tap (ok 1, ok 2, ...)
- backtrace
- collect core dump, in case of crash

## TAP (Test Anything Protocol)

- [tap](https://testanything.org/)

TAP is gut for parallel testing. It may find concurrency failures and
missing/skipped tests.

## Testing frameworks concepts

### unit test vs mock

- [What is
  mocking?](https://stackoverflow.com/questions/2665812/what-is-mocking/40244095#40244095)

Suppose you have a class foo that has as its member class bar. You only want to test foo (not bar).
You mock class bar (create a fake object) and give it to class foo.

Mocks are also useful when testing if calls to mocked objects are correct.

A usage example would be: no more memory available, write to a device that does not exist, etc.

## firmware

- dedicated HW
- simulator
- emulator
- CrashDump
- LTP (Linux Testing Project)
- cbmc (Formal symbolic verification)

## strategies


For easy C project, `make check` or `make test` is sufficient.

## collect core dump

- Make sure the system does not limit the core dump file to zero: `ulimit -c unlimited` (do this for
  every new terminal)
- Compile the program with debugging flag `-g`
- Examine core dump file with gdb: `gdb PROGRAM FILE`

Note: generated code file is called `core`. You can change its name by doing:
`echo "./new_name > /proc/sys/kernel/core_pattern`.

## valgrind

To test more easily specifically memory leak, compile with the `-g` flag and then:

```sh
valgrind -s --leak-check=full ./a.out
```

## kcachegrind

kcachegrind is profiler tool that runs on valgrind and must be installed
separately.

```sh
valgrind --tool=callgrind <programm>
kcachegrind callgrind.out.<PID>
```

## address sanitizer (ASan)

- [Address sanitizer
  algorithm](https://github.com/google/sanitizers/wiki/AddressSanitizerAlgorithm)

Is a compiler flag for both GCC and Clang: `-fsanitize=address`.

