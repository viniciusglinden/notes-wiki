
# Makefile (GNU utility)

- [g++ and make](https://web.stanford.edu/class/cs193d/handouts/make.pdf)

## syntax

- target: name of the executable or object
- prerequisites/dependencies: list of required files to create the target
- command: action that `make` carries out
- rule: how and when to (re)make a target

A simple `make` call executes the first rule by default.

```
target ... : dependencies ...
	command
	...
```

- `make -d` to spit out what it is doing

### substitution

```
OBJ = ${SRC:.c=.o}
```

### set variable if unset (fallback)

`?=` indicates to set the `KDIR` variable only if it's not set/doesn't have a value.

For example:

```
KDIR ?= "foo"
KDIR ?= "bar"

test:
    echo $(KDIR)
```

Would print "foo"

### displaying help

```
options:
	@echo dwm build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"
```

### global installation

Use the predefined variable `${DESTDIR}` and `${PREFIX}`:

```
cp -f ${TARGET} ${DESTDIR}${PREFIX}/bin
```

You can also modify it when calling the `make` command:

```sh
make DESTDIR=/tmp install
```

`PREFIX` is preferable over `DESTDIR`.

### phony rules

Suppose you have a directory called `clean` and you want to add the typical
`clean` rule (nothing to do with the directory). Thus, you should list it as a
phony rule:

```
.PHONY: clean

clean:
	${RM} *.o
```

In terms of `make`, a phony target is simply always out-of-date.

## macros

- `$@`: target name
- `$^`: dependency name
- `$?`: name of the changed dependents
- `$<`: name of the related file that caused the action
- `$*`: prefix shared by target and dependent files

```make
%.bin: %.asc
        echo @ $@
        echo ^ $^
        echo ? $?
        echo < $<
        echo * $*
```

Will print

```
echo @ rgb.bin
@ rgb.bin
echo ^ rgb.asc
^ rgb.asc
echo ? rgb.asc
? rgb.asc
echo < rgb.asc

echo * rgb
```

### conventional macros

Type `make -p` to print out the defaults.

- `CC`: program to compile C (default is `cc`)
- `CXX`: program to compile C++ (default is `g++`)
- `RM`: command to remove files (default is `rm -f`)

- `CFLAGS`: extra flags to give the C compiler
- `CXXFLAGS`: extra flags to give the C++ compiler
- `CPPFKAGS`: extra flags to give the C preprocessor and programs (e.g.: `-g
  -Wall -I/usr`)
- `LDFLAGS`: extra flags to give the compilers when they invoke the linker `ld`
  (e.g.: `-L/usr... -lCoolFns`)

## Examples

### Complex

Structure is:

- src folder for `.c`, `.cpp`, `.h`, `.hpp`...
- obj folder for `.o`, `.so`...
- bin folder for executables

```
TARGET   = projectname

CC       = gcc
# compiling flags here
CFLAGS   = -std=c99 -Wall -I.

LINKER   = gcc
# linking flags here
LFLAGS   = -Wall -I. -lm

# change these to proper directories where each file should be
SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin

SOURCES  := $(wildcard $(SRCDIR)/*.c)
INCLUDES := $(wildcard $(SRCDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
rm       = rm -f

$(BINDIR)/$(TARGET): $(OBJECTS)
    @$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
    @echo "Linking complete!"

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
    @$(CC) $(CFLAGS) -c $< -o $@
    @echo "Compiled "$<" successfully!"

.PHONY: clean
clean:
    @$(rm) $(OBJECTS)
    @echo "Cleanup complete!"

.PHONY: remove
remove: clean
    @$(rm) $(BINDIR)/$(TARGET)
    @echo "Executable removed!"
```

### Simple with multiple targets

There is only one folder containing everything.

```
CC       = gcc
LFLAGS   =

SOURCES := $(wildcard *.c)
TARGETS := $(patsubst %.c, %, $(SOURCES))

all: $(TARGETS)

$(TARGETS): $(SOURCES)
	@echo Compiling $@...
	$(CC) $(LFLAGS) $@.c -o $@

clean:
	@rm -f $(TARGETS)

.PHONY: clean
```

### one file

Not that you dont specify how to make `hello`, but it works anyways.

```
CC = gcc
CFLAGS = -DSUBJECT="\"World"\"


hello.o: hello.c
    $(CC) $(CFLAGS) -c hello.c -o hello.o

hello: hello.o

clean:
    rm hello.o hello
```

## generate `compile_commands.json`

```sh
make --always-make --dry-run \
 | grep -wE 'gcc|g\+\+' \
 | grep -w '\-c' \
 | jq -nR '[inputs|{directory:".", command:., file: match(" [^ ]+$").string[1:]}]' \
 > compile_commands.json
```

Or simply use [Brear](https://github.com/rizsotto/Bear)

## execute shell command

```
EXAMPLE := $(shell cat ./example.txt)
```

## printing commands without executing

```sh
make -d <target>
```

## functions

- [file name
  functions](https://www.gnu.org/software/make/manual/html_node/File-Name-Functions.html)

## debug

[Use this rule to print
variables:](https://stackoverflow.com/questions/16467718/how-to-print-out-a-variable-in-makefile)

```make
print-%:
	@echo $* = $($*)
```
