
# Xilinx Vivado (VHDL simulator)

## Compiling Vivado libraries from Bash

I never did implemented this, so I don't know if this works.

```sh
VSimBinDir=/opt/questasim/10.4d/bin
DestDir=xilinx-vivado

Simulator=questa
Language=vhdl
Library=all
Family=all

CommandFile=vivado.tcl

echo "compile_simlib -force -library $Library -family $Family -language $Language -simulator $Simulator -simulator_exec_path $VSimBinDir -directory $DestDir" > $CommandFile
if [ $? -ne 0 ]; then
  echo 1>&2 -e "${COLORED_ERROR} Cannot create temporary tcl script.${ANSI_NOCOLOR}"
  exit -1;
fi
echo "exit" >> $CommandFile

# compile common libraries
$Vivado_tcl -mode tcl -source $CommandFile
if [ $? -ne 0 ]; then
  echo 1>&2 -e "${COLORED_ERROR} Error while compiling Xilinx Vivado libraries.${ANSI_NOCOLOR}"
  exit -1;
fi
```

## Vivado Simulation from terminal

Running the Vivado Simulation from the terminal (batch) is a three step process:

1. Identifying and parsing design files.
2. Elaborating and generating an executable simulation snapshot of the design.
3. Running simulation using the executable snapshot.

When running a timing simulation three additional steps are required:

* Generating a timing netlist

Note that, in Windows, you should need to write `call ` before each command.

* `xvlog`: adds a verilog file to the project
* `xvhdl`: adds a vhdl file to the project
* You can specify what library it is added to with `-work library`
* You need to add files in order, that means, if A is instatiated in B, you must add A befor B
* `xelab`: elaborates the design
* `-debug typical` option is recommended for logic simulations
* `-s` snapshot is necessary to specify a name
* `xsim`: simulates the snapshot

Note that, if you have Intellectual Proprieties (IPs), you may also have to add them. To do this, you may:

* Export individual IP
```sh
export_ip_user_files -of_objects [get_ips ip_name] -no_script -force
export_simulation -simulator ies -directory ./export_script \
-of_objects [get_ips ip_name]
```

* Export every used IP
```sh
export_ip_user_files -no_script -force
export_simulation -simulator ies -directory ./export_script
You can also generate simulation scripts for block design objects:
```

The following is the important bits executed when simulation is run via GUI:

```tcl
xvhdl --incr --relax -prj sim_stepper_counter_vhdl.prj 2>&1 | tee -a compile.log
xelab -wto 0b496526122f44eaab977b7295d05d72 --incr --debug typical --relax --mt 8 -L work -L secureip --snapshot sim_stepper_counter_behav work.sim_stepper_counter -log elaborate.log
source /home/vglinden/VivadoProjects/common/common.sim/2_stepper_cnt/behav/xsim/xsim.dir/sim_stepper_counter_behav/webtalk/xsim_webtalk.tcl -notrace
open_wave_config /home/vglinden/VivadoProjects/common/sim_stepper_counter_behav.wcfg
source sim_stepper_counter.tcl
# set curr_wave [current_wave_config]
# if { [string length $curr_wave] == 0 } {
#   if { [llength [get_objects]] > 0} {
#     add_wave /
#     set_property needs_save false [current_wave_config]
#   } else {
#      send_msg_id Add_Wave-1 WARNING "No top level signals found. Simulator will start without a wave window. If you want to open a wave window go to 'File->New Waveform Configuration' or type 'create_wave_config' in the TCL console."
#   }
# }
# log_wave -r /
```

This section is incomplete. It's easier to configure by GUI and generate tcl to save configuration.

## Print timing diagram

- Save as a VCD
- Import into TimingAnalyzer
- Use TimingAnalyzer to manipulate the plots and produce high quality images
- Export in format of your choice

## Create a .vcd file

Run the simulation and type in the tcl console:

```tcl
open_vcd
log_vcd [get_object /<toplevel_testbench/uut/*>]
run *ns
close_vcd
```
