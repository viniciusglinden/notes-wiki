
# VHDL (hardware description language)

## References

- [FPGA designs with
  VHDL](https://vhdlguide.readthedocs.io/en/latest/vhdl/package.html)
- [OpenCores](https://opencores.org/): open-source HDL code
- [VUnit unit testing framework for
  VHDL/SystemVerilog](https://vunit.github.io/)

## Attributes

* [VHDL Predefined Attributes](https://www.csee.umbc.edu/portal/help/VHDL/attribute.html)

### timing

* Event: Change in the current value of a signal. E.g.: '1' to '0' or 'H' to '1'.
* Active: when  it acquires an updated value during simulation cycle, regardless of whether the updated value is the same. E.g. '1' to '1'.

|attribute|type|description|
|---|---|---|
|'event|boolean|signal has a new value|
|'active|boolean|signal has been assigned a value|
|'transaction|bit|toggle on new assignment|
|'delayed(T)|same as signal|same signaled, delayed by atime T|
|'stable(T)|boolean|true: no event during time T|
|'quiet(T)|boolean|true: no activity during time T|
|'last_event|time|amount of time that has elapsed since last event|
|'last_active|time|amount of time that has elapsed since last assignment|

## Wait statement

`wait [sensitivity clause] [condition clause] [timeout clause];` with

* sensitivity clause: on sensitivity list
* condition clause: until condition
* timeout clause: for time expression

|statement|sensitivity list|condition|timeout|
|---|---|---|---|
|wait|none|true|time'high|
|wait on S1, S2|S1, S2|true|time'high|
|wait until clk'event|clk|clk'event|time'high|
|wait until clk'event and clk = '1'|clk|clk'event and clk = '1'|time'high|
|wait on S1 until clk = '1'|S1|clk = '1'|time'high|
|wait on S2 for 100 ns|S2|true|100 ns|
|wait on S1, S2 until clk = '1' for 10 us|S1, S2|clk = '1'|10 us|
|wait on clk for VAR * 1 ns|clk|true|VAR * 1 ns|
|wait on S1 until VAR = 10|S1|VAR = 10|time'high|
|wait until VAR = 10|none|VAR = 10|time'high|
|wait on S1'transaction|S1'transaction|true|time'high|
|wait for 100 ns;|none|none|100 ns|
|wait on S1'event|error: S1'event is not a signal|true|time'high|
|wait on S1'active|error: S1'active is not a signal|true|time'high|
|wait on S1'delayed(100ns)|S1'delayed(100 ns)|true|time'high|
|wait on S1'delayed(100 ns)'transaction|S1'delayed(100 ns)'transaction|true|time'high|

## Linter

### VHDL Style Guide (VSG)

VSG not only is a linter, but also a style checker.

- [Read the Docs](https://vhdl-style-guide.readthedocs.io/en/latest/index.html)
- [Github](https://github.com/jeremiah-c-leary/vhdl-style-guide)

Usage:

- analyze the file: `vsg -f file.vhd`
- analyze and auto-fix the file: `vsg -f file.vhd --fix`
- analyze the file \& output the results in syntastic format: `vsg -f file.vhd
  --output_format syntastic`

Note: it doesn't currently have Syntastic integration. (See `:help syntastic-checkers`).

### GHDL

GHDL compiler also provides a linter [GHDL wiki page](ghdl.md).

mcode is 2 times slower than gcc and llvm flavors at simulation, but
it launches immediately, which makes it ideal for small-scale
simulations.

## impure functions

An impure function with side-effects ie. may access signals that are outside its
sensitivity list and also alters them.

* [How to use an impure function in VHDL](https://vhdlwhiz.com/impure-function/)

## with ... select

The `with ... select` statement is supported in the latest VHDL specification.

Example:

```vhdl
with cs select saida <=
    entrada1 when '0',
    entrada2 when others;
```

## module \& remainder operators

```vhdl
  9  mod   5  =  4
  9  rem   5  =  4
  9  mod (-5) = -1
  9  rem (-5) =  4
(-9) mod   5  =  1
(-9) rem   5  = -4
(-9) mod (-5) = -4
(-9) rem (-5) = -4

  a  rem   b  =  r
a = 0 1 2 3 4 5 6 7 8 9 a b c d e f
b = 3 3 3 3 3 3 9 9 9 9
r = 0 1 2 0 1 2 6 7 8 0
```

For equal sign:
9/5=-9/-5=1.8 gets 1

```vhdl
9 mod 5 = 9 rem 5
-9 mod -5 = -9 rem -5
```

For unequal signs:
9/-5 = -9/5 = -1.8

- In "mod" operator -- -1.8 gets -2
- In "rem" operator -- -1.8 gets -1

Examples:

1. 9,-5

- 9 = (-5\*-2)-1 then: (9 mod -5) = -1
- 9 = (-5\*-1)+4 then: (9 rem -5) = +4

2. -9,5

- -9 = (5\*-2)+1 then: (-9 mod 5) = +1
- -9 = (5\*-1)-4 then: (-9 rem 5) = -4

3. -9,-5

- -9 = (-5\*1)-4 then: (-9 mod -5) = -4
- -9 = (-5\*1)-4 then: (-9 rem -5) = -4

4. 9,5

- 9 = (5\*1)+4 then: (9 mod 5) = +4
- 9 = (5\*1)+4 then: (9 rem 5) = +4

Operands may be a signal or constant and an integer, or (un)signed

## vector manipulation

```vhdl

a <= (0 => '1', 1 => '0'); -- a(1 downto 0) <= "01" (may be assigned out-of-order)
b <= (2 => '0', 1 => '1', 0 => '1'); -- b(2 downto 0) <= "011"
c <= (c'left downto c'right+1 => '1', others => '0'); -- c(2 downto 0) <= "110"
d <= (d'left downto d'right+3 => '1', others => '0'); -- d(9 downto 0) <= "1111111000"
d <= (others => '0', d'left downto d'right+3 => '1'); -- Error! others must be the last
e <= (e'left downto e'right+1 => '1') & '0'; -- e(9 downto 0) <= "1111111110"
f <= (e'left downto e'right => e, others => '0'); -- Error! 9 is out of range 7 downto 0
g <= (g'range => '0'); -- g(2 downto 0) <= "000", same as g <= (others => '0')
```

## predefined attributes

- [attributes](https://redirect.cs.umbc.edu/portal/help/VHDL/attribute.html)

## function

1. meant for combinatoric logic
2. executes in zero stimulation time
3. usually return a single value
4. parameters of mode IN
5. does not have wait statement
6. calls state always concurrent

## procedure

1. meant for sequential logic
2. may or may not execute in zero stimulation time depending
3. upon whether it has wait statement or not
4. may or may not return a single value
5. parameters of `in`, `out` or `inout`
6. has wait statement.
7. procedure call statement may be concurrent or sequential

Both procedure and functions may be implemented in package

## process

Always use the same process for the same functionality. Rationale: the compiler
puts the process in one part of the FPGA, so it is not split. If the
functionalities are interdependent, splitting them may cause synchronization
issues.

## `std_logic` values

* 'U': uninitialized. This signal hasn't been set yet
* 'X': unknown. Impossible to determine this value/result
* '0': logic 0
* '1': logic 1
* 'Z': High Impedance
* 'W': Weak signal, can't tell if it should be 0 or 1
* 'L': Weak signal that should probably go to 0
* 'H': Weak signal that should probably go to 1
* '-': Don't care.

## synthesis

* signal initialization (`signal a : std_logic := '1'`) is ignored in synthesis
- only use functions and procedures for "pre-compiler" stuff

## read file

Add the following library:

```vhdl
library STD;
use STD.TEXTIO.ALL;
```

The file described below as "input.txt" is used as an example

```
123
456
789
```

If you use the process below

```vhdl
architecture Behavioral of tb_template is
	file file_input					: text;
	signal chaS						: character;
begin
	process
		variable line_input			: line;
		variable character_input	: character;
	begin
	file_open(file_input,"input.txt",read_mode);
		while not endfile(file_input) loop
			readline(file_input,line_input);
			read(line_input,character_input);
			chaS <= character_input;
			wait until clk'event and clk = '1';
		end loop;
		file_close(file_input);
		wait;
	end process;
end Behavioral;
```

Output will read `1477777777...`

If one takes the `wait;` statement out, the file will repeat reading forever.

## write file

Add the following library:

```vhdl
library STD;
use STD.TEXTIO.ALL;
```

- Output file will **always** be written to the simulation folder (doesn't matter if you add a file with the same name in another location).
- The `writeline` command will always leave one more line open

The `JUSTIFIED` value inserts space characters for justification, so a bit vector "110" with

1. `JUSTIFIED = right` and `FIELD = 10`
2. `JUSTIFIED = left` and `FIELD = 10`

```
       110| (1)
110       | (2)
----------|
0123456789|
```

The example in figure below

```wavedrom
{ signal: [
  { name: "clk",  wave: "p......" },
  { name: "bus",  wave: "=======", data: ["000", "001", "010", "011", "100", "101"] },
  { name: "wire", wave: "0.1...0" }
])
```

Will print exactly (0 clock cycle of latency)

```
010
011
100
101
```

- `to_bitvector` is used to convert from `std_logic_vector`.

## Assert verification

Use `assert` to express the desired situation. For example:

```vhdl
assert error = '0' report "error occured!" severity warning;
```

## Instantiation

You may instantiate with direct, indirect and primitive instantiations (direct instantiation) or a mixed type instantiation.

Legend:

* `x` is the architecture;
* `y` is the component; and
* `work` is the library.

### Direct instantiation

This instantiation is recommended for everyday usage. This cannot be used for IPs ".xci". For these, use indirect instantiation instead.


```vhdl
label : entity work.y
port map (
...
);
```

### Indirect instantiation

This is the mandatory usage of IP ".xci" instantiation. May be used for everyday instantiations.

```vhdl
architecture inst of x is

component y is
port (
...
);
end component y;

begin

label : component y
port map (
...
);

end int;
```

Also, when instantiating IP ".xci", do not directly assign a value, as in `rst => '0'`.

When using IPs, the instantiation template is available at `.scr>sources_1>ip>NAME_OF_IP.vho`.

### Primitive instantiation

Used only for primitives. Click on "Language Templates" in the "Flow Navigator" panel for these emplates.

```vhdl

label : y
port map (
...
);
```

### Mixed type instantiation

For example:

```vhdl
archirecture ... is

component y
	port (...)
end component;

for component_name : component use entity work.component;
...

begin
component_name : component port map (...);
```

However, it seems as if this type brings no advantage over the others.

## coding style and convention

- [Xilinx HDL Coding style](https://wiki.electroniciens.cnrs.fr/images/Xilinx_HDL_Coding_style.pdf)

- The `ieee.std_logic_arith` may be used exclusively for synthesis purposes (not for implementation).
- The for asynchronous reset processes, reset should be the highest priority: `process(rst,clk)`
- Always mix the reset and clock if statement. This is because the synthesizers will link this statement to the silicon-level flip-flop. Example:

```vhdl
process(rst,clk)
begin -- executes when startup
	if rst = '1' then
		-- reset stuff
	elsif clk'event and clk = '1' then
		-- actual code
	end if;
end process;
```

- If the process does not involve clock signal, be free to create the sensitivity list that you wish.
- Do not use `rising_edge` or equivalent functions, because of portability issues.
- Asynchronous signals (excluding reset) should only be processed in a combinatoric manner (outside of process).
- Never use variables.
- Use parenthesis inside a boolean equation only when necessary. Parenthesis should be used to establish precedence of operations when the desired operation would be executed not in the correct order. The conventional order is preconceived and available on the Internet.
- Use boolean exclusively in generics.
- When naming a signal, for example *busy*, 1 means **yes** and 0 means **no**. So in the example case, when *busy* is 1 it means that the signal is telling that the module is busy. This means "active-high".
- For active-low functions (the above negated), include an "n" in-front of the port's name - in the previous example it would be *nbusy*.
- Use the architecture name before the file, for instance, when naming a file `m_axis` that has an architecture RTL (real-time logic), the file name will be `rtl_m_axis.vhd`.
- Architecture names shall be: RTL for real-time logic, inst for instantiation, sim for simulation and "tb" for test bench. Behavioral may be used for simulations and test benches as well.
- Top-level file will always be considered the one which adapts logic to the board/FPGA.
- Last instantiation, the which is the closest to the top-level file, will be logic only (no adaptation for the board interfaces).
- There is no distinction between modules and sub-modules
- Sub-modules are all the modules instantiated inside other modules, so one sub-module may contain other sub-modules
- Instantiations are HDL files which are only used for gathering modules, with
  no logic involved - only linking.
- Top-level are adaptation of instantiation to the end-board, therefore no complex logic is involved
- Simulation, being simple stimulae or complex test benches *may* encompass the top-level or tested unit.
- Depending on the simulation, it may be called "test bench".

```
+----------------------------+
|        simulation          |
| +------------------------+ |
| |       top-level        | |
| | +--------------------+ | |
| | |    instantiation   | | |
| | | +----------------+ | | |
| | | |    modules     | | | |
| | | | +------------+ | | | |
| | | | | sub-modules| | | | |
| | | | | (modules)  | | | | |
| | | | +------------+ | | | |
| | | +----------------+ | | |
| | +--------------------+ | |
| +------------------------+ |
+----------------------------+
```

## VHPI (C-API for testing)

This [GHDL UART Simulation](http://www.dossmatik.de/ghdl/GHDL_uart_sim.pdf)
gives a very good example how to simulate.

- [Bus Functional Model example](https://github.com/tirfil/VHDL_BFM_FMWK)
https://github.com/Architech-Silica/Designing-a-Custom-AXI-Master-using-BFMs
