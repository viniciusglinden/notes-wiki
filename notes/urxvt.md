
# URXVT

URXVT is outdated and difficult to configure. Use another terminal.

## Installing URXVT-256color in RHE 7

No use trying to compile from source (see above). Do the following:

1. `sudo yum install rxvt-unicode` (dependency)
2. downloaded urxvt-256color from [CentOS pkgs repo](https://centos.pkgs.org/7/epel-x86_64/rxvt-unicode-256color-9.21-3.el7.x86_64.rpm.html)
3. `chmod +x file_name`
4. `sudo rpm -i file_name`

This left me with the urxvt and urxvt-256colors.

## font listing

To list multiple fonts in `.Xdefault` add ",". For example:

```
URxvt.font: xft:Meslo\ LG\ S\ DZ\ for\ Powerline:style=RegularForPowerline:size=9, \
            xft:Pomodoro:size=6, \
            xft:FontAwesome:size=6, \
            xft:icomoon:size=6, \
            xft:octicons:size=6
```



