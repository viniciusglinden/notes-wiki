
# FreeRTOS

- [FreeRTOS website](https://www.freertos.org/)
- [FreeRTOS source code](https://github.com/FreeRTOS)

## Example

### Raspberry Pi Pico

For the CMake, please check the picoprobe project.
Blinky example:

```c
#include <stdbool.h>

#include "pico/stdlib.h"
#include "FreeRTOS.h"
#include "task.h"

void vBlinkTask(void) {
   while (true) {
      gpio_put(PICO_DEFAULT_LED_PIN, 1);
      vTaskDelay(250);
      gpio_put(PICO_DEFAULT_LED_PIN, 0);
      vTaskDelay(250);
   }
}

void main(void) {
   gpio_init(PICO_DEFAULT_LED_PIN);
   gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
   xTaskCreate(vBlinkTask, "Blink Task", 128, NULL, 1, NULL);
   vTaskStartScheduler();
}
```

## Message buffer and semaphore buffers

Stream buffers allow a stream of bytes to be passed from an interrupt service
routine to a task, or from one task to another task. A byte stream can be of
arbitrary length and does not necessarily have a beginning or end. Any number of
bytes can be written in one go, and **any number of bytes can be read in one
go**. They are optimized for single reader single writer scenarios, such as
passing data from an interrupt service routine to a task, or from one
microcontroller core to another.

Data is passed by copy: the data is copied into the buffer by the sender and out
of the buffer by the read.

- Stream buffers pass a continuous stream of bytes
- Message buffers pass variable sized but discrete messages, using stream
  buffers for data transfer

## Naming

- [Free RTOS Coding Standard and Style
  Guide](https://www.freertos.org/FreeRTOS-Coding-Standard-and-Style-Guide.html)

prefix|description|applies to function|applies to variable
-|-|-|-
ul|uint32_t|x|x
us|uint16_t|x|x
uc|uint8_t|x|x
x|non stdint types or size_t|x|x
ux|unsigned nonstdint types or size_t|x|x
e|enumerated|x|x
p|pointers|x|x
v|void|x||

## Heap

There are currently five different heap implementations provided by FreeRTOS.
Each use the very same API. Selection is done by linking to the different
libraries in CMake.

https://www.freertos.org/a00111.html

## Configuration

For every FreeRTOS application a configuration file has to be provided. The name
must be `FreeRTOSConfig.h`. This file must at a minimum only be included by the
application.

Check the [official documentation](https://www.freertos.org/a00110.html) for
some of the configurations to be used.

## Queue

- [Queue API](https://www.freertos.org/a00018.html)
