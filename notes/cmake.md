
# CMake

- [CMake Tutorial](https://cmake.org/cmake/help/latest/guide/tutorial/index.html)
- [CMake Reference Documentation](https://cmake.org/cmake/help/latest/)
- [CMake cookbook](https://github.com/dev-cafe/cmake-cookbook)
- [CMake with include and source
  paths](https://stackoverflow.com/questions/8304190/cmake-with-include-and-source-paths-basic-setup)
- [How to I make CMake output into a 'bin'
  dir?](https://stackoverflow.com/questions/6594796/how-do-i-make-cmake-output-into-a-bin-dir)
- [Modern Simple CMake Tutorial](https://codevion.github.io/#!cpp/cmake.md)
- [Functions](https://cliutils.gitlab.io/modern-cmake/chapters/basics/functions.html)

Is basically a Makefile compiler.

Main file is `CMakeLists.txt`.

Minimum `CMakeLists.txt`:

```cmake
cmake_minimum_required(VERSION 3.0)
project(hello_world)

add_executable(hello_world main.c)
```

Then, with libraries:

```cmake
cmake_minimum_required(VERSION 2.4)

project(hello_world)

set(CMAKE_C_STANDARD 99)

include_directories(${PROJECT_SOURCE_DIR})
add_library(applib foo.cpp)
add_executable(app main.cpp)
target_link_libraries(app applib)
```

This file will always stay on project root directory.

Note: There is also a `cmake-gui`.

It is always a good idea to make a `build` folder that will hold the Makefile.
When generating the Makefile:

```sh
cmake -S <root> -B build/
cmake --build build [--target <project>]
cmake --build build --target clean # equivalent to make clean
cmake --build build [--target <project>] --clean-first
```

## Understanding CMake

- [Introduction to modern
  CMake](https://internalpointers.com/post/modern-cmake-beginner-introduction)
- [Effective Modern
  CMake](https://gist.github.com/mbinna/c61dbb39bca0e4fb7d1f73b0d66a4fd1)
- [It's Time To Do CMake
  Right](https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/)

CMake is code, production code. "Old" CMake is like imperative programming,
whereas Modern CMake is like OOP. Modern CMake starts at version `3.0.0`.  In
Modern CMake, `CMakeLists.txt` lists a collection of _targets_ and
_proprieties_. A target is a binary, e.g., the targets propriety a source file,
dependencies, etc.

First thing to do is to specify the CMake version. Second thing to do is to
specify the project, with optional flags, such as `LANGUAGES`.

`add_executable()` will define a binary target. Every target propriety
specification command start with `target_`. These commands also require the
_scope_ definition, that is, how should proprieties propagate when you include
the project into other CMake-based parent projects. Scope can either be
`PRIVATE`, `PUBLIC` or `INTERFACE`.


You may configure what language standard to use with `target_compile_features()`
command. Moreover, you may define compiler flags with
`target_compile_options()`.

If you have a precompiler flag, you may define it with `option()` command.

## scope definition

[CMake: public vs private vs
interface](https://leimao.github.io/blog/CMake-Public-Private-Interface/)

- `PRIVATE`: internal to the build process modeling _build requirements_. This
  is the default scope.
- `INTERFACE`: external by the users of the target, modeling _usage
  requirements_.
- `PUBLIC`: both `PRIVATE` and `INTERFACE`.

For any `target`, in the preprocessing stage, it comes with a `INCLUDE_DIRECTORIES` and a
`INTERFACE_INCLUDE_DIRECTORIES` for searching the header files building.
`target_include_directories` will populate all the directories to `INCLUDE_DIRECTORIES` and/or
`INTERFACE_INCLUDE_DIRECTORIES` depending on the keyword `<PRIVATE|PUBLIC|INTERFACE>` we specified.

- `PRIVATE` only cares about himself and does not allow inheritance
- `INTERFACE` only cares about others and allows inheritance
- `PUBLIC` cares about everyone and allows inheritance

The easiest example would be specifying library includes with
`target_include_directories`, alas:

- `PUBLIC`: library and library-includer will have access to the headers
- `PRIVATE`: only library will have access to the headers
- `INTERFACE`: only library-includer will have access to the headers

## syntax

- [Set CFLAGS and CXXFLAGS options using
  CMake](https://stackoverflow.com/questions/10085945/set-cflags-and-cxxflags-options-using-cmake)

- `project(<project name>)`
- `add_executable(<executable name> <source file(s)>)`: defines a main
  executable (put this at the start)
- `add_subdirectory(<folder>)`: include a "sub" CMakeLists.txt
- `include_directories(<header files folder>)`: tells the main object where to
  find the headerfile
- `add_library(<library name> <library source files>)`: creates a library object
- `target_include_directories(<library name> <access type> <library name>)`:
  where the library finds its headerfiles
- `target_link_libraries(<library name> <executable name>)`: links the library
  to an executable
- `message([<mode>] "<text>")`: echo
- `set(VARIABLE what)`: just like `VARIABLE=what` in shell
  - Special variables: `CMAKE_BUILD_TYPE`, `CMAKE_C_FLAGS`,
    `CMAKE_C_FLAGS_DEBUG`, `CMAKE_C_FLAGS_RELEASE`
- `message(<level> "message")` to print a message (if level is `FATAL_ERROR`,
  compilation will be aborted)
- `add_compile_definitions(<global definitions>)` or
  `target_compile_definitions(<target> [scope] <definitions>)`
  - eg. `target_compile_definitions(foo PUBLIC FOO=1)`

## test integration

[C++ project setup with CMake and unit
tests](https://raymii.org/s/tutorials/Cpp_project_setup_with_cmake_and_unit_tests.html)

Include `enable_testing()` in the `CMakeLists.txt` of your testing project. And
use the following directives:

```cmake
add_executable(runUnitTests
    project1_unittests.cpp
)
target_link_libraries(runUnitTests
    gtest # ${GTEST_LIBRARIES}
    gtest_main # ${GTEST_MAIN_LIBRARIES}
    pthread
)
```

Finally, add the test to be executed:

```cmake
add_test(
    NAME runUnitTests
    COMMAND runUnitTests
)
```

## integration with language server

Most language servers require one to export a file that describes how the
compilation was done. This is mostly the `compile_commands.json` file. It can be
automatically produced by invoking `cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1`
(`set(CMAKE_EXPORT_COMPILE_COMMANDS ON)`).

The `compile_commands.json` looks like the following:

```json
[
    {
        "directory": "/path/to/root",
        "command": "compiler -c ...",
        "file": "path/to/file"
    },
    ...
]
```

This however will only be generated if CMake runs successfully. For debugging
set `CMAKE_VERBOSE_MAKEFILE` to `ON`.

## cmake executables

- `cmake`: cli interface
- `ccmake`: curses cmake
- `cmake-gui`: GUI cmake
- `ctest`: run unit tests (gtest, catch2, ...), better than running
  individually, because it runs hybrid and generates a log

Note: run `cmake --build .` instead of `make`. Reason is `cmake --build .` runs
the intended build system, as described via `CMakeLists.txt`.

## generate debug, release, different builds

- [Debug vs Release in
  CMake](https://stackoverflow.com/questions/7724569/debug-vs-release-in-cmake)
- [Userful Variables: compilers and
  tools](https://gitlab.kitware.com/cmake/community/-/wikis/doc/cmake/Useful-Variables#compilers-and-tools)

CMake will not generate a different Makefile target. You have to use the
`--build` flag. For example:

```sh
cmake -D CMAKE_BUILD_TYPE=Debug .
cmake --build .
cmake -D CMAKE_BUILD_TYPE=ujneubue .
cmake --build .
```

Then you can specify different flags inside `CMakeFiles.txt`:

```cmake
SET(CMAKE_C_FLAGS "-Wall")
SET(CMAKE_C_FLAGS_DEBUG "$(CMAKE_C_FLAGS) -O0")
SET(CMAKE_C_FLAGS_UJNEUBUE "$(CMAKE_C_FLAGS) -Os")
```

Note: by default, CMake uses the debug build. To switch this off you can just
`cmake -D CMAKEBUILD_TYPE= .`

The environment variables LDFLAGS, CFLAGS, CXXFLAGS or FFLAGS are also evaluated by CMake.

## make verbosity

```sh
cmake -D CMAKE_VERBOSE_MAKEFILE:BOOL=ON .
make
```

or

```sh
cmake .
make VERBOSE=1
```

Or, in the `CMakeFiles.txt`:

```cmake
set(CMAKE_VERBOSE_MAKEFILE on)
```

## integration with Doxygen

- [FindDoxygen - CMake
  Documentation](https://cmake.org/cmake/help/latest/module/FindDoxygen.html)

```cmake
find_package(Doxygen)
if (DOXYGEN_FOUND)
    add_custom_target(doc_doxygen ALL
            COMMAND ${DOXYGEN_EXECUTABLE}
            COMMENT "Generating Doxygen documentation")
else (DOXYGEN_FOUND)
    message("Doxygen need to be installed to generate the doxygen documentation")
endif (DOXYGEN_FOUND)
```

## ctest

- `ctest -N` list all tests
- `ctest -R` run specific test, supports regex

## clean target

Use the internal cmake_clean.cmake scripts generated for for each target - which, unfortunately, are
not directly accessible through make from the root path. For example, if you have a target named
foo, you would write:

```sh
cmake -P CMakeFiles/foo.dir/cmake_clean.cmake
```

## generator expressions

- [CMake generator
  expressions](https://stackoverflow.com/questions/46206495/cmake-generator-expressions)
- [What does "$<$<CONFIG:Debug>:Release>" mean in
  cmake?](https://stackoverflow.com/a/34490397)

A generator is responsible for writing the input files for a native build
system.

CMake has two phases:

- Configuration Phase
- Generator Phase (generate the build system)

The generator expression are for everything only the generator could know.

`$<x:y>` is nearly a half of all usage of generator expressions. Its meaning in
short: if generator expression x is evaluated to TRUE (as boolean), then value
of generator expression y is used. Otherwise, empty string is used. Simply put:
`SET(foo $<x:y>)` translates to:

```
foo=
if x:
    foo=y
```

## format CMakeLists code

Just use `cmake-format`.

## rule for a single object

Use `CMAKE_<LANG>_COMPILE_OBJECT`:

```cake
set(CMAKE_C_COMPILE_OBJECT "${CMAKE_C_COMPILE_OBJECT} -Wa,-alh=<OBJECT>.lst")
```

## standard variables

[Userful variabels](https://gitlab.kitware.com/cmake/community/-/wikis/doc/cmake/Useful-Variables)

```
project/
  CMakeLists.txt
  a/
    CMakeLists.txt
    src/
    inc/
  build/
    ...
    a/
```

You are using the root `CMakeLists.txt`. `CURRENT` indicates where the current
`CMakeLists.txt` being processed (`add_subdirectory`, not `include`).

A "listfile" is a CMake script (typically called `CMakeLists.txt`) that
describes projects and targets.

- `CMAKE_CURRENT_SOURCE_DIR`: `project/a`
- `CMAKE_SOURCE_DIR`: `project`
- `CMAKE_CURRENT_LIST_DIR`: `project/a`
- `CMAKE_BINARY_DIR`: `project/build`
- `CMAKE_CURRENT_BINARY_DIR`: `project/build/a`
- `CMAKE_MODULE_PATH`: empty, this is the directory that CMake will search for
  modules ([list of standard CMake
  modules](https://cmake.org/cmake/help/latest/manual/cmake-modules.7.html))

Please note that `CMAKE_LIST_DIR` does not exist. Use `CMAKE_SOURCE_DIR` to get
the project root folder.

## variables (`set`)

- [Variables
  explained](https://hsf-training.github.io/hsf-training-cmake-webpage/05-variables/index.html)

Variables are manipulated with `set`.

If the variable should be set via the command line (`-D variable`), use the
`CACHE` keyword: `set(variable value CACHE)`. However, this will also make it
persistent across runs. Remove it from the cache by using the `unset` command or
via `-U variable` in the command line. Caches variables are listed in the
`CMakeCache.txt` file.

Keep in mind that a variable is contained inside the included CMake file. Try
using `set(variable value PARENT_SCOPE)`.

Note: use `include(CMakePrintHelpers)` to use `cmake_print_properties` and
`cmake_print_variables`.

## variable types

For example in `set` or `option`:

- `BOOL`: Boolean ON/OFF value
- `PATH`: Path to a directory
- `FILEPATH`: Path to a file
- `STRING`: Generic string value
- `INTERNAL`: Do not present in GUI at all
- `STATIC`: Value managed by CMake, do not change
- `UNINITIALIZED`: Type not yet specified

## getting environment variables

To get system environment variables:

```cmake
$env{MY_ENV_VAR}

if(DEFINED ENV{MY_ENV_VAR})
    # do something is MY_ENV_VAR is defined
endif()
```

## functions vs macros

The only difference between a function and a macro is scope; macros don't have
one.

Macros

- are expanded inline when they are called, similar to text substitution
- do not create a new scope
- arguments passed to macros are treated as literal text and not evaluated as
  CMake expressions
- can modify variables in the caller's scope
- changes made by macros are visible after the macro call
- are typically used for simple text substitutions or to define custom control
  structures

Functions:

- are defined using the `function()` command and called using the `call()`
  command
- create a new scope, and variables defined within a function are local to that
  scope
- arguments passed to functions are evaluated as CMake expressions
- cannot modify variables in the caller's scope directly, but they can use the
  `set()` command to modify variables within their own scope
- changes made by functions are not visible after the function call unless
  explicitly returned or stored in variables.
- are more flexible and allow for more complex logic and flow control
- can have multiple return values

If you need simple text substitution or want to modify variables in the caller's
scope, macros are sufficient. If you need more complex logic, local variables,
or multiple return values, functions are a better choice.

## Import another cmake file

- [Collection of cmake modules](https://github.com/rpavlik/cmake-modules)

```cmake
import(<file>.cmake)
```

## Cross-compiling

- [CMake toolchain for ARM
  compiler](https://github.com/vpetrigo/arm-cmake-toolchains)

## Presets commands

```sh
cmake . --list-presets
cmake . --preset <configure preset name>
cmake --build --preset <build preset name>
cmake --build --preset <build preset name> --clean-first
```

## library

These are the library types:

- STATIC, SHARED, MODULE
- OBJECT: compile source without archiving nor linking
  - When linking against something else, (be it library or executable), only the
    used functions will be kept!
- INTERFACE: header files only
- IMPORTED
- ALIAS

## Building a dependency graph

- [CMakeGraphVizOptions](https://cmake.org/cmake/help/latest/module/CMakeGraphVizOptions.html)

```sh
# configure stage
cmake [...] --graphviz=<file.dot>
dot -Tpng -o <file.png> <file.dot>
# or for only a specific target
dot -Tpng -o <file.png> <file.dot>.<target-name>
```

The different dependency types have different edges:

- PUBLIC: solid
- INTERFACE: dashed
- PRIVATE: dotted

## Auto configure clangd with CMake

Create a clangd configuration template:

```
# .clangd.template
# Tweak the parse settings
CompileFlags:
  CompilationDatabase: @COMPILATION_DB@
  Remove: [
      -m32x32, # Example
  ] # strip unknown flags to clangd
```

Add this to the `CMakeLists.txt`:

```cmake
set(COMPILATION_DB "${PROJECT_BINARY_DIR}")
configure_file(${CMAKE_SOURCE_DIR}/.clangd.template ${CMAKE_SOURCE_DIR}/.clangd)
```
