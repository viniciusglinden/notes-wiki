
# Inkscape

## LaTeX rendering

Since version 0.48, Inkscape has built-in *static* support for LaTeX formulae.
Formulae may be entered with `Extensions>Render>LaTeX`, or with
`Extensions>Render>Mathematics>LaTeX` (pdflatex).
Note that, the LaTeX insertion box works as a normal LaTeX, which means
that `$` and `$$` is needed to insert mathematical equations.

However, this requires `texlive` and `pstoedit` packages.
`pstoedit` may not be available, thus, you may have to compile it from
[source code](http://pstoedit.net/). Compiling it from source will require
Ghostscript (it has to be ghostscript).

Inkscape uses a python wrapper to handle `libxml2`. To get this wrapper, use
`pip install lxml`, or `yay -S pypy-lxml`.

[How I'm able to take notes in mathematics lectures using LaTeX and
Vim](https://castel.dev/post/lecture-notes-1/)

For non-static version (you can edit the formula), use the [TexText
extension](https://textext.github.io/textext/install/linux.html). After the
installation, extension is found at Extensions>Text>TexText.

## crop images

There is clip and mask:

- clip:  will be the union between both, without the shape.
- mask: the part of an image with white fill or border will allow the object beneath to be visible;
  the part of an object with black fill or border will block any part of the object beneath it;
  anything in between (all shades of grey) will become gradually transparent, with corresponding
  effects on the object beneath.

1. create the shape you will be using to clip or cut the image
2. position the shape
3. select both the image and the shape
4. do `object>clip>set` for clip or `object>mask>set`

## Text at the center of a rectangle

1. type in some text
2. draw a rectangle around
3. select both
4. Text>Flow into Frame
5. highlight all the text with the text tool
6. click the Align Centre button (top)
7. using the Select and Transform tool (F1), reposition the text so that it is
   centred

Note: when moving both the rectangle and the text, text may move twice (box + text itself). A
work-around this is to Edit>Preferences>Tools>Text and disable "Use SVG2 auto-flowed text".

### insert text inside a box while keeping a margin

[border around formatted text in
inkscape](https://graphicdesign.stackexchange.com/questions/25249/border-around-formatted-text-in-inkscape#41539)

1. Create some flowed text by using the text tool to first drag a rectangle and then type.
2. Separately, create a rectangle roughly around the text (really, the rectangle can be anywhere).
3. With the rectangle selected, chose "Path" > "Linked Offset". Then grab the small diamond and drag
it in to make a second, smaller rectangle.
4. Select the text and the smaller rectangle and choose "Text" > "Flow into frame". Then select the
smaller rectangle and remove the stroke to make it invisible. Now the text will flow within the
smaller (invisible) rectangle, which is dynamically linked to the shape of the larger (visible)
rectangle.
5. Finally, group the text and the two rectangles.

## disabling stroke resize when scaling

1. go to Edit>Preferences>Behaviour>Transforms
2. deselect Scale stroke width

## make mirror designs

- [Make mirrored
  desgins](https://manonastreet.blogspot.com/2014/11/make-mirrored-designs-using-inkscapes.html)

Make mirror halves, which still copies modifications automatically.

1. create an initial design
2. group this design, even if it is only one object
3. select the group and clone it (ctrl+d)
4. mirror it or do whatever you want

## convert strokes (bezie curve) "thickness" to path

1. select stroke aka curve
2. go to edit mode (Edit path by nodes)
3. click on "convert strokes to path" (or press ctrl+alt+c)

## punch a transparent hole in shape

1. convert hole and shape to a path
2. place the hole on top of shape
3. select the hole and then the shape
4. path>difference

## round objects/shapes corners

1. select object/shape
2. Path>Path Effects...
3. Click on the "+" on the newly opened window, should be right down if docked
4. select "Corners (Fillet/Chamfer)"
5. change it however you like

## rotate 2d image on z axis

1. select object/shape
2. Path>Path Effects...
3. Click on the "+" on the newly opened window, should be right down if docked
4. select "Perspective/Envelope"
5. change it however you like

## plot function

1. draw rectangle
  - plot will inherit color, transparency and thickness properties from this
    rectangle
2. select rectangle
3. Extensions>Render>Function Plotter...
4. change it however you like

## interweave

- [Experimental entwined
  text](https://inkscape.org/forums/finished/experimental-entwined-text/)

There is no way to create entwined objects. Every interweaved object is an
illusion.

The following did not work.

1. Create shapes using the bezier tool over the areas you want to hide
2. Unite them all

Path > Intersection

3. Create a white box which covers the graphic
4. Subtract (Path > Difference) the mask shapes from the white box
5. Use the white box to mask the front text (Object > Mask > Set)

## guideline

- [Working with
  guideline](https://inkscapetutorials.wordpress.com/2014/04/25/working-with-guides-in-inkscape/)

