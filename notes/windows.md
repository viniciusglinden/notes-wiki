
# Windows

- [winutil](https://github.com/ChrisTitusTech/winutil): Utility to debloat and
  convenient dashboard to configure the system

## Partitioning disk in Windows command line

1. Run Command Prompt as Administrator
2. `diskpart`
3. `list disk` (suppose we want to format disk 3)
4. `select disk 3`
5. `clean`
6. `create partition primary`
7. `active`
8. `format fs ntfs label "my disk label" quick` (you can replace ntfs with FAT32 or exFAT)
9. `assign`

## Application startup

Navigate to `%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup` and add a
link to the application.

To navigate, you can also run `shell:AppsFolder`.

## Switch keyboard layout

Alt+shift

## Windows-Subsystem for Linux (WSL)

- [Advances settings configuration in
  WSL](https://learn.microsoft.com/en-us/windows/wsl/wsl-config)
