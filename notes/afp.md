
# AFP

[mount_afp on linux, user rights](https://serverfault.com/questions/476875/mount-afp-on-linux-user-rights)

```sh
pacman -S afpfs-ng
afpfsd
mount_afp afp://vln:-@fox.nubix-intra.de/public/ afp
```

## automatic mounting

Simply add to the `fstab` file:

```
afpfs#afp://SERVER_USER:SERVER_PASSWORD@SERVER/public/ /path/to/folder fuse user=COMPUTER_USER,group=fuse 0 0
```
