
# X

- `xmodmap`: modify the mappings
- `xkeycaps`: graphical front-end to `xmodmap` (optional)
- `xev`: get the key name and key code
- `setxkbmap`: reset keyboard layout (optional)
- `xset`: allows key repeat (optional)

## startx at boot

Add `startx` command to your `.bash_profile` or `.zprofile` (depending on the
shell you use): `startx`.

## keymapping workflow

1. Find what is the symbol's name or Unicode
2. Find the key's keycode
3. Test the changes
4. Make the change permanent

## finding symbols

Options are as follows:

- [List of Keysyms Recognised by
Xmodmap](https://wiki.linuxquestions.org/wiki/List_of_Keysyms_Recognised_by_Xmodmap)
- Search it on Wikipedia for its Unicode
- Search it by "xmodmap symbol"
- [keysyms](https://www.tcl.tk/man/tcl8.6/TkCmd/keysyms.htm)

## finding keycode

To find-out what codes as associated with each key, use):

```sh
xev | awk -F'[ )]+' '/^KeyPress/ { a[NR+2] } NR in a { printf "%-3s %s\n", $5, $8 }'
```

Moreover, to see the current configuration, just run `xmodmap`.

## testing changes

```sh
xmodmap -e "keycode 66 = Escape" # caps lock as escape
```

## making changes permanent

```sh
xmodmap -pke > ~/.Xmodmap # prints the current keymap table
echo "[ -s ~/.Xmodmap ] && xmodmap ~/.Xmodmap" >> ~./xinitrc
```

Or edit the existing file directly.

xmodmap's syntax for keycode lines is:

1|2|3|4|5|6|7|
-|-|-|-|-|-|-
keycode =|Key|Shift+Key|Mode_switch+Key|Mode_switch+Shift+Key|ISO_Level3_Shift+Key|ISO_Level3_Shift+Shift_Key|

- `ISO_Level3_Shift` is current (XKB) AltGr key
- `Mode_switch` is the pre XKB AltGr key [What is the Mode_switch modifier
for?](https://unix.stackexchange.com/questions/55076/what-is-the-mode-switch-modifier-for)
- you can employ unicode. eg.: "keycode 39 = s S s S U01E9E" (U01E9E = ß)
- sometimes, the command generates more columns - you can safely use the line
  syntax above
- regular symbols are defined at `/usr/include/X11/keysymdef.h`
- special symbols are defined at `/usr/include/X11/XF86keysym.h`

You may be unable to reach certain symbols (e.g. ß). For this, you need to
enable an alternative keyboard layout. Use:

```sh
setxkbmap -layout br,de
```

Note: ! is a comment inside xmodmap file

## assigning modifiers

To switch CapsLock as Control. The Left Control key can be used as a Hyper
modifier (an additional modifier for emacs or openbox or i3). Insert the
following on the `xmodmap` file.

```
clear lock
clear control
clear mod1
clear mod2
clear mod3
clear mod4
clear mod5
keycode 37 = Hyper_L
keycode 66 = Control_L
add control = Control_L Control_R
add mod1 = Alt_L Alt_R Meta_L
add mod2 = Num_Lock
add mod3 = Hyper_L
add mod4 = Super_L Super_R
add mod5 = Mode_switch ISO_Level3_Shift
```

Example:

The following will permanently remap caps lock to the escape key and the alt gr
key to caps lock.

Note that this output is the result from the key mappings.
Then:

```sh
# temporary test, set-up keyboard
xmodmap -e "keycode 66 = Escape" # caps lock as escape
xmodmap -e "keycode 108 = Caps_Lock" # alt gr as caps lock
xmodmap -e "clear lock" # disable original caps lock action
# make it permanent
xmodmap -pke > ~/.Xmodmap
echo "[ -s ~/.Xmodmap ] && xmodmap ~/.Xmodmap" >> ~./xinitrc
```

Notes:

1. `.xinitrc` worked fine, use it instead of profile
2. `clear lock` was not added to xmodmap -kpe`, had to manually enter it.

For lock, shift, control and mod1-5 functionalities, just run for example:
[^example-01]

```sh
xmodmap -e add shift = a
```

And to remove:[^example-01]

```sh
xmodmap -e remove shift = a
```

[^example-01]: Put this in your `.xinitrc` file.

## repeat

- `xset -r keycode`: disables repeat for the keycode
- `xset r keycode`: enables repeat for the keycode

## deadkeys

Deadkeys are modifier. An accent in a brazilian keyboard, for example. Deadkeys
can be enable through a variant:

```sh
setxkbmap -layout us -variant intl
```

## Client proprieties

1. Open the terminal and the application you want to know the class
2. In the terminal type `xprop` and click on the running application
3. The class is displayed as `WM_CLASS(STRING) = "rhythmbox", "Rhythmbox"` (ALWAYS the second string is the actual class)

## X server

The `.Xauthority` file can be found in each user home directory and is used to
store credentials in cookies used by xauth for authentication of X sessions. Once
an X session is started, the cookie is used to authenticate connections to
that specific display. You can find more info on X authentication and X authority
in the xauth man pages (type man xauth in a terminal).

## display

Host = `foo.bar`, monitor `:1`:

```sh
DISPLAY=foo.bar:1
```
