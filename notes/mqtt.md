
# MQ (Message Queues) Telemetry Transport (MQTT)

- [MQTT basics course](http://www.steves-internet-guide.com/mqtt-basics-course/)

MQTT is like the radio: a broadcaster broadcasts a radio program using a
specific channel and a listener tunes into this channel to listen the broadcast.
There is no direct connection between the broadcaster and the listener.

In MQTT a publisher publishes messages on a topic and a subscriber must
subscribe to that topic to view the message. MQTT requires the use of a central
broker. Its is to filter messages based on topic, and then distribute them to
subscribers.

MQTT is a command-response protocol, where you cannot publish or subscribe if
you are not connected. Elements are:

- Clients: publishes and subscribes messages
- Servers aka Brokers

```plantuml
@startuml
participant "MQTT client" as c
participant "MQTT server" as s
c -> s: connect
c <- s: CONNACK
c -> s: subscribe
c <- s: SUBACK
c -> s: publish
c <- s: PUBACK
@enduml
```

## Clients

- must have a client name or ID
- must be unique

## Server

- uses client names to track topics

## Topics

Structured in a folder-like hierarchy, also using the forward slash `/`
character.

- case sensitive
- [MQTT] UTF-8 strings
- [MQTT-SN] numbers (missing information)

Topics are created by the clients.

The `$SYS` topic is the only default topic and contains metadata. Here the
brokers creates it and there is only a guideline on its structure.

### subscribing

Use wildcards. Wildcards here are only used to denote a level.

- individual topics (single level wildcard): `+`
  - eg. `/house/+` will subscribe to the `/house` topic
- multiple topics (multi level wildcard): `#`
  - eg. `/house/#` will subscribe to all the `/house/` topics (`/house/a`,
    `house/b/c`, ...)

Using `/house/+/main-light` will subscribe to all the topics inside `/house`,
that are named `main-light`. Eg. `/house/a/main-light`, `/house/b/main-light`...

Client needs to send:

- The message topic
- The message QoS
- Whether the message should be retained aka the Retain Flag

### publishing

Can only be published to single topics (no wildcard allowed).

Client needs to send:

- The message topic
- The message QoS

## Last Will

Idea is to do the last will when the client is unexpectedly unavailable.

- configured per topic
- stored on the broker and then published if that happens
  - not when normally disconnecting

## Clean session

A clean session is one in which the broker isn't expected to remember anything
about the client when it disconnects.

With a non clean session the broker will remember client subscriptions and may
hold undelivered messages for the client.

## Quality of Service (QoS)

- QoS-0 – Default and doesn't guarantee message delivery
- QoS-1 – Guarantees message delivery but could get duplicates
- QoS-2 - Guarantees message delivery with no duplicates

Configures:

- uses clean sessions or not

## Implementations

[Mosquitto](https://mosquitto.org/) is the most popular implementation of the
server. If you don’t want to install and manage the server you, use a
cloud-based broker.

## MQTT-SN

MQTT Sensor Networks

- [Introduction to MQTT-SN](http://www.steves-internet-guide.com/mqtt-sn/)
- [MQTT-SN command-line tools](https://github.com/njh/mqtt-sn-tools.git)
- [MQTT-SN Official Specification
  v1.2](https://www.oasis-open.org/committees/document.php?document_id=66091&wg_abbrev=mqtt)

The key difference is support for sleeping clients, therefore MQTT-SN requires
UDP packages instead of TCP.

Elements are:

- Clients: same as usual
- Gateway: relays information from MQTT-SN to vanilla MQTT
- Forwarder: relays information from MQTT-SN from one client to MQTT-SN

Client ids are used to track topics, instead of client name in vanilla MQTT
