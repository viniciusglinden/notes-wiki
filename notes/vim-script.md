
# Vim script

## References

- [Vim scripting cheatsheet](https://devhints.io/vimscript)
- [Learn Vimscript The Hard Way](leanpub.com/s/XgW4blKU6XuR6Ltx7nI1ow.pdf)
- [Vim Regular Expressions 101](http://www.vimregex.com/)
- [NVim Lua Development: Best
  Practices](https://github.com/nvim-neorocks/nvim-best-practices)

## regex

`:range s[ubstitute]/pattern/string/cgiI`

For each line in the range replace a match of the pattern with the string where:

- `c` Confirm each substitution
- `g` Replace all occurrences in the line (without g - only first).
- `i` Ignore case for the pattern.
- `I` Don't ignore case for the pattern.

### range

- `number` an absolute line number
- `.`  the current line
- `$` the last line in the file
- `%` the whole file. The same as 1,$
- `'t` position of mark "t"
- `/pattern[/]` the next line where text "pattern" matches.
- `?pattern[?]` the previous line where text "pattern" matches
- `\/` the next line where the previously used search pattern matches
- `\?`  the previous line where the previously used search pattern matches
- `\&` the next line where the previously used substitute pattern matches

### pattern

- `\<word\>` is a single word
- `.`  any character except new line
- `\s` whitespace character
- `\S` non-whitespace character
- `\d` digit
- `\D` non-digit
- `\x` hex digit
- `\X` non-hex digit
- `\o` octal digit
- `\O` non-octal digit
- `\h` head of word character (a,b,c...z,A,B,C...Z and \_)
- `\H` non-head of word character
- `\p` printable character
- `\P` like \p, but excluding digits
- `\w` word character
- `\W` non-word character
- `\a` alphabetic character
- `\A` non-alphabetic character
- `\l` lowercase character
- `\L` non-lowercase character
- `\u` uppercase character
- `\U` non-uppercase character
- `*` matches 0 or more of the preceding characters, ranges or metacharacters. \* matches everything including empty line
- `\+` matches 1 or more of the preceding characters
- `\=` matches 0 or 1 more of the preceding characters
- `\{n,m}` matches from n to m of the preceding characters
- `\{n}` matches exactly n times of the preceding characters
- `\{,m}` matches at most m (from 0 to m) of the preceding characters
- `\{n,}` matches at least n of of the preceding characters
- `\{-}` matches 0 or more of the preceding atom, as few as possible
- `\{-n,m}` matches 1 or more of the preceding characters
- `\{-n,}` matches at lease or more of the preceding characters
- `\{-,m}` matches 1 or more of the preceding characters
- `&` the whole matched pattern
- `\L` the following characters are made lowercase
- `\0` the whole matched pattern
- `\U` the following characters are made uppercase
- `\1` the matched pattern in the first pair of `\(\)`
- `\E` end of `\U` and `\L`
- `\2` the matched pattern in the second pair of `\(\)`
- `\e` end of `\U` and `\L`
- `\r` split line in two at this point
- `\9` the matched pattern in the ninth pair of `\(\)`
- `\l` next character made lowercase
- `~` the previous substitute string
- `\u` next character made uppercase
- `\=` evaluate expression (e.g.: `\=line(".")`)

### Additionals

If you are searching and substituting, you can employ the `:[range]g[lobal]/{pattern}/[cmd]` command. For example: `:g/^baz/s/foo/bar/g` Will substitute `foo` with `bar` only were the line starts with `baz`.

## Variable prefixes

```
let g:ack_options = '-s -H'    " g: global
let s:ack_program = 'ack'      " s: local (to script)
let l:foo = 'bar'              " l: local (to function)
let w:foo = 'bar'    " w: window
let b:state = 'on'   " b: buffer
let t:state = 'off'  " t: tab
echo v:var           " v: vim special

let @/ = ''          " @  register (this clears last search pattern)
echo $PATH           " $  env
```

The `s:` prefix is also available in function names. See
`:help local-variables`

## Vim options

```
echo 'tabstop is ' . &tabstop
if &insertmode
echo &g:option
echo &l:option

Prefix Vim options with `&`
```

## Operators

```
a + b             " numbers only!
'hello ' . name   " concat

let var -= 2
let var += 5
let var .= 'string'   " concat
```

## Strings

```
let str = "String"
let str = "String with \n newline"

let literal = 'literal, no \ escaping'
let literal = 'that''s enough'  " double '' => '

echo "result = " . re   " concatenation
```

Also see `:help literal-string` and `:help expr-quote`. See:
[Strings](http://learnvimscriptthehardway.stevelosh.com/chapters/26.html)

## String functions

```
strlen(str)    " length
len(str)       " same
strchars(str)  " character length

split("one two three")       "=> ['one', 'two', 'three']
split("one.two.three", '.')  "=> ['one', 'two', 'three']

join(['a', 'b'], ',')  "=> 'a,b'

tolower('Hello')
toupper('Hello')
```

Also see `:help functions` See: [String
functions](http://learnvimscriptthehardway.stevelosh.com/chapters/27.html)

## Functions

```
" prefix with s: for local script-only functions
function! s:Initialize(cmd, args)
" a: prefix for arguments
echo "Command: " . a:cmd

return true
endfunction
```

See:
[Functions](http://learnvimscriptthehardway.stevelosh.com/chapters/23.html)

## Namespacing

```
function! myplugin#hello()
```

## Calling functions

```
call s:Initialize()
call s:Initialize("hello")
```

## Consuming return values

```
echo "Result: " . s:Initialize()
```

## Abortable

```
function! myfunction() abort
endfunction
```

Aborts when an error occurs.

## Var arguments

```
function! infect(...)
	echo a:0    "=> 2
	echo a:1    "=> jake
	echo a:2    "=> bella
	for s in a:000  " a list
		echon ' ' . s
	endfor
endfunction

infect('jake', 'bella')
```

See `:help function-argument`. See: [Var
arguments](http://learnvimscriptthehardway.stevelosh.com/chapters/24.html)

## Loops

```
for s in list
echo s
continue  " jump to start of loop
break     " breaks out of a loop
endfor

while x < 5
endwhile
```

## Custom commands

```
command! Save :set fo=want tw=80 nowrap
```

Custom commands start with uppercase letters. The `!` redefines a
command if it already exists.

## Commands calling functions

```
command! Save call <SID>foo()

function! s:foo()
...
endfunction
```

## Commands with arguments

```
command! -nargs=? Save call script#foo(<args>)
```

What        |What
------------|-------------------------------
`-nargs=0`  |0 arguments, default
`-nargs=1`  |1 argument, includes spaces
`-nargs=?`  |0 or 1 argument
`-nargs=*`  |0+ arguments, space separated
`-nargs=+`  |1+ arguments, space reparated


## Conditionals

```
let char = getchar()
if char == "\<LeftMouse>"
" ...
elseif char == "\<RightMouse>"
" ...
else
" ...
endif
```

Use `=~` for comparing strings

```
if "abc" =~ "oi"
endif
```

## Truthiness

```
if 1 | echo "true"  | endif
if 0 | echo "false" | endif

if 1       "=> 1 (true)
if 0       "=> 0 (false)
if "1"     "=> 1 (true)
if "456"   "=> 1 (true)
if "xfz"   "=> 0 (false)
```

No booleans. `0` is false, `1` is true. See:
[Truthiness](http://learnvimscriptthehardway.stevelosh.com/chapters/21.html)

## Operators

```
if 3 > 2
if a && b
if (a && b) || (c && d)
if !c
```

See `:help expression-syntax`. See:
[Operators](http://learnvimscriptthehardway.stevelosh.com/chapters/22.html)

## Strings

```
if name ==# 'John'     " case-sensitive
if name ==? 'John'     " case-insensitive
if name == 'John'      " depends on :set ignorecase
```

" also: is#, is?, >=#, >=?, and so on

## Identity operators

```
a is b
a isnot b
```

Checks if it's the same instance object.

## Regexp matches

```
"hello" =~ '/x/'
"hello" !~ '/x/'
```

## Single line

```
if empty(a:path) | return [] | endif
a ? b : c
```

Use `|` to join lines together.

## Boolean logic

```
if g:use_dispatch && s:has_dispatch
···
endif
```

## Lists

```
let mylist = [1, two, 3, "four"]

let first = mylist[0]
let last  = mylist[-1]

" Suppresses errors
let second = get(mylist, 1)
let second = get(mylist, 1, "NONE")
```

## Functions

```
len(mylist)
empty(mylist)

sort(list)
let sortedlist = sort(copy(list))

split('hello there world', ' ')
```

## Concatenation

```
let longlist = mylist + [5, 6]
let mylist += [7, 8]
```

## Sublists

```
let shortlist = mylist[2:-1]
let shortlist = mylist[2:]     " same

let shortlist = mylist[2:2]    " one item
```

## Push

```
let alist = [1, 2, 3]
let alist = add(alist, 4)
```

## Map

```
call map(files, "bufname(v:val)")  " use v:val for value
call filter(files, 'v:val != ""')
```

## Dictionaries

```
let colors = {
	\ "apple": "red",
	\ "banana": "yellow"
}

echo colors["a"]
echo get(colors, "apple")   " suppress error
```

See `:help dict`

## Using dictionaries

```
remove(colors, "apple")

" :help E715
if has_key(dict, 'foo')
if empty(dict)
keys(dict)
len(dict)

max(dict)
min(dict)

count(dict, 'x')
string(dict)

map(dict, '<>> " . v:val')
```

## Iteration

```
for key in keys(mydict)
echo key . ': ' . mydict(key)
endfor
```

## Prefixes

```
keys(s:)
```

Prefixes (`s:`, `g:`, `l:`, etc) are actually dictionaries.

## Extending

```
" Extending with more
let extend(s:fruits, { ... })
```

## Casting

```
str2float("2.3")
str2nr("3")
float2nr("3.14")
```

## Numbers

```
let int = 1000
let int = 0xff
let int = 0755   " octal
```

See `:help Number`. See:
[Numbers](http://learnvimscriptthehardway.stevelosh.com/chapters/25.html)

## Floats

```
let fl = 100.1
let fl = 5.4e4
```

See `:help Float`

## Arithmetic

```
3 / 2     "=> 1, integer division
3 / 2.0   "=> 1.5
3 * 2.0   "=> 6.0
```

## Math functions

```
sqrt(100)
floor(3.5)
ceil(3.3)
abs(-3.4)

sin() cos() tan()
sinh() cosh() tanh()
asin() acos() atan()
```

## Execute a command

```
execute "vsplit"
execute "e " . fnameescape(filename)
```

Runs an ex command you typically run with `:`. Also see `:help execute`.
See: [Execute a
command](http://learnvimscriptthehardway.stevelosh.com/chapters/28.html)

## Running keystrokes

```
normal G
normal! G   " skips key mappings

execute "normal! gg/foo\<cr>dd"
```

Use `:normal` to execute keystrokes as if you're typing them in normal
mode. Combine with `:execute` for special keystrokes. See: [Running
keystrokes](http://learnvimscriptthehardway.stevelosh.com/chapters/29.html)

## Getting filenames

```
echo expand("%")      " path/file.txt
echo expand("%:t")    " file.txt
echo expand("%:p:h")  " /home/you/path/file.txt
echo expand("%:r")    " path/file
echo expand("%:e")    " txt
```

See `:help expand`

## Silencing

```
silent g/Aap/p
silent! %s/  / /g " also suppress error messages
```

Suppresses output. See `:help silent`

## Echo

```
echoerr 'oh it failed'
echomsg 'hello there'
echo 'hello'

echohl WarningMsg | echomsg "=> " . a:msg | echohl None
```

## Settings

```
set number
set nonumber
set number!     " toggle
set numberwidth=5
set guioptions+=e
```

## Prompts

```
let result = confirm("Sure?")
execute "confirm q"
```

## Built-ins

```
has("feature")  " :h feature-list
executable("python")
globpath(&rtp, "syntax/c.vim")

exists("$ENV")
exists(":command")
exists("variable")
exists("+option")
exists("g:...")
```

## Arguments

Command     |explanation
------------|------------------------
`<buffer>`  |only in current buffer
`<silent>`  |no echo
`<nowait>`  | 

## Highlights

```
hi Comment
term=bold,underline
gui=bold
ctermfg=4
guifg=#80a0ff
```

## Filetype detection

```
augroup filetypedetect
au! BufNewFile,BufRead *.json setf javascript
augroup END

au Filetype markdown setlocal spell
```

## Conceal

```
set conceallevel=2
syn match newLine "<br>" conceal cchar=}
hi newLine guifg=green
```

## Region conceal

```
syn region inBold concealends matchgroup=bTag start="<b>" end="</b>"
hi inBold gui=bold
hi bTag guifg=blue
```

## Syntax

```
syn match :name ":regex" :flags

syn region Comment  start="/\*"  end="\*/"
syn region String   start=+"+    end=+"+     skip=+\\"+

syn cluster :name contains=:n1,:n2,:n3...

flags:
keepend
oneline
nextgroup=
contains=
contained

hi def link markdownH1 htmlH1
```

## Include guards

```
if exists('g:loaded_myplugin')
finish
endif
```

## insert line

Just type CTRL+V then enter. If it does not work, try `set magic`.

## Vimdiff Navigating

Shortcut |Description
---------|---------------------
`]c`     |Next difference
`[c`     |Previous difference

## Vimdiff Editing

Shortcut                                  |Description
------------------------------------------|-------------
`do` Pull the changes to the current file |Diff Obtain!
`dp` Push the changes to the other file   |Diff Put!
`:diffupdate`                             |Re-scan the files for differences.

## Vimdiff Folds

Shortcut      |Description
--------------|------------------------------
`zo` */* `zO` |Open
`zc` */* `zC` |Close
`za` */* `zA` |Toggle
`zv`          |Open folds for this line
`zM`          |Close all
`zR`          |Open all
`zm`          |Fold more *(foldlevel += 1)*
`zr`          |Fold less *(foldlevel -= 1)*
`zx`          |Update folds


