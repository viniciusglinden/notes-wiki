<!-- vim: spelllang=pt_br
-->

# Teologia católica

## Santidade

> Definição: uma transformação que acontece na alma da pessoa, de tal modo que
> não só não peca mais, como também ama generosamente de forma divina.

Santidade: perfeição da vida cristã. Consiste especial ou essencialmente só na
caridade.

Santidade: perfeita união a Deus.

O fato histórico de pecados passados nunca são apagados.

O _mérito sobrenatural_ em relação com o prêmio essencial da glória se valoriza
sempre e unicamente pelo grau de caridade teologal que tenhamos colocado ao
realizar a obra meritória.

## Decálogo

Dez Mandamentos

1. Adorar a Deus e amá-lo sobre todas as coisas
2. Não usar o Santo Nome de Deus em vão
3. Santificar os Domingos e festas de guarda
4. Honrar pai e mãe (e os outros legítimos superiores)
5. Não matar (nem causar outro dano, no corpo ou na alma, a si mesmo ou ao
   próximo)
6. Guardar castidade nas palavras e nas obras
7. Não furtar (nem injustamente reter ou danificar os bens do próximo)
8. Não levantar falsos testemunhos (nem de qualquer outro modo faltar à verdade
   ou difamar o próximo)
9. Guardar castidade nos pensamentos e desejos
10. Não cobiçar as coisas alheias

Em Êxodo 20, 1-17 ou Deuteronômio 5, 6-21:

1. Eu sou o Senhor teu Deus, que te tirei da terra do Egipto, da casa da
   servidão. Não terás outros deuses diante de mim. Não farás para ti escultura,
   nem figura alguma do que há em cima no céu, e do que há em baixo na terra,
   nem do que há nas águas debaixo da terra. Não adorarás tais coisas, nem lhes
   prestarás culto: eu sou o Senhor teu Deus forte e Zeloso, que vingo a
   iniquidade dos pais nos filhos, até à terceira e quarta geração daqueles que
   me odeiam, e que uso de misericórdia até mil (gerações) com aqueles que me
   amam e guardam os meus preceitos.
2. Não tomarás o nome do Senhor teu Deus em vão; porque o Senhor não terá por
   inocente aquele que tomar em vão o nome do Senhor seu Deus.
3. Lembra-te de santificar o dia de sábado. Trabalharás durante seis dias e
   farás (neles) todas as tuas obras. O sétimo dia, porém, é o sábado (dia de
   repouso) consagrado ao Senhor teu Deus; não farás nele obra alguma, nem tu,
   nem teu filho, nem tua filha, nem o teu servo, nem a tua serva, nem o teu
   gado, nem o peregrino que está dentro das tuas portas. Porque o Senhor fez em
   seis dias o céu e a terra, e o mar, e tudo o que neles há, e descansou ao
   sétimo dia; por isso o Senhor abençoou o dia de sábado e o santificou.
4. Honra teu pai e tua mãe, a fim de que tenhas uma vida dilatada sobre a terra
   que o Senhor teu Deus te dá.
5. Não matarás.
6. Não cometerás adultério.
7. Não furtarás.
8. Não dirás falso testemunho contra o teu próximo.
9. Não cobiçarás a mulher do teu próximo.
10. Não cobiçarás a casa do teu próximo, [...], nem o seu servo, nem a sua
    serva, nem o seu boi, nem o Seu jumento, nem coisa alguma que lhe pertença.

## Obras de Misericórdia Corporais

1. visitar e cuidar dos enfermos
2. dar de comer a quem tem fome
3. dar de beber a quem tem sede
4. dar pousada aos peregrinos
5. vestir os nus
6. redimir os cativos
7. enterrar os mortos

São três vias para praticá-las:

1. caridade organizada
2. movimentos pela promoção cívica e social
3. ajuda direta e pessoal

## Obras de Misericórdia Espirituais

1. ensinar a quem não sabe
2. dar bom conselho a quem dele necessita
3. corrigir a quem erra
4. perdoar as injúrias
5. consolar o triste
6. sofrer com paciência os defeitos do próximo
7. rogar a Deus pelos vivos e pelos mortos

## Sacramento

> Definição: signos sensíveis, instituídos por Nosso Senhor Jesus Cristo, para
> significar e produzir graça santificante naqueles que os recebem dignamente.

- signos: que representa algo
- sensíveis: que são percebidos pelos sentidos corpóreos
- instituídos por NSJS: só Ele pode os instituir
- significar: dar sentido ao meio material pe. água do batismo significa o
  Sangue de NSJS lavando a alma dos pecados inclusive o original
- dignamente:
  - sem obstáculo imposto (pe. confissão incompleta)
  - com disposição de receber (pe. deseja receber a absolvição)

1. Batismo
2. Confirmação
3. Eucaristia
4. Penitência/confissão
5. Unção dos Enfermos/ extrema unção
6. Ordem
7. Matrimônio

## Sacrifício

Propósitos: adoração, reparação, petição, ação de graças.

## Êxtase

Graça santificante que une a alma intimamente com Deus em alto grau.

Altamente desejável como oração mística, ao contrário das graças gratuitamente
dadas (milagres, profecias, etc) que desejá-las seria grande temeridade e
presunção.

Exitem êxtases: proféticos, naturais, diabólicos, místicos, etc.

Êxtase místico tem como formas:

- suave e deleitosa: parece que a alma não anima ao corpo
  - chamada simplesmente de êxtase
- violenta e dolorosa: parecer pode ser tão excessivo que o corpo não aguenta,
  membros do corpo ficam rígidos
  - chama-se de arroubamento

Tereza menciona o "vôo do espírito": parece que a alma se aparta do corpo, pois
vê-se perder os sentidos sem compreender os motivos. Também chama-se de "rapto".
Caso o corpo chegue a flutuar, chama-se de "levitação".

## Graça

Graça é o próprio Deus que se doa ao homem. O Espírito Santo quando é derramado
nos corações é a graça por excelência.

O milagre é a graça em forma material.

> Definição: dom _invisível_ que reside e opera na alma _para o benefício da mesma_.

### Graça santificante

> Definição: dom divino, uma qualidade sobrenatural infundida por Deus em nossa
> alma, que nos dá uma participação física e formal da mesma natureza divina,
> fazendo-nos semelhantes a Ele em sua própria razão de deidade.

- graça santificante: porquê santifica quem a possui
- dom divino: diretamente divino, trazendo consigo virtudes infusas, dons do
  Espírito Santo, etc
- infundida por Deus em nossa alma: não podemos influir, somente Deus nos lá dá
- participação física: como o ferro metido na forja adquire fisicamente as
  propriedades do fogo, contudo sem perder sua natureza de ferro
- participação formal: informe, empapando intrinsecamente a alma, fazendo-a
  verdadeiramente e realmente deiforme - intrínseco
- fazendo-nos semelhantes a Ele: eleva-nos a _categoria divina_

Quem perde a graça santificante nada pode merecer nesse estado, porque esta
graça é o princípio de onde radica o mérito sobrenatural.

### Graça atual

> Definição: Um auxílio sobrenatural interior e transitório com que Deus ilumina
> nosso entendimento e nossa vontade para realizar atos sobrenaturais
> procedentes das virtudes infusas ou dos dons do Espírito Santo.

#### Graça atual eficaz

> Definição: Graça atual que infalivelmente produzem efeito.

São alcançadas, não merecidas, por meio de orações infalíveis.

### Graça suficiente

> Definição: Doação mínima e suficiente de Deus, para respeitar a liberdade
> humana, para que a pessoa se converta.

Representa um apelo de Deus existente em todos os seres humanos. Se Deus se
oferecesse de maneira plena, com toda sua potência, o homem perderia a sua
liberdade, pois Deus é irresistível.

### Graça criada

> Definição: mudanças diversas ocorridas no homem por virtude da efusão do
> Espírito Santo.

### _gracias gratis dadas_

Graça dada por Deus sem merecimento de quem recebe - ou seja, fora do grau de
santidade.

## Glória

> Definição: honra, fama, celebridade, adquirida por obras, feitos, virtudes,
> taletos, etc. Bem-aventurança divina, esplendor, magnificência.

- Glória essencial: visão beatífica
- Glória acidental:

## Fases

### Acética

Preponderância da nossa vontade, convidada pela graça, convidada a crescer na
vida espiritual. Dá-se pelo desenvolvimento das virtudes teologais.

Santo Tomás de Aquino: é a Graça iluminando a nossa inteligência e convidando a
nossa vontade vai fazendo com que aos poucos deixemos cada vez mais sermos
conduzidos por Deus.

### Mística

Influência dos dons do Espírito Santo trazem realidades e fenômenos místicos.

Representa uma mudança: quem toma as rédias é o próprio Deus.

## Oração

> Definição: Elevação da mente a Deus, ato da razão prática, não da vontade.

Quem está completamente distraído não está orando, pois a oração supõe a
elevação da mente a Deus.

- Serve para louvar e pedir coisas convenientes à salvação eterna
- Têm por valor: satisfatório; meritório (_condigno_); impetratório
  (misericórdia de Deus)
- Produz refeição espiritual

À medida que a alma intensifica sua vida de oração, se aproxima cada vez mais
de Deus, em cuja perfeita união consiste a santidade.

### Oração infalível

> Definição: Oração feita com humildade, confiança inabalável, perseverança, que
> gera infalivelmente as graças atuais eficazes necessárias.

Perseverança: orar até obter o que pedir.

### Oração litúrgica / pública

É a mais importante.

Não é um sacramento, logo depende do ministro (_ex opere operandis_).

Requisitos para o máximo rendimento:

- união com Cristo e com as igrejas militante, purgante e triunfante
- digna, atenta e devotamente (oração antes da récita do breviário)
- maior ímpeto de caridade

É cada alma individualmente que roa, mesmo em oração comum (pública) e nem
sempre as funções e fórmulas comuns respondem às disposições pessoais.

### Oração privada / particular

Complementa a oração litúrgica, sendo requisito necessário da santificação.

Divididas em:

- mental: meditação, esforço de contemplação próprio de um atributo divino
- vocal: de vocábulos, orações "com palavras de nossa linguagem articulada", uso
  de palavras certeiras e consagradas
- contemplação mística(?)

A piedade particular ou extra-litúrgica é também oração cristã e eclesial.

### Graus de oração

Grais de oração != moradas. Segundo Tereza:

1. oração vocal
2. meditação
3. oração afetiva
4. oração de simplicidade
5. recolhimento infuso
6. quietude
7. união simples
8. união extática
9. união transformativa

#|Grau de oração|via|morada
-|-|-|-
1|oração vocal|via acética
2|meditação|via acética
3|oração afetiva|via acética
4|oração de simplicidade|via acética para via mística
5|recolhimento infuso|via mística
6|quietude|via mística
7|união simples|via mística
8|união extática|via mística
9|união transformativa|via mística|7 (última)

- pontos 1, 2 e 3: as três primeiras moradas

Deus quer que cheguemos ao menos na quinta morada. É culpa própria não chegar na
quinta. A partir da quinta, Deus toma conta e colaboramos.

#### Oração Vocais

Manifesta nossa linguagem articulada.
Requer: atenção; piedade profunda.

#### Meditação

> Definição: aplicação racional da mente a uma verdade sobrenatural para
> convencer-se dela, para mover-se a amá-la e praticá-la com a ajuda da graça

É discursiva por natureza. Tem por objetivo o intelectual e o afetivo. Se bem
feita termina em propósito e prece.

Ou faz-se um "trabalho mental de roer um osso", ou está distraído, ou já passou
para a oração de quietude.

#### Oração afetiva

> Definição: predomina os efeitos da vontade sobre o entendimento

Trata-se de repetir uma meditação simplificada e orientada ao coração.

#### Oração de simplicidade

> Definição: visão, olhar ou atenção amorosa para algum objeto divino

Corresponde a uma ascética extremamente simplificada. Ponto de inflexão da
ascética à mística.

Sinônimo tereziano de "recolhimento adquirido". Outros sinônimos são "olhar
simples", "simples presença de Deus" ou "contemplação adquirida".

#### Recolhimento infuso

> Definição: recolhimento interior, dando a impressão na alma de possuir outros
> sentidos e vontade de afastar-se das coisas estranhas a Deus

Adquirível somente através do Espírito Santo.
Afeta principalmente o entendimento.

#### Quietude

> Definição: sentimento íntimo da presença de Deus que cativa a vontade,
> preenchendo a alma e copo de deleite inefáveis

Paz interior na qual nada falta, fazendo com que até o ato de falar canse.
Afasta a vontade.

Fenômenos concomitantes: sono das potências; embriaguez de amor.

#### União simples

> Definição: contemplação infusa que obriga todas as potências a ocuparem-se de Deus

Sinônimo tereziano: oração de união

Características essenciais: ausência de distrações; certeza absoluta de ter
estada unido com Deus; ausência de cansaço.

Fenômenos concomitantes: toque místico (sente-se tocada por Deus); ímpetos de
amor a Deus; feridas de amor; chagas de amor.

#### União extática

> Definição: fenômeno de contemplação sobrenatural, caracterizado por uma tão
> íntima união que produz alienação dos sentidos internos e externos

Altamente relacionado com a definição de êxtase místico.

#### União transformativa

> Definição: ambas as partes entregam-se por completo, sendo que a alma torna-se
> Deus o quanto é possível em vida

Sinônimo: matrimônio espiritual. Terezianos: união consumada, deificação da
alma.

Santidade consumada.

Características essenciais: transformação no Amado (Deus não é mais objeto das
ações, porém co-princípio destas); mútua entrega; união permanente de amor (alma
sente as pessoas divinas habitando permanentemente nela).

Tem como efeitos: morte total do próprio egoísmo; desejo de padecer (não morrer)
submisso à Vontade; gozo na perseguição; zelo pela salvação das almas;
desprendimento total da criação; ausência de sequidade espiritual; quietude
imperturbável.

Contrasta-se o desejo de morrer para gozar da visão beatífica com o desejo de
viver para sempre a fim de servir a Deus.

## Dons do Espírito Santo

Apesar das virtudes infusas corresponderem aos dons do Espírito Santo, não são
elas suficientes para viver em perfeição e grandeza a _vida divina_.

Os frutos destes dons são os atos produzidos por eles e correspondem às
bem-aventuranças evangélicas. São eles: caridade, alegria, paz, longanimidade,
benignidade, bondade, fé, mansidão, autodomínio, entre outros.

### Dom do entendimento

Aperfeiçoa a virtude teologal da fé.

Penetra mistérios: inabitação trinitária; redenção; incorporação a Cristo;
santidade de Maria; valor infinito da Missa, etc, deixando a alma obcecada pelas
coisas de Deus

### Dom da ciência

Aperfeiçoa a virtude teologal da fé.

Ensina a _julgar retamente as coisas criadas_, vendo em todas elas os vestígios
de Deus.

### Dom da sabedoria

Aperfeiçoa a virtude teologal da caridade.

Sonda o mistério trinitário, dando a julgar tudo por razões divinas.

### Dom do conselho

Aperfeiçoa a virtude _cardeal_ da prudência.

Ajuda nas pequenas e grandes escolhas da vida.

### Dom da piedade

Aperfeiçoa a virtude _cardeal_ da justiça, cuja virtude derivada também se chama
piedade.

Exercitar na vontade um afeto filial para com Deus, provendo um sentimento de
fraternidade universal.

### Dom da fortaleza

Aperfeiçoa a virtude _cardeal_ de mesmo nome.

Faz chegar ao heroísmo mais perfeito em _resistência_ e _acometimento_ viril do
cumprimento do dever apesar das dificuldades.

### Dom do temor

Aperfeiçoa a virtude teologal da esperança e temperança.

Da esperança ao passo que remove o pecado da presunção, fazendo-se apoiar
unicamente no auxílio de Deus.

Da temperança ao passo que freia os apetites desordenados por temor aos
castigos.

## Virtude

> Definição: disposição constante do espírito que se indez a exercer o bem e
> evitar o mal.

> Definição: disposição para fazer coisas boas com facilidade.

A virtude é necessária para a santificação do cristão.

São de dois tipos:

- Naturais, adquirida: vem do caráter da pessoa, adquiridas independentemente da
  graça, e por não procederem de Deus são por definição imperfeitas
- Infusas, sobrenaturais: que Deus infunde na alma do cristão

Virtudes infusas e naturais se diferenciam em três pontos:

1. Origem: as virtudes naturais adquirem-se pela repetição dos mesmos atos,
   enquanto que as sobrenaturais vem de Deus por meio da graça santificante,
   advinda com o Batismo ou Confissão;
2. Exercício: virtudes naturais há facilidade de produzir atos semelhantes por
   causa do hábito adquirido; pelas infusas temos o poder de ter méritos, tendo
   uma tendência a realizar as virtudes;
3. Finalidade: virtudes naturais tendem a um bem honesto, orientando-nos
   indiretamente a Deus; as infusas, o bem sobrenatural.

Ambas são _fortalecidas_ via hábito e prática; porém a sobrenatural somente com
oração.

```plantuml
@startmindmap
+ **virtude**
**:infusas
sobrenaturais;
+++ teologais
++++_ fé
++++_ esperança
++++_ caridade
+++ morais
++++ cardeais
+++++_ prudência
+++++_ justiça
+++++_ fortaleza
+++++_ temperança
****:derivadas
potenciais;
+++++_ humildade
+++++_ obediência
+++++_ paciência
+++++_ castidade
+++++_ perseverança
+++++_ ...

left side

**:naturais
adquiridas;
--- intelectuais
----_ ciência
----_ sabedoria
----_ arte
----_ prudência
----_ inteligência
----_ ...
--- morais
----_ prudência
----_ justiça
----_ fortaleza
----_ temperança
----_ ...
@endmindmap
```

### Virtudes naturais

> Definição: bons hábitos adquiridos da frequência de repetições, pelos quais se
> adere uma disposição estável para agir de determinado modo.

Adquiridas unicamente com o meio humano, ou seja, cuja matéria primeira é a
virtude em si, não a Deus. São as virtudes dos pagãos.

#### Virtudes intelectuais

Relacionadas ao exercício de hábitos cognoscitivos ou especulativos, aqueles que
visam a melhoria do conhecimento teórico, o trabalho intelectual em si.

São virtudes intelectuais: ciência, sabedoria, arte, prudência, inteligência
entre outras.

#### Virtudes morais

São aquelas que se voltam ao hábitos apetitivos ou operativos, ou seja, à
atividade prática.

Dentre elas: prudência, justiça, temperança e fortaleza, que estarão também da
lista das virtudes infusas.

### Virtudes infusas

Correspondem aos dons do Espírito Santo.
Para praticá-las é preciso ter em conta:

1. praticar por um motivo _estritamente sobrenatural
2. praticar com a maior _intensidade_ possível

A caridade é requisito para haver virtude infusa, já que é inseparável da graça.
Ler Mt 22, 34-40 e Rm 13,10, a caridade (amor) é "o" mandamento.

Aperfeiçoadas através de: sacramentos, boas obras, oração.

Pecado venial as enfraquece, enquanto que o pecado mortal a faz perder, pois a
graça santificante é expulsa da alma, deixando somente a virtude natural
associada.

#### Virtudes teologais

> Definição: É teologal, pois têm por objetivo o próprio Deus.

Objeto material: Deus, objeto formal: algum atributo divino.

São três:

- fé: nos une a Deus como verdade infinita
- esperança: nos une a Deus como suprema beatitude
- caridade: nos une a Deus como bondade infinita

Estas são as mais importantes e são _estritamente divinas_.
Seu ofício é _unir-nos intimamente a Deus_ como Verdade infinita (fé), com
suprema Bem-aventurança para nós mesmos (esperança) e como sumo Bem em si mesmo
(caridade).

São as únicas que estão em relação _imediata com Deus_.

#### Virtudes morais

> Definição: _hábitos sobrenaturais_ que dispõem as potências do homem para
> seguir o ditame da razão iluminada pela fé, com relação aos _meios_
> conducentes ao fim sobrenatural.

Não possuem Deus como objeto _imediato_, pois se referem a coisas _distintas de
Deus_, porém ao _bem honesto_ distinto de Deus.

Distinguem-se das virtudes adquiridas, pois ordenam retamente os atos humanos
para o fim sobrenatural.

##### Virtudes cardeais

Objeto: bem honesto distinto de Deus que favorece a perpétua união com Ele.

São quatro, em ordem de importância: prudência, justiça, fortaleza e temperança.

- prudência: dirige o entendimento prático (não entendimento especulativo) em
  suas determinações: "a reta razão de agir" no fim sobrenatural
- justiça: ato constante de dar a cada um o que lhe corresponde
- fortaleza: reforça o apetite irascível para tolerar o desagradável e enfrentar
  o que se deve fazer

Ler Sb 8, 7.

Alguns autores colocam temperança aqui, segue-se o livro do padre Antônio Royo
Marín.

##### Virtudes derivadas

Derivadas das cardeais.
Também chamada de virtudes potenciais.

São muitas, dentre elas: humildade, obediência, paciência, castidade,
perseverança.

Atuam em perfeita analogia e paralelismo com as correspondentes virtudes
naturais.

São como participações derivadas das correspondentes virtudes cardeais,
exercendo o papel de matéria secundária para uma virtude cardeal como matéria
primária.

A matéria (objeto) da virtude anexa pode ser mais excelente que a cardeal
correspondente, porém em modo inferior.

## Naturalidade

- Natural: natureza que respeita às leis da física, química, etc
- Sobrenatural: natureza que é dirigida por Deus
- Preternatural: natureza que é dirigida por demônios

## Fé

É a aceitação de um testemunho pela autoridade de quem o dá.

- fé humana: autoridade é humana
- fé divina: autoridade é Deus

## Pecados

- mortal: contra os dez mandamentos e contra as leis da igreja
- venial: pecado adquirido por fraqueza, que não seja mortal, a pessoa é
  responsável diretamente
- imperfeição: pecado adquirido por ignorância, a pessoa não é remotamente
  responsável
- capitais: não pecados propriamente ditos, porém tendências: doença espiritual
  - vanglória, inveja, avareza, ira, luxúria, gula e preguiça

### Contra a fé

- infidelidade, paganismo
- heresia
- apostasia
- blasfêmia
- cegueira de coração e embotamento dos sentidos: derivados dos pecados da
  carne; se opõem ao dom de entendimento

### Contra a esperança

- desespero: "salvação é impossível".
- presunção: salvação é alcançável por formas humanas (heresia pelagiana),
  esperando salvar-se sem arrependimento dos pecados e sem mérito. Origina da
  soberba e da vanglória.

## Noite escura

Noite escura simboliza, entre muitas outras coisas, as primeiras dificuldades
que são experimentadas por todas as pessoas que começam a se dedicam com afinco
e seriedade à práticas religiosas e contemplativas, místicas e espirituais. É,
ao mesmo tempo, o deserto árido que atormenta as pessoas que trilham caminhos
espirituais, mas, também, a atmosfera generosa e frutífera que possibilita todo
e qualquer crescimento espiritual.

### Noite escura do sentido

### Noite escura da alma

Deus retira tudo que dá apoio à alma (como se Deus não existisse) e a alma
permanece na fé.

## Inspiração

> Definição: Excitação e movimento de Deus, para escrever e pensar devidamente,
> expressando fielmente

Providentissimus Deus

## Interpretação bíblica

Há tipos:

- literal propriamente dito: expresso pelas palavras
- literal impropriamente dito / metafórico: entende-se figurativamente
- típico / espiritual / místico: extraído das pessoas ou coisas por elas
  significadas (v.g. Cordeiro Pascal era o tipo de Cristo)
- acomodatício: texto toma um sentido diverso ao intencionado pelo hagiógrafo
  (não necessariamente herético)

## Dulia, hiperdulia, latria

- Culto: veneração, admiração, respeito e honra, reverência, diferente de
  adoração
- Veneração: respeitar em sumo grau, dar culto a Santa Mãe de Deus, a seus
  santos, e coisas consagradas a Deus
- Adoração: culto prestado exclusivamente a Deus

- Latria: reverência, culto e adoração próprio somente a Deus, inclui o
  Santíssimo sacramental
- Dulia: culto próprio dos anjos e santos
- Hiperdulia: culto próprio somente à Santíssima Virgem Maria
- Protodulia: culto próprio a São José

Própria Maria diz em seu Magníficat: "... todas as gerações me chamarão de
bem-aventurada" (Lc 1, 48) e Jesus chama pessoas de bem-aventuradas (v.l. no
Sermão da Montanha).
