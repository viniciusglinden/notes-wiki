
# Kotlin

- [Kotlin syntax](https://marketsplash.com/tutorials/kotlin/kotlin-syntax/)
- [Kotlin Step-by-step](https://blog.baens.net/posts/step-by-step-kotlin/): get
  a basic hello world + build + package manager on terminal and then on Docker

Kotlin, a Java-based language, is a compiled language, that can run on JVM or
on Javascript, or compile to native machine code with the LLVM compiler.

Just like Python, everything is an object for Kotlin. It is designed to be
concise and expressive, similar to Python. However, it places a strong emphasis
on type safety, which means many runtime errors are caught at compile-time. As a
well suited Domain-Specific Language (DSL), Kotlin is officially supported for
Android development (running on JVM).

Kotlin, although type-strong, provides type inference, which allows you to omit
explicit type declarations in many cases while still ensuring type safety.
Explicitly naming the type is also a language feature. Unless the type is
explicitly declared `Any`, variable types are immutable.

Some of Kotlin features are: pointers, null safety (null pointer safety
features), inference, extension functions (the addition of new functions to
existing classes without modifying their source code), smart casts, native
coroutines (asynchronous programming), interoperability with Java (native
support for of Java libraries and code), lambda expressions.

**Each** Kotlin dataclasses (list, maps, etc) can be declared mutable or
immutable.

Learn the language with [Kotlin
Koans](https://play.kotlinlang.org/koans/overview).

## Command line

Not recommended.

```sh
kotlinc test.kt
kotlin TestKt.class
```

## Scripting

Inside `<name>.main.kts`:

```kotlin
#!/usr/bin/env kotlin
println("hello")
```

Just run: `./<name>.main.kts`.

## Package manager

Gradle.

Search dependencies on [the JetBrains
website](https://package-search.jetbrains.com/).

### Compile to jar

https://stackoverflow.com/a/61372736/12932725

```kotlin
tasks.jar {
    manifest {
        attributes(
            "Implementation-Title" to "Example",
            "Implementation-Version" to "1.0.0",
            "Main-Class" to "Example.ApplicationKt"
        )
    }

    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    from(sourceSets.main.get().output)
    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })
    destinationDirectory = File("$rootDir")
    archivesName = "Example"
}
```

## Running as an application

With the single-file application `hello.kt`:

```sh
kotlinc hello.kt -include-runtime -d hello.jar
java -jar hello.jar
```

Note: [compile to machine code is also
possible](https://github.com/JetBrains/kotlin/releases).

## Compiling a library

With the single-file library `hello.kt`:

```sh
kotlinc hello.kt -d hello.jar
kotlin -classpath hello.jar HelloKt
```

## Scope functions

- [Scope
  functions](https://kotlinlang.org/docs/scope-functions.html#function-selection)

Scope functions are provided in the standard library to execute a block of code
within the context of an object.

- `let`: used for null-checking and object transformation. Perform operations on
  a non-null object and provides a way to handle nullability. It takes the
  object as a parameter (`it`) and returns the result of the lambda expression.
- `run`: execute a block of code on an object and return the result. It can be
  used with both nullable and non-null objects. The object is accessed using the
  `this` keyword or can be omitted altogether.
- `with`: takes an object and a lambda expression and allows you to access the
  object's properties and functions directly without qualifying them with the
  object name. It doesn't return a result explicitly.
- `apply`: used for initializing properties of an object or modifying its state.
  It returns the object itself after the operations are applied.
- `also`: used for logging, side effects or chaining operations. It is similar to
  apply but provides a way to perform additional actions on an object without
  modifying its state, returning the object itself.

## In-code documentation

Use [KDoc](kdoc.md).

## Unit tests

https://phauer.com/2018/best-practices-unit-testing-kotlin/

## Logging

Use [slf4j](https://www.slf4j.org/manual.html) logging facade (`slf4j-api`),
then choose a logger like `slf4j-simple` or one of
[these](https://www.slf4j.org/manual.html#swapping).

## Unit tests

[Best practices unit testing in
Kotlin](https://phauer.com/2018/best-practices-unit-testing-kotlin/)

