
# ST Microcontroller

- [Debugging ARM based microcontrollers in neovim with
  gdb](https://chmanie.com/post/2020/07/18/debugging-arm-based-microcontrollers-in-neovim-with-gdb/)
- [Bare Metal STM32
  Programming](https://vivonomicon.com/2018/04/02/bare-metal-stm32-programming-part-1-hello-arm/)
- [GCC arm
  cross-compiler](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)
- [Description of STM32F4 HAL and low-layer
  drivers](https://www.st.com/resource/en/user_manual/dm00105879-description-of-stm32f4-hal-and-ll-drivers-stmicroelectronics.pdf)
- [pyOCD](https://pyocd.io/)
- [STM32 HAL tutorial (HAL ressources)](https://github.com/Xxxxhx/STM32_HAL_Tutorial)

Note: for Linux serial, use `picocom`.

There are different ways to achieve this.

- [Platformio libre, multiple vendors compilation tool](platformio.org)
- [Start Developing STM32 on
  Linux](https://www.instructables.com/Start-Developing-STM32-on-Linux/)
- [Learning STM32](https://riptutorial.com/ebook/stm32)

- STM32Nucleo: Device
- STM32Cube: IDE suite (not necessary)
- STM32CubeMX: Code generator
- STLink-V2: Hardware programmer
- STLink: Firmware programmer
- [OpenOCD](openocd.md): Firmware
  debugger and programmer
- SWD: Serial Wire Debug
- [libopemcm3](https://github.com/libopencm3/): Low level libre library for ARM
  cortex
- newlib: _one_ C standard library implementation for embedded devices
- CMSIS: Common Microcontroller Software Interface Standard, enables consistent device support and
  simple software interfaces to the processor and its peripherals
- LL: ST Low-Level library

Notes:

- It is recommended to compile Open On-Chip Debugger from source
- STLink has a GUI (`stlink-gui`)
- STLink-V2 can be replaced with a USB-serail converter (like the FT232,
  CP2102...), but it gives you debugging options
- STM32CubeIDE can generate Makefile projects

## arm coss-compiler

- install gcc and newlib: `pacman -S arm-none-eabi-gcc arm-none-eabi-newlib`
- optionally install utilities: `pacman -S arm-none-eabi-binutils`: utilities
  like objcpy, objdump, strip, readelf, ar, as, ld, etc

## flashing with ST-LINK

- [libopemcm3 examples](https://github.com/libopencm3/libopencm3-examples)
- [Linux STM32 Discovery
  GCC](https://www.wolinlabs.com/blog/linux.stm32.discovery.gcc.html)
- [STM32 Boitier
  ST-LINK](https://riton-duino.blogspot.com/2019/03/stm32-boitier-st-link.html)
- [stlink Tools Tutorial](https://github.com/stlink-org/stlink/blob/develop/doc/tutorial.md)

One way to flash the device is:

- Add the following udev rule:

```
# /etc/udev/rules.d/45-usb-stlink-v2.rules

#FT232
ATTRS{idProduct}=="6014", ATTRS{idVendor}=="0403", MODE="666", GROUP="plugdev"

#FT2232
ATTRS{idProduct}=="6010", ATTRS{idVendor}=="0403", MODE="666", GROUP="plugdev"

#FT230X
ATTRS{idProduct}=="6015", ATTRS{idVendor}=="0403", MODE="666", GROUP="plugdev"

#STLINK V1
ATTRS{idProduct}=="3744", ATTRS{idVendor}=="0483", MODE="666", GROUP="plugdev"

#STLINK V2
ATTRS{idProduct}=="3748", ATTRS{idVendor}=="0483", MODE="666", GROUP="plugdev"

#STLINK V2.1
ATTRS{idProduct}=="374b", ATTRS{idVendor}=="0483", MODE="666", GROUP="plugdev"
```

Note: The user should belong to the `GROUP` named above.

- connect the device through your STLink-V2 (or integrated ST-LINK hw)
- probe to see if the board is correctly connected: `st-info --probe`
- erase the MCU: `st-flash erase`

Write a program with `libopencm3` library as a substitute for HAL. Create a
linker script defining
the ROM and RAM.

- compile to object: `arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Wall -g -O0 -I
  . -I lib/inc -c -o
  main.o main.c`
- link to ELF: `arm-none-eabi-gcc
  -Wl,--gc-sections,-Map=main.elf.map,-cref,-u,Reset_Handler -I . -I
  lib/inc -L lib -T stm32.ld main.o stm32f10x_it.o lib/libstm32.a --output
  main.elf`
- translate to bin: `arm-none-eabi-objcopy -O binary main.elf main.bin`
- program the firmware: `st-flash write main.bin 0x08000000` (this number
  corresponds to a region configured by the linker as ROM)
- STM32CubeMX can create Makefile

## gdb

Either use OpenOCD or use `st-util -m`.

#### simulating

- [simulating with gdb](http://cs107e.github.io/guides/gdb/)

The simulation will not care about peripherals and timers.

- `arm-none-eabi-gcc ./PROGRAMM.elf`
- `target sim`
- `load`

## interfacing with SWD

- [Advanced Debug Features: Part 2](https://www.youtube.com/watch?v=Zqwq9nzTNF8)
- [How to programm a L432KC with an external
  ST-Link?](https://community.st.com/s/question/0D53W00000G2NJuSAN/how-to-programm-a-l432kc-with-an-external-stlink?t=1665483125146)

VDD supply and GND are needed for your board to work. For a ST-LINK debug
connection

- GND, SWCLK, and SWDIO are minimum
- reset (NRST) is optional and useful
- SWO is optional and maybe useful. SWO is a fast tracing output which can be
  used for printf tracing (logging) similar to a `VCP_TX` but using a different
  protocol and needing a SWO capable debugger/logger
- `VCP_RX` and `VCP_TX` is a useful serial console interface which can be used
  for printf tracing (logging) as well as for user IO as you like. It can be
  routed to any USART on your PCB and is not closely related to SWD, just
  happens to show up on the same USB. Shows up as a COM port on the host PC.

> What about a reset? I thought, the debugger has to tell the MCU to reset after
> flashing?

A MCU reset can also be triggered by some magic SWDIO+SWDCK commands. But this
is "soft" and works only when the SWD interface is available, i.e. SWD pins are
not reconfigured and MCU not in certain low-power modes. The NRST can give it a
"hard" reset anytime, which is more powerful.

Take a look at the broken-off of Nucleo-64 board (UM2206). It has GND, SWCLK,
SWDIO, NRST, SWO on pin 2-6 of CN3 and `VCP_RX` and `VCP_TX` on the connector
nearby. You cannot fail if you make those signals available on your board.

CN3 Pin 1 marked `VDD_TARGET` is a special case. It is an input to SWD for
detecting the target (debuggee) VDD voltage which can be evaluated by the
debugger. High-end debug probes can use that pin for IO level conversion,
galvanic decoupling etc.. Think of it as a 1-channel digital voltmeter input.
Maybe useful to connect too, especially to detect low battery... scenarios.

Beware: some low-cost ST-LINK debug clones have a 3.3V pin which acts as an
output to power the target, this is not the standard, do not connect.

STM AN4989 Application note "STM32 microcontroller debug toolbox has valuable
info on debugging, SWO etc..

### Correspondance table with JTAG

JTAG|SWD
-|-
TMS|SWDIO
TCK|SWCLK
TDO|SWO

## flashing troubleshooting

- If when doing an `st-flash write` an error occurs, try disconnecting and reconnecting **from the
  STM32 side** of the cable (not from the computer side of the cable)
- After installation, try running `dmesg` and see if there is an error message
  related to the STM. If there is an error message:
  - test if you can run `st-link` and `st-util`
  - try changing USB ports
  - try updating the STLink through the IDE
- [STM32 Standard Periferal
  Library](https://www.st.com/en/embedded-software/stm32-standard-peripheral-libraries.html):
  some MCUs are not included
- If MCU is not included, then maybe try the STM32CubeMX
- If you are having trouble communicating with UART, try adding yourself to the `dialout` group

## creating the Makefile from STM32CubeMX

- [Makefile4CubeMX](https://github.com/duro80/Makefile4CubeMX)
- [Hacky Makefile generator](https://github.com/baoshi/CubeMX2Makefile)

## Communicating though UART

Device will be either `/dev/ttyUSBx` or `/dev/ttyACMx`.

Try looking at `dmesg`, if it displays an error. Error `-71` means a power supply issue, which will
not let you mount as a `/dev/ttyACMx` device.

## creating a cmake project

[STM32 CMake](https://github.com/ObKo/stm32-cmake)

## Thumb vs ARM instruction set

ARM instructions are 32 bits wide (`-marm`), and Thumb instructions (`-mthumb`) are 16 wide. Thumb
mode allows for code to be smaller, and can potentially be faster if the target has slow memory.

Note that, when printing the program counter in GDB with thumb instruction set, you have to print
as `x/10i $pc+1` (note the plus one). Convention is, to always use the `+1` when it is about a thumb
instruction.

## gcc command

First compile every `.c` file into and `.o` object:

```sh
arm-none-eabi-gcc
-c
-mcpu=cortex-m4 # Specify the name of the target processor, used to determinate architecture-specific instruction set
-mthumb # != -marm Select between generating code that executes in ARM and Thumb states
-mfpu=fpv4-sp-d16 # floating-point processor
-mfloat-abi=hard # hardware floating point support
-DUSE_HAL_DRIVER
-DSTM32L452xx
-IInc
-IDrivers/STM32L4xx_HAL_Driver/Inc
-IDrivers/STM32L4xx_HAL_Driver/Inc/Legacy
-IDrivers/CMSIS/Device/ST/STM32L4xx/Include
-IDrivers/CMSIS/Include
-Og # Optimization
-Wall # all warnings enabled
-fdata-sections # place data items into a separate section
-ffunction-sections # place function into a separate section
-g # debug information
-gdwarf-2 # debugging information in drawf version 2 format
-MMD -MP -MF"build/main.d" # list user listed dependencies appropriate for Makefile
-Wa,-a,-ad,-alms=build/main.lst # -Wa passes option to assemble; -a generated lst, -ad omit
    debugging directives, -al includes assembly, -am include macro expansion, -as include symbols
    "build/main.lst" is the listing file
Src/main.c
-o
build/main.o
```

Then compile the `.s` assembly into a `.o` object:

```sh
arm-none-eabi-gcc
-x assembler-with-cpp
-c
-mcpu=cortex-m4
-mthumb
-mfpu=fpv4-sp-d16
-mfloat-abi=hard
-DUSE_HAL_DRIVER
-DSTM32L452xx
-ICore/Inc
-IDrivers/STM32L4xx_HAL_Driver/Inc
-IDrivers/STM32L4xx_HAL_Driver/Inc/Legacy
-IDrivers/CMSIS/Device/ST/STM32L4xx/Include
-IDrivers/CMSIS/Include
-Og
-Wall
-fdata-sections
-ffunction-sections
-g
-gdwarf-2
-MMD
-MP
-MF"build/startup_stm32l452xx.d"
startup_stm32l452xx.s
-o
build/startup_stm32l452xx.o
```

Link it all together into an `.elf` file:

```sh
arm-none-eabi-gcc
build/main.o
-mcpu=cortex-m4
-mthumb
-mfpu=fpv4-sp-d16
-mfloat-abi=hard
-specs=nano.specs # https://gcc.gnu.org/onlinedocs/gcc/Spec-Files.html
-TSTM32L452RETx_FLASH.ld
-lc
-lm
-lnosys
-Wl,-Map=build/board-tester.map,--cref
-Wl,--gc-sections
-o
build/board-tester.elf
```

Finally convert the elf into a `.bin` or `.hex` (`.ihex`, Intel hex file):

```sh
arm-none-eabi-objcopy -O ihex build/board-tester.elf build/board-tester.hex
arm-none-eabi-objcopy -O binary -S build/board-tester.elf build/board-tester.bin
```

Note: default STM32 code requires two additional functions to compile: `void
SystemInit(void)` and `void _exit(int code)`. Their implementation can be left
empty.

## libc: newlib, newlib-nano and subfunctions (stubs)

- [The newlib embedded C Standard Library and how to use
  it](https://hackaday.com/2021/07/19/the-newlib-embedded-c-standard-library-and-how-to-use-it/)
- [STM32 printf scanf](https://www.codetd.com/article/13468279)

Standard C defines common libraries such as `stdio.h` among others which makes
the presumption of at least a TTY. Because a MCU does not necessarily have an
OS, or TTY, newlib (the standard libc for MCUs) will define some ["stub
functions"](https://sourceware.org/newlib/libc.html#Stubs) (stub here means
empty). Those have to be defined according to your needs (see [basic IO
implementation section](#basic-io-implementation).

There are two newlibs: the one from Red Hat and the one from ARM, often referred
to newlib-nano or simply nanolib. The one from Red Hat often will not fit inside
a microcontroller. To name another alternative: picolibc.

Specify which libc to use by passing the [`-specs=XX`
option](https://gcc.gnu.org/onlinedocs/gcc/Spec-Files.html). The values
correspond to files that are under the `lib/` inside the cross-compiler
installation folder.

- `nosys.specs`: defines syscalls should be implemented as stubs that return
  errors when called with `-lnosys`
- `nano.specs`: newlib-nano libc

These files add other flags and create flag substitution (eg. `-lc` is
substituted by `-lc_nano` when `-specs=nano.specs` is given). Multiple specs
files are possible and they are processed in order. Note: libgloss "is a library
for all the details that usually get glossed over", it contains startup and IO
support code.

## basic IO implementation

For printf and scanf, etc, you can either use [lightweight
printf](https://github.com/mpaland/printf) or define the following functions:

```c
#ifndef DEBUG_INTERFACE_H
#define DEBUG_INTERFACE_H

#include <sys/stat.h>

#include "stm32XXXXXXXX_hal.h" // UART_HandleTypeDef definition

#define STDIN_FILENO  0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2

int printf(const char *restrict format, ...); // just the definition
void debug_interface_init(UART_HandleTypeDef *huart);
int _isatty(int fd);
int _write(int fd, const char* buf, size_t len);
int _read(int fd, const char *buf, size_t len);
int _close(int fd);
int _lseek(int fd, int* ptr, int dir);
int _fstat(int fd, struct stat* st);
```

Implementation:

```c
#include <stdio.h>

#include "debug_interface.h"

UART_HandleTypeDef* debug_uart;

void debug_interface_init(UART_HandleTypeDef *huart) {
    debug_uart = huart;
    setvbuf(stdout, NULL, _IONBF, 0); // disable I/O buffering
}

int _isatty(int fd) {
    switch(fd) {
        case(STDIN_FILENO):
        case(STDOUT_FILENO):
        case(STDERR_FILENO):
            return 1;
        default:
            return 0;
    }
}

int _write(int fd, const char* buf, size_t len) {

    if (fd == STDOUT_FILENO || fd == STDERR_FILENO) {
        HAL_UART_Transmit(debug_uart, (uint8_t *) buf, len, HAL_MAX_DELAY);
        return 1;
    }
    return -1;
}

int _read(int fd, const char *buf, size_t len)
{
    if (fd == STDIN_FILENO) {
        HAL_UART_Receive(debug_uart, (uint8_t*) buf, 1, HAL_MAX_DELAY);
        return 1;
    }
    return -1;
}

int _close(int fd) {
    switch(fd) {
        case(STDIN_FILENO):
        case(STDOUT_FILENO):
        case(STDERR_FILENO):
            return 0;
        default:
            return 0;
    }
}

int _lseek(int fd, int* ptr, int dir) {
    return -1;
}

int _fstat(int fd, struct stat* st) {
    switch(fd) {
        case(STDIN_FILENO):
        case(STDOUT_FILENO):
        case(STDERR_FILENO):
            st->st_mode = S_IFCHR;
            return 0;
        default:
            return 0;
    }
}
```

And then add the linker flag `-specs=nano.specs -specs=nosys.specs`. Note: by adding this flag, the
code gets bigger - ie. only add when `-D DEBUG`.

## Libraries

It is possible to select between HAL and LL for a specific hardware handler (eg. `hi2c1`) in the
STM32CubeMX: Project Manager \> Advanced Settings > Driver Selector, then click on device and change
the driver.

## simulation

- [QEMU ARM Emulation](https://balau82.wordpress.com/arm-emulation/)
- [Renode ARM Emulation](https://interrupt.memfault.com/blog/intro-to-renode)
- [QEMU ARM](https://xpack.github.io/qemu-arm)
- [bochs](https://bochs.sourceforge.io/)

While it is possible to emulate ARM targets with QEMU, QEMU original intention is to emulate systems
with OS.

## interrupts

In the ST context, lower numbers mean higher priority.

## HAL callback

Callback functions refer to interrupt calls on hardware events (eg. I²C receive).

Things to check:

- SysTick really should have the highest priority
- Callbacks are called under interrupt context, via HAL call-ins from the IRQ Handlers
- Interrupt on event for particular handler is enabled: **also when using DMA**
- Disable SCL stretch when debugging
- Make sure debugging hardware is as fast as possible (eg. USART connected to the computer)

Callback functions should also be as fast as possible: **no printfs**, etc.

It should look like:

```c
void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef* hi2c) {
    // reenable listen after error treatment
    HAL_I2C_EnableListen_IT(hi2c);
}

void HAL_I2C_AddrCallback(I2C_HandleTypeDef* hi2c, uint8_t TransferDirection, uint16_t AddrMatchCode) {
    // AddrMatchCode is 8 bit address
    if (TransferDirection == I2C_DIRECTION_TRANSMIT) {
        HAL_I2C_Slave_Seq_Receive_DMA(hi2c, &cnt, 1, I2C_FIRST_FRAME);
        HAL_I2C_Slave_Seq_Receive_DMA(hi2c, buffer, cnt, I2C_LAST_FRAME);
    } else {
        HAL_I2C_Slave_Seq_Transmit_DMA(hi2c, &cnt, 1, I2C_FIRST_FRAME);
        HAL_I2C_Slave_Seq_Transmit_DMA(hi2c, buffer, cnt, I2C_LAST_FRAME);
    }
}

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef* hi2c) {
}

void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef* hi2c) {
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef* hi2c) {
    // use HAL_I2C_GetError() to treat errors
}

int main() {
    // initialize
    // ...
    HAL_I2C_EnableListen_IT(&hi2c3); // enable callback
    // ...
}
```

- Address callback is called only when there is an address match
- Tx/Rx is called on each frame, lasting until a stop or restart condition

Treated byte per byte through `HAL_I2C_SlaveRxCpltCallback()` in Slave side until a STOP condition.

`HAL_I2C_ListenCpltCallback()`, slave is informed of the end of communication with master.

- `I2C_FIRST_FRAME`: start, no stop
- `I2C_FIRST_AND_NEXT_FRAME`: start, no stop, master only
- `I2C_NEXT_FRAME`: no start, no stop, possible restart
- `I2C_LAST_FRAME`: no start, stop or restart
- `I2C_LAST_FRAME_NO_STOP`: restart condition, master only
- `I2C_OTHER_FRAME`: restart condition, master only

### Use register callback

When you set `USE_HAL_I2C_REGISTER_CALLBACKS` to `1`, the callbacks must be registered via
`HAL_I2C_RegisterCallback`. In this case, the callbacks are true C function pointers. No linker
magic/weak involved (see `I2C_HandleTypeDef` definition).

If you leave it at `0`, the weak functions are called unless you implement your own functions with
the exact same names. The linker resolves this in favor of the non-weak function definition.

To change any `USE_HAL_XXX_REGISTER_CALLBACKS`, do it though the STM32CubeMX: Project Manager
\> Advanced Settings

When using DMA, initialize it **before** other connectivity hardwares (eg. `hi2c`).

## half-way transmit/receive

There is no half-way TxRx callback function, but you set to use DMA and the DMA does have a
half-operation callback (`XferHalfCpltCallback` field in the DMA handle).

## HAL I²C Slave

- [I²C Two Boards Restart Advanced COmmunication Interrupt
  Example](https://github.com/STMicroelectronics/STM32CubeL0/tree/a7b74aed35ecb7baeadeb16107aa8fddb6823589/Projects/NUCLEO-L073RZ/Examples/I2C/I2C_TwoBoards_RestartAdvComIT)
- [I2C Slave mode problem while receiving
  data](https://github.com/STMicroelectronics/stm32l0xx_hal_driver/issues/5)
- ["Manual" I²C](https://cnnblike.com/post/stm32-iicbug/)

## SWO usages

- [Orbuculum debugging toolset](https://github.com/orbcode/orbuculum)
- [show Cortex-M4 SWO log with
  openocd](https://stackoverflow.com/questions/50075076/show-cortex-m4-swo-log-with-openocd/60772130#60772130)

Here is a comparison:

- semihosting: resource intensive and slow, but available in every arm chip
- serial: fast, but extra pins and possibly hardware (USB to UART) needed
- Virtual COM Port (VCP): fast, but USB pins needed
- Instrumental Trace Macrocell (ITM): fast over dedicated SWO, but only
  available on Coortex M3 (Cortex-M3+?) and M4 parts
  - check the reference manual

### semihosting

- [How to use semihosting with
  STM32](https://shawnhymel.com/1840/how-to-use-semihosting-with-stm32/)
- [Introduction to ARM
  Semihosting](https://interrupt.memfault.com/blog/arm-semihosting)
- [Bare metal embedded lecture-7: Integrating 'C' standard library and
  semihosting](https://youtu.be/AzYC5hO2uJk?t=1731)

Semihosting is a mechanism that enables code running on a target to use host
facilities. Facilities are, for example: keyboard input, screen output, and disk
I/O - it even allows the transfer of files. SWO must be connected.

1. use newlib-nano (ignore `syscalls.c` if present, no `__io_putchar` or
   `_write` functions allowed)
  - remove `-nostartfiles` if present
2. add `-lrdimon` and `-specs=rdimon.spes` (stands for remote dispatch interface
   monitor ie. semihosting)
3. add `extern void initialise_monitor_handles(void)` to the code
4. initialize with `initialise_monitor_handles()`
5. call `printf` as normal
6. use OpenOCD command `monitor arm semihosting enable`

When using semihosting, the device will always wait for the debugger to connect.

### Instrumental Trace Macrocell (ITM)

- [Getting started with instrumentation trace macrocell in
  STM32CubeIDE](https://medium.com/@g.bharathraj19/getting-started-with-instrumentation-trace-macrocell-in-stm32cubeide-4af179eb0034)

Same purpose as normal semihosting.

Check [this](https://interrupt.memfault.com/blog/profiling-firmware-on-cortex-m)
for profiling / debugging alternatives (including ITM).

## sleep modes

- [Low power modes in
  STM32](https://controllerstech.com/low-power-modes-in-stm32/)
- [Getting started with
  PWR](https://wiki.st.com/stm32mcu/wiki/Getting_started_with_PWR)

## boot modes

MCU will start at address 0, this address is mappable to another address. To
remap it, use provided registers `SYSCFG`.

STM32 can boot from flash, from RAM or from system memory. STM32L452RE has both
a `FLASH_OPTR` register or from system memory that controls that and an external
`BOOT0` pin. In this case, when this pin is set to high, boot comes from RAM or
system memory; low, from flash.

If the programmer will not change `BOOT0` pin, it is suggested to use a
pull-up/down resistor of `100kΩ`.

Note that, do ensure a software reset, `BOOT0` pin should be stable.

## STLinkV2 Programmer Clone

- [Install ST-LinkV2
  SWD](https://github.com/jeanthom/DirtyJTAG/blob/master/docs/install-stlinkv2-swd.md)

There are different problems with the clone:

- Optional SWO pin is not connected, you should solder it manually
- Firmware is buggy
- Reset is not controllable

## programming via UART bootloader

- [Flashing STM32 with UART Bootloader](https://youtu.be/1cleO3mHjWw?t=1356)

Bootloader is only via certain pins accessible, refer to AN2606.
The bootloader requires even parity to be used.
Watch out for byte-size, parity and stop bits, specified in the same document.

Program it with [stm32flash](https://sourceforge.net/p/stm32flash/wiki/Home/)
software. If the current version fails, try the precedent one.

You can use the same UART to communicate to the device afterwards.

The STM32L4xx Bootloader is activated by configuring the pins BOOT0="high" and
BOOT1="low" and then by giving it a reset pulse by default.

Application in user flash is activated by configuring the pin
BOOT0="low" (BOOT1 is ignored) and then by applying a reset.

Let us say that the steps are:

1. reset the board
2. change the state of the boot pin
3. in some targets, yet another pin has to be configured (PB2 tied to GND in
   case of STM32F407VG).
4. unrestart [now entering the bootloader mode]
5. talk to the bootloader API, flash, etc
6. unchange the state of the boot pin

Translating it to a command:

```sh
stm32flash -R -i rts,dtr,-rts:rst,-dtr,-rst /dev/ttyUSB0
```

- `-R`: reset when it stops working
- `-i`: GPIO sequence
- `rst,dtr,-rts`: enter boot loader sequence: reset high, wait 100ms, terminal
  ready high, 100ms, reset low
- `rst,-dtr,-rst`: exit boot loader sequence: reset high, wait 100ms, terminal
  ready low, 100ms, reset low

The entry and exit bootloader sequence can also be made manually though jumpers:
link boot pin to VDD or GND, leave reset alone.

Note that the `-R` option may be omitted, because since version 0.6, an exit
sequence will always be executed if specified, regardless of the -R option, to
ensure the signals are reset. If `-R` is specified, but no exit sequence, a
software-triggered reset will be performed.

## USB implementation

- [STM32USB](https://github.com/CShark/stm32usb/wiki)
- Introduction to USB hardware and PCB guidelines using STM32 MCUs DM00296349

[Implementing USB is very
brittle](https://electronics.stackexchange.com/a/149756): USB interrupts must
work correctly, otherwise the host will not accept the device.

### Debugging

- [How to debug an
  USB-Connection?](https://community.st.com/t5/stm32cubemx-mcu/how-to-debug-an-usb-connection/td-p/102240)

- high speed UART (921600 bps eg.)
  - SWO printf can go faster than that
- high resolution timestamp (extra timer or DWT cycle counter when available)
  for timestamping logs
- Create a text buffer in RAM for logging
  - inspect or dump it later
  - set breakpoints in code and to check
- use free pins with logic analyzers for certain states of the connection
- tools that are similar to the log + LA technique: TraceX or
  [TimeDoctor](https://gitlab.com/KnarfB/timedoctor)
- Wireshark may capture USB data on the wire
- Use asserts
- Alternative to the log via buffer implementation is: a FIFO to output to
  UART/SWO asynchronously
- Use a USB protocol analyser like Ellisys USB Explorer 260 or LeCroy Mercury T2
  - if STM32 is acting as host, PC USB analyzer cannot be used
- Use [OpenOCD Debug Communications Channel (DCC)
  library](https://openocd.org/doc-release/html/OpenOCD-Project-Setup.html)

Or implement it as a large FIFO buffer and UART/SWO running asynchronously at it's own pace.

## Device Firmware Upgrade (DFU)

- [DFU Mode on a STM32
  Microcontroller](https://oshgarage.com/dfu-mode-on-a-stm32-microcontroller/)
- Getting started with DfuSe USB device firmware upgrade (UM0412)
- [hex2dfu](https://github.com/nanoframework/hex2dfu)
- [elf2dfuse](https://github.com/majbthrd/elf2dfuse)

The DFU bootloader resides in the ROM of the STM32 core and is not erasable, so
this bootloader is un-brickable.

### Bootloader mode

- [Calling the STM32 SystemMemory Bootloader from your
  application](https://www.youtube.com/watch?v=cvKC-4tCRgw)

Per hardware: through pins, check AN2606.
Per software:

Basically: call bootloader from inside the software, consult "Bootloader
  device-dependent parameters" section from AN2606. <!-- NOT TESTED: set these
  pins as outputs; set them as if they were handled via connectors; call the
  reset routine (`NVIC_SystemReset()`) -->

More concretely:

1. shut down any tasks
2. switch to the HSI clock source - no PLL
3. disable interrupts
4. set main stack pointer (MSP) to its default value
5. load the program counter with the SystemMemory reset vector

### programming

Check if microcontroller supports and the requirements (AN2606). One can use
`dfu-util` or use the STM32CubeProgrammer, for now on, `dfu-util` will be
assumed. `dfu-util` accepts `.bin` and `.dfu`, but not `.elf`. The `.dfu` is a
file specific for DFU that contains all the necessary information to flash. DFU
can access flash, ram, special byte, etc, with each being appearing as a
separate DFU device when you connect. Check `dfu-util -l` to see what device
index you should pass to `-a`.

After putting the device on DFU mode, run:

```sh
dfu-util -a 0 -D firmware.dfu
# or
dfu-util -a 0 -D firmware.bin -s 0x08000000
```

Alternatively, after having installed `fwupd`, run

