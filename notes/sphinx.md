
# Sphinx

- [Sphinx
  setup](https://www.jetbrains.com/guide/python/tutorials/sphinx_sites/setup/)
- [Cheatsheet](https://sphinx-tutorial.readthedocs.io/cheatsheet/)
- [Documenting python code with
  sphinx](https://towardsdatascience.com/documenting-python-code-with-sphinx-554e1d6c4f6d)

## basic

[Write Beautiful Python Documentation with
Sphinx](https://python.plainenglish.io/documentation-with-sphinx-dd86bedb7512)

```reST
This is a heading
==================
This is an introduction paragraph.
A subheading here
---------------------
I am *italic* and I am **bold**. I am an ``x,y = 2,3`` an inline code example.
An unordered list of items:
* item a
* item b
* item c
An ordered list of items:
#. first item
#. second item
#. third item
A code example::
    import sys
    import os
    print('Hello world')
.. image:: <image_path>
A list can be nested.
* fruits
* vegetables

  * broccoli
  * peas
* cereals (continuing first level list)
.. list-table:: table caption
    :header-rows: 1
    * - Operation
      - Description
      - Parameters
      - Flop Counts
    * - abs(x)
      - Absolute values
      - x
      - n
    * - <x,y>
      - Inner product
      - x, y
      - 2n
Here is an external link to `Python <https://www.python.org/>`_.
```
