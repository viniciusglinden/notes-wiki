
# GHDL

[GHDL](https://github.com/ghdl/ghdl) is recommended, because it is mature and open-source. GHDL is only a compiler, it does not have waveform viewer. For an open-source waveform viewer, use GTKWave.

## installing

- [GHDL User Guide](https://buildmedia.readthedocs.org/media/pdf/ghdl/latest/ghdl.pdf)
- [GHDL News](https://ghdl.readthedocs.io)

You also need to install a wave viewer (like GTKWave). You may try to install it from the package manager. If not successful, download the source from the [official github](https://github.com/ghdl/ghdl). If you do this, you should modify the file `libraries/vendos/config.sh` to the appropriate Vivado version.

You should also verify if you have the required libraries, mainly the gcc-ada or gcc-gnat.

There are three types of installation: mcode, LLVM and GCC. mcode is the simplest to install (but not without disadvantages), so we will install this one. mcode has the advantage that it does not require the "elaborate" step to logically simulate the design.

```sh
cd ghdl
mkdir build
cd build
../configure --prefix=/usr/local/
make
sudo make install
```

Unrecommended: Once installed, you can add any other proprietary library available (e.g. Vivado libraries), when in your simulation folder with `/usr/local/lib/ghdl/vendors/compile-xilinx-vivado.sh --all`. Note that there are more options available. `--all` will compile unisim, unimacro, unifast and secureip in the 1993 standard.

Recommended: import vhd from `/opt/Xilinx/Vivado/2019.1/data/vhdl/srd/unisims`

## simulating

Keep in mind that, the files are just a way to contain the VHDL code.
The VHDL means: entity and architecture.

### steps overview

1. set-up a directory
2. syntax check + analyze the files
3. elaborate the design so you may have an executable that can simulate a waveform
4. simulate it either using GHDL or the compiled file itself (`./entity --help`,
   GCC/LLVM only)

### commands

- `ghdl -d` shows the entities and architecture currently listed
- `ghdl -c` creates the `.cf` library file
* `ghdl -s file.vhd` does syntax check on file (linter)
* `ghdl -a file.vhd` analyzes the file: in this step, the file "is open",
  exposing the entity (so the following steps can take place)
* `ghdl -e entity` elaborates the entity
* `ghdl --gen-makefile entity > Makefile` generate the Makefile for automatic
  file elaboration
* `ghdl -m file.vhd` analyzes and elaborate the design
* `ghdl -r entity` runs the elaborated entity (entity is now a file name)
* `ghdl -r entity --entity.vcd` will run the elaborated entity while elaborating the Value Change Dump (vcd), needed for GTKWave.
* `gtkwave entity.vcd` will start the GTKWave with the elaborated entity opened

Only the files that are going to be simulated and/or implemented need to be
elaborated. That means that, if you are only simulating a Test Bench, and not
the Unit Under Test, you may only elaborate the Test Bench.

Example designs are provided at `ghdl/doc/examples/quick_start` try to test the adder as:

```sh
ghdl -d
ghdl -c
ghdl -s adder.vhdl
ghdl -s adder_tb.vhdl
ghdl -a adder.vhdl
ghdl -a adder_tb.vhdl
ghdl -a adder_tb.vhdl
ghdl -e adder_tb
```

Then either:

```sh
ghdl -r adder_tb --wave=wave.ghw
```

or

```sh
./adder_tb --stop-time=1ms --wave=adder_tb.ghw
```

Using the `wait;` VHDL statement at a Test Bench will automatically finish the
simulation.

### additional steps

In order not to pollute the sources with the artifacts (WORK library), it is a
good idea to create a work/subdirectory. To any GHDL commands, we will add
the --`workdir=workoption,` so that all files generated by the compiler (except
the executable) will be placed in this directory.

```sh
cd project
mkdir work
ghdl -i --workdir=work *.vhd*
ghdl -m --workdir=work tb_entity
ghdl -d --workdir=work
ghdl -r --workdir=work tb_entity
```

### Vendor specific libraries

Vendor-specific libraries cannot be shipped with GHDL. However, shell scripts
are; thus enabling easy inclusion. For example, run the following on the
**working folder**

`/usr/lib/ghdl/vendors/compile-xilinx-vivado.sh --all` or
`/usr/local/lib/ghdl/vendors/compile-xilinx-vivado.sh --all` will compile all
the hard and soft macros that comes with Vivado, in the current folder.
With these scripts, each vendor gets a new directory.

Or, you doesn't want to compile each new project, before installing GHDL by
source, setup the `config.sh` to point to the directory which contain
these files:

```sh
declare -A InstallationDirectory
InstallationDirectory[XilinxVivado]="/opt/Xilinx/Vivado/2017.4"
```

## GTKWave

[GTKWave](http://gtkwave.sourceforge.net/gtkwave.pdf) have a configurable via the
`~/.gtkwaverc` or the `.gtkwaverc` in the project folder. An example can be
acquired from `/usr/share/gtkwave/examples/gtkwaverc`. To see what each
command means just type `man gtkwaverc`

Note that the `GTKWAVE_EDITOR` env variable can also be used to point to the RC
file (didn't check it).
