
# Music

- [MPD for local listening](https://hristos.lol/mpd-local-listening/)
- [How to configure mpd and ncmpcpp](https://computingforgeeks.com/how-to-configure-mpd-and-ncmpcpp-on-linux/)

Use the following:

- `mpd`: music player daemon (back-end, server)
- `mpc`: command line interface to `mpd`
- `ncmpcpp` (ncurses music client plus plus): terminal-based front-end
- `mpd-runit`: for initializing process

1. Then configure `mpd` (`~/.config/mpd/mpd.conf` or `XDG_CONFIG_HOME/mpd/mpd.conf`).
2. Add tracks to the `music_directory`, indicated inside the mpd's configuration
file.
3. `mpc update`
4. Then test with the commando `mpd ~/.config/mpd.conf`.
5. Open `ncmpcpp` to test.
6. Enable the daemon at startup with `ln -s /etc/runit/sv/mpd /run/runit/service` (runit)

- Note: enabling with `runit` did not work, maybe because larbs was already calling it
  from xprofile.

Configurations for `ncmpcpp` are `~/.config/ncmpcpp/config` and for bindings
  `~/.config/ncmpcpp/bindings`
