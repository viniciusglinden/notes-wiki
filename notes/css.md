
# CSS

- [VisBug](https://github.com/GoogleChromeLabs/ProjectVisBug)

## HTML reference

Add to the HTML's head:

```html
<link rel='stylesheet' type='text/css' href="style.css">
```

## Basic syntax

```css
selector {
    declaration: value;
}
```

Example:

```css
body {
	color: white;
	background: black;
	max-width: 800px;
	margin-left: auto;
	margin-right: auto;
}

h1 {
	color: red;
}

h1, h2 {
	text-align: center;
}

em {
	font-size: xx-large;
}
```

### selector syntax

[css selectors](https://blog.webdevsimplified.com/2021-12/css-selectors/)

- class: can be applied everywhere
- id: can only be applied to one element

```css
* { declaration: value; } /* universal, applies to everything */
type { declaration: value; } /* type = div, p, etc */
.class { declaration: value; }
#id { declaration: value; }
```

Examples:

```css
p.center {
    text-align: center;
    color: red;
}
```

Note that if you declare:

```html
<p class="center large">paragraph</p>
```

Paragraph gets class center and class large styled.

```cs
div b { background: red; } /* type b inside type div */
```

```html
<div><b>example1</b></div> <!-- applies -->
<div><span><b>example1</b></span></div> <!-- applies -->
<b>example2<b> <!-- does not apply -->
```
