
# UML

- [UML
  cheatsheet](http://www.cheat-sheets.org/saved-copy/UML%20Reference%20Card.pdf)
- [UML quick reference](https://holub.com/uml/)
- Book: UML for mere mortals
- [Low-Level Design, Object Oriented Design and Principles, UML
  Diagrams](https://lovepreet.hashnode.dev/low-level-design-series-introduction-uml-diagrams)


Use `astah` as diagram software (supports Linux, functional safety, SysML, UML and more) - there is
also a free version (astah community).

Use `gaphor` as an libre software for UML diagrams.


```
                                           ┌───────┐
                                           │diagram│
                                           └───▲───┘
                                               │
                       ┌───────────────────────┴──────────────┐
                       │                                      │
                  ┌────┴────┐                            ┌────┴────┐
                  │structure│                            │behaviour│
                  │diagram  │                            │diagram  │
                  └────▲────┘                            └────▲────┘
                       │                                      │
    ┌─────┬─────┬──────┼──────┬─────┬─────┐            ┌──────┼──────┬──────────┐
    │     │     │      │      │     │     │            │      │      │          │
    │ ┌───┴───┐ │ ┌────┴────┐ │ ┌───┴───┐ │       ┌────┴───┐  │  ┌───┴────┐     │
    │ │class  │ │ │component│ │ │object │ │       │activity│  │  │use case│     │
    │ │diagram│ │ │diagram  │ │ │diagram│ │       │diagram │  │  │diagram │     │
    │ └───────┘ │ └─────────┘ │ └───────┘ │       └────────┘  │  └────────┘     │
    │           │             │           │                   │             ┌───┴───┐
┌───┴───┐  ┌────┴────┐   ┌────┴─────┐ ┌───┴───┐         ┌─────┴─────┐       │state  │
│profile│  │composite│   │deployment│ │package│         │interaction│       │machine│
│diagram│  │structure│   │diagram   │ │diagram│         │diagram    │       │diagram│
└───────┘  │diagram  │   └──────────┘ └───────┘         └─────▲─────┘       └───────┘
           └─────────┘                                        │
                                        ┌─────────────┬───────┴──────┬───────────┐
                                        │             │              │           │
                                    ┌───┴────┐ ┌──────┴──────┐ ┌─────┴─────┐ ┌───┴───┐
                                    │sequence│ │communication│ │interaction│ │timing │
                                    │diagram │ │diagram      │ │overview   │ │diagram│
                                    └────────┘ └─────────────┘ │diagram    │ └───────┘
                                                               └───────────┘
```

```plantuml
@startwbs
* diagram
** structure diagram
*** profile diagram
*** class diagram
*** composite structure diagram
*** component diagram
*** deployment diagram
*** object diagram
*** package diagram
** behaviour diagram
*** activity diagram
*** use case diagram
*** state machine diagram
*** interaction diagram
**** sequence diagram
**** communication diagram
**** interaction overview diagram
**** timing diagram
@endwbs
```

## UML vs SysML

- UML is a standardized language for specifying software systems.
- SysML is a domain-specific language based on UML. It specifies a superset of a
subset of UML that takes the more generic UML and adapts it to the development
of complex systems.

While **UML is software-centric**, **SysML is more engineering
systems-oriented**. For example, SysML is used in system level design of System
on Chips.

## design method

1. list the requisites
2. describe the app: go through a simple use case
3. identify the main objects
4. describe their interactions
5. create the class diagram

## basic class diagram steps

Remember that a class diagram is not only for planning, but also for
documentation. The idea is not to make it more complicated, but to help.

1. conceptual: "this is more or less how it is going to be"
2. intermediary: "its time to define a little bit more"
3. implementation: "this is how i did. Whoever wants to refactor/extend it may
   do so as well"

## classes

![class diagram cheatsheet](https://radek.io/assets/posts/cheat_sheet4.png)

- [class diagram explanation (almost a
  cheatsheet)](https://courses.cs.washington.edu/courses/cse403/16au/lectures/L07.pdf)

Classes are drawn as a block and contain three horizontal contiguous section:
title, properties (member variables) and methods

- title: just the name
  - for enum class: `<<Enumeration>> name`
  - for abstract class: `<<name>>` or _name_
- access types
  - `+`: public
  - `#`: protected
  - `-`: private
  - `~`: package/default (access only inside the package)
  - `/`: derived (not stored, but can be computed)
- underline means static
- types are just the name of the type: `int`, `string`...
  - types with templates use "<>" sign. Eg. `list<int>`
- properties are declared like so: `<access type> <name> : <type>`
  - example: `+ test : int` for public integer named "test"
- methods are declared like so: `<access type> <name>([<type> [name][,<type>
  [name]...): <return type>`
  - example: `- set(int test, bool): float` for private method called "set" that takes
    two arguments: an int called "test" and a not mentioned bool, returning a
    float

Note: when programming in python, use the command `pyreverse <package folder>`
(from pylint). Eg. `pyreverse -o svg -f ALL MyPackage`.

## arrows explanation

- [Explanation of the UML arrows](https://stackoverflow.com/a/40694853/12932725)

- association: one can access the other, "has-a relation"
  - undirected: A can access B; B can access A
  - directed: A can access B; B can't access A
- aggregation: A has an instance of B; B can exist without A, "has-a relation"
- composition: A has an instance of B; B can't exist without A
- generalization: "is-a relation"
  - inheritance: B inherits from A; B "is a" A
  - realization: B implements A
- dependency: B is dependent on A; changes in one affects the other

### symbols

- association
  - undirected: straight line
  - directed: line with arrow, pointing to B
- aggregation: hollow diamond, pointing to A
- composition: filled diamond, pointing to A
- generalization
  - inheritance: line with hollow arrow pointing to A
  - realization: dashed line with hollow arrow pointing to A
- dependency: dashed simple arrow (->) pointing to A

## Multiplicity

Annotation on top of the arrows. Indicates amount of instances in a
relationship. Eg. `1 --> *` indicates a one-to-many. `A 1 -> * B` means that A
can have many Bs. `1..*` indicates one or more.
