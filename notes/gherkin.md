
# Gherkin

- [Gherking](https://specflow.org/learn/gherkin/)
- [Gherkin autoformatter](https://github.com/antham/ghokin)
- [Best practices](https://github.com/andredesousa/gherkin-best-practices)
- [BDD 101: Gherkin by
  Example](https://automationpanda.com/2017/01/27/bdd-101-gherkin-by-example/)
- [Python integration with
  `behave`](git@github.com:gherkin-by-example/python-behave.git)

Alternative: Robot Framework

## Keywords

- [Intro to Gherkin](https://www.youtube.com/watch?v=Twx4MtrfuSg)
- [Gherkin Reference](https://cucumber.io/docs/gherkin/reference/)

- 1x `Feature`: a description of what the UUT is supposed to do, as documentation
- 1x `Scenario`: describes the test case itself
- nx `Given`: context, most cases, describes something in the past
- `When`: event, trigger by anothe system or by the user
- `And`, `But`: repeat last keyword
- `Then`: outcome, expected result
- `Examples`: table with variables (parameters), whose lines are individual tests,
  use the `<>` (delimited parameters) symbol to cite these variables
  - Use `Scenario Outline` instead of `Scenario` when using the `Examples`
    keyword

Synonyms:

- `Example` = `Scenario`
- `Scenario Outline` = `Scenario Template`
- `Examples` = `Scenarios`

Basically:

```
Feature feature title

Scenario test case description
    Given preconditions or initial context
    When event or trigger
    Then expected output
```

- Tags (`@mytag`) can also be added to bundle test scenarios togueder
- Tags may be used more that once (on the same line)
- Descriptions are any text that does not start with a keyword.

### Examples

```
Feature Authentification Background

Authentification runs on the background and can have different outcomes.

# This is a comment

@authentification @basic
Scenario: Successful user authentification
Given user is already registered to the website
Given user is on the login page
When user inputs the correct email address
And user inputs the correct password
And user clicks the login button
Then user should be authentificated
And user should be redirected to their dashboard
And user should be presented with a success message
```

## recommendations

- [How to write Gherkin scripts](https://www.youtube.com/watch?v=i0Q5orC5jSQ)

Start from the end, with a `Then` statement. Then proceed backwards with `When`
and `Given`. Finally translate this whole mess into different `Feature`s. Add
tags where useful.

- Do not cover user interactions in the `Given` keyword
- Only use `When` once

## filetypes

Each file corresponds to a `Feature` and the extention is `.feature`. Convention
is to translate the feature into lower snake case.

## translations

You may write Guerkin in languages other than English. For this, add a `#
language: <code>` at the start of the file. Language codes are, `no` for
Norwegian eg.
