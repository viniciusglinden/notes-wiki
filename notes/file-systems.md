
# File system

A file system (FS) is all of the following:

1. Data storage: The primary function of any file system is to be a structured place to store and retrieve data.
2. Namespace: A naming and organizational methodology that provides rules for naming and structuring data.
3. Security model: A scheme for defining access rights.
4. API: System function calls to manipulate file system objects like directories and files.
5. Implementation: The software to implement the above.

## Inode

> The inode (index node) is a data structure in a Unix-style file system that describes a file system object such as a file or a directory. Each inode stores the attributes and disk block location(s) of the object's data. File system object attributes may include metadata (times of last change, access, modification), as well as owner and permission data.

From [An introduction to Linux's EXT4 file system](https://opensource.com/article/17/5/introduction-ext4-file system)

> What is an inode? Short for index-node, an inode is a 256-byte block on the disk and stores data about the file. This includes the file's size; the user IDs of the file's user and group owners; the file mode (i.e., the access permissions); and three timestamps specifying the time and date that: the file was last accessed, last modified, and the data in the inode was last modified.

## Bitmap

Maps what fields are used. Thus a Inode bitmap maps what inodes are being used, a block bitmap maps what blocks are being used, etc.

## EXT4 file system

AS = Address Space (disk)

The first 1024B of AS are reserved for boot. The rest of the AS is then divided in Block Groups (BG), with each BG the listed fields below (in order):

1. Super Block (SB)
2. Group Descriptor Table (GDT)
3. Block Bitmap
4. Inode Bitmap
5. Data Blocks

Note that, only the Data Blocks is data, the rest is metadata. Each block is further divided.

Journaling will vary depended on the implementation.

## Journaling

There are currently three modes of journaling:

1. Write-back mode, which only writes the modified metadata to the journal area.
2. Ordered mode. Similar to write-back mode, ordered mode also writes the
modified metadata to journal area.  But it provides higher consistency insurance
than write-back mode by strictly writing journals after file data is written.
3. Journal mode. In this mode, both metadata and data are written to the journal
area first. The performance of journal mode is worse than the previous two modes
for all information are written twice. The file system first writes the dirty
inode in memory to the journal area and then update the physical inode in the inode table.
