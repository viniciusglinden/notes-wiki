
# Mobile Industry Processor Interface (MIPI)

MIPI Alliance created multiple busses.

## MIPI Radio Frequency Front-End (RFFE)

- Front-End: control and configuration of amplifiers, antenna sensors, etc,
  everything considered a RFFE slave
- It is a two-wire interface, with a bus frequency up to 52 MHz and mechanisms
  for timing-critical functions
- Used to connect RFIC to RFFE Slaves


