
# FPGA

- [nandland](https://nandland.com/) great for beginners and refreshing concepts,
  VHDL and Verilog
- [vhdlwhiz](https://vhdlwhiz.com/) if nandland does not work, vhdlwhiz will
  probably do
- [zipcpu](https://zipcpu.com/) good training material on formal verification
  methodology, typically DSP or formal verification related
- [thedatabus](https://thedatabus.io/) machine learning, HLS, cocotb (auto
  verification tool) posts
- [makerchip](https://makerchip.com/) web IDE, focus on teaching TL-Verilog
- [controlpaths](https://www.controlpaths.com/) FPGA and DSP (FIR and IRR
  Filters)
- [F4PGA](https://f4pga.org/) open-source toolchain for FPGA development
- [Online HDL IDE](https://www.edaplayground.com/)

The EDIF/EDF extension is the IP as a blackbox.
Yosys is Verilog only tool, but with the help of GHDL, it can translate from VHDL to Verilog and then compile it with 100% FOSS tooling.

https://tomverbeure.github.io/2022/11/18/Primitive-Transformations-with-Yosys-Techmap.html

## VHDL Verilog mixed use

https://github.com/im-tomu/fomu-workshop/tree/master/hdl

## Debugging soft CPUs

- [GDBWave - A Post-Simulation Waveform-Based RISC-V GDB Debugging
  Server](https://tomverbeure.github.io/2022/02/20/GDBWave-Post-Simulation-RISCV-SW-Debugging.html)
