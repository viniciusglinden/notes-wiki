
# Debian Packaging

- [building a binary package using
  dpkg-buildpackage](https://github.com/FooBarWidget/debian-packaging-for-the-modern-developer/blob/master/tutorial-2/README.md)
- [A Minimum Complete Example of Debian
  Packaging](https://metebalci.com/blog/a-minimum-complete-example-of-debian-packaging-and-launchpad-ppa-hellodeb/)

- `dpkg-deb`: low-level tool to build packages
- `dpkg-buildpackage`: high-level tool to build packages, uses `dpkg-deb`
