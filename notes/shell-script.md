
# Shell script

- [explainshell](https://explainshell.com/): explain raw shell commands
- [Bash Cheatsheet](https://devhints.io/bash)
- [Bash Cheatsheet](https://quickref.me/bash.html)
- [Google Shell Style
  Guide](https://google.github.io/styleguide/shellguide.html)
- [Bash Unit Test Framework](bach.sh)

For commands, also see [commands](commands.md)

## Exit codes

|number|meaning|comment|
|-|-|-|
|0|success|`exit 0` = `exit`|
|1|matchall for general errors|miscellaneous erros, such as "divide by zero"|
|2|misuse of shell builtins|missing keyword or command, or permission problems|
|126|command invoked cannot execute|permission problem or command is not an executable|
|127|command not found|possible problem with `$PATH` or a typo|
|128|invalid argument to exit|exit takes only integers within range 0 to 255|
|128+n|fatal error signal "n"||
|130|script terminated by ctrl+c|ctrl+c is a fatal error signal 2 (128+2=130)|
|255|exit status out-of-range||

## command line redirection

- [Bash one liners explained part three](https://catonmat.net/bash-one-liners-explained-part-three)

There are three IO devices available on the command line:

* 0: standard in
* 1: standard out
* 2: standard error

- To redirect **only** standard out to a file (and overwrite the file), use: `command > file.log`
- To do the same, without overwriting (append/concatenate): `command >> file.log`
- To redirect standard error to the file.log, use: `command 2> file.log`
- And to append: `command 2>> file.log`
- To combine the outputs into one stream and send them all to one place: `command > file.log 2>&1`
  - This sends 2 (`stderr`) into 1 (`stdout`), then sends `stdout` to `file.log`
- To redirect standard in into a command that expects `stdin`: `command << file.txt`
- Hide stdout (`/dev/null` pseudo-device): `./command > /dev/null`
- Hide stdout and stderr: `./command > /dev/null 2>&1`
  - Shorthand in bash: `./command &> /dev/null`
- Hide stdout and stderr and release terminal (run the command in background): `./command > /dev/null 2>&1 &`
- Close stdout file descriptor: `exec 1<&-`
- Open stdout as file.log for read and write: `exec 1<>file.log`

## difference between " and '

`"` will perform the interpolation, `'` will be taken as litteral:

```sh
e=$HOME vln
# dash: 1: vln: not found
i="$HOME vln"
y='$HOME vln'
echo $e
# /home/vln
echo $i
# /home/vln vln
echo $i
# $HOME vln
```

- No `"`: `$var` takes the value of `var`, splits it into whitespace-delimited parts, and interprets
  each part as a glob pattern

Nested `"` are valid:

```sh
echo "example: $(printf "a + b = %d" "${a}")"
```

[What is the difference between the
quotes](https://unix.stackexchange.com/questions/503013/what-is-the-difference-between-the-and-quotes-in-th)

- `'...'` is entirely literal.
-  "..."` allows both variables and embedded quote characters.
-  $'...'` performs character escapes like \n, but doesn't expand variables.
-  $"..."` is for human-language translations in Bash and ksh.

## flags

```bash
help set
```

The current set of flags can be found using `$-`.

- `set -e`: aborts on first error
- `set -u`: aborts if you reference a variable that does not exist
- `set -o pipeline`:  pipeline return value is the status of last command to
  exit with a non-zero status or zero if no command exited

## cat / heredoc

We use the here-document redirection operator "<< MARKER". This operator
instructs bash to read the input from stdin until a line containing only
`MARKER` is found.

```sh
cat << EOF
This line
and this line
and another one
EOF
```

When inputting to a file:

```sh
cat << EOF > file
This line
and this line
and another one
EOF
```

## sh test operations

Reference: `man test`

- *flag*: true expression
- -b: the FILE exists and is a block special file.
- -c: the FILE exists and is a special character file.
- -d: the FILE exists and is a directory.
- -e: the FILE exists and is a file, regardless of type (node, directory, socket, etc.).
- -f: FILE exists & is a regular file (not a directory or device).
- -G: FILE exists & has the same group as the user running the command.
- -h: FILE exists & is a symbolic link.
- -g: FILE exists & has set-group-id (sgid) flag set.
- -k: FILE exists & has a sticky bit flag set.
- -L: FILE exists & is a symbolic link.
- -O: FILE exists & is owned by the user running the command.
- -p: FILE exists & is a pipe.
- -r: FILE exists & is readable.
- -S: FILE exists & is socket.
- -s: FILE exists & has nonzero size.
- -u: FILE exists & set-user-id (suid) flag is set.
- -w: FILE exists & is writable.
- -x: FILE exists & is executable.
- -z: variable is uninitialized or empty

See `man test`.

An empty string is evaluated to false.

## arrays

Arrays are not defined in `sh` (POSIX compliance). Use `bash` or `zsh` instead.
The `zsh` indexing is 1-based, whereas the `bash` is 0-based.

- How to define an array: `a=(this is a test)`[^arrays-1]
- To declare an empty array: `declare -a array`[^arrays-2]
- How to display the n'th member: `echo ${a[n]}`
- Display the complete array: `echo ${a[@]}`
- Display its length: `echo ${#a[@]}`
- Display from a particular n member forward: `echo ${a[@]:n}`
- Display from a particular n member to the m member: `echo ${a[@]:n:m}`

You can also do search and replace within a variable: `echo ${arr[@]//a/A}`

Bash allows you to reference element 0 of an array variable using scalar notation: instead of
`${arr[0]}`, you write `$arr`; in other words: if you reference the variable as if it were a scalar,
you get the element at index 0.

[^arrays-1]: you may also use this as `x=($(sed "s/,/\r/g" file))` for example.
[^arrays-2]: `typeset` is an alias for `declare`.

## storing a command's output

To read a command's output into a variable use `$()`, backticks or Piping.
Example: `arch=$(uname -m)`

The advantage of using `$()` over backticks is that you can cascade them.
Example: `filelist=$(rpm -ql $(rpm -qa))`

## empty strings

An empty string evaluates to false. Example:

```sh
[ $result ] && echo "this will run if $result is not empty"
```

This does not work with `set -u` or `set -nounset` option.
Possible improved solutions:

```sh
[ -z "$result" ] | result="foo"
: "${result:=foo}"
```

## empty directory

```sh
if [ "$(ls -A $DIR)" ]; then
     echo "Take action $DIR is not Empty"
else
    echo "$DIR is Empty"
fi
```

## arithmetic

`expr` is deprecated, use `$(( ))` instead. Example: `$(( 1 + 2 ))`.
Within if statements, you can use normal symbols (i.e. >, <, >=, etc)

## for loops

Here is an example for a for-loop. It makes a Backup of all text files:

```sh
for i in *.txt; do # not functional
	Cp $i $i.bak
done

for i in $(seq 1 1 3)
do
	echo $i
	[ "$i" -eq 3 ] && echo "breaking the for loop" && break
done
```

Note: if you are using bash, instead of writing `for i in $(seq 1 3)`, you could write `for i in
{1..3}`.

## negations

! is the negation operator. Example: `[ ! "$x" = "Linden" ] && echo "Not Linden"`

## functions

Function definition and use is as the example below:

```sh
greet() {
	echo "Hello, $person!"
}
person="Linden"
greet
```

Note that inside a function `$1` (and others) have different meaning than
outside the function.

## react on `CTRL_C`

The command trap allows you to trap `CTRL_C` keystrokes so your script will not be aborted

```sh

#!/bin/sh

trap shelltrap INT

shelltrap()
{
    echo "You pressed CTRL_C, but I don't let you escape";
}

while true; do read line; done
```

Note: You can still pause your script by pressing `CTRL_Z,` send it to the background and kill it there. To catch `CTRL_Z,` replace `INT` by `TSTP` in the above example. To get an overview of all signals that you might be able to trap, open console and enter `kill -l`

## get parent folder name

Steps are:

1. get the absolute path to the script
2. replace `/` with `\n`
3. use tail command to get the last line

```sh
DIR="$(dirname "$(readlink -f "$0")")"
DIR="$(echo "$DIR" | tr "/" "\n")"
parentfolder=$(echo "$DIR" | tail -1)
echo "$parentfolder"
```

Alternatively (recommended):

```sh
DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" & >/dev/null && pwd)"
parentfolder="$(basename -- "${DIR}")"
```

Note: `${BASH_SOURCE[0]}` will have the name of the file - not who it has been
called (different than C) - same as `$0`.

## temporary files

```sh
tmpfile=$(mktemp /tmp/my-script.XXXXXX)
```

## Special variables

* `$0` The filename of the current script
* `$n` The arguments with which a script was invoked, `n` corresponds to the
  argument
* `$#` The number of arguments supplied to a script
* `$*` **All the arguments are double quoted**. If a script receives two
  arguments, `$*` is equivalent to `"$1 $2"`
* `$@` All the arguments are **individually double quoted**. If a script
  receives two arguments, `$@` is equivalent to `"$1" "$2"`
* `$?` The exit status of the last command executed
* `$$` The process number (PID) of the current shell. For scripts, this is the
  process ID under which they are executing
* `$!` The process number of the last background command
- `$-` Shell options
- `IFS` Internal Field Separator, word splitting after expansion and to split
  lines into words (read, for i in, etc)

```sh
cp file1.txt file2.txt
$0 $1        $2
   |-----------------|
   $@
```

```sh
read name
echo "hello $name"
```


```bash
print_arguments "Hello" "World" "with spaces"

# Output using $@:
# Hello
# World
# with spaces
#
# Output using $*:
# Hello World with spaces

## read builtin commands
```

It is a common mistake to write `read $name`.

`read` simply reads the user input and stores in variable `REPLY`, if no
variable is provided.

- `-p` hint text
- `-n` specify maximum character length
- `-s` do not echo while typing
- `-r` consider backslash as a character

```sh
read -p "Type in text"
echo $REPLY
read -p "Now using variable my_var" my_ var
read -p "Type values for VAR1 and VAR2" VAR1 VAR2
```

You can also sort strings/values into multiple variables. Use the `IFS` special
variable to change this behavior.

```sh
read NOME MEIO FAMILIA << (echo Vinícius Gabriel Linden)
```

Use the following to read a CSV file and sort columns into variables.
Note that the script is **reading** a file, not echoing a variable.

```sh
file=/tmp/domains.txt

# set the Internal Field Separator to ,
IFS=','
while read -r domain ip webroot ftpusername
do
	printf "*** Adding %s to httpd.conf...\n" $domain
	printf "Setting virtual host using %s ip...\n" $ip
	printf "DocumentRoot is set to %s\n" $webroot
	printf "Adding ftp access for %s using %s ftp account...\n\n" $domain $ftpusername
done < "$file"
```

If you want to read into an array:

```sh
cat demo.txt
1 2 3
4 5 6
7 8 9
readarray myarray < demo.txt
echo ${myarray[1]}
4 5 6
```

## Default variable values

```sh
FOO=${VARIABLE:-default}  # If variable not set or null, use default.
FOO=${VARIABLE:=default}  # If variable not set or null, use default and set it to default.
```

The first one is recommended.

## Variable manipulation

Here, "match" means whatever is after the operator.

- `#`: remove the smallest *prefix* of the expansion matching the pattern
- `##`: remove the biggest *prefix* of the expansion matching the pattern
- `%`: remove the smallest *suffix* of the expansion matching the pattern
- `%%`: remove the biggest *suffix* of the expansion matching the pattern

Therefore, if `var="this is a test"`:

- `${var#t*is}`: prints " is a test"
- `${var##t*is}`: prints "a test"
- `${var%t*st}`: prints "this is a"
- `${var%%t*st}`: prints empty string

Suppose we have `var="apple orange"` then we do:

- `echo ${var%% *}` will only print apple;
- `echo ${var##* }` will only print orange;

Thus, `${var%% *}` means, to read `var` and delete a space followed by any
string from the start of the variable.

## select text columns

Suppose we have `file.csv` (comma separated) and we want only the second and
third column:

```sh
cut -d, -f 2,3 file.csv
```

## Mod function

```sh
$(( remainer = number % divisor ))
```

Examples:

- `-3 % 2` = -1
- `12 % 4` = 0
- `54 % -7` = 5

## if statement

```sh
if [ expression 1 ]
then
elif [ expression 2 ]; then
elif [ expression 3 ]
then
else
fi
```

For multiple conditions use:

- AND: `&&` or `-a`
- OR: `||` or `-o`

```sh
[ "$type" = "svg" -o "$type" = "png" ]
[ "$type" = "svg" ] || [ "$type" = "png" ]
```

Note: `=~` is the same as `==`, but with regex `man regex` [bash only]. For example `[[ "${1}" =~
def* ]]` will match anything starting with "def". Note that is does not need a `*` at the end.

## numeric comparisons

```sh
[ $x -ge $y ]
[ -ge $x $y ]
```

Possible values;

- `ge` greater than or equal to
- `gt` greater then
- `le` less than or equal to
- `lt` less than
- `ne` not equal to
- `eq` equal to

Note that this is for integers. The "<" or ">" operators, compare strings:

- `<` sorts before, lexicographically
- `>` sorts after, lexicographically

## text colors

Note: Sequences using RGB values only work in 24-bit true-color mode.

|Sequence|Description|Value|
|-|-|-|
|`\033[38;5;<NUM>m`|Set text foreground color|0-255|
|`\033[48;5;<NUM>m`|Set text background color|0-255|
|`\033[38;2;<R>;<G>;<B>m`|Set text foreground color to RGB color|R, G, B|
|`\033[48;2;<R>;<G>;<B>m`|Set text background color to RGB color|R, G, B|

Text Attributes

|Sequence|Description|
|-|-|
|`\033[m`|Reset text formatting and colors|
|`\033[1m`|Bold text|
|`\033[2m`|Faint text|
|`\033[3m`|Italic text|
|`\033[4m`|Underline text|
|`\033[5m`|Slow blink|
|`\033[7m`|Swap foreground and background colors|
|`\033[8m`|Hidden text|
|`\033[9m`|Strike-through text|

Use the `-e` option on `echo`.

## until

Runs content while COMMAND returns exit code not zero.

```sh
until CONDITION
do
	# do some stuff
done
```

## single-line boolean logic

```sh
[ "$1" = "Oi" ] && {cmds if true;} || {cmds if false;}
```

## get script and path names

```sh
name=$(basename "$0")
echo "This is $name script"
DIR=$(dirname "$(readlink -f "$0")")
```

## cd to the script directory

```sh
cd $(dirname $0)
```

## help message suggestion

```sh
PROG="shutdown"

usage () {
cat <<EOF

$PROG [ -h | -p | -r ] time

$PROG <options> time

options :
    -h: halt the machine
    -p: poweroff the machine
    -r: reboot the machine
EOF
exit 111
}

die() {
    printf "${PROG}:fatal: options must be set first\n" >&1
    usage
}

[ -z ${1} ] && die
```

## use unaliased command

Example use case: when making an script as `. script`, all aliases are imported.

To use the original command inside the script, just quote it: `"command"`.

## terminating list of parameters

For some commands (specially builtin), there is a `--` argument, that marks end of parameter
(option) list.

## `$BASH_SOURCE` vs `$@`:

`${BASH_SOURCE[0]}` (or simply `$BASH_SOURCE`) contains the (potentially relative) path of the
containing script in all invocation scenarios, notably also when the script is sourced, which is not
true for `$0`.

```sh
#!/bin/bash
echo "[$0] vs. [${BASH_SOURCE[0]}]"
```

```
$ bash ./foo
[./foo] vs. [./foo]
$ ./foo
[./foo] vs. [./foo]
$ . ./foo
[bash] vs. [./foo]
```

## Get the function's name

- [Bash environment variables](https://www.shell-tips.com/bash/environment-variables/)

Use `${FUNCNAME[0]}`.

## disable alias temporarily

`\cmd`, `'cmd'`, `"cmd"`, `command cmd`.

## syntax for inputting options (getopts)

- [getopt](https://www.shellscript.sh/tips/getopt/index.html)

`getopt` is an external tool, while `getopts` is a bash embedded function.

```sh
#!/bin/sh

OPTERR=0 # suppress showing of errors (not recommended)
while getopts "ab:" option; do
    case "${option}" in
        a)
            echo "a"
            ;;
        b)
            echo "b = ${OPTARG}"
            ;;
        *)
            printf "Invalid option: -%s\\n" "${OPTARG}"
            exit 1
            ;;
    esac
done
shift "$((${OPTIND} - 1))" # remove the options from $@
```

Note: `shift $(($OPTIND -1))` will not work if user puts options after
non-option arguments.

or

```bash
#!/bin/bash

unset DB_DUMP TARBALL ACTION

while getopts 'srd:f:' c
do
  case $c in
    s) ACTION=SAVE ;;
    r) ACTION=RESTORE ;;
    d) DB_DUMP=$OPTARG ;;
    f) TARBALL=$OPTARG ;;
  esac
done

if [ -n "$DB_DUMP" ]; then
  case $ACTION in
    SAVE)    save_database $DB_DUMP    ;;
    RESTORE) restore_database $DB_DUMP ;;
  esac
fi

if [ -n "$TARBALL" ]; then
  case $ACTION in
    SAVE)    save_files $TARBALL    ;;
    RESTORE) restore_files $TARBALL ;;
  esac
fi
```

Or, when using in functions, declare OPTIND as local:

```bash
#!/bin/bash
t() {
  local OPTIND
  getopts "a:" OPTION
  echo Input: $*, OPTION: $OPTION, OPTARG: $OPTARG
}
```

## longopts

Note: this uses getopt (instead of getopts). An alternative is presented
[here](http://abhipandey.com/2016/03/getopt-vs-getopts/).

```bash
#!/bin/bash
# Set some default values:
ALPHA=unset
BETA=unset
CHARLIE=unset
DELTA=unset

usage()
{
  echo "Usage: alphabet [ -a | --alpha ] [ -b | --beta ]
                        [ -c | --charlie CHARLIE ]
                        [ -d | --delta   DELTA   ] filename(s)"
  exit 2
}

PARSED_ARGUMENTS=$(getopt -a -n alphabet -o abc:d: --long alpha,bravo,charlie:,delta: -- "$@")
VALID_ARGUMENTS=$?
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

echo "PARSED_ARGUMENTS is $PARSED_ARGUMENTS"
eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    -a | --alpha)   ALPHA=1      ; shift   ;;
    -b | --beta)    BETA=1       ; shift   ;;
    -c | --charlie) CHARLIE="$2" ; shift 2 ;;
    -d | --delta)   DELTA="$2"   ; shift 2 ;;
    # -- means the end of the arguments; drop this, and break out of the while loop
    --) shift; break ;;
    # If invalid options were passed, then getopt should have reported an error,
    # which we checked as VALID_ARGUMENTS when getopt was called...
    *) echo "Unexpected option: $1 - this should not happen."
       usage ;;
  esac
done

echo "ALPHA   : $ALPHA"
echo "BETA    : $BETA "
echo "CHARLIE : $CHARLIE"
echo "DELTA   : $DELTA"
echo "Parameters remaining are: $@"
```

```sh
./alphabet.sh -a -b -c charlie -d river lorem ipsum
./alphabet.sh --alpha --bravo --charlie charlie --delta river lorem ipsum
```

## Progress bar

```bash
progress() {
    count=0
    until [ $count -eq 7 ]; do
        echo -n "..." && sleep 1
        ((count++))
    done;
    if ps -C $1 > /dev/null; then
        echo -en "\r\e[K" && progress $1
    fi
}
```

## Random

Not posix compliant:

```bash
${RANDOM}
```

## Lower case - Upper case

In bash 4.0:

```bash
x="ThIS IS a Test"
echo "${x,,}" # this is a test
x="this is a test"
echo "${x^^}" # THIS IS A TEST
```

## debug, breakpoint

```sh
breakpoint() {
    set +x
    trap - debug
    set -o functrace
    while read -p "Type in command: " -sn 1 cmd; do
        echo
        case "${cmd}" in
            [cC])
                set -x
                echo "continuing"
                break
                ;;
            [sS])
                trap read debug
                echo "continuing"
                set -x
                break
                ;;
            *)
                eval "${cmd}"

        esac
    done
}
```

Also
[see](https://unix.stackexchange.com/questions/155551/how-to-debug-a-bash-script)

## using pipes

```bash
#!/bin/bash

declare -a PIPE
PIPE[0]=/tmp/pipe_0
PIPE[1]=/tmp/pipe_1

clean_up() {
    rm -f "${PIPE[@]}"
}

if [[ -p "${PIPE[0]}" ]]; then
    echo "secondary"
    while :; do
        echo "sent from the other shell:"
        if read line < "${PIPE[0]}"; then
            echo "${line}"
        fi
        echo "send to the other shell:"
        if read line; then
            echo "${line}" > "${PIPE[1]}"
        fi
    done
else
    echo "primary"
    for pipe in "${PIPE[@]}"; do mkfifo "${pipe}"; done
    trap "clean_up" EXIT
    while :; do
        echo "send to the other shell:"
        if read line; then
            echo "${line}" > "${PIPE[0]}"
        fi
        echo "sent from the other shell:"
        if read line < "${PIPE[1]}"; then
            echo "${line}"
        fi
    done
fi
```

## Process substitution

- [Process
  Substitution](http://www.gnu.org/software/bash/manual/html_node/Process-Substitution.html)
- [What does "< <(command args)" mean in the
  shell?](https://stackoverflow.com/a/2443120/12932725)

`<()` is similar to a pipe, but passes an argument in form of a FIFO block
device intead of using stdin. Same goes for `>()`. In zsh, `=()` creates a
temporary file instead of a pipe.
