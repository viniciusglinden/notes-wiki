# groff

- [groff macros (with
  examples)](https://www.stephenlindholm.com/groff_macros.html)
- [Scientific
  paper example](https://github.com/SudarsonNantha/LinuxConfigs/tree/master/.config/groff/Examples/paper)
- [Manual](https://www.gnu.org/software/groff/manual/groff.pdf)

groff is a minimal text editor - like latex, but more minimalist. Default
behavior is output a postscript file (`.ps`) to `stdout`. To print as PDF, use a
command like:

```sh
groff -ms -k groff.ms -T pdf > groff.pdf
```

Each line is interpreted as instructed. This means that, if you use the title
macro and want to write a paragraph afterwards, you have to use the paragraph
macro.

You always insert the macro at the start of the line. Double-quotes will
indicate the argument (bold, italic) - use the escape character otherwise.

Each new line adds a space. Macro formatting are as follows: .MACRO after
before. Therefore (in `ms`):

```sh
.B "bold"
!
.B "bold" ). (
```

This will print

```
bold!
(bold).
```

groff's siblins are:

- roff: original typesetting system
- groff: GNU roff
- troff: another version of roff
- nroff: plain text for screens and printers

groff must compile with the `-k` or `-K utf8` flag in order to use UTF-8
characters. This employs the `preconv` preprocessor.

According to its `man` page, example files are located at
`/usr/share/doc/groff/example`. However, I was not able to find it. Use instead
`info groff` and, most usefully (for ms macro) `man groff_ms`.

## references

- [troff documentation](troff.org/papers.html)

## macros

You have to chose a macro. Default macro that people use is `ms`.

### manuscript (ms)

File extension is `.ms`. Compilation is:

```sh
groff -ms groff.ms
```

- .TL title
- .AU author
- .AI institution
- .AB start abstract
- .AE end abstract
- .PP indented paragraph text: double space will insert a new paragraph without
  indentation
- .LP unindented paragraph text: double space will insert a new paragraph without
  indentation
- .UH new unnumbered heading (section)
- .NH new numbered heading (section)
- .NH 1 new numbered heading (section)
- .NH 2 second numbered heading (subsection)
- .SH new unnumbered heading (section)
- .SH 1 new unnumbered heading (section)
- .SH 2 second unnumbered heading (subsection)
- .B bold
- .I italic
- .BI bold italic
- .RS start indentation
- .RE end indentation
- .UL underline
- .BX boxed text
- .IP list item (just the indentation)
- .IP \(bu list item with the bullet points (weird space between text and point)
- .IP \(bu 2 list item with the bullet points with two points of padding
- .de begin definition of a macro (see below)
- .. end definition of a macro (see below)

```groff
.de BL
.IP \(bu 2
..
```

- .so macros source the `macros` macro file

## preprocessors

If you need to input something like mathematical equations, references or images,
you have to use a preprocessor. Therefore, instead of feeding the file
directly to groff, you _preprocess_ it on the command line, or to use the
preprocessor flag.

### eqn

Typical use is:

```sh
eqn groff.ms -Tpdf | groff -ms -Tpdf > groff.pdf
groff -e groff.ms -ms -Tpdf > groff.pdf
```

- .EQ begin equation
- .EN end equation

```groff
.EQ
define disc `b sup 2 -4ac`
.EN
.EQ
b sup 2-4ac != +- 5 sub x
.EN
.EQ
phi = {1 + sqrt 5} over 2
.EN
.EQ
s = sqrt {
{sum from i=1 to N (x sub i - x bar ) sup 2}
over
N-1
}
.EN
.EQ
pi = int from -1 to 1 dx over sqrt {1-x sub 2}
.EN
.EQ
disc
.EN
```

This will yield the equivalent of

```latex
b^{2-4ac} \neq \pm 5_x \\
\phi = \frac{1 + \sqrt{5}}{2}
s = \sqrt{\frac{\sum^N_{i=1}{(x_i - \bar{x})}^2}{N-1}}
\pi = \int_{-1}^1 \frac{\mathrm{d}x}{\sqrt{1-x^2}}
b^2 - 4ac
```

- `delim` will define the limits of the equation: useful for inline math

```groff
.EQ
x
delim $$
.EN
.PP
This is a text $sum 1 to 10$
text continuation
.EQ
delim off
.EN
I bought this for $10.
```

### refer

```sh
refer -SPe -p$REFBIB groff.ms | groff -ms -T pdf > groff.pdf
```

(Parenthesis type of citation with footnote changed to rolling bibliography).

Use `man refer` to see what the macros are, both for the citation file and for
the groff file.

- %K tag (optional)
- %A author
- %T title
- %B book
- %E editor
- %I publisher
- %D date


- .[ begin reference
- .] end reference
- .R1 begin additional options (remember to insert the key-word "accumulate" so
  not to overwrite the command line)
- .R2 end additional options
