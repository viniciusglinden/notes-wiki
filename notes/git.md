
# Git

- [Cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
- [Visually test commands](https://learngitbranching.js.org/?NODEMO)
gitconfig entries are case insensitive. Check `git help config` to see all the
available configurations.
- [GitLab Documentation](https://docs.gitlab.com/)

## Git global setup

```sh
git config --global user.name "Vinícius Gabriel Linden"
git config --global user.email "viniciusglinden@pm.me"
git config --global core.autocrlf input # line ending
```

You can also set a global gitignore:

```sh
git config --global core.excludesfile ~/.gitignore
```

## Push an existing folder

```sh
git init
git remote add origin https://gitlab.com/viniciusglinden/note-taking.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

## Push an existing Git repository

```sh
git remote rename origin old-origin
git remote add origin https://gitlab.com/viniciusglinden/note-taking.git
git push -u origin --all
git push -u origin --tags
```

## commit

- [Conventional Commits](https://www.conventionalcommits.org/): a git commit
  specification

### changing a commit author

If it was the last one and you want to change to your own email and name:

```sh
git commit --amend --reset-author
```

If it is a past commit, go to the commit via `git rebase -i commit~1`.

### Tailers

- [Git commit messages with
  attributes](https://zerokspot.com/weblog/2020/10/24/git-commit-messages-with-attributes/)

Trailers are key-value pairs written inside of commit messages that, for
instance, make things like references to JIRA tickets programmatically
accessible.

```
Title

Some longer exaplanation.

JIRA-Ref: PROJ-123
```

## Cloning wikis from git

### Bitbucket

```sh
git clone http://bitbucket.org/MY_USER/MY_REPO/wiki
```

It will clone into the folder wiki, not MY_REPO/wiki.

[Clone and edit
pages](https://confluence.atlassian.com/bitbucket/clone-and-edit-pages-317195798.html)

### Github

```sh
git clone git@github.com:myusername/foobar.wiki.git
```

[How to automatically do full backup of repo, wiki, issue tracker etc. in
bitbucket?](https://stackoverflow.com/questions/19218995/how-to-automatically-do-full-backup-of-repo-wiki-issue-tracker-etc-in-bitbuck)

### Gitlab

```sh
git clone git@gitlab.com:myuserbane/foobar.wiki.git
```

## editing not-pushed commit message

This includes changing the commit message and adding more modifications.

```sh
git commit --amend
```

## merging while keeping the branch

If you only do a `git merge`, if there is no other commit in the branch, it will
automatically suppress the merged branch. Use `git merge -no-ff` instead.

## Rescuing deleted git files

* `git rev-list -n 1 HEAD -- <file_path>` gives last commit ID since file_path changed
* `git checkout <deleting_commit>^ -- <file_path>` checks-out this deleting_commit at file_path
* `git checkout $(git rev-list -n 1 HEAD -- "file_path")^ -- "file_path"` will rescue file_path from last commit

## SSH key

You have to install `openssh` package.

* `ssh-keygen -t ed25519 -C "viniciusglinden@gmail.com" (ed25519 is the key type)
* Leave it with no filename and no password, if you don't want to be asked for this password
* This creates a file as `~/.ssh/id_ed25519` and `~/.ssh/id_ed25519.pub` (both files contain the same information)
* Add this key to git user configuration, by pasting the contents of the `.pub` file and naming this to identify which computer it is

You may need to type in `ssh-add`.

SSH use for a repository is configured with git commands. You may clone it as an
SSH repository

## Issue vs Hotfix

A hotfix is an issue which rises from the necessity to act immediately upon an
undesired state of a live production version.

## git-flow methodology

- [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)

Note: this is an example git-flow methodology

There are two **official** branches:

1. master: main branch, only containing codes that have been accepted by the team
2. develop: proposed codes, integrated via merge-request

So the flux goes:

1. an issue is created in the project's board, with a corresponding hashtag issue (e.g. #9)
2. when the developper is available, he will go to his local repository and create a branch with the same hastag issue

```sh
git checkout develop
git pull origin develop
git checkout -b issue_9
```

3. local commits will be done in this branch, if necessary
4. when he decides that the task has been solved, he will make a commit mentioning the issue

```sh
git commit -s -m 'issue #9 - Add git-flow instructions'
```

5. He will then make a merge-request to the develop branch
6. another developed is then tasked to review and approved the merge-request
7. once approved, the developer will move the board's task from "doing" to "closed"

You may be interested in
[git-flow](https://danielkummer.github.io/git-flow-cheatsheet/)

## Gerrit

Gerrit is a code review from Google.

## writing good commit messages

[How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)

## merge conflicts

[Take the pain out of git conflict resolution: use
diff3](https://blog.nilbus.com/take-the-pain-out-of-git-conflict-resolution-use-diff3/)

Merge conflicts of one file are marked inside this one file - there is no
supplementary files.

Terminology:

* LOCAL: current branch
* REMOTE: merging branch
* BASE: common ancestor
* MERGE: merged version

You can change the "style" of a merge conflict by inputting the following in the
terminal:

```sh
git config --global merge.conflictstyle diff3
```

This changes the style to:

```
<<<<<<< ours
||||||| base
=======
>>>>>>> theirs
```

If not effective, one may do the following:

```sh
git checkout --conflict=diff3 <file>
```

This may be explained like the following:

```
<<<<<<< HEAD
How code looks like at HEAD
for rebase, this will be the rebase target
||||||| merged common ancestor
ancestor code
=======
How code looks like at merged-branch
>>>>>>> merged-branch
```

### select changes from current or from another branch

[git - ours \& theirs](https://nitaym.github.io/ourstheirs/)

You may apply diff3 as above and then `git diff` to display the difference. The
following is self-evident.

#### merge

On branch `feature`, **merge** `master`:

```sh
git checkout feature
git merge master

Auto-merging Document
CONFLICT (content): Merge conflict in codefile.js
Automatic merge failed; fix conflicts and then commit the result.
```

Select changes done on

- feature: `git checkout --ours codefile.js`
- master: `git checkout --theirs codefile.js`

and then continue:

```sh
git add codefile.js
git merge --continue
```

#### rebase

On branch `feature`, **rebase** `master`:

```sh
git checkout feature
git rebase master

First, rewinding head to replay your work on top of it...
Applying: a commit done in branch feature
error: Failed to merge in the changes.
```

Select changes done on

- master: `git checkout --ours codefile.js`
- feature: `git checkout --theirs codefile.js`

then continue as normal:

```sh
git add codefile.js
git rebase --continue
```

Upon conflicts:

- HEAD is the `master` code
- incoming is the `feature` code

## Hooks

Hooks are scripts that run when some git command is run. They are located at
`.git/hooks`. By default, this folder is filled with examples, with headers
commentating what they are about. Make sure they are executable.

Hook files must not have an extension, otherwise they will not be run.

## Bare

Instead of putting the git files inside the `.git/`, put them somewhere else.

This will also cause the `remote.origin.fetch` value not to be populated = you
cant have a bare and use `fetch` by default. For this, run:
`git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"`.

## Dotfiles with git bare

Git bare is a type of git repository which does not create the typical `.git`
folder. Instead, it places all the git files in the root.

Suppose we call the folder `.dotfiles`.
To create with git bare, do the following:

1. `cd`
2. `mkdir .dotfiles`
3. `echo ".dotfiles" >> .gitignore`, so the very directory is
   ignored[^dotfiles-1]
4. `cd .dotfiles`
5. `git init --bare`
6. `alias dgit='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'`[^dotfiles-1]
this simplifies the dotfiles management
7. `dgit config --local status.showUntrackedFiles no`

[^dotfiles:] This step is unnecessary, because we are setting this repository to
ignore untracked files

Then, add files as usual by using `dgit` instead of `git`. E.g.: `dgit add
.bashrc; dgit commit -m 'commit message'`.

To pull an existing remote dotfiles repository, as ".dotfiles":

1. `git clone --bare <git-repo-url> $HOME/.dotfiles`
2. `alias dgit='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'`[^dotfiles-1]
4. `dgit checkout`[^dotfiles-2]

To use, just type `dgit` instead of `git`.

If the error below occurs at `dgit checkout`, execute the followed script:

```sh
error: The following untracked working tree files would be overwritten by checkout:
 .bashrc
 .gitignore
Please move or remove them before you can switch branches.
Aborting

mkdir -p .config-backup && \
config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .config-backup/{

dgit checkout
```

[^dotfiles-1]: `--git-dir` and `--work-tree` are equivalent to setting the
  variables `GIT_DIR` and `GIT_WORKING_DIR`, respectively
[^dotfiles-2]: if no checkout is forced, the files will not be loaded

## push to multiple remotes

The git configuration file is located at `.git/config`. When cloning a fresh
repo, it may be:

```
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[remote "origin"]
	url = git@bitbucket.org:vgl94/test-repo.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
```

(local branch is master, remote name is origin)

To show this information via command line, type `git remote -v`.

Then, do the following:

```sh
git remote add bitbucket git@bitbucket.org:vgl94/test-repo.git
git remote add gitlab git@gitlab.com:storagesystemslaboratory/git-related-tests.git
```

This adds references to these remote repositories, leaving the config file as:

```
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[remote "origin"]
	url = git@bitbucket.org:vgl94/test-repo.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
[remote "gitlab"]
	url = git@gitlab.com:storagesystemslaboratory/git-related-tests.git
	fetch = +refs/heads/*:refs/remotes/gitlab/*
[remote "bitbucket"]
	url = git@bitbucket.org:vgl94/test-repo.git
	fetch = +refs/heads/*:refs/remotes/bitbucket/*
```

Now, you have to tell git that you want to push to these two remotes:

```sh
git remote set-url --add --push origin git@bitbucket.org:vgl94/test-repo.git
git remote set-url --add --push origin git@gitlab.com:storagesystemslaboratory/git-related-tests.git
```

Config file will be:

```
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[remote "origin"]
	url = git@bitbucket.org:vgl94/test-repo.git
	fetch = +refs/heads/*:refs/remotes/origin/*
	pushurl = git@gitlab.com:storagesystemslaboratory/git-related-tests.git
	pushurl = git@bitbucket.org:vgl94/test-repo.git
[branch "master"]
	remote = origin
	merge = refs/heads/master
[remote "gitlab"]
	url = git@gitlab.com:storagesystemslaboratory/git-related-tests.git
	fetch = +refs/heads/*:refs/remotes/gitlab/*
[remote "bitbucket"]
	url = git@bitbucket.org:vgl94/test-repo.git
	fetch = +refs/heads/*:refs/remotes/bitbucket/*
```

For command line check: `git remote show origin`.

Commits will be retroactively pushed.

## wiki repository

When creating separate pages, lets say I have: `home.md`, which has a link to
`test.md`.

- Bitbucket: I can do: `(test page)[test]` or `(test page)[test.md]`, both will display the
page.
- Gitlab: I can do : `(test page)[test]` for displaying the page, but not `(test page)[test.md]`. The latter one opens the page as a source code.

## submodule

- [Git Submodules vs Git
  Subtrees](https://martowen.com/2016/05/01/git-submodules-vs-git-subtrees/)

submodule is a git repository inside another git repository.

- to clone: `git clone --recurse-submodules` or `git clone --recursive`
- to populate the submodule: `git submodule update --init --recursive`
- to update the repositories: `git submodule update --remote --recursive`
- to add a repository: `git submodule add URL`
- to remove contents of a submodule: `git submodule deinit <module>`

Then just work on them as normal repositories and commit and push changes. But
now you also have to update the parent directory. If the parent is not updated,
then when it clones, it will clone an out-dated version of the submodule.

To specify the commit, just go inside the submodule folder and use `git
checkout`. Note that this commit specification does not appear under
`.gitmodules`, but it is considered nevertheless.

## foreach

`git submodule [--recursive] foreach` iterates over every submodule.

Note: if it fails on one (eg. regex does not match), it will abort. For this reason, consider using
a `true` at the end:

```sh
git submodule foreach --recursive "git restore . || true"
```

## stash (shelve) changes

`git stash` is a local temporary storage, useful when switching branches.

### saving on the stash

Always prefer to name the stash

- to store in a named stash: `git stash push -m MESSAGE`
  - Alternatively (deprecated since v2.16): `git stash save MESSAGE`
- to store only tracked files: `git stash`
- to store tracked and untracked files: `git stash -u`
- to store tracked, untracked and ignored files: `git stash -a`
  - this is not recommended: if you have build files, they will also get stashed
    and will result in a conflict later "<file> already exists, no checkout"
- to interactively stash changes: `git stash push -p`
- to stash certain changes: `git stash push <file a> <file b> ...`

### getting from the stash

[Force git stash to overwrite added
files](https://stackoverflow.com/questions/16606203/force-git-stash-to-overwrite-added-files)

- Note: reference the stash number by using `stash@{n}`
- Note: reference the stash name by using `stash^{/name}`
- Note: when no reference is given, it assumes the latest stashed changes

- Note: stash apply you just apply the stashed changes
- Note: stash drop you discard the stash with its changes
- Note: stash pop you do apply and then drop

- to apply stash: `git stash apply <stash>`
- to apply and drop from the stash: `git stash pop <stash>`
- to remove stash: `git stash drop <stash>`
- to remove all stashes `git stash clear`

- to selectively apply changes (filewise): `git checkout stash <stash> -- <paths>`
- to selectively apply changes (changewise): `git checkout -p stash <stash>`
- to keep changes to other files in the working directory, a less heavy-handed
  alternative: `$ git merge --squash --strategy-option=theirs stash <stash>`

### visualizing the stash

- to view available stashes: `git stash list`
- to view changes in a stash: `git stash show <stash>`
- to view changes in a stash like in gitdiff: `git stash show -p <stash>`

## rename branch

For local branch (you can be in the old-branch):

```sh
git checkout old-name
git branch -m new-name
```

Alternatively

```sh
git branch -m old-name new-name
```

For remote branch, first rename the local, then:

```sh
git push origin --delete old-name # this deletes the remote old-name branch
git push origin -u new-name # this pushes the renamed branch as new
```

Or

```sh
git push origin :old-name new-name # this overwritesd the remote branch name
git push -u new-name
```

## delete branch

- local: `git branch -d branch_name`
- remote: `git push -d origin branch_name`, when remote name is origin

## edit commit message

```sh
git commit --amend
```

## git LFS

Git Large File Storage is a Git extension developed that reduces the impact of
large files in your repository by downloading the relevant versions of them
lazily. Specifically, large files are downloaded during the checkout process
rather than during cloning or fetching. To use Git LFS, you will need a Git LFS aware host.

- install git lfs on arch: `pacman -S git-lfs`
- use it on an existing repository: `git lfs install`
- pull from LFS: `git lfs pull`

Then you have to tell LFS what type of files to track - adding files as normal
right now will still use the normal git.

- to force LFS on every `.file` extension: `git lfs track "*.file"`

Then just use git as normal.

## gitignore templates

- [gitignore.io](gitignore.io)

## unignoring files (gitignore exception)

Use the `!` at the beginning of a line. To ignore a binary, having the same name
as a folder, do the following:

```
test
!test/
```

## check why is path ignored

Explain why path/file is being ignored

```sh
git check-ignore -v <path>
```

## rebase

Rebase is similar to merge, but erases the pasts commits, making it look like:

```sh
git checkout master
git pull
git checkout my_branch
git rebase master
```

### interactive

Let's say you have:

```sh
ls # empty directory
git init
touch a && git add . && git commit -m a # hash = hashA
touch b && git add . && git commit -m b # hash = hashB
touch c && git add . && git commit -m c # hash = hashC
```

If you do `git rebase -i hashB` it will give you the following screen:

```
pick hashC c

# Rebase hashB..hashC onto hashB (1 command)

# Comments:
# ...
```

You have the following options:

- `pick`: use the commit
- `reword`: change message
- `edit`: edit commit
- `squash`: meld with previous commit (= hashB

Just replace the original `pick` with the option you want, so for example:

```
reword hashC c
```

Then just save and it will interactively do what you want.

## default merge resolution

```sh
git config pull.rebase false  # merge (the default strategy)
git config pull.rebase true   # rebase
git config pull.ff only       # fast-forward only
```

## switch vs checkout

The `switch` command is new and made to mark the distinction of the two uses of
`checkout`: switching to a branch and checking out a past commit.

Basically, `git switch branch` and `git checkout branch` is the same thing.

`git switch --` specifies that you are talking about a file, not a branch.

## unstaging

```sh
git reset <file|directory>
```

## tag

You can tag some particular commit:

```sh
git tag <tag_name> [commit_hash]
git push --tags
```

Omit commit hash to tag the current HEAD.

## delete tag

- local: `git tag -d <tag_name>`
- remote: `git push --delete origin <tag_name>`

## list tags

- `git tag`: list local tags
- `git ls-remote --tags`: remote tags
- `git tag -l <regex>`: search by regex

## merging a single file from another branch

```sh
git checkout <branch> <file>
```

## remove file from being tracked

```sh
git rm --cached <file>
```

## fetch vs pull

`fetch` just downloads the data. `pull` downloads and tries to update the HEAD.

## show commit hash

- hash: `git rev-parse HEAD`
- shortened hash: `git rev-parse --short HEAD`

## switching from HTTP to SSH

Either manually edit `.git/config` file, or:

```sh
git remote set-url origin <identification, URL, SSH>
```

## delete changes

[How to remove local untracked files in git working
directory](https://initialcommit.com/blog/git-remove-untracked-files)

- `git checkout .`: removes unstaged tracked files only
- `git clean -f`: removes untracked files only
  - `git clean -df`: remove untracked files recursively
- `git reset --hard`: removes staged tracked and unstaged tracked files only
- `git stash -u`: removes all changes
- `git reset --soft HEAD~1`: remove last commit (does not delete changes)

Note that a submodule will not be altered in any of these cases. Use `git diff`
to verify changes.

## detect what commit does not compile

`git bisect`

## HEAD, ref

- `ref`erence is a human readable name that references a commit ID. E.g. `master`,
  `develop` or even tags such as `v0.1`
- `HEAD` is a special `ref` that usually points to the tip of current branch. A
  detached `HEAD` means that it is not pointing to the tip.

### Relative navigation

[What's the difference between HEAD^ and HEAD~ in
Git?](https://stackoverflow.com/questions/2221658/whats-the-difference-between-head-and-head-in-git)

Use `~[commits]` to navigate backwards. Eg. `git checkout HEAD~` to go back one
commit relative to `HEAD`.

`git checkout HEAD^[commits]`: Selects the parent, the first per default - in a
linear git history, it is equivalent to `~`.

These specifiers or selectors can be chained arbitrarily, e.g., `topic~3^2` is
the second parent of the merge commit that is the great-grandparent (three
generations back) of the current tip of the branch `topic`.

## diff and log

- To see difference of staged and unstaged: `git diff HEAD`
- To list the changed files: `git diff --name-only OTHER_BRANCH`
- To display the log from every branch: `git log --all`
- To display the tree graph: `git log --graph --all`
- To display the log with the changed files: `git log --name-only`
- To display only what has been staged: `git diff --staged`

## revert/undo a commit

```sh
git revert <HASH>
```

## show graph

```sh
git log --graph
```

## show changes in commits

```sh
git log -p
git log -p -2 # also show files difference from commits (2 last)
git format-patch -1 commit-hash # only change from a specific commit
```

## create patch

For example, tag2 has made changes since tag1:

```sh
git diff tag1..tag2 > patch.file
git diff -R tag2..tag1 > patch.file # equivalent to above
git apply patch.file
```

This will create a file that can apply the changes from tag2.

Notes:

- tag1 will be marked with `-`, whereas tag2 will be marked with `+`.
- it will fail if you do tag2..tag1

## get repo name

To get the repository name _in your computer_: `git rev-parse --show-toplevel`.

## git filemode

```sh
git config code.filemode true
```

Filemode simply means that, when set to true, permission changes are considered changes.

## gitconfig

```
[alias]
	s = switch
    f = "!f() { echo shell script; }; f"
    run-script = "!sh script.sh"
```

## tracking folders

Git does not track folder, just add a file. The name `.gitkeep` is a quasi convention.

## git pull another branch


```sh
git fetch <remote> <source>:<destination>
git fetch origin branch:branch
```

To pull every file:

```sh
git pull <remote> <branch>
git pull origin branch
```

## gitattributes

- [Please add gitattributes to your git
  respository](https://dev.to/deadlybyte/please-add-gitattributes-to-your-git-repository-1jld)
- [gitattributes templates](https://github.com/alexkaratarakis)

Git automatically saves the file according to the attributes specified, every
time a file is created or saved.

The simplest example is to enforce that source files have a Unix end-of-line:

```
*.c eol=lf
*.h eol=lf
#* text=auto # uncomment to make it decide automatically
```

To update repo with new gitattributes files:

```sh
git add --renormalize .
```

Check the line ending with `git ls-files --eol <file>`.

## git bisect

Used to _help_ discover which commit introduced a bug. Steps are:

1. get a known good commit
2. `git bisect start` (does not return anything)
3. `git bisect good <good-commit-hash>`
4. `git bisect bad <bad-commit-hash>` (or `git bisect bad` to use the current
   hash)
5. git automatically switches to a middle commit
6. mark it as good or bad as above
7. repeat last step until it gives you a feedback

Note: `git bisect reset` aborts the process.

## worktree

- [Workarounds to Git worktree using bare repository and cannot fetch remote
  branches](https://morgan.cugerone.com/blog/workarounds-to-git-worktree-using-bare-repository-and-cannot-fetch-remote-branches/)

what|usual|worktree
-|-|-
Checkout a new branch|`git checkout -b <new-branch> <start-point>`|`git worktree add <path> <commit-ish>`
List worktrees|`git branch`|`git worktree list`
Lock a worktree|`git checkout <branch>`|`git worktree lock <path>`
Move a worktree|`git mv <old-path> <new-path>`|`git worktree move <old-path> <new-path>`
Prune a worktree|`git branch -d <branch>`|`git worktree prune`
Create a new worktree|`git clone <repo> <path>`|`git worktree add <path> <commit-ish>`


- checkout a new branch into a path: `git worktree add <path> <branch name>`
- checkout an existing branch into a path: `git worktree add <path> <branch name>`

## pull, fetch, merge

- [Git PULL vs FETCH](https://www.youtube.com/watch?v=T13gDBXarj0)

pull = fetch + merge

- fetch get updates from remote
- merge updates HEAD to whatever hash, when no hash is given, `origin/<current
  branch>` is assumed

When a divergence between remote and local on the same branch occurs, and user
does a pull, an automatic method to solve the conflict has to be used:

- rebase (`--rebase`, `git config pull.rebase true`): local commits will be
  rebased on top of remote
- fast-forward (`--ff`, `git config pull.rebase false`): will do a merge
- fast-forward only (`--ff-only`, `git config pull.ff only`): will not do
  anything if there is a conflict
