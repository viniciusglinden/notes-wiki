
# Font

Different OS have different ways of installing/configuring fonts.

There are two types of font installations: user-basis and system-wide.

- `fontconfig`: compile a list of fonts available on the system, searching directories that are by
  default listed in the `/etc/fonts/fonts.conf`
- `fc-list`: list all fonts compiled by the system
- `fc-match <family>: see what is currently being used (remember to recompile the list)
- `fc-match -a <family>: see what is currently and the fallback order
- `display font.ttf`: visualize a font

Read the `/etc/fonts/font.d/README` for explanation.

## Install

Default paths are (`man fonts-conf`):

- `/etc/fonts/`
- `$XDG_CONFIG/fontconfig/conf.d/`
- `$XDG_CONFIG/fontconfig/fonts`
- `~/.fonts.conf.d/`
- `~/.fonts.conf`

Supposing you want to install at `/usr/share/fonts`:

1. copy it to the `/usr/local/share/fonts/` (or `/usr/share/fonts/`) directory
  - create it if that directory does not exist
2. [optional] create a subdirectory for each font-family you are installing
  - some fonts have multiple files for bold, italic, and so on
3. `fc-cache /usr/local/share/fonts/` or simply `fc-cache -fv`

It is also possible to install founts outside the default system paths if you list the directory
under `/etc/fonts/fonts.conf`.

You can install an extra font which will be available to a specific user:

1. copy the font to the `~/.local/share/fonts/`
2. `fc-cache ~/.local/share/fonts`


## xml file

See a better example at `man fonts-conf`.

```xml
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "urn:fontconfig:fonts.dtd">
<fontconfig>
    <alias>
        <family>monospace</family>
        <prefer>
        <!--
        just list the fonts in order of preference
        The name can is the second column of fc-match monospace
        -->
            <family>Font 1</family>
            <family>Font 2</family>
        </prefer>
    </alias>
</fontconfig>
```


