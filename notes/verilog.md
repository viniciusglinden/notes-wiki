
# Verilog (HDL)

- [Verilog HDL: quick reference
  guide](https://sutherland-hdl.com/pdfs/verilog_2001_ref_guide.pdf)
- [chipverify](https://www.chipverify.com/verilog/verilog-tutorial) verilog
  reference
- [asic world](https://www.asic-world.com/) great verilog reference in therms of
  design and verification
- [hdlbits](https://hdlbits.01xz.net/) start practicing verilog and
  understanding the basics
