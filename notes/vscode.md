
# VSCode

- [Launch
  reference](https://github.com/microsoft/vscode-docs/blob/main/docs/cpp/launch-json-reference.md)

## Making it faster

- [Here's How to Make VSCode Faster](https://vscode.one/make-vscode-faster/)

Some extension come installed by VSCode itself. They cannot be uninstalled, by
disabled:

- <F1> Developer: Show Running Extension
- right click and then disable

You can also disable extension for specific workspaces.

You can also check if the process "extensionHost" is consuming too much CPU.

## Debug Run / `launch.json`

- [Configure C/C++
  debugging](https://code.visualstudio.com/docs/cpp/launch-json-reference)
- [VS Code debug specs](https://vscode-debug-specs.github.io/)

## CMake integration

- [Configure CMake Tools
  settings](https://github.com/microsoft/vscode-cmake-tools/blob/main/docs/cmake-settings.md)
