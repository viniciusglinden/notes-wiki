
# Research Paper

## Reference

- [Scimago Journal \& Paper Ranking](https://www.scimagojr.com)
- Google, for quick access to impact factor (type name of the journal, followed
  by "impact factor)

## Methods

There are several metrics to consider when evaluating a research paper. They typically fall into two categories: traditional and nontraditional.

### Traditional

* Impact factor – the average number of citations per article published in science and social science journals (calculated by dividing the number of citations in the year by the total number of articles published in the two previous years). You can use the Journal Citation Reports (JCR) to locate impact factors or the open-source Impact Factor Search.

$$
IF = \frac{\overline{citation}}{\Sigma_{two years}articles}
$$

* Eigenfactor – rating of the total importance of a journal according to the number of new citations where high ranking journals are weighted higher to make a larger contribution to the Eigenfactor than those from poorly ranked journals. Note: An Eigenfactor assigns journals to a single category and makes it difficult to compare across disciplines. Some folks believe the Eigenfactor score isn't much different than raw citation counts (check out this blog post, for a review).
* Article Influence Score – measures the average influence, per article, of the papers published in a journal (calculated by dividing the Eigenfactor by the number of articles published in the journal).
* SCImago Journal & Country Rank – measures scientific influence of scholarly journals (accounts for both the number of citations received by a journal and the "influence" of the journals where these citations come from).
* An author-level metric that highlights the author's productivity 
* h-index – measures scientific productivity and impact of an individual scientist, and it's also used to rank journals.

### Non-traditional

* Views - HTML views and PDF downloads
* Discussions about the work - journal comments, science blogs, Wikipedia, Twitter, Facebook and other social media
* Saved - Mendeley, CiteULike and other social bookmarks
* Cited - citations in the literature, tracked by Web of Science, Scopus, CrossRef, etc.

