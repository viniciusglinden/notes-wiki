
# C/C++ toolchain

## ld

You should not call `ld` directly. When using `gcc`, it automatically calls `ld`. To pass flags that
are specific to `ld`, use `gcc -Wl,<ld flags>`.

`ld` is sensitive to the flags order. Use `--start-group` and `--end-group` to make it think about
the right order.

You can write a `-specs=<script>` instead of relying in the command line.

Another useful flag is `--verbose`.

### ld wrap

This is useful for mocking in C.
If you have a function called `void foo(int a)`, you may do: `ld --wrap=foo`. This will:

1. transfer the original `foo` code to a `__real_foo`
2. link `foo` to `__fake_foo`

Now you can write a `void __wrap_foo(int a)` **in a separate file**. You can still call the original
function under the `__real__foo` name. This should work with any symbols (not only functions).

Be careful with the binding order:

1. executable
2. preloaded libraries
3. libraries in the linking order

If you have problems, you can debug symbol bindings with `LD_DEBUG=symbols ./a.out` (see `man
ld.so`).

## libc

- [Understanding the libc init
  array](https://stackoverflow.com/questions/15265295/understanding-the-libc-init-array)

When you link with libc (default), a linker script is automatically used (`/usr/lib/ldscripts/`)
with a default bootloader (`start()`). This bootloader initialized static values, etc as usual, but
it also calls initializing and deinitializing functions from libc. For glibc:

```c
extern void (*__preinit_array_start []) (void) __attribute__((weak));
extern void (*__preinit_array_end []) (void) __attribute__((weak));
extern void (*__init_array_start []) (void) __attribute__((weak));
extern void (*__init_array_end []) (void) __attribute__((weak));
extern void (*__fini_array_start []) (void) __attribute__((weak));
extern void (*__fini_array_end []) (void) __attribute__((weak));
```

