
# Debuggers

## binary

- Binary Ninja: diagrams in assembly
- Ghidra: assembly code with inferred C code
- radare2: similar to Binary Ninja

## vulnerabilities

- [american fuzzy lop (afl)](https://lcamtuf.coredump.cx/afl/): fuzzy inputs to
  a file and discover crashes. Use the afl++ version instead.

## C

- [gdb](gdb.md)
- [TimeDoctor)(https://gitlab.com/KnarfB/timedoctor) for embedded profiling

## Python

### PDB

[pdb](https://docs.python.org/3/library/pdb.html)

Either inside the code with

```py
import pdb; pdb.set_trace() # breakpoint
```

Or, with python 3.7, just call `breakpoint()` and `pdb` will be automatically
imported.

Or, call the script as follows:

```sh
python -m pdb myscript.py
```

[Syntax should be very similar do
gdb.](https://static.realpython.com/guides/pdb-command-reference.pdf)

Alternatively, use ipython with ipdb:

```sh
pacman -S ipython python-ipdb
```

```sh
import ipdb; ipdb.set_trace() # breakpoint
```

## shell

- [Bash Debugger](https://bashdb.sourceforge.net/)
- [Bash stepping xtrace](https://github.com/mug896/bash-stepping-xtrace)
- [Bash debuggin](https://tldp.org/LDP/abs/html/debugging.html)

Check for syntax errors by using `shellcheck` and `sh -n SCRIPT`.

### Custom

```sh
shopt -s expand_aliases
alias breakpoint='
    while read -p "Debugging(Ctrl-d to exit)> " debugging_line
    do
        eval "${debugging_line}"
    done'
```

Then just call `breakpoint` or:

```bash
trap_debug() {
    echo "# ${BASH_LINENO[0]}: ${BASH_COMMAND}"
    while read -r -e -p "debug> " command; do
        if [[ -n "${command}" ]]; then
            eval "${command}"
        else
            break
        fi
    done
}
trap 'trap_debug' DEBUG
```

Useful `trap`s are:

- `exit`: on shell exit
- `return`: when function returns or sourcing finishes
- `err`: on command failure

Note: source the script while using `set -T` to inherit traps.
