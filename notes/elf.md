
# ELF

- [Anatomy of an ELF core
  file](https://www.gabriel.urdhr.fr/2015/05/29/core-file/)
- `man elf`

![ELF a Linux executabloe
walkthrough](https://upload.wikimedia.org/wikipedia/commons/e/e4/ELF_Executable_and_Linkable_Format_diagram_by_Ange_Albertini.png)

There is an `elf.h` header file.

When type `e_type` is

- `ET_EXEC` (executable): `e_entry` points to `_start`
- `ET_CORE` (core dump): `e_entry` points to 0

## Memory Addresses

- The Virtual Memory Address (VMA) is where the section will have when the
  output file is run
  - also referred to as "execution address"
  - address from which an application was linked to run
- The Load Memory Address (LMA) is the address to which the section will be
  loaded
  - also referred to as "physical memory address"
  - address where an application resides
  - cannot from from this address unless it is the same as the VMA

The data section is loaded into ROM during the flashing process. When the
program starts, the data section is copied into RAM. In this case the **ROM
would be the LMA**, and the **RAM would be the VMA**.
