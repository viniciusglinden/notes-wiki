
# ssh

ssh has a client and a server side. The server side has to be started via the
init system.

## installation

Choose a implementation. Traditionally we use `openssh`. In arch based systems,
the `openssh` package downloads both the server and the client.
If you want a ssh server, remember to install the init system package as well.
For runit:

```sh
p -S openssh openssh-runit
ln -s /etc/runit/sv/openssh /run/runit/service
```

## configuration files

- server (daemon):`/etc/ssh/sshd_config`
- client:`/etc/ssh/ssh_config`

## syntax

- shell: `ssh -p port user@server-address` (`-p` is optional, default 22)
- copy files from server to client: `scp user@server:file /local/path`
- copy files from client to server: `scp file user@server:/remote/path`
- copy directories from server to client: `scp -r user@server:directory /local/path`
- running a command on remote: `ssh user@server COMMAND`

## adding ssh-key

Just like in git (or any other program):

```sh
ssh-keygen -t ed25519 # rsa is unsafe
```

or

```sh
gpg --full-gen-key
```

But, to copy the key to the server:

```sh
ssh-copy-id user@server-address
```

Public keys are stored in `~/.ssh/authorized_keys` file (not folder). The syntax
for each line is as follows:

```
# ssh-type key name
ssh-rsa ASFBHUUVVU...JHBVUV linden@PersonalComputer
```

## server fails to recognize SSH key

If git is telling you „Unable to negotiate” and shows you available keys (that
match yours), try `ssh-add -L`. If this command returns `The agent has no
identities.`, then just `ssh-add`.

For bitbucket be sure to use ed22519, for rsa is no longer be supported.

```
Host gitlab.com
    UpdateHostKeys no
```

## alias

In the `~/.ssh/config` file:

```
Host ubuntu
    HostName 192.168.225.50
    User senthil
    IdentityFIle ~/.ssh/id_rsa_remotesystem
```

Each field may be left off, with the exception of HostName.

There are other fields as well.

## server

Server side is already installed, but the init may be missing.

Configuration is in `/etc/sshd_config`.

Always initialize it with the init system. This means, for runit:

```sh
sshd
sshd re-exec requires execution with an absolute path
```

Do this instead:

```sh
pacman -S openssh-runit
ln -s /etc/runit/sv/sshd /run/runit/service
sv start sshd
```

## using hostname.local

This has to do with zeroconf. Arch wiki recommends using avahi.

## Execute commands on target

```sh
ssh target command # single command
ssh target 'bash -s' < ./local-script.sh
```

## additional port mapping

Port mapping means, for a remote port to correspond with a local port. For instance:

```sh
ssh remote_username@remote_ip -L local_port:localhost:remote_port
```

Will do whatever command you provide (or a section if not specified) with `remote_port` correponding
to `local_port`. One example use case is: access a remote gdb with a local client via ssh.

## Get the remote IP address inside the session

```sh
echo $SSH_CLIENT # prints client IP
echo $SSH_CONNECTION # prints client IP and remote IP
```

## Escape sequences

- [How can I break out of ssh when it
  locks?](https://askubuntu.com/questions/29942/how-can-i-break-out-of-ssh-when-it-locks)

Escape sequence is: `Enter`, `~`. In International keyboard, you can use `Enter`, `~`, `~`.

Supported escape sequences:

- `~?` this message
- `~.` terminate connection (and any multiplexed sessions)
- `~B` send a BREAK to the remote system
- `~C` open a command line
- `~R` request rekey
- `~V/v` decrease/increase verbosity (LogLevel)
- `~^Z` suspend ssh
- `~#` list forwarded connections
- `~&` background ssh (when waiting for connections to terminate)
- `~~` send the escape character by typing it twice

Note that escapes are only recognized immediately after newline.
