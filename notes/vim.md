
# VIM

- [Vim from scratch](https://www.vimfromscratch.com/)
- [A guide to using Lua in Neovim](https://github.com/nanotee/nvim-lua-guide)

## customization

All permanent customizations have to be inputted into the `.vimrc` file. This
file depends on: the version and the operating system.

- ViM(v8)+Windows: `_vimrc` (no extension). Ex: `C:\Program Files (x86)\Vim\_vimrc`
- ViM(v8)+Linux: `~\.vimrc`
- Neovim+Linux: `$XDG_CONFIG_HOME/nvim/init.vim`, ex: `~\.config/nvim/init.vim`


You may have to create this file, if it is not found.

## map command

- `lhs`: left-hand side
- `rhs`: right-hand side

`:map lhs rhs` will map the lhs key to rhs command. If unspecified, this will enact on every mode, except insert mode.

```
:nmap - Display normal mode maps
:imap - Display insert mode maps
:vmap - Display visual and select mode maps
:smap - Display select mode maps
:xmap - Display visual mode maps
:cmap - Display command-line mode maps
:omap - Display operator pending mode maps
```

If `map` is used, it will rely on whatever map is set. Therefore, always use `noremap` instead of just `map`.

You can enter any character through its value, just type in:

- `^Vnnn`, for decimal value (000 <= nnn <= 255)
- `^VOnnn` or `^Vonnn`m for octal value (with 000 <= nnn <= 377)
- `^VXnn` or `^Vxnn` for hex value (with 00 <= nn <= FF)
- `^Vunnnn`, for hex value for BMP Unicode codepoints (with 0000 <= nnnn <= FFFF)
- `^VUnnnnnnnn`, hex value for any Unicode codepoint (with 00000000 <= nnnnnnnn <= 7FFFFFFF)

Notes:

- If `^V` (ctrl+v) does not work, use `^Q` instead
- In all cases, initial zeros may be omitted if the next character typed is not a
  digit in the given base (except, of course, that the value zero must be
  entered as at least one zero).
- Hex digits A-F, when used, can be typed in upper or lower case, or even in any
  mixture of them.
- Your encoding will need to be set to "utf-8" to take full advantage of the
  Unicode characters.

## System clipboard support

In version 8, vim has to be compiled with clipboard support. To check if it has
when using X, type `vim --help | grep clipboard`. If no `+` is `xclipboard`,
then there is no support.
ty

## save as sudo

If you forgot to open a file as sudo, simply use: `:w !sudo tee %`

## set configuration for a particular file (modeline)

- [Modeline](https://vim.fandom.com/wiki/Modeline_magic)

- `modeline`: enables modeline mode
- `modelines`: number of lines to check

You can insert this line at the start or end of the file.
To check if you have then enable, type: `:verbose set modeline? modelines?`. It
should return something like:

```
modeline
	Last set from /usr/share/vim/vimrc
modelines=4
	Last set from ~/.vim/vimrc
```

There are two forms of modelines.
**This form does not need to use `set`**:
`[text]{white}{vi:|vim:|ex:}[white]{options}`

- `[text]`: any text or empty
- `{white}`: at least one blank character (<Space> or <Tab>)
- `{vi:|vim:|ex:}`: the string "vi:", "vim:" or "ex:"
- `[white]`: optional white space
- `{options}`: a list of option settings, separated with white space or ':',
where each part between ':' is the argument for a ":set" command (can be empty)

```vimL
vi:noai:sw=3 ts=6
vim: tw=77
```

The second form (deprecated):
`[text]{white}{vi:|vim:|Vim:|ex:}[white]se[t] {options}:[text]`

- `[text]`: any text or empty
- `{white}`: at least one blank character (<Space> or <Tab>)
- `{vi:|vim:|Vim:|ex:}`: the string "vi:", "vim:", "Vim:" or "ex:"
- `[white]`: optional white space
- `se[t]`: the string "set " or "se " (note the space); When "Vim" is used it
must be "set".
- `{options}`: a list of options, separated with white space, which is the
argument for a ":set" command
- `:`: a colon
- `[text]`: any text or empty

```vimL
/* vim: set ai tw=75: */
/* Vim: set ai tw=75: */
```

## see default key bindings

```vimL
:help index
```

To search for a specific default keybind:

```vimL
:help CTRL-X
```

## neovim python support

In arch systems with neovim, support for python is a package:

```sh
sudo pip install neovim
```

To check if everything is ok, run inside vim:

```vimL
:checkhealth
```

## neovim-remote (NVR)

Control neovim sessions through the terminal.
[Link](https://github.com/mhinz/neovim-remote)

```sh
pip3 install neovim-remote
```

Neovim always starts a server. Get its address via `:echo $NVIM_LISTEN_ADDRESS` or `:echo v:servername`. Or specify an address at startup: `NVIM_LISTEN_ADDRESS=/tmp/nvimsocket` nvim.

nvr will use `$NVIM_LISTEN_ADDRESS` or any address given to it via `--servername`.


There are two ways to get a connection: expose `$NVIM_LISTEN_ADDRESS` to the environment before
neovim; `:echo $NVIM_LISTEN_ADDRESS`, then `nvr --servername=/tmp/nvim.....`

`--serverlist` lists all the servers (open and closed).

## Using SyncTeX

There are two types of searches with SyncTeX: forward \& backward search. The
first one is to indicate in the PDF reader what line you are currently in the
TeX file; the latter means the contrary. We will be exploring the combination of
neovim and zathura.

Install neovim-remote, then using vim-tex, you have to install `pip3 install pynvim`,
but we will not be using this.

### Backward search

1. Open the `.tex` file with a known socket/server (e.g. use the flag `--listen
/tmp/nvimscket`)
2. Add to your zathurarc file the following:
```
set synctex true
set synctex-editor-command "nvr --remote +%{line} %{input}"
```
3. Then compile the LaTeX document with the `synctex=1` flag (latexmk,
 pdflatex, etc)
4. Open the pdf with zathura
5. To jump, ctrl+left click in the pdf document

### Forward search

1. Put this in your vimrc
```
function! SyncTexForward()
	let linenumber=line(".")
	let colnumber=col(".")
	let filename=bufname("%")
	let filenamePDF=filename[:-4]."pdf"
	call jobstart("zathura --synctex-forward " . linenumber . ":" . colnumber . ":" . filename . " " . filenamePDF . "&>/dev/null")
endfunction
```
2. Map this function to some key (or call it within a `.tex` document)

Notes:

1. in neovim, you can't use `!...&`. Use `jobstart` instead
2. this script supposes that your pdf is named after the file you have opened
3. you can open the pdf by calling forward search

## replace in multiple buffers

```vimL
:bufdo %s/pattern/replace/ge | update
```

- `bufdo`: Apply the following commands to all buffers
- `%s`: Search and replace all lines in the buffer
- `pattern`: Search pattern
- `replace`: Replacement text
- `g`: Change all occurrences in each line (global)
- `e`: No error if the pattern is not found
- `|`: Separator between commands
- `update`: Save (write file only if changes were made)

When asked "replace with REPLACE (y/n/a/q/l/^E/^Y)?":

- `y` to replace the match
- `l` to replace the match and quit
- `n` to skip the match
- `q` or Esc to quit substitution
- `a` option substitutes the match and all remaining occurrences of the match
- `CTRL+Y` scroll down
- `CTRL+E` scroll up

## edit remote file

vim has integrated scp functionality:

```sh
vim scp://user@host/relative_path_to_home
nvim scp://debian/relative_path_to_home
```

Suppose that I have in my ssh config the following:

```
Host debian
	root
	HostName localhost
	Port 8022
```

If I run `nvim scp://debian/teste`, it will edit `/root/teste` in remote. And if
I run `nvim scp://debian//teste`, it will edit `/teste` in remote. Basic netrw
protocol is: `protocol://[user@]hostname[:port]/[path]`

Note: should work the same with rsync. For help type `:help rsync`

## lua integration

neovim has integrated lua syntax. You may configure nvim only using lua.

- [nvim lua guide](https://github.com/nanotee/nvim-lua-guide)
- [configuring neovim using
  lua](https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/)

### accessing vim variables from lua

Lets take the `autoindent` variable. In lua you:

- set it with `vim.opt.autoindent = <value>`
- get it with `vim.opt.autoindent:get()`

`vim.opt` options are all "objects". You can use `vim.inspect()` to see all its members:
`lua print(vim.inspect(vim.opt.autoindent))` or (after v0.7) `lua = vim.opt.autoindent.

## netrw

[Using Netrw, vim's builtin file
explorer](https://vonheikemen.github.io/devlog/tools/using-netrw-vim-builtin-file-explorer/)

## get set value

```
set filetype?
```

## omnifunc

Is one native completion menu in vim (the menu that appears when typing in insert mode - `: h
omnifunc`). To open it manualy, type `<c-x><c-o>` (in sequence in insert mode).

Also see `:h compl-omni`

## Start in debug mode

```sh
nvim -s <(echo '<keys>') -D
```

## Buffer, Window and tab

- buffer: in-memory text of a file
- window: a viewport on a buffer
- tab: collection of windows
