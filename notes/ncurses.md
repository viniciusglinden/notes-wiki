
# ncurses

## references

Use the man command.

- [Ncurses howto](https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/)
- [How do I use getch from curses without clearing the
  screen](https://stackoverflow.com/questions/4772061/how-do-i-use-getch-from-curses-without-clearing-the-screen)

For an alternative, see urwid.

## compiling

- C++ with g++: `g++ -lncurses test.cpp -o test`
- C++ with gcc: `gcc -lstdc++ -lncurses test.cpp -o test`

## plug-in explanations

- `initscr()`: initializes screen
- `move(line, column)`: moves the curse to the (y, x) location
- `printw("Hello world\n")`: prints something to the screen
- `refresh()`: refreshes the memory
- `getch()`: gets a key press
- `mvprintw(0, 0, "%d", c)`: move && printw
- `clear()`: clears the screen
- `endwin()`: ends the window
- `WINDOW * win = newwin(height, width, startx, starty)`: creates a window
- `stdscr` // default window
- `box(win, keyvalue, keyvalue)`: creates a box around the window
- `wborder(win, left, right, top, bottom, tlc, trc, blc, brc)`: more precise box()
- `wrefresh(win)`: refreshes the window
- `wprintw(win, "Hello world\n")`: prints something to the window
- `mvwprintw(win, y, x, "Hello world\n")`: move && prints something to the window
- `cbreak()`: character break, enables ctrl+c
- `raw()`: treat all input as normal (escape special)
- `noecho()`: do not echo user input
- `attron(attribute)`: attribute on
- `attroff(attribute)`: attribute off
- `wattron(attribute)`: attribute on for a specific window
- `wattroff(attribute)`: attribute off for a specific window
- `has_colors()`: returns true if terminal support colors
- `start_color()`: initialize color usage
- `init_pair(id, foreground, background)`: creates a color pair
- `init_color(selected_color, red 0-999, green 0-999, blue 0-999)`: change default color
- `can_change_color()`: true if terminal can change color
- `getyx(stdscr, y, x)`: gets the current cursor's position
- `getbegyx(stdscr, ybeg, xbeg)`: gets the begging of the current window
- `getmaxyx(stdscr, ymax, xmax)`: gets maximum line and column of the current window
- `wgetch(inputwin)`: get a key press within provided window
- `keypad(inputwin, true)`: enables keypad
- `KEY_UP, KEY_DOWN`: arrow keys
- `KEY_F()`: function number (F1, F2, etc)
- `addch(win, y, x, char)`: adds a single char (opposed of a string)
- `attr_t`: variable type for combining attributes
- `chtype`: variable type for combining characters with attributes
- `curs_set(0);`: hides the cursor
- `addstr("string")`: adds a string and finishes the line (similar to
`printw("string\n")`
- `LINES, COLS`: ncurses definitions for maximum number of lines and columns,
  respectively
- `chgat(number_of_characters, attribute, color_index_id, NULL)`: update
  characters from the current cursor
- `keyname(char)`: prints the name i.e. character

## window \& screen concepts

ncurses has two screens: a virtual and an actual screen.

- `refresh()` copies the virtual to the actual screen
- `refresh()` does refresh in the standard screen (and not in the window
  screen)
- `wrefresh()` does refresh in the window screen

- functions like `box()` write to the virtual screen
- `getch()` will first refresh the window and then get the char

- use `wrefresh()` if only the window is changing
- use `refresh()` if you want to refresh all the windows (but not display)

## input mode

When the user inputs something, by default it goes to a buffer. Thus, there
is a small delay.
- `cbreak()` allows bypassing the buffer.
- `halfdelay(x)` is like `cbreak()` with the added functionalities of waiting
*x* deci-second before outputting an error.
- `nodelay(win, true)` is the opposite of `halfdelay(x)` in the sense that if
there is no character available, it will not wait.
- `timeout(x)` is like `halfdelay(x)`, but you can input any value (including
negative value) and uses ms instead of ds.


