
# Markdown

## References

- [CommonMark Markdown Specification](https://commonmark.org/)
- [Markdown Guide: Extended
  Syntax](https://www.markdownguide.org/extended-syntax)
- [Markdown
  Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## Equations

Just type as in LaTeX. Ex:

$$
\int y \mathrm{d}x = I
$$

## Code

For inline code, use single backticks, `example`. For expended lines of code,
use triple backticks, with optional language declaration. Ex:

```python
print("Test")
```

```
This is a test.
```

## Environment

Depending on what is interpreting the document, you can annotate stuff just line
expanded lines of code, whose language is how to be interpreted as ([flow](https://flowchart.js.org/),
[sequence](https://bramp.github.io/js-sequence-diagrams/), [mermaid](https://github.com/mermaid-js/mermaid), etc). Example:

```sequence
Andrew->China: Says Hello
Note right of China: China thinks\nabout it
China-->Andrew: How are you?
Andrew->>China: I am good thanks!
```

## figures

You may directly cite a figure within the wiki. Lets suppose you have the structure:

```
wiki-root
- real-time-logic_legend.svg
- ./documents
--  test.png
-- ./notes
---- test.md
---- test.png
```

To cite them at `test.md` use:


```
![test](test.png)

![test2](../test.png)

![test3](../../real-time-logic_legend.svg)

![test3](../../real-time-logic_legend.svg "hover text")

![alt text][example]

[example]: ../../real-time-logic_legend.svg "hover text"
```

## table

Alignment example:

| Syntax      | Description | Test Text     |
| :---        |    :----:   |          ---: |
| Header      | Title       | Here's this   |
| Paragraph   | Text        | And more      |

## footnotes

This[^1] is the footnote syntax[^2]

[^1]: Footnote
[^2]: Test

You may use any variable of footnote you want, this is helpful for long files
with many footnotes [^example]

[^example]: This footnote has been assigned a sequential value.

## Translating web pages to markdown

You can convert any page (including wiki pages) using pandoc:

```sh
pandoc -f html -t markdown --request-header User-Agent:"Mozilla/5.0" https://www.fsf.org
```

Or simply:

```sh
pandoc -f html -t markdown test.html
```

## Linking to chapter in page

```markdown
[name](ulr#section)

# new section {#section}

[section within file](#section)
```

## support for foreign (non-ASCII) characters

Use the `--pdf-engine=xelatex` option.
