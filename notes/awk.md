
# awk

## command line

Use the following syntax:

```sh
awk 'AWK_SCRIPT' FILE
```

## awk script

Use the following syntax:

```sh
awk -f AWK_SCRIPT FILE
```

Note: `AWK_SCRIPT`'s extensions may be ".awk"

There is no need for single quotes inside the script. Commands are the same as
above. Each command runs awk another time. You can also declare functions inside
the script file.

## commands

By default, each field is identified by a space (this can be changed).
Every command in awk is enclosed within "{}". There is a `print` command:

- `awk '{print}' file.txt`: prints the whole file
- `awk '{print $0}' file.txt`: prints the whole file
- `awk '{print $1}' file.txt`: prints the first column
- `awk '{print $2}' file.txt`: prints the second column
- `awk '{print $1,$2}' file.txt`: prints the first and second column
- `awk '{print $1,$2,oi}' file.txt`: prints the first and second column only
- `awk '{print $1,$2,"oi"}' file.txt`: prints the first and "oi" in a separate
  field
- `awk '/gcc/ {print $1}' file.txt`: prints the second column, whose lines has "gcc" in them
- `awk '/^w/ {print $1}' file.txt`: prints the second column, whose lines begin with "w"
- `awk '{print $1/1024} file.txt`: prints the first column (a number), divided by 1024
- `awk '{print $1/1024"Ki"} file.txt`: prints the first column (a number), divided by 1024 and inserting a "Ki" at the end
- `awk '$1 > 100 {print $1}' file.txt`: prints the first column, whose values are greater than 100
- `awk '$2 > 100 && /^w/ {print $1}' file.txt`: prints the first column, whose lines have the second column have values greater than 100

## functions

There is a floor function called "int":

- `echo "19" | awk '{print int($1/10)` results in `1`
- `echo "11" | awk '{print int($1/10)` results in `1`

Lets define a custom function for rounding around 0.5:

```awk
func round(n) {
	n = n + 0.5
	n = int(n)
	return n
}
```

Then use it as a normal function:

- `echo "19" | awk '{print round($1/10)` results in `20`
- `echo "11" | awk '{print round($1/10)` results in `1`

Another more complex example:

```awk
func printlist(n) {
	for(i=1;i<=n;i++) {
		printf("%d ",i)
	}
	printf("\n")
}
```

## variables

- awk variables are not shell variables
- if you want to use shell variables inside awk, you first have to transform
  them to awk variables

```sh
echo | awk -v x="var" '{print x}'
x="oi"
echo | awk -v x="$x" '{print x}'
```

Note: you have to double quote for shell variable with awk in order to expand it

```sh
awk '/Aaron/{ first_name=$2 ; second_name=$3 ; print first_name, second_name ; }' names.txt
```

## separator

To change the separator, in the command line: `-F <character>`.

```sh
python -V #Python 3.10.7
version=$(python -V | awk -F "[ .]" '{print 10000*$2+100*$3+$4}')
```

Moreover

```sh
awk -F: '{print $1}'
awk -v FS=: '{print $1}'
awk '{print $1}' FS=:
awk 'BEGIN{FS=":"} {print $1}'
```
