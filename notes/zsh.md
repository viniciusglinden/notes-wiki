
# zsh

- [The Z Shell Manual](https://zsh.sourceforge.io/Doc/Release/)
- [Mastering zsh](https://github.com/rothgar/mastering-zsh)

## bindkey

- [bindkey table](https://www.lshell.com/post/2021/12/zsh-bindkey-table/)
- [bind keys](https://jdhao.github.io/2019/06/13/zsh_bind_keys/)
- [How to remove a zsh keybinding if I dont know what it
  does](https://unix.stackexchange.com/questions/285208/how-to-remove-a-zsh-keybinding-if-i-dont-know-what-it-does)

- Man page: `zshzle` (this also contains the default functions)
- Show all current keybindings: `bindkey -L`, alternatively simply `bindkey`
  - By default this shows "main" mode. Use `-M <mode>` to show in an alternative
    mode
- Display key:
  - `showkey -a`
  - ctrl+v + the character that you want
  - `cat` then the character that you want
- To display a specific key and what it does: `keybind "<ctrl+v+key>"`
- To unbind a key: `bindkey -r "<ctrl+v+key>` (this binds the key to
  `undefined-key`), alternatively bind it to an empty string
- [Carat notation](https://en.wikipedia.org/wiki/Caret_notation) or [escape
  sequences](https://en.wikipedia.org/wiki/Escape_sequence) can be used

## `$FPATH`

Similar to `PATH`, but used to search for function definitions (to run in the
current pid). Mostly used by auto completion.

`PATH` is to search for file definitions to run (as a child process).

## autoload

- [Functions](https://zsh.sourceforge.io/Doc/Release/Functions.html)

A function can be marked as _undefined_ using the autoload builtin (or
`functions -u`[^functions-keyword] or `typeset -fu`). Such a function has no
body. When the function is first executed, the shell searches for its definition
using the elements of the `fpath` variable. Thus to define functions for
autoloading, a typical sequence is:

[^functions-keyword]: note the plural in `functions`

```zsh
fpath=(~/myfuncs $fpath)
autoload myfunc1 myfunc2
```

## zcompile

- [Shell Builtin
  Commands](https://zsh.sourceforge.io/Doc/Release/Shell-Builtin-Commands.html#Shell-Builtin-Commands)

Compile zsh files that define functions for quick-loading.

## previous commands

- `!!` last command
- `!*` or `!:*` or `!$` all previous arguments
- `!^` or `!:1` fist argument
- `!foo` last command that started with `foo`
- `!-3:$` last word from 3 commands ago
- `!!:s/foo/bz` take previous command and replace first instance of foo with bar
- `^foo^bar` same as above
- `!!:sg/foo/bz` take previous command and replace all instances of foo with bar
- `^foo^bar^:G` same as above
- `fc` edit last command in a visual editor

Syntax:

`!<relative line number><argument specifier>:<substitution>`
