
# Raspberry Pi

Microcontroller = Raspberry Pi Pico.

Variants:

- W: wifi included
- H: commercial name for the board with the soldered pin header

## ASCII pinout

- [PiPico Pinout Ascii Art](https://github.com/dommodnet/Pi-Pico-Pinout-Ascii-Art)

Attention: check if pinout is correct: there are more than one pico board from the Raspberry Pi
foundation on the market.

```
            +-----+
+-----------|||||||-----------+
|           +-----+           |
|     [LED] GP25              |
|1                          40|
|>GP0/U0Rx               VBUS<|
|>GP1/U0Tx               VSYS<|
|]GND                     GND[|
|>GP2                  3V3_EN<|
|>GP3                     3V3<|
|>GP4                    AREF<|
|>GP5                 A2/GP28<|
|]GND                     GND[|
|>GP6                 A1/GP27<|
|>GP7                 A0/GP26<|
|>GP8                     RUN<|
|>GP9                    GP22<|
|]GND                     GND[|
|>GP10                   GP21<|
|>GP11                   GP20<|
|>GP12                   GP19<|
|>GP13                   GP18<|
|]GND                     GND[|
|>GP14                   GP17<|
|>GP15                   GP16<|
|20                         21|
|         [ ] [ ] [ ]         |
|       SWCLK GND SWDIO       |
+-----------------------------+
```

## PIO

- [Programmable IO](https://docs.micropython.org/en/v1.18/rp2/tutorial/pio.html)
- [Raspberry Pi Pico Programmable
  I/O](https://dernulleffekt.de/doku.php?id=raspberrypipico:pico_pio)
- [Programing with PIO State
  Machines](https://medium.com/geekculture/raspberry-pico-programing-with-pio-state-machines-e4610e6b0f29)
- [RP2040 PIO
  Emulator](https://rp2040pio-docs.readthedocs.io/en/latest/index.html)

PIO = Programmable Input/Output

[This video](https://www.youtube.com/watch?v=yYnQYF_Xa8g) is highly recommended.

Similar to an FPGA, used for stricter time constraints. It as a configurable
state machine that runs parallel to the main program, interacting with it via
FIFOs. In the Raspberry Pi Pico, we only have four off them:

```
                       2xPIO blocks
                            │
      ┌─────────────────────┴─────────────────────┐
                        multiple
                            │
            ┌───────────────┴──────────────┐
        ┌───┴────┐                     ┌───┴────┐

    ┌   ┌────────┐  ┌───────────────┐  ┌────────┐
4x ─┤ ┌►│  FIFO  ├─►│ STATE MACHINE ├─►│  FIFO  ├─┐
    └ │ └────────┘  └───────────────┘  └────────┘ │
      /                                           / 32 bits
      │ ┌───────────────────────────────────────┐ │
      └─┤             MAIN PROGRAM              │◄┘
        └───────────────────────────────────────┘
```

Features are:

- Tx/Rx FIFO: 32-bit value
- Input/Output Shift Register (ISR/OSR): volatile data to interact with main
  program
- Scratch registers: `x` and `y`, 32-bits
- Configurable clock divider: system clock can be scaled by 16-bit (65536)
- GPIO mapping: direct GPIO access as input, output, set or side-set
- maximum of 5 side set pins
- DMA link to FIFO: if more than 32 bits are required
- Combine FIFOs: option to use the 4 four FIFOs to create a 8-word FIFO
- IRQ flags: 8 global flags can be set/cleared, main program and state machines
  can interact with it

Steps are:

- Declare the state machine via PIO language, an assembly subset
- Declare its interface to the main program (binding)
- Tell program how to use this state machine
- Tell it to run the state machine

For the PIO programming language, refer to [C
SDK](https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-c-sdk.pdf).

```
<instruction> .side(<side_set_value>) [<delay_value>]
```

The `.side()` and delay parts are both optional, and allow
the instruction to perform more than one operation.

Instructions are:

- `jmp()`: transfers control to a different part of the code
- `wait()`: pauses/wait for action
- `in_()`: get bits from scratch register or set of pins to the input shift
  register
- `out()`: shifts the bits from the output shift register to a destination
- `push()`: sends data to the RX FIFO
- `pull()`: receives data from the TX FIFO
- `mov()`: moves data from a source to a destination
- `irq()`: sets or clears an IRQ flag
- `set()`: writes a literal value to a destination

Modifiers are:

- `.side()`: sets the side-set pins at the start of the instruction
- `[x]`: delays for a `x` cycles after instruction execution

Directives are:

- `wrap_target()`: specifies where the program execution will get continued from
- `wrap()`: specifies the instruction where the control flow of the program will
  get wrapped from
- `label()`: sets a label for use with `jmp()` instructions
- `word()`: emits a raw 16 bit value which acts as an instruction in the program

### C

The PIO file is compiled via sdk, creating a header file to interact with.

Naming example:

- state machine declaration: `?`
- binding: `hello_program_init`, `hello_program_get_default_config`
- C header: `hello.pio.h`
- program: `hello_program`

## Micropython udev rules

```
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2e8a", MODE:="0666"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2e8a", MODE="0666"
```

