
# TypeScript

```sh
tsc # TypeScript compiler
tsc -w # watch ts and automatically compile to javascript
tsc --init # create a tsconfig.json configuration file
```
