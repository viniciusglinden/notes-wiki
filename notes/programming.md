
# Programming

## module operator

`x % y` produces the remainder when x is divided by y:

- y completely divides x: result of the expression is 0
- x is not completely divisible by y: result will be the remainder in the range [1, x-1]
- x is 0: division by zero is a compile-time error

```
x % y = r
x = 0 1 2 3 4 5 6 7 8 9 a b c d e f
y = 3 3 3 3 3 3 9 9 9 9
r = 0 1 2 0 1 2 6 7 8 0

        x
 ◄──────────────────┐
                    │
┌────────┬──────────┤
│        │          │
│   1    │     0    │
│       ┌┴─┐        │
│      ┌┘  └┐       │
│   ┌──┘    └─┐     │
│  ┌┘    2    └──┐  │
│┌─┘             └──┤
└┴──────────────────┘
```

## multithreading, concurrent, parallel, asynchronous

- [What is the difference between concurrency, parallelism and asynchronous
  methods?](https://stackoverflow.com/a/48530284/12932725)

- Concurrency: have multiple tasks run in concurrence for a resource (eg. CPU
  core)
- Parallelism: have multiple tasks run in parallel, for they don't need to share
  the same resource
- Multi-tasking: same as parallelism
- Multi-threading: software implementation of multiple threads, concurrent or
  parallel

Concurrency is used when for example there is a wait time for a response.
Parallel systems when throughput is critical.

Separate concept is asynchrony:

- Asynchrony: non-blocking processing (eg. via a callback)

## Multi-threading execution problems

- From: Clean Code

- Bound resource: resources of a fixed size or number used in a concurrent
  environment
- Mutual exclusion: only one thread can access shared data at a time
- Starvation: one thread or group is prohibited from proceeding for an
  excessively long time
- Deadlock: threads waiting for the other one to finish executing
- Livelock: threads in lockstep, each trying to do work but fining another "in
  the way"
