# USB

- [Official USB website](https://www.usb.org)
- [Introduction to USB with
  STM32](https://wiki.st.com/stm32mcu/wiki/Category:USB)
- [USB wikipedia](https://en.wikipedia.org/wiki/USB)
- [USB in a NutShell](https://www.beyondlogic.org/usbnutshell/usb1.shtml)

- Power: bus powered or self powered

OTG refers to the On-The-Go specification, which allows a device to act as a
host or device, depending on the situation. Usage examples are: cellphones.

Status are:

1. attached [hardware]: physically connected; no power
2. powered [hardware]
3. default [software]: powered then reset by the host; device operates with
   convenient speed selected by hardware during reset; device address is 0
4. addressed [software]: unique address
5. configured [software]: device first receives non-zero configuration number
6. suspended [software]: automatic on no data transfer after a known period
   (speed depended) inflicted by the host

## Topology

Strict tree network with master/slave. USB is strongly host-centric.

```
┌────┐   ┌─────────────────────────────────┐
│HOST├─┐ │          USB device             │
└────┘ │ │          ┌───────┐   ┌────────┐ │
       │ │        ┌─┤EP0 IN │◄──┤        │ │
       │ │        │ ├───────┤   │        │ │
       │ │ ┌────┐ ├─┤EP0 OUT├──►│        │ │
       ├─┼─┤ADDR├─┤ ├───────┤   │FUNCTION│ │
       │ │ └────┘ ├─┤EP1 IN │◄──┤        │ │
       │ │        │ ├───────┤   │        │ │
       │ │        └─┤EP1 OUT├──►│        │ │
       │ │          └───────┘   └────────┘ │
         └─────────────────────────────────┘
```

## Endpoint

Endpoint = source or destination data buffer, always on device side.
Characterized by a number + direction. Each endpoint is unique. Endpoint zero:
exclusive for control data, required for communication establishment.

- out endpoint transferred from the host
- in endpoint: transferred into the host

Endpoints can be viewed as simple buffers / FIFOs that divide the hardware and
software domains.

## Function

USB slaves / devices can implement different functions (USB-IF), based on
[device classes](https://www.usb.org/defined-class-codes). As an example, the
Communications Device Class (CDC, a Virtual COM port on Windows), which allows
us to use the USB as like an UART device; the Mass Storage Class (MSC) are
targeted for storage devices; Human Interterface Device (HID) are for keyboard,
mouse, etc.

## Data transfers

USB is LSbit first. D+ and D- differential pair uses NRZI encoding.

- control: small data
- bulk: large data
- interrupt: minimal latency
- isochronous: real-time requirements

Always on the host perspective

- data out = host -> device
- data in = device -> host

## Packet types / Datagrams

Four types of packets are allowed

### Token type

Used for type, destination, data phase direction

Values are: in, out or setup.

SYNC | PID | ADDR | ENDP | CRC5 | EOP

### Data type

Optional. Data types are DATA0, DATA1 and for HS DATA2 and MDATA.
Maximum payload size:

- LS: 8 bytes
- FS: 1023 bytes
- HS: 1024 bytes

Data can be sent in multiple bytes.

SYNC | PID | DATA | CRC16 | EOP

### Handshake type

Optional, depending on the token; also used for ACK

ACK, NACK or STALL, with STALL meaning that an intervention from the host is
required.

SYNC | PID | EOP

### Start of frame type

11 bit frame number send every:

- FS: 1ms ± 500ns
- HS 125µ ± 0.0625 µs

SYNC | PID | frame number | CRC5 | EOP

### Packet fields

- SYNC: 8 bit for LS and FS, 32 bit for HS. Used to synchronise the clock. Last
  two bits indicate where the PID field starts.
- PID: Packet ID. Each nibble has its particular meaning. The nibble is repeated
  to ensure correctness. Up to four PID can be send in sequence (?).
- ADDR: Varies between 1 to 127, with 0 being reserved for still-unconfigured
  (newly attached) devices. The enumeration procedure is responsible for the new
  devices detection and inclusion in the software bus.
- ENDP: end point field, 4 bits long, 16 possible endpoints. LS allows maximum 4
  endpoints.
- CRC: CRC5 or CRC16
- EOP: end of packet. single ended zero (SE0) for 2 bit times followed by J for
  1 bit time

## Descriptors

Set of data stored in the device in a specific order that is known both to the
device and to the host. The host can request the descriptors which will be sent.
Descriptors are both generic and class specific.

## Driver

After connecting, the appropriate driver, if provided, will be decided by the
PID/VID[^PID] (Product ID / Vendor ID). There are VID for fiddling (hobbyist),
non-commercial activities and teaching. PIDs can be bought from third-party
vendors.

[^PID]: Depending on the context, PID may mean Packet ID

## Hardware

- classic connector: type A
- "arduino" connector: type B (used for devices)
- Bus voltage: 5V
- differential pair voltage: 200mV
- characteristic impedance: 90Ω
- maximum cable length: 5m

pin number|color|function
-|-|-
1|red|VBUS
2|white|D- / DN
3|green|D+ / DP
4|black|GND

Speed|Frequency [Hz]
-|-
High Speed (HS)|480Mbps

The differential pair can be used as single-ended to broadcast special signals,
like the reset signal. High speed mode uses a 17.78mA constant current for
signaling to reduce noise.
