
# ffmpeg

## resources

- [20+ FFmpeg Commands For Beginners - OSTechNix](https://ostechnix.com/20-ffmpeg-commands-beginners/)

## video formats

- `.mkv` can have subtitles stored in the same file
- `.avi` cannot have subtitles stored in the same file
- `.mp4`
- `.m4v`

## subtitle formats

- ASS (Advanced Substation Alpha)
- STR

## subtitle conversion

If your subtitle is in SubRip, MicroDVD or any other supported text subtitles, you have to convert it to ASS before using this filter:

```sh
ffmpeg -i subtitle.srt subtitle.ass
```

## Burn subtitles into video

You can burn text subtitles (hardsubs) with one of two filters: subtitles or ass.

### TL;DR

I had `teste.mkv` and `teste.str`. So I ran:

```sh
ffmpeg -i teste.srt teste.ass
ffmpeg -i teste.mkv -vf "ass=teste.ass" out.avi
```

This will permanently print the subtitle onto the pictures. Thus, you will not
able to turn off. Perhaps changing the output file format will do something?!?

### subtitles filter

Draw subtitles on top of input video using the libass library. This filter requires ffmpeg to be compiled with `--enable-libass`.

If the subtitle is a separate file called `subtitle.srt`, you can use this command:

```sh
ffmpeg -i video.avi -vf subtitles=subtitle.srt out.avi
```

If the subtitle is embedded in the container `video.mkv`, you can do this:

```sh
ffmpeg -i video.mkv -vf subtitles=video.mkv out.avi
```

### ass filter

Same as the subtitles filter, except that it doesn't require libavcodec and libavformat to work. This filter requires ffmpeg to be compiled with `--enable-libass`. On the other hand, it is limited to ASS subtitles files.

```sh
ffmpeg -i video.avi -vf "ass=subtitle.ass" out.avi
```

Windows users will have to setup font paths to get libass to work

### Picture-based subtitles

You can burn "picture-based" subtitles into a movie as well, by using the overlay video filter to overlay the images. For instance, dvdsub is a type of picture-based overlay subtitles. Example of an MKV with dvdsub subtitles in a separate stream:

```sh
ffmpeg -i input.mkv -filter_complex "[0:v][0:s]overlay[v]" -map "[v]" -map 0:a <output options> output.mkv
```

If you have multiple subtitle streams, you can select which one to use by replacing `[0:s]` with `[0:s:0]` to select the first subtitle stream or `[0:s:1]` to select the second subtitle stream, and so on.

Sometimes adding this filter have a side effect of breaking encoding when input file has multiple audio streams and some of those may have problems; in this case selecting one (or more) of the audio streams should fix the problem:
```sh
ffmpeg -i input.ts -filter_complex "[0:v][0:s]overlay[v]" -map "[v]" -map 0:a:0 <output options> output.mkv
```

## convert format

To list the supported formats: `ffmpeg -formats`

```sh
ffmpeg -i video.mp4 video.avi
```

Or, to preserve the source's quality:

```sh
ffmpeg -i input.webm -qscale 0 output.mp4
```

## video to audio

```sh
ffmpeg -i input.mp4 -vn output.mp3
```

Possible flags:

- `vn`: disable video
- `ar`: audio frequency (common values: 22050, 44100, 48000 Hz)
- `ac`: audio channels
- `ab`: audio bitrate
- `f`: output file format

Example:

```sh
ffmpeg -i input.mp4 -vn -ar 44100 -ac 2 -ab 320 -f mp3 output.mp3
```

## change volume in audio files

```sh
ffmpeg -i input.mp3 -af 'volume=0.5' output.mp3
```

## change video resolution

```sh
ffmpeg -i input.mp4 -filter:v scale=1280:720 -c:a copy output.mp4
ffmpeg -i input.mp4 -s 1280:720 -c:a copy output.mp4
```

## compress video

```sh
ffmpeg -i input.mp4 -vf scale=1280:-1 -c:v libx264 -preset veryslow -crf 24 output.mp4
```

`crf` flag controls the aggressiveness (the higher the value, the more
compressed the file is).

Use the following flags to also compress the audio:

```sh
-ac 2 -c:a aac -strict -2 -b:a 128k
```

## compress audio

`ab` = audio bitrate.

```sh
ffmpeg -i input.mp3 -ab 128 output.mp3
```

Common bitrates are: 96, 112, 128, 160, 192, 256, 320 kbps.

## remove audio from video

```sh
ffmpeg -i input.mp4 -an output.mp4
```

## video screenshot

```sh
ffmpeg -i input.mp4 -r 1 -f image2 image-%2d.png
```

- `r`: frame rate
- `f`: output format (image)
- `%2d`: 01, 02, 03, etc (for 001, 002, 003, use `%3d`)

## crop (readjust video dimensions)

```sh
ffmpeg -i input.mp4 -filter:v "crop=w:h:x:y" output.mp4
```

- `-filter:v`: video filter
- `crop`: crop filter
- `w`: width of the rectangle we want to crop
- `h, x, y`: height, x and y coordinates, respectivly

Example, width 640 px, height 480 px, position (200, 150):

```sh
ffmpeg -i input.mp4 -filter:v "crop:640:480:200:150" output.mp4
```

## convert a specific portion of video

```sh
ffmpeg -i input.mp4 -t 10 output.avi
```

Will convert the first 10 seconds of input.mp4 to output.avi. It is also
possible to specify time in `hh.mm.ss`.

## change aspect ratio

```sh
ffmpeg -i input.mp4 -aspect 16:9 output.mp4
```

Common aspect ratios are: 16:9 (1920x1080), 4:3, 16:10, 5:4, 2:21:1, 2:35:1,
2:39:1.

## combining image to audio files

```sh
ffmpeg -loop 1 -i image.jpg -i audio.mp3 -c:v libx264 -c:a aac \
	-strict experimental -b:a 192k -shortest output.mp4
```

Note: you may have to change the audio encoder according to the audio file
format.

## trim video and audio files

```sh
ffmpeg -i input.mp4 -ss 00:00:50 -codec copy -t 8 output.mp4
```

- `-ss`: starting time (start stamp)
- `-t`: total time

Therefore, this command would trim `input.mp4`, starting from 50 seconds to 58
seconds, outputting it to `output.mp4`.
Another way to do the same thing is by using the `-to` flag:

- `-to`: end time (to time)

```sh
ffmpeg -i input.mp4 -ss 00:00:50 -codec copy -to 58 output.mp4
```

## split video/audio

```sh
ffmpeg -i input.mp4 -t 00:00:30 -c copy part1.mp4 -ss 00:00:30 \
	-codec copy part2.mp4
```

## merging/concatenating files into one

First, create a list file (join.txt) as below:

```
file /home/vglinden/video1.mp4
file /home/vglinden/video2.mp4
```

Then:

```sh
ffmpeg -f concat -i join.txt -c copy output.mp4
```

If you get an "unsafe file name" or "not permitted" error, just add the safe
flag:

```sh
ffmpeg -f concat -safe 0 -i join.txt -c copy output.mp4
```

## add subtitles to video

```sh
ffmpeg -i input.mp4 -i subtitle.srt -map 0 -map 1 -c copy -c:v libx264 -crf 23 \
	-preset veryfast output.mp4
```

## file preview

```sh
ffplay video.mp4
ffplay audio.mp3
```

## change video speed

```sh
ffmpeg -i input.mp4 -vf "setpts=0.5*PTS" output.mp4
```

Note: `0.5*PTS` increases playback speed, `4.0*PTS` decreases playback speed

## change audio speed

```sh
ffmpeg -i input.mp4 -filter:a "atempo=2.0" -vn output.mp4
```

## screencast (record screen)

You can stop it with "ctrl+c".
Sometimes, if the video is too small, it will not write to the file.

For recording with sound, add `-f pulse -i default` for pulse audio, or `-f alsa
-i default` for alsa. Note that you must have set the "default" recorder before.
You can also add the `-ac 2` flag, thus `-f pulse -ac 2 -i default`.

### screen

```sh
ffmpeg -f x11grab -i :0.0 out.mkv
```

Note: to record the webcam inside the screen, open a window:

```sh
mpv --no-cache --no-osc --input-conf=/dev/null --title=webcam /dev/video0
```

### webcam

```sh
ffmpeg -i /dev/video0 out.mkv
```











