
# KDoc

KDoc is the official syntax for documenting Kotlin code.
[Dokka](https://github.com/Kotlin/dokka) is the official documentation compiler.

## Example

```kotlin
/**
 * This is a sample interface that represents a calculator.
 * It defines basic arithmetic operations.
 */
interface Calculator {
    /**
     * Adds two numbers and returns the result.
     *
     * @param a The first number.
     * @param b The second number.
     * @return The sum of the two numbers.
     */
    fun add(a: Int, b: Int): Int

    /**
     * Subtracts one number from another and returns the result.
     *
     * @param a The number to subtract from.
     * @param b The number to subtract.
     * @return The difference between the two numbers.
     */
    fun subtract(a: Int, b: Int): Int
}

/**
 * A group of *members*.
 *
 * This class has no useful logic; it's just a documentation example.
 *
 * @param T the type of a member in this group.
 * @property name the name of this group.
 * @constructor Creates an empty group.
 */
class Group<T>(val name: String) {
    /**
     * Adds a [member] to this group.
     * @return the new size of the group.
     */
    fun add(member: T): Int { ... }
}
```
