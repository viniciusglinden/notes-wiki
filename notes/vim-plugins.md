
# Vim plugins

## Vim-plug plugin manager

[vim-plug](https://github.com/junegunn/vim-plug), and execute in terminal the curl script. This will automatically install vim-plug in your vim folder, by creating a autoload folder.

Add to the vimrc file the following:

```
call plug#begin()
	Plug 'user/plugin'
call plug#end()
```

This only references the plugin, now we have to install it with `PlugInstall` (no options).

Every plugin will be inside `.vim/plugged` folder.

The user and plugin names are the same as in Github.
Full URL can also be inserted. System paths can also be inserted. Options for
which branch of git can also be inserted: `Plug 'user/plugin', { 'branch': 'stable' }`.

To search for a plugin, access [vimawesome website](https://vimawesome.com/).

## YouCompleteMe

Not recommended.

1. Make you sure you have the packages: `build-essential`, `python3-dev`
2. First, make sure you have support for `python3`
3. Then, let you vim plugin manager install it for you
4. `cd ~/.config/nvim/plugged/YouCompleteMe`
5. `python3 install.py --all` for full support to all languages

In arch systems with neovim, support for python is a package.

By default YCM will not do anything in C language files. YCM uses a python script called .`ycm_extra_conf.py` to set project wide settings, needed to provide completion and syntax check. Search for `ycm_extra_conf.py` and point to it in your vimrc with `let g:ycm_global_ycm_extra_conf = "~/.config/nvim/ycm_extra_conf.py"`.
Not tested: You can get CMake to generate this file for you by adding: `set( CMAKE_EXPORT_COMPILE_COMMANDS 1 )` to your CMakeLists.txt file.
You may also be interested in [YCM-Generator](https://github.com/rdnetto/YCM-Generator).
Or just get the one from the [arch wiki
page](https://wiki.archlinux.org/index.php/Vim/YouCompleteMe).

## Syntastic

Syntastic-compatible linter per language:

- python: `flake8` or `pylint`
- prose: [proselint](https://pypi.org/project/proselint/) (could not get it to
  work with syntastic)
- VHDL: ghdl

## Conquer of Completion (CoC)

Autocompletion through Language Server Protocol (LSP). Requires Node.js.

You also have to install a Language Server and tell CoC which one to use. For
C(++) I did:

1. `sudo pacman -S npm ccls` (`ccls` is a C language family server)
2. entered vim
3. `:CocConfig`
4. typed in:

```vimL
{
	"languageserver": {
		"ccls": {
			"command": "ccls",
				"filetypes": [
					"c",
				"cpp",
				"objc",
				"objcpp"
				],
				"rootPatterns": [
					".ccls",
				"compile_commands.json",
				".vim/",
				".git/",
				".hg/"
				],
				"initializationOptions": {
					"cache": {
						"directory": "/tmp/ccls"
					}
				}
		}
	}
}
```

- [neoclide/coc.nvim](https://github.com/neoclide/coc.nvim)
- [Configure coc.nvim for C/C++
  Development](https://ianding.io/2019/07/29/configure-coc-nvim-for-c-c++-development/)
- [Language Servers](https://github.com/neoclide/coc.nvim/wiki/Language-servers)
- [JSON Compilation Database Format
  Specification](https://clang.llvm.org/docs/JSONCompilationDatabase.html)

## Cscope

cscope is a programming tool which works in console mode,
text-based interface, that allows computer programmers or
software developers to search source code of the programming
language C, with some support for C++ and Java.

First, you have to build the database, then use it. Steps are (for C language):

1. `cd ~/project`
2. `find . -name "*.c" -o -name "*.h" > cscope.files`
3. `cscope -q -R -b -k -i cscope.files`
4. `cscope -d`

- `-q` build faster;
- `-R` recursive;
- `-b` do not start interactive use;
- `-i` sources the file;
- `-d` start interactive use.
- `-k` do not look under `/usr/include`

This will generate four files:

1. `cscope.files`: list of included files
2. `cscope.out`: symbol cross-reference
3. `cscope.in.out`, `cscope.po.out`: inverted index for symbol search

To change the default text editor, set the `CSCOPE_EDITOR` environment variable
(note the backticks, not single-quotes [are they to evaluate the expression?]):

```sh
export CSCOPE_EDITOR=`which emacs`
```

Notes:

- ctrl-c disables case-sentive search. Use ctrl-d
- ctrl-b search for previous search terms

## clang-rename

[clang-rename](https://clang.llvm.org/extra/clang-rename.html#vim-integration)

## clangd LSP

- [clangd installation](https://clangd.llvm.org/installation.html)

If all files in a project use the same build flags, you can put those flags
one-per-line in `compile_flags.txt` in your source root.

## Developing a lua plugin

- [Debugging Lua in
  Neovim](https://zignar.net/2023/06/10/debugging-lua-in-neovim/)
- [How to write a neovim plugin in
  lua](https://miguelcrespo.co/posts/how-to-write-a-neovim-plugin-in-lua/)
- [How to write nvim plugins in
  lua](https://dev.to/2nit/how-to-write-neovim-plugins-in-lua-5cca)


Nvim puts additional functions on top of lua, therefore you cannot simply write
a lua file to test some stuff. Fortunately, you can use nvim as the interpreter
with the `#!/usr/bin/env -S nvim -l` shebang[^nlua]. This lets you use lua test
frameworks like [busted](https://lunarmodules.github.io/busted/) to run unit
tests.

[^nlua]: Alternatively use [nlua](https://github.com/mfussenegger/nlua) instead

There is also [one small step for
vimkind](https://github.com/jbyuki/one-small-step-for-vimkind) lua plugin debug
adapter.

A plugin anatomy looks like the following:

```
├── LICENSE
├── plugin
│  └── plugin-file.lua
├── lua
│  └── main-file.lua
└── README.md
```

- plugin: all files inside will get executed as soon as neovim starts
- lua: executed only when explicitly `require`d

To develop it, it is recommended to put the repository somewhere in the computer
and load it with the path via the plugin manager.
