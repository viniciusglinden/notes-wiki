
# Sudo

Comments are marked by `#` and can be put at any line, working just like a C++
comment.

Basically use `su` to get super user privileges, while `$USER` still being
your user. Use `sudo -i` to get super user privileges, while being root.

## give a user sudo priviledge

```sh
usermod -aG sudo <USER>
# or
gpasswd -a <USER> sudo
```

## editing sudoers file

- set **for the root** the `EDITOR` variable to some editor, eg nvim
- `su`
- `visudo`

If you want to add a command for unprivileged execution for a particular user:

```
joe ALL=(ALL) NOPASSWD: /full/path/to/command
```

To restrict access to this command, eg. no execution of `pacman` but only
`pacman -Sy`:

```
joe ALL=(ALL) NOPASSWD: /usr/bin/pacman -Sy
```

It functions basically like the following:

```
root ALL=(ALL:ALL) ALL
(1)  (2) (3)  (4)  (5)
```

1. username that the rule will apply to
2. which host the rule will apply to
3. can run commands as all users
4. can run commands as all groups
5. rules to apply

Groups are marked by the `%` character.

The `#includedir /etc/sudoers.d` sources other rules.

## alias

```
User_Alias	GROUPONE = abby, brent, carl
Runas_Alias	OP = root, operator
Host_Alias	OFNET = 10.1.2.0/255.255.255.0
Cmnd_Alias	PRINTING = /usr/sbin/lpc, /usr/bin/lprm
```

Then

```
GROUPONE ALL = /path/to/command
```

## autofix sudoers file

```sh
pkexec visudo
```

