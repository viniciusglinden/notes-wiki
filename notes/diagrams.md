
# Diagrams

## ASCII

- [asciiflow](http://asciiflow.com) to draw diagrams by hand
- [ASCII Art](https://ascii.mastervb.net/) to automatically convert images to
ASCII art

From dimmest to brightest:

```
.,-~:;=!*#$@
```

## Waveform

- [WaveDrom](https://wavedrom.com)

Wavedrom is available for pc and web. There is also a python implementation
(`pip install wavedrom`), which lets you do everything in python. For example:

```py
import wavedrom
svg = wavedrom.render("""
{signal: [
  {name: 'clock', wave: 'p.....|...'},
  {name: 'dado',  wave: 'x.====|=x.', data: ['1', '2', '3', '...', 'N']},
  {name: 'valid', wave: '0.10..|...'},
  {name: 'ready', wave: '1..0..|..1'}
]}
""")
svg.saveas("nvme_stream.svg")
```

Alternatively, use [PlantUML
timing-diagrams](https://plantuml.com/timing-diagram)

- [ASCIIWave](https://github.com/Wren6991/asciiwave#asciiwave-wavedrom-to-ascii-art)
- [TimingAnalyzer](https://www.timing-diagrams.com)

Timing diagrams can be created with the GUI, Python scripts, VCD files, and logic
simulations.

## bitfield

- [bitfield](https://github.com/wavedrom/bitfield): you can also use the
  wavedrom online editor

Basic syntax is:

```
{reg: [
    { "name": "IPO",   "bits": 8, "attr": "RO" },
    {                  "bits": 7 },
    { "name": "BRK",   "bits": 5, "attr": "RW", "type": 4 },
    { "name": "CPK",   "bits": 1 },
    { "name": "Clear", "bits": 3 },
    { "bits": 8 }
]}
```

## flowchart

- [flowchart.fun](https://flowchart.fun/)

## logic circuit

- [LogiDrom](https://github.com/wavedrom/logidrom)

## datasheet generator

- [datasheet](https://github.com/wavedrom/datasheet)

## graph

Graph Visualization Software.

- [graphviz examples](https://sketchviz.com/graphviz-examples)
- [graphviz](https://www.graphviz.org/)
- [graphviz online graphical editor](http://magjac.com/graphviz-visual-editor/)
- [graphviz online graphical editor](https://edotor.net/)
- [graphviz documentation](https://www.graphviz.org/documentation/)

Note: Markdown renderer normally supports this syntax.

### State Machine

- [How to create a state diagrams with
  Graphviz](http://sfriederichs.github.io/how-to/graphviz/2017/12/07/State-Diagrams.html)
- [Finite Automaton](https://graphviz.org/Gallery/directed/fsm.html)

```dot
digraph example {
    rankdir=LR; //left-right orientation
    splines=ortho
    node [shape=record, fontname=Helvetica, fontsize=10];
    b [ label="class B" URL="\ref B"];
    c [ label="{mealy | value }"];
    b -> c [ arrowhead="open", style="dashed" ];
}
```

## Generic

- [DrawIO](https://www.draw.io)

## PlantUML

[PlantUML](plantuml.md)

## ASCII circuit diagrams

- [AACircuit](https://github.com/Blokkendoos/AACircuit)
