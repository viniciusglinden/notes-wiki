# PlantUML

[PlantUML](https://plantuml.com/) is a superset of graphviz diagrams, made for
UML and some non-UML diagrams.

- [Guide to
  PlantUML](https://crashedmind.github.io/PlantUMLHitchhikersGuide/layout/layout.html)
- [All skin
  parameters](https://plantuml-documentation.readthedocs.io/en/latest/formatting/all-skin-params.html)
- [PlantUML tips and
  tricks](https://sarafian.github.io/tips/2021/03/11/plantuml-tips-tricks-1.html)
- [PlantUML class diagrams](https://plantuml.com/class-diagram)

It is possible [to render PlantUML in Gitlab and
Github](https://blog.anoff.io/2018-07-31-diagrams-with-plantuml/), requiring
just a setup configuration.

```plantuml
@startuml
!theme carbon-gray
skinparam {
    dpi 200
    ArrowColor Black
    defaultFontColor Black
    defaultFontName Tahoma
    SequenceLifeLineBorderColor Black
    ParticipantBorderColor Black
}
'scale 100 width

participant Me as A
participant You as B
participant "Someone else" as C
C -> A: Some really long string
A <- A: getting\nto\nitself
A -> B: he he
' This is a comment on a single line
/' You quote alors use slash-and-quote
to split your comments on several
lines '/
@enduml
```

