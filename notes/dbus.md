
# D-Bus

- [Quick Guide to D-Bus with
  Python](https://www.onurguzel.com/quick-guide-to-d-bus-with-python/)
- [Creatomg a BLE peripheral with
  bluez](https://punchthrough.com/creating-a-ble-peripheral-with-bluez/)
- [dbus tutorial](https://dbus.freedesktop.org/doc/dbus-tutorial.html)
- [Introduction to dbus](https://www.freedesktop.org/wiki/IntroductionToDBus/)

D-Bus is an Inter-process Communication (IPC) method that allows one to make
Remote Procedure Calls (RPC) between different processes.
To monitor dbuses, use `d-feet`.

There are two kind of buses: system and session. There can only be one system
bus, but many session bus.

- bus name: applications
- object path: implements one or more interfaces, eg. `/com/company/foo/bar`
- interfaces: object types, same name as bus names, provides methods, signals
  and properties.
- methods: functions that act on
- signals: events other processes may listen for
- proprieties: objects constants or variables
- proxies object: programming language object to represent an object path
  reply, making it easier to send and wait for replies

```
Address -> [Bus Name] -> Path -> Interface -> Method
```
