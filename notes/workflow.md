
# Workflow

## Coding style

Useful rules to follow:

- a function is always at the same abstraction level
- use meaningful names
- don't pass boolean to functions


## Code review workflow

- [Engenharia de software: qualidade com
  automação](https://www.youtube.com/watch?v=lLnuQlK_iiw&feature=youtu.be)

Use [Jenkins](https://www.jenkins.io/) with
[Gerrit](https://www.gerritcodereview.com/).

- Jenkins is an automation server, which helps automate the parts of software
  development related to building, testing and deploying
- Gerrit is a code collaboration tool, thus teams can review each other's
  modifications on their source code via the web browser, approving or rejecting
  changes

Jenkins can be loaded to test the source code for unit tests, syntax and much
more.

It is also possible to use Gitlab (or GitHub) with Jenkins.
