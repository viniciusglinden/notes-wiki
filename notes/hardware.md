
# Hardware

- [mbedded.ninja](https://blog.mbedded.ninja/)

- simplex: data is sent in one direction only
- half-duplex: only one direction at a time, but can be sent both ways
- full-duplex: can send to both directions simultaneously

## Endianess

Memo:
Example: 0x0102

- Big Endian: the MSB (big) end comes first: 0x01, 0x02
- Little Endian: the LSB (little) end comes first: 0x02, 0x01

```
BYTE3 BYTE2 BYTE1 BYTE0
MSB                 LSB
        higher
big    address   little
BYTE0     ▲       BYTE3
BYTE1     │       BYTE2
BYTE2     │       BYTE1
BYTE3     │       BYTE0
        lower
       address
```

[Image with description](https://en.wikipedia.org/wiki/File:Endianessmap.svg)

The web is always Big-endian (convention). For programming in C, there are standard libraries to
convert from internet convention to host machine: see `htonl` (host to long). For structs there is
also an `__attribute__(()))` called `__attribute__((scalar_storage_order("big-endian"))`.

## baudrate

Baudrate is the number of symbols per second.

Baudrate = bit rate [bps] / number of bits [bit per symbol].

A frame may or may not be a symbol - depends on the protocol.

## PCIe bus

[PCIe bus page](pcie.md)

## SPI bus

- [Using Serial Peripheral Interface (SPI) Master and Slave with Atmel AVR
  Microcontroller ](http://www.ermicro.com/blog/?p=1050)
- [Introduction to SPI
  interface](https://www.analog.com/media/en/analog-dialogue/volume-52/number-3/introduction-to-spi-interface.pdf)

- MISO = Master Input, Slave Output
- MOSI = Master Output, Slave Input
- SDI = Serial Data Input
- SDO = Serial Data Output
- DI = Data Input
- DO = Data Output
- SI = Serial Input
- SO = Serial Output
- CPOL = Clock POLarity
- CPHA = Clock PHase

MOSI:

- SDI, DI, DIN, SI - on slave devices; connects to MOSI on master
- SDO, DO, DOUT, SO - on master devices; connects to MOSI on slave

MISO:

- SDO, DO, DOUT, SO - on slave devices; connects to MISO on master
- SDI, DI, DIN, SI - on master devices; connects to MISO on slave

```
┌───────────────┐    ┌───────────────┐
│               │    │               │
│            CSn├───►│CSn            │
│  MASTER       │    │        SLAVE  │
│            SCL├───►│SCL            │
│               │    │               │
│           MOSI├───►│SDI            │
│               │    │               │
│           MISO│◄───┤SDO            │
│               │    │               │
└───────────────┘    └───────────────┘
```

Devices other than MCUs having SPI interface tend to use SDI/SDO or DIN/DOUT
convention (e.g. ADC/DAC chips, digital potentiometers, sensors etc). But MCUs
always use MOSI/MISO convention (MSP430 series MCUs have a slight difference at
this point: They have SIMO/SOMI pins which are totally the same as MOSI/MISO).
This is because the MCUs are assumed (which should be) to be master.

With 3-wire interface, there is only: CS, SCL and Data. Data bus is negotiated
between master and slave.

### two MCUs communicating

If two MCUs are communicating through SPI then the "master" tag alternates
between the two (i.e. sender becomes "master" at the time), but the connection
does not change: MOSI on the 1st chip connects to MISO on the 2nd chip and
vice-versa.

### length matching

Length matching is not necessary.

### implementation

Master and slave hardwares are almost the same, the only difference is where the
clock generation lies.

SPI can be describes as a shared circular buffer make out of shift registers.

```
  ┌───────────────────┐      ┌───────────────────┐
  │      MASTER       │      │      SLAVE        │
  │ ┌─┬─┬─┬─┬─┬─┬─┬─┐ │ MOSI │ ┌─┬─┬─┬─┬─┬─┬─┬─┐ │
┌─┤►│0│1│2│3│4│5│6│7├─┼──────┤►│0│1│2│3│4│5│6│7├─┼─┐
│ │ └─┴─┴─┴─┴─┴─┴─┴─┘ │ SCL  │ └─┴─┴─┴─┴─┴─┴─┴─┘ │ │
│ │                   ├─────►│                   │ │
│ └───────────────────┘      └───────────────────┘ │
│                       MISO                       │
└──────────────────────────────────────────────────┘
```

### modes: CPOL and CPHA

- [SPI bus: Clock Polarity and Clock Phase by
  example](https://deardevices.com/2020/05/31/spi-cpol-cpha)

First we need to acknowledge that, the data may be changed at one clock edge and
read in another.

Clock Polarity means: the bit of a valid pulse.

- `CPOL=0`: idles at 0; cycle consists of a pulse of 1
  - Ensues: leading edge is a rising edge; trailing edge is a falling edge
- `CPOL=1`: idles at 1; cycle consists of a pulse of 0
  - Ensues: leading edge is a falling edge; trailing edge is a rising edge

Clock Phase means: which edge the slave samples the bus (leading or trailing).

Therefore, to determine the mode:

- How the clock idles -> CPOL
- Where data is sampled -> CPHA

The following table is a standard, but it can also vary - always check:

mode|CPOL|CPHA|Explanation
-|-|-|-
0|0|0|sampled on rising, changed on falling
1|0|1|sampled on falling, changed on rising
2|1|0|sampled on rising, changed on falling
3|1|1|sampled on falling, changed on rising

## I2C

- [I2C Bus Specification](https://i2c.info/i2c-bus-specification)
- [I²C-Bus Specification and User Manual](https://www.nxp.com/docs/en/user-guide/UM10204.pdf)
- [I²C](https://deepbluembedded.com/stm32-i2c-tutorial-hal-examples-slave-dma/): intended for STM32,
  but contains a complete practical I²C information
- [I²C STM32 Bridge](https://github.com/daniel-thompson/i2c-star)

Compatible interfaces (subset):

- SMBus: System Management Bus
- PMBus: Power Management Bus

Name|Typ. frequency|Pull-up value
-|-|-
Standard|100kHz|10kΩ
Fast mode|400kHz|2kΩ
Fast mode+|1MHz|2kΩ

### Reserved addresses

slave adr.|rd|description
-|-|-
0000 000|0|general call address
0000 000|1|START byte
0000 001|x|CBUS address
0000 010|x|Reserved for different bus format
0000 011|x|Reserved for future purposes
0000 1xx|x|Hs-mode master code
1111 1xx|x|Reserved for future purposes
1111 0xx|x|10b slave addressing

### Features

- Clock stretch: the slave device is not ready to accept more data, thus holds the clock line low.
- Arbitration: multiple masters in one bus. If SDA is found high when it was supposed to be low, it
  is interred that another master is active.

Not a feature: [hot
swap](https://www.berrybase.de/en/adafruit-tca4307-hot-swap-i2c-buffer-mit-stuck-bus-recovery).

### standard values

- read/write bit: read = 1, write = 0
- Data 1: SDA high on SCL rising edge
- Data 0: SDA low on SCL rising edge

### data frame

[Understanding the I²C Bus](https://www.ti.com/lit/an/slva704/slva704.pdf)

```
[start][address][wr/rd][ack/nack][data 1][ack/nack][data 2][ack/nack]...[stop]
```

- start: 1 bit
- address: 7 or 10 bits
- wr/rd: 1 bit
- ack/nack: 1 bit
- data: 8 bits
- stop: 1 bit

Master signals:

- Start Condition: SDA switches from high to a low *before* SCL switches from high to low
- Restart Condition: same as start condition
- Stop Condition: SDA switches from low to high *after* the SCL switches from low to high
- Address Frame: 7 or 10 bit sequence unique to each slave that identifies the slave
- Read/Write: One bit specifying whether the master is sending data to the slave (low) or requesting
  data from it (high)

Slave signals:

- ACK: low
- NACK: high

### length matching

Length matching is not necessary.


## UART USART RS232

- [UART: A Hardware Communication Protocol Understanding Universal Asynchronous
  Receiver/Transmitter](https://www.analog.com/en/analog-dialogue/articles/uart-a-hardware-communication-protocol.html)

### pins and glossary

- XOFF, XON: special commands to start/stop slave
- DTR: Data Terminal Ready, line to say that there is a receiver on the other
  side, connect to DSR
- DSR: Data Set Ready
- RTS: Request To Send, coordinates when to send the data, connect to CTS
- CTS: Clear To Send
- Tx and Rx are normally high
- DCD: Data Carrier Detect, outdated, was used by modems, tells when an analog
  signal was being received
- RI: Ring Indicator, outdates, was used by modems, tells that the phone is
  ringing
- SG: Signal Ground, same as GND

RTS and CTS and known as "hardware flow control", whereas XOFF and XON are known
as "software flow control".

- If DTR and CTS are not used, just connect both from the same end together.
- If RTS and CTS are not used, just connect both from the same end together.

## packet frame

```
[start bit][data frame][parity bit][stop bits]
```

- start bit: 1 bit
- data frame: between 5 to 9 bits
- parity bit: 1 bit wide, optional
- stop bits: 1 to 2 bits, second bit may indicate next package frame separation

### standard values

- Baud rate: 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600, 1000000,
  1500000 [bps]
- data frame is usually LSB-first
- 1 start bit
- 8 data bits
- 1 parity bit
- 1 stop bit

## Single Wire Debug (SWD)

- [SWD](https://wiki.segger.com/SWD)
- [Introduction to ARM SWD
  protocol](https://developer.arm.com/documentation/ihi0031/a/The-Serial-Wire-Debug-Port--SW-DP-/Introduction-to-the-ARM-Serial-Wire-Debug--SWD--protocol)

Is a 2-pin interface that uses the JTAG protocol, available in (some?) ARM
CPUs. SWD has a built-in error detection. SWD can either be synchronous or
asynchronous.

- `BOOT0`
- `NRST`
- `SWCLK`, `TCK` JTAG equivalent
- `SWIO`: Bi-directional data pin, pulled up on the target, equivalent to JTAGs
  `TMS`
- `VDD` and `GND`
- `SWO`: Optional unidirectional output, used for semi-hosting

Notes:

- `BOOT0` pin is not part of the SWD protocol
- tie `NRST` pin to ground through a 100nF.

Each sequence of operations on the wire consists of two or three phases:

- Packet request: The external host debugger issues a request to the DP. The DP
  is the target of the request.
- Acknowledge response: The target sends an acknowledge response to the host.
- Data transfer phase:
  - Only present when either:
    - a data read or data write request is followed by a valid (OK) acknowledge
      response
    - the ORUNDETECT flag is set to 1 in the CTRL/STAT Register
  - The data transfer is one of:
    - to host, following a read request (RDATA)
    - to target, following a write request (WDATA).

### connection

Connection is hot-pluggable: for this the protocol is:

- host sends `SWIO` = 1 for 50 consecutive `SWCLK` cycles - aka line reset
  sequence
- host reads `IDCODE` register

## PDM

Pulse Duration Modulation is the greater set of PWM.

Let duty cycle be $D$, between 0 and $N$, inclusive. So fractional duty cycle is
really $100\%·D/N$.

To generate the PWM signal for one period:

```
For T = 0 to N-1, inclusive:
    If D < T, then:
        Output level is High
    Else:
        Output level is Low
    End If
End For
```

To generate the corresponding PDM signal:

```
V = 0
For T = 0 to N-1, inclusive:
    V = V + D
    If V ≥ N, then:
        Output level is High
        V = V - N (or, equivalently, V = V modulo N)
    Else:
        Output level is Low
    End If
End For
```
