
# Catch2

- [Official guide](https://github.com/catchorg/Catch2/blob/devel/docs/tutorial.md)

## system-wide installation on arch

- install with `pacman -S catch2`
- header file is located at `/usr/include/catch2/catch.hpp`

## writing tests in C++

```cpp
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

unsigned int Factorial( unsigned int number ) {
	return number <= 1 ? number : Factorial(number-1)*number;
}

TEST_CASE( "Factorials are computed", "[factorial]" ) {
	REQUIRE( Factorial(1) == 1 );
	REQUIRE( Factorial(2) == 2 );
	REQUIRE( Factorial(3) == 6 );
	REQUIRE( Factorial(10) == 3628800 );
}
```

If you want to separate in multiple files, implement the test cases in header
files

```cpp
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
```

## writing tests in C

```cpp
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

extern "C" {
#include "../src/blockchain.h"
}

TEST_CASE("hash is correct") {
	REQUIRE( hash(0) == 3398926610 );
}
```

## more descriptive unit test

- [Basic C++ Unit Testing with Coverage Using Catch2 and
  Gcov](https://jasoncarloscox.com/blog/cpp-testing-with-coverage)

`TEST_CASE` and `SECTION` macros are too generic.

- `SCENARIO` macro provides a top-level description of a test case
- `GIVEN` macro allows you to set up the conditions needed for the test
- `WHEN` macro is used to execute the code under test
- `THEN` macro typically contains one or more `REQUIRE` statements to assert
  that everything went as planned

```cpp
#include "catch.hpp"
#include "LinkedList.h"

SCENARIO("elements can be added to the end of a LinkedList", "[linkedlist]") {
	GIVEN("an empty LinkedList") {
		LinkedList<int> list;

		WHEN("an element is added") {
			list.add(4);

			THEN("the length increases by 1") {
				REQUIRE(list.length() == 1);
			}

			THEN("the element is added at index 0") {
				REQUIRE(list.get(0) == 4);
			}
		}
	}
}
```
