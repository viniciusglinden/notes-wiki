
# JSON Schema

- [Getting started with JSON
  Hyper-Schema](https://apisyouwonthate.com/blog/getting-started-with-json-hyper-schema/)
- [JSON Schema
  explained](https://jsoneditoronline.org/indepth/validate/json-schema-validator/)
- [JSON Schema
  reference](https://json-schema.org/understanding-json-schema/reference)

Use [JSON schema](https://json-schema.org/) to validate JSON files. The basic
idea behind it is to validate a JSON with another JSON. There is a plethora of
online tools to create the schema files from a valid JSON.

Though the name, you can also use other file formats, like YAML.

If you are working with a known schema, you can download its schema from the
[JSON Schema Store](https://www.schemastore.org/json/). Depending on the IDE, it
may also [help writing JSON while providing
"IntelliSense"](https://open-vsx.org/extension/remcohaszing/schemastore).

For a command line tool to validate schema, use
[check-jsonschema](https://github.com/python-jsonschema/check-jsonschema).

## additionalProperties

Originally intended to specify a scheme to apply to an unexpected property.
Use it to specify unnamed objects. Example:

```json
{
  "name": "John",
  "age": 30,
  "occupation": "Engineer"
}
```

Schema:

```json
{
  "type": "object",
  "properties": {
    "name": { "type": "string" },
    "age": { "type": "integer" }
  },
  "additionalProperties": { "type": "string" }
}
```

If there was no `additionalProperties`, "occupation" field would make it
invalid.

## oneOf, anyOf, oneOf

- [OneOf, anyOf, allOf Tutorial](https://apidog.com/blog/oneof-anyof-allof/)

- `allOf`: **all** of the contained schemas must be valid
- `anyOf`: one or **more** of the contained schemas must be valid
- `oneOf`: one and **only one** of the contained schemas must be valid

```json
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Address validator",
    "additionalProperties": true,
    "properties": {
        "AddressLine1": { "type": "string" },
        "AddressLine2": { "type": "string" },
        "City":         { "type": "string" }
    },
    "required": [ "AddressLine1" ],
    "oneOf": [
        {
            "description": "US style address",
            "type": "object",
            "required": [ "ZipCode" ]
            "properties": {
                "State":   { "type": "string" },
                "ZipCode": { "type": "string" }
            },
        },
        {
            "description": "UK style address",
            "type": "object",
            "required": [ "PostCode" ]
            "properties": {
                "County":   { "type": "string" },
                "PostCode": { "type": "string" }
            },
        }
    ]
}
```

## External referencing

- For referencing a file on the internet, use `$id`
- For referencing a local file: https://iancarpenter.dev/2020/03/01/how-to-reference-a-local-json-schema/
