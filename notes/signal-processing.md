[toc]

# Signal Processing

It deals with two problems:

- analysis: input signal is know, the system is known, analyzes the output
  signal
- synthesis: input signal is known, the output signal is known, synthesize the
  system

For the analysis, the following python modules are recommended:

- `numpy`: specially `pi`, `polymul`
- `control`
- `scipy`:
  - `scipy.signal`: `bilinear`, `tf2sos`, `sosfreqz`, `sosfilt`, `windows`,
    `sisfiltfilt`
  - `scipy.fft.fft`

## Transformations

- Laplace Transform: used for designing in the continuous time domain
- Z Transform: used for designing in the discrete time domain
- Fourier:
  - Discrete Fourier Series: Used for analyzing periodic signals in the discrete
    time domain
  - Continuous Fourier Series: Used for analyzing periodic signals in the
    continuous time domain
  - Discrete Fourier Transform: Used for analyzing in the discrete time domain
  - Continuous Fourier Transform: Used for analyzing in the continuous time
    domain
- Wavelet transform: detects abrupt changes in the signal

### Response

1. Calculate the $H(z)$ (Z transform) transfer function
2. subsitute $z^{-k}$ por $e^{-jk\theta} = \cos(k\theta) -j\sin(k\theta)$

With $\theta = 2\pi\frac{f}{f_s}$ and $0 \leqslant \theta \leqslant \pi$
(because of the Nyquist limit) calculate magnitude and phase responses.

- DC gain: $\theta = 0$, $z = 1$
- Frequency gain: $\theta = \pi$, $z = -1$

## Signal

> Definition: a dependent variable (function) of one or more independent
> variables.

Alternating current is a signal whereas direct current is not a signal (because
it does not depend on an independent variable.

Signals can be of two types:

- Continuous time signals (CTS)
- Discrete time signals (DTS): the study object of digital signal processing

Types of signals:

- Energy signal: if and only if the total energy is finite
- Power signal: if and only if the total average power is finite
- Neighter Energy Nor Power (NENP) signal: if magnitude of signal is infinite at
  any instant of time

$$E = \int_{-\infty}^\infty |x(t)|^2 \mathrm{d}t$$
$$P = \int_{-\infty}^\infty E(t) \mathrm{d}t$$
$$P = \text{RMS}^2$$

### Orthogonal signals

> Definitions: a property that allows transmission of more than one signal over
> a common channel with successful detection.

Think of it as a signal representing an eigenfunction.

For non periodic signals, orthogonality is guaranteed if:

$$\int_{-\infty}^\infty x_1(t)\cdot x_2(t)\cdot\mathrm{d}t = 0$$

For periodic signals:

$$\int_{-T}^T x_1(t)\cdot x_2(t)\cdot\mathrm{d}t = 0$$

orthogonality is guaranteed if

- Two harmonics of different frequencies
- Sine and cosine functions with same phase and frequency
- Constant value and sine functions

If the signal is orthogonal:

- $P_y = P_{x_1} + P_{x_2}$
- $E_y = E_{x_1} + E_{x_2}$

### Cardinal Sign Function (sinc)

Two types:

Unnormalized sinc

$$\text{sinc}(t) = \left\{\begin{array}{ll}
    1 & \text{if } t = 0 \\
    \frac{\sin(t)}{t} & \text{if } t \neq 0
    \end{array}\right.
$$

- normalized sinc: zeros are located at integer values (1, 2, 3, ...)

$$\text{sinc}(t) = \left\{\begin{array}{ll}
    1 & \text{if } t = 0 \\
    \frac{\sin(\pi t)}{\pi t} & \text{if } t \neq 0
    \end{array}\right.
$$

- $\text{sinc}(t) \rightarrow E = \pi$
- $\text{sinc}(at) \rightarrow E = \frac{\pi}{a}$
- $\text{sinc}(\pi t) \rightarrow E = 1$

### Jitter

Also called Time Interval Error (TIE).

Tough jitter is not specific to the clock, it is better to understand the clock
jitter concept first.

> Clock jitter is a short term fluctuation or variation of the clock edges with
> respect to the clocks ideal location.

```plantuml
@startwbs
+ **jitter**
++:**bounded**
deterministic
expressed as peak to peak;
+++ periodic
--- data dependent
+++ duty cycle
++:**unbounded**
random
expressed in RMS wihtin a bandwidth;
+++ thermal noise
--- short noise
+++ flicker noise
@endwbs
```

- Causes
  - System phenomena: crosstalk, impedance mismatching
  - data dependent: intersymbol interference (ISI), duty-cycle distortion
  - shot noise: electron and hole phenomena
  - Pink noise (frequency)
- To determine the jitter's origin, take a look at the crossing histogram:
  - random jitter looks like a gaussian distribution
  - deterministic looks like two humps
  - of course, a mixture of both distributions means a mixture of jitter sources
- Bit Error Rate (BER): number of erroneous bits in unit time intervals
- for unbounded jitter, gaussian noise model is used
- for bounded jitter, peak-to-peak jitter is linearly added
  - peak-to-peak jitter: difference between minimum and maximum deviations of
    the clock edge

BER|multiplication (alpha)
-|-
$10^{-9}$|11.996
$10^{-10}$|12.723
$10^{-11}$|13.412
$10^{-12}$|14.069
$10^{-13}$|14.698
$10^{-14}$|15.301
$10^{-14}$|15.883

### Clock drift

- Clock rate does not run at exactly the expected rate.
- It can be used for Random Number Generators (RNG)

### skew

- Clock skew: assuming no clock jitter and no clock drift: it is the time
  measurement of the clock's delay
- Problem is mostly created by the clock's delivery network

## System

> Definition: the meaningful interconnection of physical devices and components.

A system takes one or more signals and outputs one or more signals.

A transfer characteristic is the plot that defines the system. In case of a
single-input single-output system, one coordinate is the output; the other, the
input.

### Split system

A system is split if there are multiple output definitions based on the current
time. Eg.:

$$y(t) = \left\{\begin{array}{ll}
    x(t) & \text{if } t < 0 \\
    2x(t) & \text{if } t \geqslant 0
    \end{array}\right.
$$

### Static or Dynamic

- Static: output of system depends only on present value of input
- Static: output of system depends only on past or future value of input at any
  instant of time

### Causal or Non-Causal

- Causal: output of system is independent of future values of input
- Non-Causal: output of system depends on future values of input at any instance
  of time

### Time-Invariant or Time-Variant

> Definition: A Time-Invariant system is a system in which any delay provided an
> input must be reflected in the output

```
x(t)┌──────┐y(t)┌─────┐y(t-t0)
─┬─►│system├───►│delay├──────►
 │  │      │    │by t0│
 │  └──────┘    └─────┘
 │  ┌─────┐x(t-t0)┌──────┐z(t-t0)
 └─►│delay├──────►│system├──────►
    │by t0│       │      │
    └─────┘       └──────┘
```

- Time-Invariant: $y(t-t_0) = z(t-t_0)$
- Time-Variant: $y(t-t_0) \neq z(t-t_0)$

Conditions for TIV Systems are:

- No time scaling
- Coefficients should be constant
- Any added/subtracted term in the system relationship (except input and output)
  must be constant of zero

Be careful about analyzing [split sytems](#split-system). They are mostly TV
Systems.

### Linear or Non-Linear

> Defines: the system which follows the principle of superposition is known as a
> linear system.

### Invertible or Non Invertible

Basically if $x(t)$ is deducible from $y(t)$ at every point in time.

- One to One Mapping: for each and every possible input values, the output will
  be unique
- Many to One Mapping

> Definition: for an invertible system, there should be one to one mapping
> between input and output at each and every instant of time.

### Stable or Unstable

Bounded-Input Bounded-Output (BIBO) criteria: for a stable system output should
be bounded for bounded input at each and every instant of time.

By bounded it means that the amplitude should be finite.

Method:

1. Difference equation
2. apply transform (Z, Laplace, Fourier)
3. Transfer function
4. Calculate poles

If all poles are within the unit circle, and the inputs are bounded, the system
is stable.

A system whose pole is exactly at the border of the unit circle is called
marginally stable.

## Linear Time-Invariant Systems

Combine both properties of linearity and time invariability. This is necessary
to calculate a transfer function in the Laplace domain.

## Parseval's Power Theorem

For a signal $x(t) with period $T_0$, with $c_n$ being its corresponding the
Complex Fourier Series coefficient, the law states:

$$P_x(t) = \sum_{n=-\infty}^\infty |c_n|^2$$

## Aliasing

> Definition: can occur when the sampling rate is less than two times the
> highest frequency component in the signal

Nyquist Theorem.

### Anti-alias filter

> Definition: restricts signal bandwidth to the area of interest

Let us say that the signal contains two bands: from 0 to x and then onward and
we are only interested in the first part. We should therefore put an anti-alias
filter to eliminate the x onward frequencies.

## Implementation optimizations

- Use [SIMD](#single-instruction-multiple-data-simd)
- Unroll loop (`gcc -funroll-loops main.c`)
- Experiment with compiler optimization (O0, ..., O3): the most optimized does
  not mean that the algorithm will run faster
- Focus optimizations on the inner algorithm loop
- Select the right variable precision
- Minimize filter order
- Use look-up tables

## Wavelet transformation

- [Wavelets: a mathematical
  microscope](https://www.youtube.com/watch?v=jnxqHcObNK4)

> Definition: a wavelet is an oscillating signal constraint within a range.

$$X(a, b) = \int_{-\infty}^\infty x(t)\cdot\psi_{a, b}^*(t)\mathrm{d}t$$
$$\psi_{a, b}(t) = \psi\left(\frac{t-b}{a}\right)$$

Where:

- $\psi$ is a complex function called a wavelet
- $a$ is known as scale (as in scaling the period, high scale = stretched out)
- $b$ is known as translation (as in translation in time)
- The constraint range is called "compact support"

The result is in 2D, with one axis representing time and the other frequency.

Walelets have to adhere to the following requirements:

- Admissibility condition: zero mean $\int_{-\infty}^\infty\psi(t)\mathrm{d}t =
  0$
- Finite Energy: $\int_{-\infty}^\infty|\psi(t)|^2\mathrm{d}t < \infty$

The Heisenberg principle applies: high resolution in time compromises the
frequency resolution. In this sense, the Fourier transform provides absolute
frequency resolution in detriment of the time.

$$\Delta f\cdot\Delta t \geqslant 1$$

There is a number of $\psi$ to choose and the choice depends on the application:

- Vanishing moments: where the wavelet is 0
  - higher number = more accurate complex signal representation
  - $p$ moments = polynomials up to the $p$th order will not be identified
- Frequency selectivity: (Heisenberg uncertainty): more selective wavelet = less
  compact support (less selective in time)

At the end, choose based on the Heisenberg uncertainty boxes.

### Single Instruction Multiple Data (SIMD)

> Definition: A feature in some processors that allows one to process multiple
> data with only one instruction.

- Operates on packed 8-bit and 16-bit data

```c
q7_t* pSrc, pDst;
int32_t x;
x = *__SIMD32(pSrc)++; // pipe the data into 32 bits
*__SIMD32(pDst)++ = x; // pipe the data back from 32 bits
```

## Delay

### Sample delay

A system that delays the input by a constant amount of samples introduces a
delay to the signal.

$x[n-n_0] = z^{-n_0}\cdot x[n]$

### Phase delay

- Phase delay: phase angle at this point in frequency
- Phase delay is the delay provided by the system to the single frequency
- Phase delay describes how much a single tone is delayed

$$t_\phi = \left.-\frac{\angle H(\omega)}{\omega}\right|_{\omega=\omega_1}$$

### Group delay

- Group delay: rate of change of the phase around this point in frequency
- Group delay is the delay provided by the system to the envelope
- Group delay  describes how much the overall signal shape (envelope) is delayed
  or distorted

$$t_g = \left.-\frac{\mathrm{d} \angle
H(\omega)}{\mathrm{d}\omega}\right|_{\omega=\omega_1}$$

### Group vs phase delay

- [What is the what is the difference between phase delay and group
  delay](https://dsp.stackexchange.com/questions/206/what-is-the-difference-between-phase-delay-and-group-delay)

In case of _distortionless_ LTI systems: phase delay = group delay.

Let us have the following input: $x(t) = m(t)\cdot\cos(\omega_c\cdot t)$
(think of radio modulation).

- $m(t)$ is known as envelope or group of frequencies
- $cos(\omega_c\cdot t$ is known as high frequency carrier or single frequency
  signal

By inputting it to a LTI system, you get: $y(y) =
m(t-t_g)\cdot\cos(\omega_c[t-t_\phi])$.

- $t_\phi$ is the phase delay
- $t_g$ is the group delay: it tells you how much the "peak of the package" will
  move by

Group delay is measure of amplitude distortion if array of multiple frequencies
are applied.

The time that it takes for the modulation signal to get through the system is
given by the group delay not by the phase delay. An audio example is a kick of a
drum: this is mostly a modulated sine wave so if you want to determine how much
the kick drum will be delayed (and potentially smeared out in time) the group
delay is the way to look at it.

## Frequency response

With $H(z) = \frac{Y(z)}{X(z)}$, let
$z^{-1} = e^{-j\theta} = \cos (\theta) - j\cdot \sin (\theta)$ and

$\theta = \omega\cdot T = 2\pi \cdot f \cdot T = 2\pi \frac{f}{f_s}$

Example (IIR low pass):

$$y(z) = (1-\alpha)\cdot x(z) + \alpha\cdot z^{-1}\cdot y(z)$$
$$H(z) = \frac{1-\alpha}{1-\alpha\cdot z^{-1}}$$
$$H(\theta) = \frac{1-\alpha}{1-\alpha\cdot cos(\theta) + j\cdot\sin(\theta)}$$
$$|H(\theta)| = \frac{1-\alpha}{\sqrt{(1+\alpha) - 2\cdot\alpha\cdot\cos(\theta)}}$$
$$|H(f)| = \frac{1-\alpha}{\sqrt{(1+\alpha) -
2\cdot\alpha\cdot\cos(2\pi\cdot\frac{f}{f_s})}}$$

## Digital Filters

- [Digital Filters](https://notblackmagic.com/bitsnpieces/digital-filters/)
- [Micromodeler](https://www.micromodeler.com/): Systems modeling for embedded
  systems

- Moving Average: low pass
- FIR: stable; impulse is euqal to filter coefficients
- IIR: potentially unstable; impulse is compputed recursively from
  filter coefficients

feature|FIR|IIR
-|-|-
computational speed|slow - high order|fast - low order
phase/delay|constant|non-contant
stability|always|sometimes

The ARM CMSIS-DSP library provide
[FIR](https://www.keil.com/pack/doc/CMSIS/DSP/html/group__FIR.html) and
[IIR](https://www.keil.com/pack/doc/CMSIS/DSP/html/group__BiquadCascadeDF1.html)
filter functions that are highly optimized.

Transforming floating point to fixed point is done by multiplying it to
$2^(n-1)$ where $n$ is the number of bits and then rounding them to an integer.

For SciLab simulation: `ffilt` for FIR; `iir` for FIR.

### FIR

Basically an "N" point moving average filter.

$y(n) = \sum_{i=0}^{N-1} h(i) \cdot x(n-i)$

#### Moving average

$y(n) = \sum_{i=0}^{N-1} \frac{1}{N} \cdot x(n-i)$

### IIR

$y(n) = \sum_{i=0}^{N-1} h(i) \cdot x(n-i) + \sum_{l=1}^N a_l \cdot y(n-l)$

IIR filters are typically implemented with a cascade of Second Order Sections
(SOS), aka biquad filters. This provides:

- stability: higher order IIRs can more easily become unstable
- numerical accuracy: numerical erros are less likely to accumulate
- hardware implementation: simpler to implement in hardware; CMSIS DSP provide
  optimization funtions for biquad filtering
- design: coefficients can be directly obtained

### Cascaded Integrator-Comb (CIC) Filter

- [A Beginner's Guide to Cascaded Integrator Comb (CIC)
  Filters](https://www.dsprelated.com/showarticle/1337.php)

CIC is an optimized version of the moving average FIR. Comb here means that the
past $m$ delayed value is subtracted.

Because CICs are Linear Time-Invariant (LTI) systems, higher order CIC filters are also
cascaded with integrator and comb grouped together (eg. i0 i1 i2 c0 c1 c2).
Another characteristic of thhe LTI is that decimators and interpolators can be
inserted into the middle of the filter.

CICs can only be implemented with fixed-point because it relays on the overflow
of fixed-point math.
