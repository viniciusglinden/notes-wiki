<!-- vim: spelllang=pt_br
-->

# Catolicismo

## Liturgia das Horas

Antes do Vaticano II, também chamado de Ofício Divino.

Breviário é o livro contendo a liturgia das horas.

Compõem a Liturgia das Horas:

- Laudes: primeira hora do dia
- Hora Média: podendo ser rezada às 09h00 (chamando-se terça/tércia), às 12h00
  (chamando-se sexta) ou 15h00 (chamando-se noa/nona)
- Vésperas: no poente
- Completas: na noite, preparação para dormir

## Nomenclatura

### _ut removens prohibens_

Que remove o obstáculo.

Ex.: A humildade atua ut removens prohibens para receber a influência da graça.

### _ex opere operantis_, _ex opere operato_

Terminologia empregada em sacramentos.

- _ex opere operantis_: "da operação do operador": eficácia do "operador"
  (sacramento) deriva do ministro ou do receptor - aplica-se sobre à
  _disposição_ a qual o sacramento é recebido
- _ex opere operato_: "da operação operada": eficácia deriva do "operador"
  (sacramento) - aplica-se à eficácia intrínseca do sacramento

### _praesentialiter efficit_

"Ele age na presença". Ele é eficaz por virtude da Sua presença, direta ou
essencial.

### _per se_, _per accidens_

- [The Metaphysics of Per Se and Per Accidens Causal
  Series](https://medium.com/@dialecticalpooda/the-metaphysics-of-per-se-and-per-accidens-causal-series-c2d1658ea087)

Uma série em que:

- essencial: causalidade inerente ou não do membro
- _per accidens_ (por acidente): os membros derivam/possuem o poder
  (causalidade) da série _essencialmente_. Enquanto houver um membro da série,
  esta pode repetir-se indefinidamente.
- _per se_ (por si mesmo): os membros derivam/possuem o poder (causalidade) da
  série _não essencialmente_. Caso não houver todos os membros da série, esta
  não poderá repetir-se indefinidamente.

Exemplo:

- _per accidens_: o pai gera o filho, que gera o filho, etc
- _per se_: a mente (agente mental) move a mão para mover um galho para mover
  uma pedra

Nota-se que no _per se_, caso o agente mental não existir, não há série, pois
todo o poder deriva do agente mental.

### norma normans

### Soteriologia

### Ascética

> Definição: esforço metódico e continuo, com ajuda da graça, a favorecer o
> pleno desenvolvimento espiritual

Oração, arrependimento, busca pelo conhecimento e transformação através da
bíblia e uso de métodos disciplinares.

Relacionado à ascese: exercícios mortificantes aplicados para eliminar os
vícios.

### Mística

Místico vs misticismo...

### Discernimento dos espíritos

"Pelos frutos conhecereis".

## Heresias

### Gnosticismo

- [Gnosticismo: O que é](https://cleofas.com.br/gnosticismo-o-que-e/)

Filosofia dualista segundo a qual a salvação depende do conhecimento (gnose) da
verdade espiritual. É porém distinta de gnose. Ensina que a vida terrena é
dolorosa e radicalmente perversa. A iluminação interior, ou gnose, revela que a
alma, a qual participa da natureza de Deus, desceu ao mundo maligno da matéria e
deve ser salva pelo espírito e pela inteligência.

Acredita que há dois deuses: um deus bom e outro mau; e o
mundo teria sido criado pelo deus mau, um deus menor, que eles chamam de
demiurgo (deus criador).

As almas dos homens já existiam em um universo de luz e paz (Plenoma); mas houve
algo como uma revolta e assim esses espíritos foram castigados sendo
aprisionados em corpos humanos, como em uma cadeia, pelo deus demiurgo, e que os
impede de voltar ao estado inicial.

A salvação dessas almas só seria possível mediante a libertação dessa cadeia que
é o corpo, que é mau, e isto só seria possível através de um conhecimento (gnose
em grego) secreto, junto com práticas mágicas (esotéricas) sobre Deus e a vida,
revelados aos "iniciados", e que dariam condições a eles de se salvarem.

Não acreditam na salvação por meio da morte e ressurreição de Jesus Cristo; nem
no pecado, nem nos anjos, nem nos demônios e nem no pecado original. O Deus bom,
teria enviado ao mundo o seu mensageiro, Jesus Cristo, como redentor (um _eon_,
"Avatar"), portador da "gnósis", a palavra revelada a alguns escolhidos e que
leva à salvação (libertação do corpo). Jesus não teria tido um corpo de verdade,
mas apenas um corpo aparente (docetismo, de doceta - aparente). Jesus teria
então um corpo ilusório que não teria sido crucificado.

### Maniqueismo

- [Maniqueismo](http://www.estudantedefilosofia.com.br/doutrinas/maniqueismo.php)

Doutrina religiosa pregada por Maniqueu (ou Mani ou Manes) na Pérsia, no século
III. Sua principal característica é a concepção dualista do mundo como fusão de
espírito e matéria, que representam respectivamente o bem e o mal.

Maniqueu se acreditava o último de uma longa sucessão de profetas, que começara
com Adão e incluía Buda, Zoroastro e Jesus, e portador de uma mensagem universal
destinada a substituir todas as religiões. Registrou sua doutrina por escrito,
pretendendo fundar uma religião ecumênica.

O conhecimento salvador da verdadeira natureza e do destino da humanidade, de
Deus e do universo é expresso por uma mitologia segundo a qual a alma, enredada
pela matéria maligna, se liberta pelo espírito. O mito se desdobra em três
estágios:

- o passado, quando estavam radicalmente separadas as duas substâncias, que são
  espírito e matéria, bem e mal, luz e trevas;
- um período intermediário, correspondente ao presente, no qual as duas
  substâncias se misturam;
- e um período futuro no qual a dualidade original se restabeleceria.

Na morte, a alma do homem que houvesse superado a matéria iria para o paraíso, e
a do que continuasse ligado à matéria pelos pecados da carne seria condenada a
renascer em novos corpos.

Os eleitos (perfeitos) levavam vida ascética em conformidade com os mais
estritos princípios da sua doutrina. Os demais fiéis (ouvintes) contribuíam com
trabalho e doações. Por rejeitar tudo o que era material, o maniqueísmo não
admitia nenhum tipo de rito nem símbolos materiais externos.

Como religião organizada, expandiu-se rapidamente pelo Império Romano. Do Egito,
disseminou-se pelo norte da África, onde acabou atraindo: santo Agostinho.

O maniqueísmo inspirou na Europa medieval diversas seitas ou heresias dualistas
surgidas no seio do cristianismo. Entre elas: a dos bogomilos, na Bulgária
(século X) e, sobretudo, a dos cátaros ou albigenses, que se propagou no sul da
França no século XII.

### Bogomilismo

Ver maniqueismo.

### Cátarismo (albigensenismo)

Cátaros vem do grego e quer dizer "puros". Vinham da Bulgária.

Tinham alta vida ascética. Mesmas crenças que o maniqueismo.

### Eutiquianismo

### Pelagianismo

- [As 3 Advertências do Papa
  Francisco](https://pt.aleteia.org/2022/03/15/as-3-advertencias-do-papa-francisco/)

Seu autor, Pelágio, negava o pecado original e seus efeitos negativos sobre a
condição humana; ensinava que está no livre-arbítrio do homem salvar-se por suas
próprias forças, sem a necessidade do auxílio sobrenatural de Deus (a graça).

O pelagianismo exerceu no Ocidente cristão o mesmo papel do arianismo no
Oriente.

Preza que a salvação pode ser obtida por vontade própria, sem intervenção
divina.

### Ortodoxa oriental

### Luteranismo

### Espiritismo

### Jansenismo

Originária do bispo de Ypres, Cornelius Jansenius (1585-1638). O autor alega ter
redescoberto a teologia perdida de Agostinho sobre como a graça funciona para a
salvação do eleito. O bispo não era herético em seu pensamento, porém seus
interpretadores da universidade de Sorbonne causaram a heresia, condenada na
bula papal _cum occasione_ e na _augustinus_. São cinco as proposições:

1. "Alguns dos mandamentos de Deus são impossíveis para os homens justos que
   desejam e se esforçam para cumpri-los, considerando os poderes que eles
   realmente possuem; também lhes falta a graça pela qual esses preceitos podem
   tornar-se possíveis".
2. "No estado de natureza decaída, ninguém resiste à graça interior."
3. "Para merecer ou desmerecer, no estado de natureza decaída, devemos estar
   livres de todas as restrições externas, mas não da necessidade interior."
4. "Os semipelagianos admitiram a necessidade da graça preventiva interior para
   todos os atos, mesmo para o início da fé; mas caíram na heresia ao fingir que
   esta graça é tal que o homem pode segui-la ou resistir-lhe."
5. "É semipelagiano dizer que Cristo morreu ou derramou o Seu sangue por todos
   os homens."

### Arianismo

Negava divindade de Jesus.

### Panteísmo

### Monotelismo

Afirma que em Cristo só havia uma vontade.

### Nestorianismo

Afirma que em Jesus existem duas pessoas distintas: uma pessoa filho de Maria e
outra filho de Deus. Que estas duas pessoas estão unidas moralmente.

Nega o título a Maria de Mãe de Deus.

É heresia, pois assim não há diferença entre todos os justos e Jesus.

### Monofisismo

Pregada no século V pelo monge Eutiques em Bizâncio, o monofisismo é uma heresia cristológica que defende que, após a união do divino e do humano em Jesus Cristo, existe apenas uma única natureza, a natureza divina,
negando a plena humanidade de Jesus Cristo, afirmando que sua natureza humana teria sido absorvida pela natureza divina após a encarnação. Essa visão era vista como uma distorção dos ensinamentos cristãos sobre a natureza de Jesus.

### Donatismo

Donatistas sustentavam que a Igreja não devia perdoar e admitir pecadores, e que
os sacramentos, como o batismo, administrados pelos traditores (cristãos que
negaram sua fé durante a perseguição de Diocleciano de 303 d.C. a 305 d.C. e
posteriormente foram perdoados e readmitidos na Igreja) eram inválidos.

### Ebionismo

### Monarquianismo / monarquismo

Série de heresias que enfatizam a unidade absoluta de Deus.

#### Sebelianismo / modalismo

- [Heresia. Sabelianismo (Princípio do Séc.
  III)](https://blogdoemanueljr.blogspot.com/2008/05/heresia-sabelianismo-princpio-do-sc-iii.html)

Um tipo monarquianismo modalista. Ensina que a Trindade Santa não se configura
em três pessoas distintas, mas em modos ou módulos, ou atributos de Deus. V.g.
que a pessoa humana de Jesus é subordinada, como que subjugada a força pela sua
pessoa Divina.

Relaciona-se com igrejas pentecostais de "só Jesus".

Monarquianismo modalista = patripassianismo (próprio Deus Pai sofreu na cruz).

#### Adocionismo

Defende que o Filho não é co-eterno com o Pai, mas revestido de Deus (adotado).
Ensinava que Jesus tinha sido uma pessoa comum, mas supremamente justo, homem
até o Espírito descer sobre ele em seu batismo.

#### Ecologismo

Que a "mãe natureza" é tão mais importante que o homem, que este passa a ser
tratado como o câncer da terra.

Diferente de ecologia, que é algo positivo.

### Apolinarismo

Afirma que Cristo não tinha alma humana.

Criado por Apolinário de Laodiceia (310-390 d.C.), bispo que tentou criar um
modo de explicar a natureza dupla de Jesus de humanidade e divindade, segundo a
qual Jesus Cristo teria um corpo humano, porém dotado de uma mente
exclusivamente divina. Os argumentos em favor dessa tese de que haveria duas
naturezas completas (divindade e humanidade) impediriam Jesus Cristo de se
tornar um ser único, porque ele seria duas pessoas. Tais ideias foram condenadas
no sínodo realizado em Alexandria em 362 d.C e posteriormente pelo papa Dâmaso
I, e especialmente pelo Concílio de Constantinopla I (381 d.C.).

### Montanismo

## Estrutura

- [Igreja Católica](http://www.igreja-catolica.com/)
- [Lista de posições na hierarqhia
  católica](https://pt.wikipedia.org/wiki/Lista_de_posi%C3%A7%C3%B5es_na_hierarquia_cat%C3%B3lica)

- papa
- cardeais: conselheiros e colaboradores íntimos do papa, todos bispos. A cada
  um é atribuída uma igreja ou capela em Roma para fazer-lhe formalmente membro
  do clero da cidade.
- patriarcas: título honorífico de líder _orientais sui juris_. São todos
  orientais, com exceção de alguns prelados, e reconhecidos pelos papas.
  Eleitos pelos respectivos sínodos e possuem eparquias.
- arcebispo (metropolita ou titular): bispo líder da arquidiocese. Caso a
  arquidiocese for sede de uma província eclesiástica, possuem poder de
  supervisão e jurisdição limitada.
- bispo (diocesano, titular, emérito): sucessores direto dos apóstolos.
  Receberam necessariamente o sacramento da ordem.
- presbíteros ou padres: sacerdote colaboradores dos bispos, com jurisdição
  parcial sobre os fiéis. Caso seja o líder da paróquia, chama-se de pároco.
- monsenhor: título honorífico para um presbítero, não confere mais poderes
- diácono: auxiliares dos presbíteros e bispos, possuindo o primeiro grau do
  Sacramento da Ordem. Ordenados não para o sacerdócio, mas para o serviço de
  caridade, proclamação da Palavra e Liturgia.
- cônego: presbítero que vive sob uma regra que obriga a realizar as funções
  litúrgicas mais solenes na igreja cardeal ou colegiada
- clérigo: mesmo que cônego, porém sem ênfase em uma vida em comunidade
- prior
- abade
- reitor
- vigário
- capelão: padre militar ou que dirige uma capela
- cura: pároco de uma catedral

Santa Sé = papa + dicastérios da Cúria Romana.

## Ordens religiosas

- [Religious Order](https://en.wikipedia.org/wiki/Religious_order_(Catholic))

São uma forma da vida consagrada com profissão de votos solenes. Subcategorias:

- cônego regular: recitam o Ofício Divino e servem nas igrejas ou paróquias
- monástica: monge / freira trabalhando e vivendo em um monastério / convento
- mendicante: frei ou irmã religiosa que vive de esmolas e recitam o Ofício
  Divino e participam em atividades apostólicas
- clérigo regular: padres que professam votos religiosos e possuem uma vide
  apostólica ativa

Ordem|Redator da Regra|Tipo
-|-|-
Ordem de Santo Agostinho|Santo Agostinho|Cônego Regular
Ordem Premonstratense, Ordem de São Norberto|Santo Agostinho|Cônego Regular
Ordem dos Cônegos Regrantes de Santo Agostinho|Santo Agostinho|Cônego Regular
Ordem de Malta, Ordem Soberana e Militar Hospitalária de São João de Jerusalém, de Rodes e de Malta|São Bento
Ordem dos Cavaleiros Teutônicos de Santa Maria de Jerusalém|São Bento

TODO

### Confraria

Confraria = irmandade = ordem terceira.

Associação religiosa de leigos que promevem o culto a um santo, representado por
uma relíquia ou imagem.

Chama-se confrade ou correligionário ou irmão o membro de uma confraria.

## Grupos históricos

Grupos históricos da época de Jesus diferenciavam em diversas maneiras,
principalmente na forma de interpretar e viver a religião.

- fariseus: hebraico perushim, significando segredados. Ampla importância às
  formalidades, em particular às leis de pureza ritual. Ritualizavam tudo.
  - zelotes: subgrupo em que ligava a Lei à situação política, dando importância
    à independência nacional. Engajavam-se para-militarmente.
- saduceus: compunham a alta sociedade, ligados às famílias sacerdotais e
  representavam os judeus politicamente. Não acreditavam na vida após a morte.
  Interpretação flexível da Torá.
- essênios: grupo de ascetas que repudiavam a forma de culto praticada no templo
  e se foram viver segregados. Propósito de restaurar a santidade do povo
  eleito.

## Concílios

É uma reunião de autoridades eclersiásticas com objetivo de discutir e deliberar
questões patorais, doutrinais, de fé e costumes morais. Podem se ecumênicos,
plenários, nacionais ou diocesanos.

0. Jerusalém: relatado no Ato dos Apóstolos, definiu questões como quais leis
   judaicas são aplicadas aos gentios
1. Nicéria I: condena arianismo, dogma da conaturalidade entre Pai e Filho,
   compõe o Credo Niceno
2. Constantinopla I: dogma da natureza divina do Espírito Santo, credo
   niceno-constantinopolitano
3. Éfeso: unidade de pessoa do Verbo com o corpo e alma encarnados; condena o
   nestorianismo; dogma da maternidade divina
4. Calcedônia: condena monofisismo; dogma da unidade de pessoa do Verbo (União
   Hipostática)
5. Constantinopla II: condena escritos de Teodoro de Mopsvesta, Teodoreto e
   Íbaso
6. Constantinopla III: condena monotelismo; formulação da doutrina das duas
   vontades de Cristo
7. Nicéia II: contra iconoclastas, ortodoxia teológica do culto das imagens
8. Constantinopla IV: questão do governo da sé patriarcal de Constantinopla,
   cisão do clero em torno da deposição e condenação de Fócio
9. Latrão I: concílio ecumênico do Ocidente
10. Latrão II: questão dos símbolos, usura e celibato
11. Latrão III: condenaçao dos cátadros
12. Latrão IV: condenação dos albigenses; disciplina eclesiástica sobre os
    sacramentos e anunciação do Evangelho
13. Lyon I: questões do poder temporal dos papas
14. Lyon II: tentativa de unificação das igrejas ocidentais e orientais
15. Vienne: convocado para cassar a Ordem dos Templários
16. Constância: unificação do cristianismo; reforma da Igreja; conflito entre os
    teutônicos e a Polônia
17. Florença: unificação da Igreja
18. Latrão V: reforma da Igreja (insucesso, prossegue o Luteranismo)
19. Trento: reforma da Igreja; série de decretos dogmáticos
20. Vaticano I: documentos dogmáticos sobre a fé, o racionalismo e a
    infalibilidade papal
21. Vaticano II: reforma interna da Igreja; adaptação das atividades às
    necessidades atuais (aggiornamento)

## Bíblia

- Pentateuco / Lei: Gênesis, Êxodo, Levítico, Números, Deuteronômio
- Livros históricos
  - Velho Testamento: Josué, Juízes, Rute, 1° Samuel, 2° Samuel, 1° Reis, 2°
    Reis, 1° Crônicas, 2° Crônicas, Esdras, Neemias, Tobias, Judite, Ester, 1°
    Macabeus, 2° Macabeus
  - Novo Testamento: Evangelhos, Atos dos Apóstolos
- Livros didáticos
  - Velho Testamento: Jó, Salmos, Provérbios, Eclesiastes, Cântico dos Cânticos,
    Sabedoria, Eclesiástico
  - Novo Testamento: Epístolas de São Paulo aos Romanos, 1ª e 2ª aos Coríntios,
    aos Gálatas, aos Efésios, aos Filipenses, aos Colossenses, 1ª e 2ª aos
    Tessalonicenses, 1ª e 2ª a Timóteo, a Tito, a Filêmon, aos Hebreus; Epístola
    de São Tiago; 1ª e 2ª Epístola de São Pedro; as três de São João; Epístola
    de São Judas
- Livros proféticos
  - Velho Testamento: Isaías, Jeremias, Trenos de Jeremias, Baruch, Ezequiel,
    Daniel, Oséias, Joel, Amós, Abdias, Jonas, Miquéias, Naum, Habacuc,
    Sofonias, Ageu, Zacarias, Malaquias
  - Novo Testamento: Apocalipse

## Defeitos

- acídia: preguiça ontológica, vocacional. Espécie de preguiça de tristeza para
  com o desafio da santidade. Principal tendência nos católicos.

## Temperamentos

O temperamento é uma predisposição biológica que leva ao defeito dominante.
Portanto a motivação do estudo do temperamento é:

- Como usar bem esta herança genética do temperamento
- Como lutar bem contra o defeito dominante

Temperamentos são classificados de acordo com a resposta as seguintes perguntas:

1. a pessoa é estimulada facilmente?
    - quão rápido é a reação passional quando a pessoa é estimulada
    - quão rápida é a reação quando um estimulo esterno é provido
    - quão estimulável exteriormente é a pessoa
2. permanece por tempo prolongado este estímulo?
    - uma vez estimulada, qual a duração do estimulo
    - qual é a "meia vida" do estímulo

Lembrando que a memória da alma != memória afetiva.

||1ª pergunta|2ª pergunta|tendência
-|-|-|-
fleumático|não|não|acídia
melancólico|não|sim|memoriam sui
sanguíneo|sim|não|dispersão
colérico|sim|sim|analítico

Para a ciência medieval, o corpo humano é formado pelos quatro elementos (terra,
água, fogo, ar) e suas quatro qualidades essenciais (secura, umidade, calor,
frio). A combinação de tais fatores define os humores, ou seja, as
características físicas psicológicas do ser humano:

- sanguíneo: sociáveis: ar, quente e úmido
- coléricos: impulsivos: fogo, quente e seco
- melancólicos: pessimistas: terra, fria e seca
- fleumáticos: introvertidos: água, fria e úmida

### Fleumático

- Tendência: acídia; inação
- Drama: o que fazer para movê-lo na direção do Cristo
- Combate: diversão em movimento (vg. esporte), inspirar-se em santos de vida
  ativa
- Positivo: quando se dá ao trabalho de julgar, julga de forma objetiva, pois
  não há muita paixão envolvida
- Conciliadores e diplomatas
- Santo: Tomás de Aquilo

### Melancólico

- Tendência: encapsular-se contemplando os seus problemas; escrúpulo
- Drama: suscitar a Memoriam Dei
- Combate: lembrar-se onde Deus tocou no passado
- Positivo: pode usar sua boa memória para lembrar-se de Deus
- Santo: Terezinha do Menino Jesus; João da Cruz

### Sanguíneo

- Tendência: ligar-se aos sentidos (sensualidade), buscar consolações, dispersão
- Drama: adquirir vida interior, deixar de dispersar-se pelo o externo, caso
  contrário vai a aridez
- Combate: meditação
- Positivo: consolações
- Santo: Agostinho, Filipe Neri

### Colérico

- Tendência: dizer que tudo vai mal, menos a si próprio
- Drama: como domar este comportamento genético muito bem dotado
- Combate: segurar a ira, propor-se a guardar a mansidão e caridade; ter vida
  interior, exame de consciência
- Positivo: pró-atividade, orientado a resolução do problema; geralmente é líder
- Santo: Davi, João Bosco, Francisco de Sales, Inácio

## Gradual vs Missal

- Gradual refere-se especificamente aos cantos e hinos litúrgicos
- Missal abrange todo o conteúdo textual utilizado na celebração da missa

O Gradual é uma parte integrante do Missal, mas não são a mesma coisa.

## Memória, Solenidade e Festa

- Memória: lembrança da vida de um santo ou de um dogma sem celebração especial
  - Facultativa: lembrança local de uma região
  - Obrigatória: lembrança católica
- Festa: lembrança de um santo ou de um dogma com celebração especial
- Solenidade: Festa com preceito, marcando a importância da lembrança para a fé
  católica
