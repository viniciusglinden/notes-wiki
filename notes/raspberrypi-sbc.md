
# Raspberry Pi

- [Raspberry Pi Official
  Documentation](https://www.raspberrypi.com/documentation/computers/configuration.html)
- [Python Periphery](https://github.com/vsergeev/python-periphery)
- [Raspberry Pi Pinout](https://pinout.xyz/)

## installing in \*nix

Notes:

- SD Card must be at least 8GiB for the Zero W
- Raspberry consumes 1A: USB maybe is not sufficient, don't use USB hubs
- SD Card adapter muss have "manual" write protection disabled (selector is
  against interface pins)
- Using the `rpi-imager` did not work and probably damaged the SD Card

Choose between Raspberian or Debian:

- [Raspberian](https://www.raspberrypi.org/software/operating-systems/)
- [Debian](https://raspi.debian.net/)

- Download an `<image.img.xz>` file and check the `sha256sum`
- `xzcat <image.img.xz> | sudo dd of=/dev/<sd card> vs=64k oflag=dsync
  status=progress`

- After `dd`, two partitions were created, use `lsblk -f` and mount the `vfat`
  one (smaller partition)
- To use ssh: `touch ssh`

We need to configure access through WiFi, for this we use `wpa`:

- `wpa_passphrase NAME > wpa_supplicant.conf`
- Type the password
- Remove the `#psk="password"` line
- File looks like this, in 09-09-2022

```
country=<country code>
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="<WiFi name>"
    psk=<hashed password>
    scan_ssid=1
    key_mgmt=WPA-PSK
}
```

Note: `key_mgmt=PSK` is not valid ([list of accepted
key_mgmt](https://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf))

- Put the SD Card inside the Raspberry
- [Connect via a keyboard to set a user and
  password](https://www.raspberrypi.com/news/raspberry-pi-bullseye-update-april-2022/)
- Finally, try to connect with ssh: `ssh pi@raspberrypi.local`
  - if that does not work, `ping raspberrypi.local` it and then use the IP

Note: Raspberry will display its IP address when booting. Just connect a monitor
and see "Here is my IP: ....."

### Backing up and restoring SD card

- backup: `dd if=/dev/mmcblk0 bs=4M conv=noerror,sync status=progress | gzip -c
  > backup.gz`
- restore: `gunzip -c backup.gz | dd of=/dev/mmcblk0 status=progress`

## UART

Model|first PL011 (UART0)|mini UART (UART1)
-|-|-
Raspberry Pi Zero|primary|secondary
Raspberry Pi Zero W|secondary (Bluetooth)|primary
Raspberry Pi 1|primary|secondary
Raspberry Pi 2|primary|secondary
Raspberry Pi 3|secondary (Bluetooth)|primary
Compute Module 3|primary|secondary
Compute Module 3+|primary|secondary
Raspberry Pi 4|secondary (Bluetooth)|primary

`/dev/serial0` and `/dev/serial1` are symbolic links to the correct device are
represent `UART0` and `UART1`.

Note: mini UART is disabled by default.

To use the UART instead of Bluetooth, disable it in the `/boot/config.txt` file.

The UART may be used as a login shell. In this case, default baudrate is 115200.
To change this behavior, either use `raspi-config`. Note that when you disable
this, `/boot/config.txt` is not changed.

## models

- [Checking Raspberry Pi revision number & board
  version](https://www.raspberrypi-spy.co.uk/2012/09/checking-your-raspberry-pi-board-version/)

Model and PCB Revision|RAM|Revision|Pi Revision Code from cpuinfo
-|-|-|-
Model B Rev 1|256MB||0002
Model B Rev 1 ECN0001 (no fuses, D14 removed)|256MB||0003
Model B Rev 2|256MB||0004 0005 0006
Model A|256MB||0007 0008 0009
Model B Rev 2|512MB||000d 000e 000f
Model B+|512MB||0010 0013 900032
Compute Module|512MB||0011
Compute Module|512MB||0014 (Embest, China)
Model A+|256MB||0012
Model A+|256MB||0015 (Embest, China)
Model A+|512MB||0015 (Embest, China)
Pi 2 Model B v1.1|1GB||a01041 (Sony, UK)
Pi 2 Model B v1.1|1GB||a21041 (Embest, China)
Pi 2 Model B v1.2|1GB|1.2|a22042
Pi Zero v1.2|512MB|1.2|900092
Pi Zero v1.3|512MB|1.3|900093
Pi Zero W|512MB|1.1|9000C1
Pi 3 Model B|1GB|1.2|a02082 (Sony, UK)
Pi 3 Model B|1GB|1.2|a22082 (Embest, China)
Pi 3 Model B+|1GB|1.3|a020d3 (Sony, UK)
Pi 4|1GB|1.1|a03111 (Sony, UK)
Pi 4|2GB|1.1|b03111 (Sony, UK)
Pi 4|2GB|1.2|b03112 (Sony, UK)
Pi 4|2GB|1.4|b03114 (Sony, UK)
Pi 4|4GB|1.1|c03111 (Sony, UK)
Pi 4|4GB|1.2|c03112 (Sony, UK)
Pi 4|4GB|1.4|c03114 (Sony, UK)
Pi 4|8GB|1.4|d03114 (Sony, UK)
Pi 400|4GB|1.0|c03130 (Sony, UK)
Pi Zero 2 W|1GB|1.0|902120 (Sony, UK)

Chip|Model
-|-
BCM2835|1 A, 1 A+, 1 B, 1 B+, 0, 0 W, Compute Module 1
BCM2836|2 B
BCM2837|3 B, 2B (later models), Compute Module 3
BCM2837B0|3 A+, 3 B+, Compute Module 3+
BCM2711|4 B, 400, Compute Module 4
RP3A0/BCM2837|3, 2 W

## Get model from shelll

- [Which model Raspberry Pi am I
  running?](https://raspberrypi.stackexchange.com/questions/61699/which-model-raspberry-pi-i-am-running)

```sh
cat /proc/device-tree/model
cat /proc/cpuinfo
```

## Power

- [Raspberry Pi Power
  Limitations](https://raspberrypi.stackexchange.com/a/51616)
- [What are the max current rating for 3.3V and 5V
  rail](https://raspberrypi.stackexchange.com/questions/104825/what-are-the-max-current-rating-for-3-3v-and-5v-rail-of-the-rpi-4b#104832)
- [Exploring the 3V power
  rail](https://raspberrypise.tumblr.com/post/144555785379/exploring-the-33v-power-rail)
- [How much power does Raspberry Pi use? Power
  measurements](https://raspi.tv/2018/how-much-power-does-raspberry-pi-3b-use-power-measurements)

The maximum permitted current draw from

- 3.3V pins: 50mA
- 5V: limited by USB current (usually 1A) - board current draw
- 3V3 rail: tested to be 800mA

Depending on you model (>= B+, except Zero) print an under voltage warning from
the kernel.

## GPIO

- Output: A GPIO pin designated as an output pin can be set to high (3V3) or low
  (0V).
- Input: A GPIO pin designated as an input pin can be read as high (3V3) or low
  (0V). This is made easier with the use of internal pull-up or pull-down
  resistors. Pins GPIO2 and GPIO3 have fixed pull-up resistors, but for other
  pins this can be configured in software.
  - Pull-up: $50k$ to $65k\Omega$
  - Pull-down: $50kOhm$ to $60k\Omega$
- Maximum current output of 16mA per pin
  - Total current from all pins should not exceed 50mA (3mA per pin)
  - This information is not certain, no official value is provided and no
    BCM2835 datasheet is provided[^GPIO-source]
- Maximum sink of 17mA per GPIO
- GPIO up to 8: default state is 3V3
- GPIO 9 to 27: default state is 0V

[^GPIO-source]: information is from [Compute Module
3+](https://datasheets.raspberrypi.org/cm/cm3-plus-datasheet.pdf)

### access via shell

[GPIO via
shell](https://elinux.org/RPi_GPIO_Code_Samples#sysfs.2C_part_of_the_raspbian_operating_system)
[sysfs](https://www.kernel.org/doc/Documentation/gpio/sysfs.txt)

Directly through file system:

```sh
echo "4" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio4/direction
echo 1 > /sys/class/gpio/gpio4/value
echo "4" > /sys/class/gpio/unexport # remove after use


create_output() {
    for number in "${@}"; do
        [ ! "${number}" ] && echo "Missing GPIO number" && return -1
        [[ ! "${number}" =~ ^[0-9]+$ ]] && \
            echo "\"${number}\" is not a valid gpio number" && return -1
        ((number>27)) && echo "\"${number}\" is outside the range" && return -1
        path="/sys/class/gpio/gpio${number}"
        [ -h "${path}" ] && echo "GPIO-"${number}" already exists" && \
            sudo echo "out" > "${path}/direction" && \
            continue

        echo "creating GPIO-${number}"
        sudo echo "${number}" > "/sys/class/gpio/export"
        sleep 0.1
        sudo echo "out" > "${path}/direction"
    done
}

remove_output() {
    for number in "${@}"; do
        [ ! "${number}" ] && echo "Missing GPIO number" && return -1
        [[ ! "${number}" =~ ^[0-9]+$ ]] && \
            echo "\"${number}\" is not a valid gpio number" && return -1
        ((number>27)) && echo "\"${number}\" is outside the range" && return -1
        path="/sys/class/gpio/gpio${number}"
        [ ! -h "${path}" ] && echo "GPIO-"${number}" was never created" && \
            continue

        echo "removing GPIO-${number}"
        sudo echo "${number}" > "/sys/class/gpio/unexport"
    done
}

create_output 1 2 3
remove_output 1 2 3
```

With WiringPi (there are many):

```sh
gpio mode 7 out
gpio write 7 1
```

To know the status of each pin, use `raspinfo`.

## I²C internal pullups

- [Does the Pi4 have pullups on
  SDA/SCL?](https://raspberrypi.stackexchange.com/questions/101568/does-the-pi4-have-pullups-on-sda-scl)

The GPIO2 (SCA) and GPIO5 (SCL) have internal pullups. You have to activate
them.

## UART

- [RPi Serial Connection](https://elinux.org/RPi_Serial_Connection)

## I²C

- [I²C](https://github.com/fivdi/i2c-bus/blob/master/doc/raspberry-pi-i2c.md)

The [python-smbus2](https://smbus2.readthedocs.io/en/latest/?badge=latest)
library is the most popular, however it lacks control of the write part of a
read command. For this, you can [implement I²C
controller](https://github.com/switchdoclabs/SDL_Pi_HDC1080_Python3/blob/master/SDL_Pi_HDC1080.py)
by hand via the `io` module:

```python
import struct, array, time, io, fcntl

HDC1080_fr= io.open("/dev/i2c-"+str(twi), "rb", buffering=0)
HDC1080_fw= io.open("/dev/i2c-"+str(twi), "wb", buffering=0)
fcntl.ioctl(HDC1080_fr, I2C_SLAVE, HDC1080_ADDRESS)
fcntl.ioctl(HDC1080_fw, I2C_SLAVE, HDC1080_ADDRESS)

def readTemperature():
    s = [HDC1080_TEMPERATURE_REGISTER] # temp
    s2 = bytearray( s )
    HDC1080_fw.write( s2 )
    time.sleep(0.0625)              # From the data sheet

    data = HDC1080_fr.read(2) #read 2 byte temperature data
    buf = array.array('B', data)

# Convert the data
    temp = (buf[0] * 256) + buf[1]
    cTemp = (temp / 65536.0) * 165.0 - 40
    return cTemp
```

### changing the frequency

```
# /boot/config.txt
# 1000 Kbps
dtparam=i2c_arm=on,i2c_arm_baudrate=10000000
```

In 3B, 3B+ and 0W, the clock is linked to the VPU-Code frequency: if this
frequency is not defined it will vary according to the load. Define it like
this:

```
# /boot/config.txt
core_freq=250
```

## raspberry as a programmer

- [OpenOCD on Raspberry
  Pi](https://www.pcbway.com/blog/technology/OpenOCD_on_Raspberry_Pi__Better_with_SWD_on_SPI.html)

## ASCII pinout

- [Raspberry ASCII](https://github.com/tvierb/raspberry-ascii)

```
                            J8
                           .___.
                  +3V3---1-|O O|--2--+5V
          (SDA)  GPIO2---3-|O O|--4--+5V
         (SCL1)  GPIO3---5-|O O|--6--_
    (GPIO_GLCK)  GPIO4---7-|O O|--8-----GPIO14 (TXD0)
                      _--9-|O.O|-10-----GPIO15 (RXD0)
    (GPIO_GEN0) GPIO17--11-|O O|-12-----GPIO18 (GPIO_GEN1)
    (GPIO_GEN2) GPIO27--13-|O O|-14--_
    (GPIO_GEN3) GPIO22--15-|O O|-16-----GPIO23 (GPIO_GEN4)
                  +3V3--17-|O O|-18-----GPIO24 (GPIO_GEN5)
     (SPI_MOSI) GPIO10--19-|O.O|-20--_
     (SPI_MISO) GPIO9 --21-|O O|-22-----GPIO25 (GPIO_GEN6)
     (SPI_SCLK) GPIO11--23-|O O|-24-----GPIO8  (SPI_C0_N)
                      _-25-|O O|-26-----GPIO7  (SPI_C1_N)
       (EEPROM) ID_SD---27-|O O|-28-----ID_SC Reserved for ID EEPROM
                GPIO5---29-|O.O|-30--_
                GPIO6---31-|O O|-32-----GPIO12
                GPIO13--33-|O O|-34--_
                GPIO19--35-|O O|-36-----GPIO16
                GPIO26--37-|O O|-38-----GPIO20
                      _-39-|O O|-40-----GPIO21
                           '---'
                       40W 0.1" PIN HDR
```
