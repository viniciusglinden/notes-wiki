
# REST API

REST (Representational State Transfer API) assumes the exclusive transfer of
stateless resources (objects). Stateless will mean that the server is not
storing client-specific information so that the responses may be cached.

Hypermedia as the Engine of Application State (HATEOAS) is a subset of REST that
responds with hyperlinks, allowing to evolve independently without breaking
client functionality.

## REST Requirements / Rules

### Uniform interface

A REST API should have a uniform interface that follows a consistent set of
rules and conventions. This includes:

- standard HTTP methods (GET, POST, PUT, DELETE) for different operations
- utilizing resource-based URLs
- returning appropriate HTTP status codes
- self-descriptive messages

### Resource-Oriented

REST APIs are designed around resources, which are the key entities or objects
that the API exposes. Each resource should have a unique identifier (URL) and
should be accessible through the API endpoints.

### Stateless

REST APIs should be stateless, meaning that the server should not store any
client-specific data between requests. Each request should contain all the
necessary information for the server to process it, and the server should not
rely on any previous requests.

### Client-Server Architecture

REST APIs follow a client-server architecture, where the client (e.g., a web
application or mobile app) interacts with the server through HTTP requests. The
server is responsible for processing the requests and returning appropriate
responses.

### Caching

REST APIs can utilize caching mechanisms to improve performance and reduce the
load on the server. By specifying caching headers in the API responses, clients
can cache the response data and reuse it for subsequent requests.

### Security

API security is crucial to protect sensitive data and prevent unauthorized
access. Common security measures include using HTTPS for secure communication,
implementing authentication and authorization mechanisms (e.g., API keys,
OAuth), and validating and sanitizing user input to prevent common security
vulnerabilities like SQL injection or cross-site scripting (XSS).

### Error Handling

REST APIs should provide clear and informative error messages in case of
failures or invalid requests. This includes returning appropriate HTTP status
codes (e.g., 400 for bad requests, 404 for not found) and providing error
details in the response body.

### Versioning

As APIs evolve over time, it is important to have a versioning strategy to
ensure backward compatibility. This allows clients to continue using older
versions of the API while new versions are introduced.

## Specifications

The developer must always constrain the REST API with specifications.

- [REST specification](https://jsonapi.org/)

## Testing client access

- [DummyJSON](https://dummyjson.com/): create fake data to request from
- [JSON Placeholder](https://jsonplaceholder.typicode.com/): multiple fake
  resources
- [REST API Placeholder](https://restapi-placeholder.com/): user credentials
- [PokéAPI](https://pokeapi.co/): Pokémon skills API
