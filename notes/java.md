
# Java

- JVM: Java Virtual Machine
- JRE = Java Runtime Environment: package which includes JVM, classes and
  binaries
- JDK = Java Development Kit: package which includes JRE and
  development/debugging tools and libraries to build applications

## headless mode

- [What is Headless mode in
  Java](https://blog.idrsolutions.com/2013/08/what-is-headless-mode-in-java/)

Headless mode means no human interface component (keyboard, screen, mouse, etc).

## installing with APT

- [How to install Java on Debian 10
  Linux](https://linuxize.com/post/install-java-on-debian-10/)

```sh
apt install default-jre # or
apt install default-jdk
update-alternatives --config java # configure and get installation location
vim /etc/environment
```

```
# /etc/environment
JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64/"
```

## utilities

- `archlinux-java`
- `update-alternatives --config java`

