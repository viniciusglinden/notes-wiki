# Compiler toolchain

## gcc flags for low memory applications

Combination will remove unreached functions, thus substantially reducing the
overall size of the program:

`-ffunction-sections -fdata-sections --gc-sections`

## linking g++ by hand

```sh
g++ src/main.cpp -o output -std=c++11 -o0 -Wall \
	-I/usr/include -L/usr/lib -lgit2
```

- `-o0`: compiler optimization level (level 0, reduce compilation time)
- `Wall`: all warnings
- `-Ipath`: path for header (include) files
- `-Lpath`: path for library files (specified with `-l`)


## using GCC for C++

```sh
gcc test.cpp -lstdc++
```

## generate dependency list in GCC syntax-oriented compilers

use the `-MD` flag.

## demangle C++ and java symbols

Demangle in this case means function/method name overloading.

```sh
c++filt -t Z4mainEUliE_
```

## generate coverage report

- [Basic C++ Unit Testing with Coverage Using Catch2 and
  Gcov](https://jasoncarloscox.com/blog/cpp-testing-with-coverage)
- [Generating Code Coverage Report Using GNU Gcov \&
  Lcov](https://medium.com/@naveen.maltesh/generating-code-coverage-report-using-gnu-gcov-lcov-ee54a4de3f11)

Gcov (GNU coverage) comes with `gcc` and `g++`, just like `gdb`. Create a report
with the unit test (catch2, gtest, etc) code. In this example, related unit test
flags are not written.

Compile with the `--coverage` flag (it activates a combination of other flags):

```sh
g++ -o bin main.cpp test.cpp --coverage
```

This will generate `.gcno` files (binaries) for each `.cpp` file.

Then run the program (`./bin`): this will generate `.gcda` (binaries) for each
`.cpp` file. These files will be a concatenation of the executable and file
name: `bin-main.*`, `bin-test.*`

And finally use Gcov on the file that you want to test coverage: `gcov main.cpp`.

Note: `gcov` uses the `.gcda` and `gcno` files. So it assumes that these files
are names `test.gcda` and `test.gcno`, which may not be the case. I had to run
`bin-main.cpp` even although this file doesn't exist.

This will print to `stdout`. You can also use `lcov` (`paru -S lcov`): `lcov
--no-external --capture --directory . --output-file main_coverage.info` and then
`genhtml main_coverage.info --output-directory out`. This will generate a very
readable HTML (like Doxygen).

## solving gcc linker parameter order crash

Add `-Wl,--start-group ` in front of flags.

## suppressing warnings or errors inside code for gcc and clang

[Suppressing Warnings in GCC and
Clang](https://nelkinda.com/blog/suppress-warnings-in-gcc-and-clang/)

```c

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"
#pragma GCC diagnostic ignored "-Wbuiltin-declaration-mismatch"
printf("hello\r\n");
#pragma CLANG diagnostic pop
```

Note: not tested: substitute CLANG for GCC when compiling with clang.

## Extract information from elf

- Check generated with memory: `objdump -d <file.elf>`
- Address to line utility: `addr2line -e example.elf 0xecf8`
- Check symbol addresses: `readelf -s <file.elf>`

