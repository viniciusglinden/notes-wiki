
# Non-Volatile Memory Express

## NVMe directives (streams)

LEGEND:
- leafs: = 0x0# is dir. op.
- branches: = 0x0# is dir. type

```
 [root]
   |
   *-*-> SEND
   |          |
   |          *-> NVME_DIR_IDENTIFY = 0x00
   |          |       |
   |          |       *-> NVME_DIR_SND_ID_OP_ENABLE = 0x01, //Directive Identify Operation Send Enable Directive */
   |          |
   |          *-> NVME_DIR_STREAMS = 0x01
   |                  |
   |                  *-> NVME_DIR_SND_ST_OP_REL_ID = 0x01, //Directive Stream Release Identifier
   |                  |
   |                 *-> NVME_DIR_SND_ST_OP_REL_RSC = 0x02, //Directive Stream Release Resources
   |
   *-*-> RECEIVE
   |          |
   |          *-> NVME_DIR_IDENTIFY = 0x00
   |          |       |
   |          |       *-> NVME_DIR_RCV_ID_OP_PARAM = 0x01, // directive identify receive return parameters
   |          |
   |          *-> NVME_DIR_STREAMS = 0x01
   |                  |
   |                  *-> NVME_DIR_RCV_ST_OP_PARAM = 0x01, //directive stream operation receive parameters
   |                  |
   |                  *-> NVME_DIR_RCV_ST_OP_STATUS = 0x02, //Directive Stream Operation Receive Status
   |                  |
   |                  *-> NVME_DIR_RCV_ST_OP_ALOC_RES = 0x03, //Directive Stream Operation Receive Alocated Resources
   |
 [END]
```

-D <dtype>, --dir-type=<dtype>
	Directive type
-O <doper>, --dir-oper=<doper>
	Directive operation
-T <tdir>, --target-dir=<nsr>
	Target directive of operation

directive send
nvme_dir_snd_id_op_enable		-D 0 -O 1 -T 1		pg 290 Enable Directive (Directive Operation 01h)
nvme_dir_snd_st_op_rel_id 		-D 0 -O 1			pg
nvme_dir_snd_st_op_rel_rsc		-D 1 -O 2

directive receive
nvme_dir_rcv_id_op_param		-D 0 -O 1 -H
nvme_dir_rcv_st_op_param		-D 1 -O 1 -H		pg 293 Return Parameters (Directive Operation 01h)
nvme_dir_rcv_st_op_status		-D 1 -O 2 -H		pg 294 Get Status (Directive Operation 02h)
nvme_dir_rcv_st_op_aloc_res		-D 1 -O 3 -H		pg 295 Allocate Resources (Directive Operation 03h)

[9. Directives] {288}
  |
  *--- 9.1 Directive Use in I/O commands {288}
  |
  *--- 9.2 Indentify (Directive Type 00h) {289}
  |  |
  |  *--- 9.2.1 Directive Receive {289}
  |  |  |
  |  |  *--- 9.2.1.1 Return Parameters (Directive Operation 01h) {289}
  |  |
  |  *--- 9.2.2 Directive Send {290)
  |     |
  |     *--- 9.2.2.1 Enable Directive (Directive Operation 01h) {290}
  |
  *--- 9.3 Streams (Directive Type 01h, Optional)
  |  |
  |  *--- 9.3.1 Directive Receive {293}
  |  |  |
  |  |  *--- 9.3.1.1 Return Paramenters (Directive Operation 01h) {293}
  |  |  |
  |  |  *--- 9.3.1.2 Get Status (Directive Operation 02h) {294}
  |  |  |
  |  |  *--- 9.3.1.3 Allocate Resources (Directive Operation 03h) {295}
  |  |
  |  *--- 9.3.2 Directive Send {295}
  |     |
  |     *--- 9.3.2.1 Release Identifier (Directive Operation 01h) {296}
  |     |
  |     *--- 9.3.2.2 Release Resources (Directive Operation 02h) {296}
  |
[10. Error Reporting and Recovery] {297}


9. Directives:

Directives is a mechanism to enable host and NVM subsystem or controller information exchange.

(Information may not be equal to data)

Support is shown in OACS field.

Both dir. send and dir. receive are commands emmited by the host.

Dir. send/receive: transfer of dir. types

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Dir. receive:	controller to host
Dir. send:		host to controller
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
to remember: we are looking through the host's perspective

dir. cmd.	dir. op. name		dir. op.
dir. rec.	return parameters	1h
dir. rec.	reserved
dir. send.	enable dir.			1h
dir. send.	reserved

Dir. Types:
	Identify:	0h
	Streams:	1h

Dir. Operation depends on dir. type

dir. identify: meta-information about the directive itself
dir. stream: information about data (streams!)

dir. stream = IO cmd. dir.

Moral: namespace/controller shows directive capabilities through directive identify to the host
host then enables these directives on-demand.
Controllers tells the host if it has support for directives.
The identify directive itself may be disable for a certain namespace, shown in the identify directive itself.

9.2. Directive Identify

Identify Dir. determine the Dir. Types that the controller supports and to enable
use of the supported Directives.

9.2.1.1 dir. rec. return  parameters

dir. op.: 1h

returns a data struct contains:
controller's supported dir., namespace's enabled dir.

9.2.2.1 dir. send en. dir.

host enables directives for specific namespaces.

At reset, everything is lost.

9.1 dir. use in I/O cmds

Dir. Type 00h in an I/O cmd = cmd with not Directives.
I/O cmd = write.

The DTYPE field is used to indicate if the IO cmd uses directive (no ID yet): DTYPE = 1h = uses directive.

DSPEC = Directive Specific: interpretation is Dir. Type dependent (may specifies the stream id)

9.3 Streams

SWS = Stream Write Size: provides optimal performance of the wr cmds to the controller
SGS = Stream Granularity Size: size of different stream regions (stream ID)
MSL = Maximum Stream Limit: maximum number of possible SGS's (return parameters)
NSA = Namespace Streams Allocated: how many resources for individual stream ID for exclusive use
NSO = Namespace Streams Open: number of open streams in the namespace

Controller tells host about SWS, SGS, and stream resources per namespace

Allocate Resources = host allocates stream resources to that namespace for exclusive use

Directive type 1h (Stream) is subdivided into the following dir. op.:
dir. cmd	dir. op. name		dir. op.
dir. rec.	return parameters	1h
dir. rec.	get status			2h
dir. rec.	allocate resources	3h
dir. rec.	reserved
dir. send	release identifier	1h
dir. send	release resources	2h
dir. send	reserved

stream identifiers (stream id) are 0001h to FFFFh (two bytes, 0h means not used) in range
but what does this stream id mean?
There is only 65535 streams possible stream identifier (coincides with two bytes)
Hypothesis: a specific stream is available to a specific namespace. Therefore, it must have a globally unique number.

When stream ids are allocated, but not in use until the host submits a write cmd using this stream id.

Format cmd releases stream ids.

9.3.1.1 Dir. rec. return parameters

controller informs fields

There are NVMe subsystem fields and namespace specifc fields.

NVM Subsystem specific: MSL, NSSA, NSSO
Namespace specific: SWS, SGS
Namespace & host specific: NSA, NSO

9.3.1.2 Dir. rec. get status

returns information about the status of open streams for the namespace

9.3.1.3 Dir. rec. allocatte resources

indicates the number of streams that the host requests for the namespace: NSR
returns the number of streams allocated: NSA

9.3.2.1 Dir. send release identifier

releases one the stream id (stream id is no longer in use)

9.3.2.2 Dir. send release resources

releases all stream ids
returns NSA = 0 if successful

## `nvme-cli` usage

echo 'hello' | sudo nvme write /dev/nvme0n1 -s 0 -z 1
Rounding data size to fit block count (4096 bytes)
SSD_WRITE
length = 8, sector = 0
write: Success

echo 'helloooooooooooooooooooooooooooooo' | sudo nvme write /dev/nvme0n1 -s 0 -z 1
Rounding data size to fit block count (4096 bytes)
SSD_WRITE
length = 8, sector = 0
write: Success

sudo nvme read /dev/nvme0n1 -s 0 -z 1
SSD_READ
length = 8, sector = 0
Rounding data size to fit block count (4096 bytes)
read: Success

echo 'hello' | sudo nvme write /dev/nvme0n1 -s 0 -z 4096
SSD_WRITE
length = 8, sector = 0
write: Success

echo 'hello' | sudo nvme write /dev/nvme0n1 -s 0 -z 4095
SSD_WRITE
Rounding data size to fit block count (4096 bytes)
length = 8, sector = 0
write: Success

echo 'helloooooooooooooooooooooooooooooo' | sudo nvme write /dev/nvme0n1 -s 0 -z 10000
SSD_WRITE
length = 8, sector = 0
write: Success

-s 0
sector = 0
-s 1
sector = 8
-s 16
sector = 128

-c 0/<default>
length = 8
-c 1
length = 16
-c 2
length = 24

cat file | sudo nvme write /dev/nvme0n1 -s 0 -c 1 -z 4096
Rounding data size to fit block count (8192 bytes)
SSD_WRITE
length = 16, sector = 0
write: Success

--dir-type (-T): The nvme-cli only enforces the value be in the defined range for the directive type, though the NVMe specification (1.3a) defines only one directive, 01h, for write stream identifiers.
--dir-spec (-S): Optinal field for directive specifics. When used with write streams, this value is defined to be the write stream identifier. The nvme-cli will not validate the stream request is within the controller's capabilities.


 
