

# OpenAPI

- [Recommended tools](https://openapi.tools/)
- [OpenAPI edition](https://editor.swagger.io/)
- [The OpenAPI specification](https://github.com/OAI/OpenAPI-Specification): the
  actual repository with the specs and examples

OpenAPI Description (OAD) is a single object containing fields adhering to the
structure defined in the OpenAPI Specification (OAS).

[Structure of an OpenAPI Description](https://learn.openapis.org/specification/structure#minimal-openapi-description-structure))

- [OAS example](https://learn.openapis.org/examples/tictactoe.yaml)
