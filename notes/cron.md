
# Cron

## installing

In arch linux-like systems, use `cronie`: `sudo pacman -S cronie`.

### activating for

#### `systemctl`

```sh
systemctl enable --now cronie.service
```

#### `runit`

```sh
pacman -S cronie-runit
ln -s /etc/runit/sv/cronie /etc/runit/runsdir/default
```

#### `OpenRC`

```sh
pacman -S cronie-openrc
rc-update add udev sysinit
```

## Crontab commands

- `crontab -e` edits with `$VISUAL`, for a specific user `crontab -e -u user`
- `crontab -` gets from `stdin`
- `crontab -l` list all user's cron tasks
- `crontab -r` remove the crontab file from the cron spooler, but not the
  crontatb file
- `crontab -v` display the last time you edited the crontab file

 To add cron jobs for the root (`sudo` command) `su -c "crontab -e"`

## file syntax

For the root user:

```sh
# Minute  Hour  Day of Month      Month         Day of Week    User    Command
# (0-59) (0-23)   (1-31)    (1-12 or Jan-Dec) (0-6 or Sun-Sat)

    0       2       *             *                *           root    /usr/bin/find
```

For regular users:

```sh
# Minute  Hour  Day of Month      Month         Day of Week    Command
# (0-59) (0-23)   (1-31)    (1-12 or Jan-Dec) (0-6 or Sun-Sat)

    0       2       *             *                *           /usr/bin/find
```

```sh
# runs command every 15 minutes:
*/15 * * * * CMD
# runs command at start-up (every time it boots):
@reboot CMD
```

`CMD` must be the full path (e.g. `/usr/bin/echo` instead of just `echo`).

|Symbol|Description|
|:---|:---|
|\*|Wildcard, specifies every possible time interval|
|,|List multiple values separated by a comma.|
|-|Specify a range between two numbers, separated by a hyphen|
|/|Specify a periodicity/frequency using a slash|

- @reboot at startup
- @yearly once a year[^file-syntax-1]
- @annually ( == @yearly)
- @monthly once a month[^file-syntax-1]
- @weekly once a week[^file-syntax-1]
- @daily once a day[^file-syntax-1]
- @midnight ( == @daily)
- @hourly once an hour[^file-syntax-1]

[^file-syntax-1]: this is OS dependent.

Note:
- I tried inserting two lines with `@reboot` and only the first would execute
  consistently
- file is located at `/var/spool/cron/user_name`

## Hints

Cronjobs may have components that require information about your current display
(X server). Whenever running an X server related, you have to add the `export
DISPLAY=:0`, which indicates the proper TTY.
Another variable to export is the DBUS address, used by many application for
interconnecting (such as *xdotool*?). To summer up:

```
eval "export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus; export DISPLAY=:0"; . /path/to/.zprofile; /path/to/command
```

## Debugging

1. Is the Cron daemon running?
- Run `ps ax | grep cron` and look for cron.
- Debian: `service cron start` or `service cron restart`
2. Is cron working?[^debugging-1]
- `* * * * * /bin/echo "cron works" >> /tmp/file 2>&1`
- You obviously need to have write access to the file you are redirecting the output to. A unique file name in /tmp which does not currently exist should always be writable.
- Is the command working standalone?
- Check if the script has an error, by doing a dry run on the CLI
- When testing your command, test as the user whose crontab you are editing, which might not be your login or root
3. Can cron run your job?
- Check `/var/log/cron.log` or `/var/log/messages` for errors.
- Ubuntu: `grep CRON /var/log/syslog`
- Redhat: `/var/log/cron`
4. Check permissions
- set executable flag on the command: `chmod +x /var/www/app/cron/do-stuff.php`
- if you redirect the output of your command to a file, verify you have permission to write to that file/directory
5. Check paths
- check she-bangs / hashbangs line
- do not rely on environment variables like PATH, as their value will likely not be the same under cron as under an interactive session
6. Don't suppress output while debugging
- commonly used is this suppression: `30 1 * * * command > /dev/null 2>&1`
- re-enable the standard output or standard error message output by removing `>/dev/null 2>&1` altogether; or perhaps redirect to a file in a location where you have write access: `>>cron.out 2>&1` will append standard output and standard error to cron.out in the invoking user's home directory.

Still not working?

1. Raise the cron debug level
- Debian
  - in `/etc/default/cron`
  - set `EXTRA_OPTS="-L 2"`
  - `service cron restart`
  - `tail -f /var/log/syslog` to see the scripts executed
- Ubuntu
  - in `/etc/rsyslog.d/50-default.conf`
  - add or comment out line `cron.crit /var/log/cron.log`
  - reload logger `sudo /etc/init.d/rsyslog reload`
  - re-run cron
  - open `/var/log/cron.log` and look for detailed error output
- Reminder: deactivate log level, when you are done with debugging
2. Run cron and check log files again

[^debugging-1]: command may run through the shell, but not in cron.
