
# Python

- [Structuring your project](https://docs.python-guide.org/writing/structure/):
  a full review of python
- [command line and environment variables
  options](https://docs.python.org/3/using/cmdline.html)
- [Python Type Annotations Full
  Guide](https://missourimrr.github.io/docs/python/type-annotations/full-guide.html)

## package vs module

A Package is basically a folder with `__init__.py` file under it and usually
some Modules, where Module is a `*.py` file.
`__init__.py` has a special behavior, which is used to gather all package-wide
definitions.

In this `__init__.py` file, one may specify which function/classes/constants
will be available outside the project:

```python
from .module1 import myClass
from .module2 import myFunction
```

It has to do with import mainly. If
you add `__init__.py` to `example` you can use:

```python
from example import test
```

Modules \& package names in lower case are viewed as pythonic.

A file `modu.py` in the directory `pack/` is imported with the statement import
`pack.modu`. This statement will look for `__init__.py` file in pack and execute
all of its top-level statements. Then it will look for a file named
`pack/modu.py` and execute all of its top-level statements. After these
operations, any variable, function, or class defined in `modu.py` is available
in the `pack.modu` namespace.

If there is too much code in one package, use subpackages.

For project structure containing `unittest`s, please see unittests section

### packaging

- [Writing your
  `pyproject.toml`](https://packaging.python.org/en/latest/guides/writing-pyproject-toml/)
- [Classifiers](https://pypi.org/classifiers/)

It is a best practice to have a `pyproject.toml` file. This file will:

- Configure the packaging
- Configure the development dependencies (ruff, mypy, pytest, etc)

It is a all-in-one file.

#### Setuptools

`setuptools` uses `distutils` (`distutils` is not used directly anymore):

You have the following structure:

```
packaging_example
|
*-package
| |
| *-module
*-setup.py
```

This `setup.py` will configure the package. It should be something like:

```python
from setuptools import setup, find_packages

setup(
    name='module',
    version='1.0',
    author='João da Silva',
    author_email='joao.silva@example.com',
    description='A short description',
    long_description='A long description',
    install_required=['other_package'],
    keywords=['key','words'],
    classifiers=['development status', 'intended audience', 'Operating System', 'etc'],
    packages=find_packages()
    # find_packages looks up into folders with __init__ and include them into distribution packages
)
```

- Create the package with the `sdist` command: `python setup.py sdist`
- To add non-python files, create `MANIFEST.in`:

```
include README
include LICENSE
include requirements.txt
recursive-include module/subfolder/ *
```

- To publish it in `pypi.org`, use `twine`: `twine upload dist/*`

`requirements.txt` looks like:

```
pyserial==3.5
RPi.GPIO==0.7.1
```

## `__main__` trick

When you directly run a file `__name__` will be `'__main__'`, therefore the following is
recommended:

```py
if __name__ == '__main__':
	main()
```

## Fundamental Data Types

[Wikipedia - Tuple](https://en.wikipedia.org/wiki/Tuple)

- None: object that denotes the lack of value
- bool: `True` (`1`) and `False` (`0`) Boolean values
- int: integer numbers - immutable
- long: long integer numbers
- float: floating point numbers
- complex: complex numbers
- str: ordered - immutable - strings of characters
- `unicode_`: immutable - unicode strings of characters
- list: ordered - mutable - indexed list of objects
- tuple: ordered - immutable - indexed list of objects
- dict: unordered - mutable - hash table for storing key-value pairs
- set: unordered - mutable - list of unique objects
- file: file objects

Notes:

- To inspect the type, use `print(type(OBJECT))`
- Immutable means you cannot update or change the values of tuple elements
- Ordered means that the object will hold the elements as given:
- Careful on empty set initialization, `{}` is a dictionary

```py
letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

string_letters = str(letters)
lists_letters = list(letters)
tuples_letters = tuple(letters)
sets_letters = set(letters)
```

This will yield:

```
String:  abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ

Lists:  [
'a', 'b', 'c', 'd', 'e', 'f', 'g',
'h', 'i', 'j', 'k', 'l', 'm', 'n',
'o', 'p', 'q', 'r', 's', 't', 'u',
'v', 'w', 'x', 'y', 'z', 'A', 'B',
'C', 'D', 'E', 'F', 'G', 'H', 'I',
'J', 'K', 'L', 'M', 'N', 'O', 'P',
'Q', 'R', 'S', 'T', 'U', 'V', 'W',
'X', 'Y', 'Z'
]

Tuples:  (
'a', 'b', 'c', 'd', 'e', 'f', 'g',
'h', 'i', 'j', 'k', 'l', 'm', 'n',
'o', 'p', 'q', 'r', 's', 't', 'u',
'v', 'w', 'x', 'y', 'z', 'A', 'B',
'C', 'D', 'E', 'F', 'G', 'H', 'I',
'J', 'K', 'L', 'M', 'N', 'O', 'P',
'Q', 'R', 'S', 'T', 'U', 'V', 'W',
'X', 'Y', 'Z'
)

Sets:  {
'G', 'U', 'P', 'K', 'Q', 'w', 'I',
'Z', 'N', 'l', 'm', 'h', 'J', 'D',
'k', 'C', 'r', 'B', 'A', 'F', 'y',
'c', 'V', 'i', 'E', 'a', 'o', 'R',
'T', 'e', 'g', 'b', 'L', 'f', 'X',
'x', 'O', 'S', 'j', 'v', 'p', 'Y',
'H', 'u', 'n', 'z', 't', 'M', 'd',
'W', 's', 'q'
}
```

## Functional Programming

- map: Applies function to every item of an iterable object and returns a list
  of the results.
- filter: Returns a sequence from those elements of iterable for which function
  returns True.
- reduce: Applies function of two arguments cumulatively to the items of
  iterable, from left to right, so as to reduce the iterable to a single value.

```python
from functools import reduce

myDict = {"a": 1, "b": 2}
sequence = [1, 2, 3, 4]

print(reduce(lambda x, y: x+y, sequence))
print('\n'.join(map(str, myDict.values())))
print(filter(lambda x: x % 2 != 0, seq))
```

### filter function

Filter is:

- apply iterable to a function
- see if the function returns True
- copy this iterable to a new filter object
- repeat to exaustion
- return the filter object

```python
def func(x):
    if x>5:
        return True
# filter(function, iterable)
result = filter(func, (1,2,6))
# result = [6]
```

If `None` is passed instead of a function, all terms that evaluate to `False`
are removed.

### map function

Filter is:

- apply iterable to a function
- copy the result to a new map object
- repeat to exhaustion
- return the map object

```python
def func(x):
    if x>5:
        return x
# map(function, iterable)
result = map(filterdata,(1,2,6))
# result = [None None True]
```

If `None` is passed instead of a function, all terms that evaluate to `False`
are removed.

### lambda function

Lambda function is the anonymous function in python. This function can only
contain one expression.

```python
# lambda arguments : expression
cube = lambda x: x*x*x
```

It can be used in combination with named functions:

```python
def power(n):
    return lambda a : a ** n

powerOf2 = power(2)

a = powerOf2(4) # = 16
```

## Numeric Types Conversions and Constructors

- bool: Returns an expression converted into a Boolean.
- int: Returns an expression converted into an integer number.
- long: Returns an expression converted into a long integer number.
- float: :Returns an expression converted into a floating point number.
- complex: Returns an expression converted into a complex number.

### Types Conversions

- bin: Returns an integer converted into a binary string.
- oct: Returns an integer converted into an octal string.
- hex: Returns an integer converted into a hexadecimal string.

## Arithmetic

- abs: Returns the absolute value of a number.
- pow: Returns a number raised to a power; or optionally a modulus of the number raised to a power and another number.
- round: Returns a floating point number rounded to a specified number of decimal places.
- divmod: Returns quotient and remainder after a division of two numbers.

## String Conversions

- chr: Returns a string of one character whose ASCII code is the specified number.
- ord: Returns an integer representing the code of the character.
- unichr: Returns a Unicode character specified by the code.
- format: Returns a formatted string.
- repr: Returns a string containing a printable representation of an object.

## Sequences Constructors

- str: Returns a string containing a printable representation of an object.
- unicode: Returns the Unicode string version of object.
- list: Converts an object into a list.
- tuple: Returns a tuple built from iterable.
- bytearray: Returns a new array of bytes.
- buffer: Returns a new buffer object which references the object argument.
- memoryview: Returns a memoryview object.
- range: Returns a list of arithmetic progressions.
- xrange: Returns an xrange object.

## Mappings Constructors

- dict: Returns a dictionary object.
- set: Returns a set type initialized from iterable.
- frozenset: Returns a frozenset object.

## Operating on Containers

- enumerate: Returns an enumerate object.
- len: Returns an int type specifying number of elements in the collection.
- reversed: Returns a reverse iterator over a sequence.
- sorted: Returns a sorted list from the iterable.
- sum: Returns a total of the items contained in the iterable object.
- zip: Returns a list of tuples, where the i-th tuple contains the i-th element from each of the argument sequences or iterables.
- slice: Returns a slice object.

## Iterators

- iter: Returns an iterator object (a class with the `__iter__` method).
- next: Retrieves the next item from the iterator by calling its `next()`
  method.

```python
class Counter:
    def __init__(self, start, end):
        self.num = start
        self.end = end

    def __iter__(self):
        return self

    def __next__(self):
        if self.num > self.end:
            raise StopIteration
        self.num += 1
        return self.num - 1
```

## Comparisons

- cmp: Compares two objects and returns an integer according to the outcome.
- max: Returns the largest item in an iterable or the largest of two or more arguments.
- min: Returns the smallest item from a collection.
- all: Returns a Boolean value that indicates whether the collection contains only values that evaluate to True.
- any: Returns a Boolean value that indicates whether the collection contains any values that evaluate to True.

## Identity

- hash: Return the hash value of the object (if it has one).
- id: Returns the “identity” of an object.

## File Objects Constructors

- file: Returns a file object.
- open: Opens a file returning a file object.

## Object Oriented Functions

- classmethod: Returns a class method for the function.
- property: Returns a property attribute for new-style classes (classes that derive from object).
- staticmethod: Returns a static method for function.
- super: Returns a proxy object that delegates method calls to a parent or sibling class of type.
- setattr: Assigns a value to the object’s attribute given its name.
- getattr: Returns the value of the named attribute of object.
- delattr: Deletes the named attribute of an object.
- hasattr: Returns a Boolean stating whether the object has the specified attribute.
- isinstance: Returns a Boolean stating whether the object is an instance or subclass of another object.
- issubclass: Returns a Bool type indicating whether an object is a subclass of a class.
- vars: Returns the mapping of an object’s (writable) attributes.
- dir: Returns the list of names in the current local scope. If supplied with an argument attempts to return a list of valid attributes for that object.
- type (1): Returns the type of an object (constructor name).
- type (2): Returns a new type object.

## Information

- callable: Returns a Boolean stating whether the object argument appears callable.
- globals: Returns a dictionary representing the current global symbol table.
- locals: Returns a dictionary representing the current local symbol table.
- help: Invokes the built-in help system.

## System

- `__import__`: Imports a module.
- `reload`: Reloads a previously imported module.
- `compile`: Returns an AST or code object.
- `execfile`: Evaluates contents of a file.
- `eval`: Returns a result of the evaluation of a Python expression.
- `input`: Evaluates user input.
- `intern`: Enters the string into interned strings table (if not already there).
- `print`: Returns a printed representation of the objects.
- `raw_input`: Reads a line from standard input stream.

## Misc

- object: Returns a new featureless object.
- apply: Returns the result of a function or class object called with supplied
  arguments.
- basestring: This abstract type is the superclass for str and unicode. It
  cannot be called or instantiated, but it can be used to test whether an object
  is an instance of str or unicode.
- coerce: Returns a tuple consisting of the two numeric arguments converted to a
  common type.

## Comprehensions

- `[]` list comprehension: Returns a list based on existing iterables.
- `{}` set comprehension: Returns a set based on existing iterables.
- `{}` dictionary comprehension: Returns a dictionary based on existing iterables.

## Generator Expression

- generator expression: Returns an iterator over elements created by using list comprehension.

## Type annotation

- `Generator[yield_type, send_type, return_type]`

## virtual environment

When activating an environment, your PATH changes.
If you are running Python 3.4+, you can use the venv module baked into Python

```sh
python -m venv <directory>
```

This command will create a `venv` in the specified directory and copy `pip` and
`easy_install` into it too.

- activate: `source myenv/bin/activate`.
- deactivate: `deactivate`
- delete: first deactivate and then `rm -r venv`

### adding environment to git

```sh
git init
echo "env" > .gitignore
python3 -m venv env
source env/bin/activate
pip3 install <packages>
pip3 freeze > requirements.txt
deactivate
git add requirements.txt
```

Then, you can just use the `#!/usr/bin/env python3` shebang and it will activate
the environment automatically (once `pip3 install -r requirements.txt` is run).

## project structure

- [Sample package](https://github.com/pypa/sampleproject)
- [Sample Module Repository](https://github.com/navdeep-G/samplemod): "sample"
  directory is so called because it is the project's name

Suppose the project is called `sample`. basic hierarchy structure could be:

```
LICENSE
Makefile
README.rst
docs/conf.py
docs/index.rst
requirements.txt
sample/__init__.py
sample/core.py
sample/helpers.py
setup.py
tests/test_advanced.py
tests/test_basic.py
```

- `sample`: code of interest
- `setup.py`: package and distribution management
- `requirements`: development dependencies
- `tests`: package integration and unit tests

Requirements can be installed with `pip install -r requirements.txt`.

## testing

There are two ways of importing the packaged modules to test:

- Expect the package to be installed in site-packages
- Use a simple (but explicit) path modification to resolve the package properly

The second is preferred, to this end create `tests/context.py` file:

```python
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import sample
```

Then, inside a test file:

```python
from .context import sample
```

Common rules for testing are:

- test unit focus on one tiny bit of functionality
- tests are fully independent
- each test must be loaded with a fresh dataset (`setUp()`) and may have some
  cleanup afterwards (`tearDown()`)

## class definition

In Python, everything is an object, and can be handled as such. This is what is
meant when we say, for example, that functions are first-class objects.
Functions, classes, strings, and even types are objects in Python: like any
object, they have a type, they can be passed as function arguments, and they may
have methods and properties. In this understanding, Python can be considered as
an object-oriented language.

[Python
OOP](https://www.programiz.com/python-programming/object-oriented-programming)

A typical class with method looks like this

```python
class Parrot:

# instance attributes
	def __init__(self, name, age):
		self.name = name
		self.age = age

# instance method
	def sing(self, song):
		return "{} sings {}".format(self.name, song)

	def dance(self):
		return "{} is now dancing".format(self.name)

# instantiate the object
		blu = Parrot("Blu", 10)

# call our instance methods
print(blu.sing("'Happy'"))
print(blu.dance())
```

## class inheritance

```python
# parent class
class Bird:

	def __init__(self):
		print("Bird is ready")

	def whoisThis(self):
		print("Bird")

	def swim(self):
		print("Swim faster")

# child class
class Penguin(Bird):

	def __init__(self):
		# call super() function
		super().__init__()
		print("Penguin is ready")

	def whoisThis(self):
		print("Penguin")

	def run(self):
		print("Run faster")

peggy = Penguin()
peggy.whoisThis()
peggy.swim()
```

## data encapsulation

```python
class Computer:

	def __init__(self):
		self.__maxprice = 900

	def sell(self):
		print("Selling Price: {}".format(self.__maxprice))

	def setMaxPrice(self, price):
		self.__maxprice = price

c = Computer()
c.sell()

# change the price
c.__maxprice = 1000
c.sell()

# using setter function
c.setMaxPrice(1000)
c.sell()
```

### property object

`property` is a class that encapsulate (other) class members to that they *always* use the setter
and getter methods.

```python
class Celsius:
    def __init__(self, temperature=0):
        self.temperature = temperature
    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32
    def get_temperature(self):
        print("Getting value...")
        return self._temperature
    def set_temperature(self, value):
        print("Setting value...")
        if value < -273.15:
            raise ValueError("Temperature below -273.15 is not possible")
        self._temperature = value
    # creating a property object
    temperature = property(get_temperature, set_temperature)

human = Celsius(37) # calls setter
print(human.temperature) # calls getter
print(human.to_fahrenheit()) # calls getter
human.temperature = -300 # calls setter
```

Class constructor: `property(fget=None, fset=None, fdel=None, doc=None)`

- `fget`, `fset`, `fdel` are for function get, set and delete respectively
- `doc` is a string for documentation

Class is broken down to:

```python
foo = property() # make empty property
foo = foo.getter(get_foo) # assign fget
foo = foo.setter(set_foo) # assign fset
```

If the functions are called `get_foo` and `set_foo`, you can use the class as a decorator:

```python
class Celsius:
    def __init__(self, temperature=0):
        self._temperature = temperature
    def to_fahrenheit(self):
        return (self._temperature * 1.8) + 32
    @property
    def temperature(self):
        print("Getting value...")
        return self._temperature
    @temperature.setter
    def temperature(self, value):
        print("Setting value...")
        if value < -273.15:
            raise ValueError("Temperature below -273 is not possible")
        self._temperature = value

c = Celsius() # Setting value...
c.temperature = 123 # Setting value...
print(c.temperature) # Getting value...
```

Note:

- `_temperature` is the value inside methods
- `@property` on top of getter
- `@temperature.setter` after
- getter is declared before the setter

## polymorphism

```python
class Parrot:

	def fly(self):
		print("Parrot can fly")

	def swim(self):
		print("Parrot can't swim")

class Penguin:

	def fly(self):
		print("Penguin can't fly")

	def swim(self):
		print("Penguin can swim")

# common interface
def flying_test(bird):
	bird.fly()

#instantiate objects
blu = Parrot()
peggy = Penguin()

# passing the object
flying_test(blu) # prints "Parrot can fly"
flying_test(peggy) # prints "Penguins can't fly"
```

## decorator function

A decorator is a function or a class that wraps (or decorates) a function or a
method. The ‘decorated’ function or method will replace the original
‘undecorated’ function or method.

```python
def my_decorator(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper

@my_decorator # equivalent to my_decorator(say_whee)
def say_whee(a, b):
    print("whee!")
    return a + b
```

Class decorators are the same as function decorators:

```python
def singleton(cls):
    instances = {}
    def get_instance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return get_instance

@singleton
class Controller_t(object):
    pass
```

### decorator function with arguments and function with no arguments

```python
def decorator(like):
    print(like)
    def inner(func, *args, **kwargs):
        func(*args, **kwargs)
    return inner

@decorator("lol")
def my_func():
    print("Inside actual function")
```

### decorator function with arguments and function with arguments

- [decorator functions with decorator
  arguments](https://www.artima.com/weblogs/viewpost.jsp?thread=240845#decorator-functions-with-decorator-arguments)

```python
def decoratorFunctionWithArguments(arg1, arg2, arg3):
    print("Inside decorator()")
    print("Decorator arguments:", arg1, arg2, arg3)
    def wrap(f):
        print("Inside wrap()")
        print("Decorator arguments:", arg1, arg2, arg3) # may also be here
        def wrapped_f(*args, **kwargs):
            print("Inside wrapped_f()")
            print("Decorator arguments:", arg1, arg2, arg3) # may also be here
            f(*args, **kwargs)
            print("After f(*args)")
        return wrapped_f
    return wrap

@decoratorFunctionWithArguments("hello", "world", 42)
def sayHello(a1, a2, a3, a4):
    print('sayHello arguments:', a1, a2, a3, a4)

sayHello(1, 2, 3, 4)
```

## exception

- [Built-in
  Exceptions](https://docs.python.org/3/library/exceptions.html#exception-hierarchy)

Use the `try`-`except` block:

```python
try:
    # run this code
except <exception>:
    # execute on exception
else:
    # execute on no exception
finally:
    # always run this code
```

## Executing external commands

- [Python run bash](https://geekflare.com/python-run-bash/)

According to PEP 324, you should either use `subprocess.run()` or
`subprocess.Popen()`. Note that both are not available for python2.

- [subprocess library](https://docs.python.org/3/library/subprocess.html)

```python
import subprocess
ret = subprocess.run("ls", capture_output=True, encoding='utf-8')
print(ret.stdout)
```

If you have command arguments, use a list of strings:

```python
subprocess.run(["touch", "kkk"], capture_output=True, encoding='utf-8')
```

or

```python
import subprocess
def run_cmd(cmd):
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    data = p.communicate()
    data = {"code": p.returncode, "stdout": data[0].decode(),
            "stderr": data[1].rstrip(b'\n').decode()}
    return data
```

- `shell=True` used when you also want to run inbuilt shell commands
- `shel=True` allows to use a string instead of list of strings
- `check=True` throw an exception if command is not successful
- `text=True` backwards compatibility, use `encoding='utf-8'`
- `input=` give yet more arguments to the command

## command line arguments

[Command Line
Arguments](https://www.tutorialspoint.com/python/python_command_line_arguments.htm)

```python
import sys
print('Number of arguments:', len(sys.argv), 'arguments.')
print('Argument List:', str(sys.argv))
```

Just like in C, argument 0 is the scripts name.

### parse arguments

Use: [argparse](https://learnpython.com/blog/argparse-module/)

Note: there is also a **deprecated** `optparse` module.

#### getopt

This is no longer recommended.

```python
getopt.getopt(args, options, [long options])
```

- options: short options, if the option requires an argument, use `:`
- long options: if the option requires an argument, use `=`

hat the script wants to recognize, with options that require an argument should
be followed by a colon

```python
import sys, getopt

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, remainder = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('test.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(help)
            print(usage)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        else:
            print(f'error, invalid {opt} option')
            print(usage)
            sys.exit()
    print('Input file is "', inputfile)
    print('Output file is "', outputfile)

if __name__ == "__main__":
    main(sys.argv[1:])
```

- `./file.py -hi argument` will have input file as `argument`
- `./file.py -hi=argument` will have input file as `=argument`
- `./file.py -hiargument` will have input file as `argument`
- `./file.py -ih argument` will have input file as `h`
- `./file.py --input=argument` will have input file as `argument`
- `./file.py --input argument` will have input file as `argument`
- `./file.py --input -h argument` will have input file as `-h`

## importing another script with full path

```python
import sys

sys.path.append('/path/to/folder') # inside folder there is test.py
import test
```

## importing another script whose name is a variable

```python
import importlib
importlib.import_module('moduleName')
```

or

```python
script = __import__("script.py")
```

## progress bar

```python
from tqdm import tqdm

text = ''
for char in tqdm(['a', 'b', 'c', 'd']):
	text = text + char
```

Or

```python
from tqdm import tqdm

text = ''
with tqdm(total=100) as pbar:
	for char in tqdm(['a', 'b', 'c', 'd']):
		text = text + char
		pbar.update(100/5)
```

Or

```python
from tqdm import tqdm

text = ''
pbar = tqdm(total=100)
for char in tqdm(['a', 'b', 'c', 'd']):
	text = text + char
	pbar.update(100/5)
pbar.close()
```

## Basic shell commands

Use the [shutil](https://docs.python.org/3/library/shutil.html) module.

## get function name

```python
def testing_func():
    print(testing_func.__name__)
```

## dictionary

```python
    a = [ 1, 2, 3, 4] # 4 elements
    b = [ 'a', 'b', 'c'] # 3 elements

    c = dict(zip(a, b))
    # c = {1: 'a', 2: 'b', 3: 'c'} # 4 is ignored

    for i in list(c.values()):
        print(i)
    # prints values

    for key, value in c:
        pass
```

## `exit` vs `sys.exit` vs `quit`

- `quit` is an alias for `exit`
- `exit` is intended to be used inside interpreter shell, not production code
- `exit` is part of the `site.py` module, which is imported unless unspecified
- `exit` does not raise an exception
- `sys.exit` raises the SystemExit exception
- `sys.exit` argument can be an object

## dataclass

Dataclasses are new to standard python3.7.
Dataclasses are a decorator that automatically generates standard methods, such as initialization,
comparison, hash, etc.

```python
from dataclasses import dataclass

@dataclass
class myData:
    var1
    var2 : int = 1

    def isNew(self):
        return self.var2 == 1
```

You can also give some options to the decorator, so to modify the standard
behavior. For example:

```python
@classes(init=False, # do not create the standard init function
freeze=True) # make the class immutable
class myData:
    #...
```

## attrs

Same goal as [dataclasses](#dataclass). Package is called `attrs` but import is
`attr`.

- [Learn attrs by example](https://www.attrs.org/en/stable/examples.html)
- [historical notation](https://www.attrs.org/en/stable/names.html#tl-dr)

## unit test

- [Explanation with simples
  examples](https://www.internalpointers.com/post/run-painless-test-suites-python-unittest)
- [Documentation on unittest
  framework](https://docs.python.org/3/library/unittest.html)

There are some options, `unittest` is a standard library testing framework for
unit testing:

```python
import unittest
import calc # my module

# you always have to create a test class
class TestCalc(unittest.TestCase): # name however you like
    def test_add(self): # have to start with "test_", otherwise will not be run
        result = calc.add(10, 5)
        self.assertEqual(result, 15)

if __name__ == '__main__':
    unittest.main()
    # this is just like gtest_main
    # if you do not add it, you will have to call the script as `python -m unittest test_calc.py`

```

### structure

The best way to organize a project with unit tests is the following

```
|
* ./my_package/
|   |
|   * __init__.py
|   * my_module.py
|
* ./tests
    |
    * __init__.py
    * test_case1.py
    * test_case2.py
```

### automating test run

You can choose between `unittest` default or `nose2` test runner.

```sh
python3 -m unittest
```

- `unittest` will search automatically for test case files, therefore, it is not
  necessary to indicate a file

To view module options, type `python3 -m unittest -h`.

Unfortunately `unittest` does not output colored results by default. You can
[`pip3 install green`](https://github.com/CleanCut/green) and then `green -vvv
--run-coverage` to do that.

### parameterized tests

- [pytest-parameterize](https://docs.pytest.org/en/latest/how-to/parametrize.html#parametrize-basics).

```python
import pytest

@pytest.mark.parametrize("paramA,paramB",
                         [(0, 10),
                         (1, 10)
                         )
def test_memory_page_alloc(paramA: int, paramB: int) -> None:
    assert paramA < paramB
```

### mocking

- [mock object library documentation](https://docs.python.org/3/library/unittest.mock.html)
- [pytest-mock documentation](https://pytest-mock.readthedocs.io/en/latest/index.html)
- [pytest-mock tutorial](https://changhsinlee.com/pytest-mock/)

## debugging with gdb

[gdb Debugging Full
Example](https://www.brendangregg.com/blog/2016-08-09/gdb-example-ncurses.html)

## parallelism multiprocessing concurrency threading and asyncio

- [python concurrency](https://realpython.com/python-concurrency/)
- [async io python](https://realpython.com/async-io-python/)

- Parallelism: multiple operations at the same time
- Multiprocessing: a means to parallelism, spreading tasks over CPUs
- Concurrency: Broader term for parallelism, without implying parallelism
- Threading: concurrent execution whereby multiple threads take turns executing
  tasks
- asyncio: library to write concurrent code

So:

- Concept concurrency has
  - threading and async IO implementation and
  - parallelism concept
- Concept parallelism has
  - multiprocessing implementation

Moreover, asyncio is: single-threaded, single-process, uses cooperative
multitasking.

Cooperative multitasking is in contrast to pre-emptive multitasking which may
interrupt even a trivial task (like `x = x + 1`).

- Asynchronous routines are able to “pause” while waiting on their ultimate
  result and let other routines run in the meantime.
- Asynchronous code, through the mechanism above, facilitates concurrent
  execution - in other words - gives the look and feel of concurrency.

concurrency type|library|switching decision|number of processors
-|-|-|-
pre-emtive mutlitasking|threading|OS decides|1
cooperative multitasking|asyncio|task decides to give up control|1
multiprocessing|multiprocessing|run at the same time on different cores|many

In layman's terms, a coroutine is a wrapped version of a function that allowes
it to run asynchronously.

### asyncio library

Keywords:

- `async`: wrap this function and return a coroutine object
- `await`: run a coroutine object, must be called inside of an asynchronous
  function
- Event loop: low level python that allows user to write asynchronous code

```python
import asyncio

async def main():
    print("begin main")
    # run task separately
    task = asyncio.create_task(foo("text"))
    print("end main")
    # now we want to wait for task to complete
    retval = await task
    print(retval)

async def foo(text):
    print('begin foo')
    print(text)
    # wait for sleep completion
    await asyncio.sleep(1)
    print('end foo')
    return 1

asyncio.run(main())
```

## UART

```python
from serial import Serial # pySerial
import time

DELTA_TIME = 5.0

uart = Serial("/dev/ttyUSB0")
uart.flushInput()
uart.flushOutput()

uart.write(b'oi')

buffer = ""
ts = 0.0
while True:
    try:
        while uart.in_waiting:
            buffer += uart.read(1).decode("utf-8")
        if time.time() > ts + DELTA_TIME:
            ts = time.time()
            print(f'{buffer = }')
            buffer = ""
    except:
        break

uart.close()
```

## string types

- string: holds unicode characters
- b string: holds numbers between 0 and 255
- f string:

## version

No that `python2 -V` outputs to `stderr` whereas `python3 -V` to `stdout`.

## args and kwargs: variable arguments

- `*args : tuple` are unnamed (positional) arguments
- `*kwargs : dict` are named arguments

```python
def func(*args, **kwargs):
    for i in args:
        print(i)
    print(kwargs)
    for i in kwargs.values():
        print(i)
func(1,2,a=3,b=4)
```

prints:

```
1
2
{'a': 3, 'b': 4}
3
4
```

## Mutable vs immutable

Python’s argument-passing model is neither “Pass by Value” nor “Pass by
Reference” but it is “Pass by Object Reference”.

Depending on the type of object you pass in the function, the function behaves
differently:

- Immutable objects show “pass by value”
- Mutable objects show “pass by reference”

## declaring immutable classes

Basic idea is: overwrite `__setattr__` to raise an exception and use
`super().__setattr__` to set immutable values.

```python
class Immutable:
    def __init__(self):
        super().__setattr__("my_key", "my_value")

    def __setattr__(self, name, value):
        raise AttributeError("This object is immutable.")

lol = Immutable()
lol.my_key # returns "my_value"
lol.my_key = 2 # throws exception
```

## Finite State Machine (FSM)

- [python-fsm](https://github.com/oozie/python-fsm)
- [Generator-based State Machines and
  Coroutines](https://gnosis.cx/publish/programming/charming_python_b5.html)

## abstract base class

```python
from abc import ABC, abstractmethod # builtin

class Shape(ABC):
    @abstractmethod
    def area(self): pass

class Square(Shape):
    def area():
        return 1
```

## overloading

- [adding function overloads to
  python](https://www.youtube.com/watch?v=yWzMiaqnpkI)

Use
[metaclass](https://stackoverflow.com/questions/100003/what-are-metaclasses-in-python).

## managing versions

```sh
pyenv install <version>
pyenv exec python3 -m venv .venv
source ./.venv/bin/activate
```

## function / method overload

[Function overfloading](https://stackoverflow.com/a/29091980/12932725)

```python
from functools import singledispatch

@singledispatch
def fun(s):
    print(s)

@fun.register(int)
def _1(s):
    print(s * 2)

@fun.register(list)
def _2(s):
    for i, e in enumerate(s):print(i, e)

@fun.register
def _2(s : dict):
    # the function name does not matter, you may repeat it
    print("dict!")

fun('GeeksforGeeks')
fun(10)
fun(['g', 'e', 'e', 'k', 's'])
```

Alternatively, use the `multipledispatch` package:

```python
from multipledispatch import dispatch

@dispatch(int, int)
def f(a, b):
    print("integers!")

@dispatch(float, float)
def f(a, b):
    print("floats!")
```

## sphinx auto code documentation

- [Documenting Python code with
Sphinx](https://towardsdatascience.com/documenting-python-code-with-sphinx-554e1d6c4f6d)
- [Sphinx primer](sphinx.md#basic)

Basically anything you type in will be accepted, but if you type in a special
key, it will be recognized and translated.

[Function
signatures](https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#python-signatures)

## Dunder methods

- [list of dunder
  methods](https://blog.finxter.com/python-dunder-methods-cheat-sheet/)

## pytest

- [How to use fixtures](https://docs.pytest.org/en/latest/how-to/fixtures.html)
- [Five Advanced Pytest Fixture Patterns](https://www.inspiredpython.com/article/five-advanced-pytest-fixture-patterns)

## logging

- [Logger
  attributes](https://docs.python.org/3/library/logging.html#logrecord-attributes)
