
# Radio

- [Modulation](#modulation): how the information is encoded onto a carrier
  signal to transmit data over a channel
- [Duplexing](#duplexing): communication system ability to allow bidirectional
  tx and rx
- [Multiplexing](#multiplexing): combining multiple analog or digital signals
  into one over a shared medium
- [Access](#access): combining multiple users access and utilize a shared medium
  or system

Note that the multiplexing and access concepts differ in the utilization,
techniques are the same (?).

## Basic glossary

- uplink: transmit downlink: receive
- symbol: smallest unit of time that can carry digital data
- ISI: Intersymbol Interference
- PAPR, PAR: Peak to Average Power Ratio

## Error Vector Magnitude

\[EVM\]: How much error there must be in order for the symbol to be interpreted
the wrong way.

- Vector: the direction
- Magnitude: how much

When dealing with binary modulations, it is a "box" in the complex plane that
shows the "symbol region".

## Modulation

- Frequency modulation
- Amplitude modulation
- Binary modulation
  - \[BPSK\] Binary Phase Shift Keying
  - \[QPSK\] Quadrature Phase Shift Keying
  - \[QAM\] Quadrature Amplitude Modulation

#### Amplitude modulation

$$y(t) = A(t)\cdot \cos(2\pi ft)$$

Demodulation occurs by multiplying it by $\cos(2\pi ft)$, which gives $z(t) =
A(t)(1-cos(4\pi ft)$ and then low-pass filtering.

#### QAM

Uses phase and amplitude to divide the complex into the symbols.

For only two bits (00, 01, 10, 11):

$$I(t) = A_I(t)\cos(2\pi ft)$$
$$Q(t) = A_Q(t)\sin(2\pi ft)$$
$$y(t) = I(t) + Q(t)$$

Demodulation occurs by multiplying $y(t)$ by $\cos(2\pi ft)$ and $\sin(2\pi
ft)$, which gives:

$$z_I(t) = y(t)\cos(2\pi ft) = \frac{A_I(t) + A_I(t)\cos(4\pi
ft)}{2}+A_Q(t)\sin(4\pi ft)$$
$$z_Q(t) = y(t)\sin(2\pi ft) = \frac{A_Q(t) + A_Q(t)\cos(4\pi
ft)}{2}+A_I(t)\sin(4\pi ft) \approx \frac{A_Q(t)}{2}$$

Then low-pass filter it and amplitude-decode the different levels:

$$z_I(t) \xrightarrow{\text{filter}} \frac{A_I(t)}{2}$$
$$z_Q(t) \xrightarrow{\text{filter}} \frac{A_Q(t)}{2}$$

Note that to know the precise phase on the demodulator side is very important.

The number in from of the QAM stands for the total number of possible symbols
for a modulation period. Eg. 4-QAM have 00, 01, 10 and 11 symbols. Typical QAMs
are AM, 4-QAM, 16-QAM and 256-QAM.

About the EVM: it is divided by the number of symbols.

## Multiplexing

- \[FDM\] Frequency Division Multiplexing
- Time multiplexing
- Binary
  - \[OFDM\] Orthogonal Frequency Division Multiplexing
    - \[OFDM\] Cyclic Prefix OFDM

### FDM

- available bandwidth is divided into non-overlapping frequency bands
- one unique frequency = one user

### OFDM

- [Concepts of Orthogonal Frequency Division Multiplexing (OFDM) and 802.11
  WLAN](https://rfmw.em.keysight.com/wireless/helpfiles/89600b/webhelp/subsystems/wlan-ofdm/content/ofdm_basicprinciplesoverview.htm)

- takes the full bandwidth and divide into narrow band channels
- each subcarrier uses a conventional modulation to carry digital data
- selected subcarrier frequencies overlap, but remain orthogonal
- all frequencies are assigned to all users
- provides higher spectral efficiency than FDM

Subcarrier spacing is the distance between two adjacent subcarriers. Larger
subcarrier spacing allows increased bandwidth but shorter range.

#### CP-OFDM

- [What is a Cyclic Prefix in
  OFDM?](https://www.youtube.com/watch?v=AJg57AEBtNw)

> Goal: Improvement over vanilla OFDM, designed to reduce ISI.

- symbols may change abruptly
- multiple paths provide multiple delays for the same signal/symbol
- by extending (prefixing) the _transmit_ symbol duration one more cycle,
  CP-OFDM ensures that the interference in the abrupt change is overcome
- by _cyclic_ prefix, we mean, in the subcarriers summation (OFDM), we copy the
  fixed end in time-domain and put it in front

#### DFT-S-OFDM

Discrete Fourier Transform Spread OFDM

## Duplexing

- \[TDD\] Time Division Duplexing
- \[FDD\] Frequency Division Duplexing

### TDD

- same frequency channel used to transmit and receive signals
- transmission and reception are separated in time

### FDD

- separate frequency bands are used for transmitting and receiving signals
- allows for full-duplex communication as it uses different frequency channels
  for the uplink (transmit) and downlink (receive) paths between two stations

## Access

- \[FDMA\] Frequency-Division Multiple Access
  - \[SC-FDMA\] Single-Carrier FDMA: allocate different OFDM subcarrier groups
    to different users and call the groups a single carrier
- \[TDMA\] Time-Division Multiple Access
- \[CDMA\] Code-Division Multiple Access

- FDMA uses some of the frequency band all of the time
- TDMA uses all of the frequency band some of the time
- CDMA uses all of the frequency band all of the time
