
# Real-time Operating System (RTOS)

- [RTOS
  series](https://www.youtube.com/watch?v=TEq3-p0GWGI&list=PLPW8O6W-1chwyTzI3BHwBLbGQoPFxPAPM&index=23)
- [Interaction Diagram](https://www.iar.com/knowledge/learn/programming/using-rtos-diagrams/)

- freertos
- zephyr
- vxworks
- uc-os-iii

RTOS are essentially a way to make multiple tasks run parallel-like in a same
core (multitasking). Thus, there is a basic need of a scheduler. Additionaly
resource management software like mutexes (aka. locks), spinlocks and
semaphores.

## Tasks

A task is just a main function.

- Task = thread
- Multitask = multithread
- Context switch: change between tasks

Because of a RTOS delay function, there must always be an idle / dummy thread
ready. E.g. you have only two tasks, both call the OS delay function, which
means to give execution to another thread. In this case the dummy thread can be
used. Idle thread are the perfect place to put the microcontroller into sleep
mode.

## Scheduler

Schedulers are classified as a

- Static-priority: assigns all priorities at design time, priorities remain
  constant for the tasks lifetime.
- Dynamic-priority: assigns priorities at runtime, based on execution parameters
  of tasks, such as upcoming deadlines.
- Mixed-priority: has both static and dynamic components.

### preemptive scheduler

Preemption means interruption.

Most RTOS use a "preemptive scheduler". This type of scheduler takes existing
Interrupt Subroutines (ISR) from peripherals[^arm-isr], adding code to the interrupts
routines to switch the context. Moreover, it is obvious that the task context
has to be stored, therefore a heap has to be allocated for every task.

[^arm-isr]: almost all RTOSes in ARM use PendSV at the lowest priority

## Priority

Deadlocks occur by circular dependencies or priority inversion.

- Hard real-time: "usefullness" of real-time is mission-critical
  - Eg. air-bag system
- Soft real-time: "usefullness" of real-time is not mission-critical, but
  required
  - Eg. messaging system

### Priority inversion

- [Introduction to Priority
  Inversion](https://barrgroup.com/embedded-systems/how-to/rtos-priority-inversion)

```plantuml
robust "Tasks" as tasks
concise "Resource" as resource

@0
tasks is "low prio"
resource is "idle"
@+1
resource is "locked"
@+1
tasks is "medium prio"
@+1
tasks is "low prio"
@+1
tasks is "high prio"

@1 <-> @4 : high waiting for low to release resource
```

Solutions:

- Priority Inheritance Protocol (PIP): lower priority task inherits priority
  from higher priority task when using common resources.
  - If the RTOS lets, one can dynamically change the task priority (just like a
    mutex) aka [dynamic priority scheduler](#scheduler).
- Priority Ceiling: Set the priority per resource instead of the task itself.
  This has the added benefit of not having to use mechanisms like mutexes
  anymore.

## priority choosing algorithms

A task set is considered schedulable if all tasks meet all deadlines all the
time.

0. know how much time does the RTOS take to do its thing (Tr)
1. get to know the task deadline $Ti$
2. estimate how long does each task take $Ci$ (os delay function + what it
   actually takes to complete)
3. calculate usage time $Ui = \frac{Ti}{Ci}$

Note that you have a problem if $\sum_i Ui > 100% - Tr$ ($Tr$ is an invention
from this wiki).

### Rate Monotonic Scheduling (RMA)

This applies for preemptive schedulers.

- [Introduction to Rate Monotonic
  Scheduling](https://barrgroup.com/embedded-systems/how-to/rma-rate-monotonic-algorithm)

RMA is the optimal static-priority algorithm. It assigns priorities according to
the deadlines. Its worst case is: $t \cdot (2^\frac{1}{t} - 1)$ ($t$ = number of
tasks).

All threads are mathematically guaranteed to reach their deadlines
if $\sum_i Ui \leqslant t \cdot (2^\frac{1}{t} - 1)$ is met. In practice, keep
the total CPU utilization below 70%:

$$
\mathrm{lim}_{n\rightarrow\infty} (2^\frac{1}{t} - 1) = \ln(2) \approx 0.7
$$

ISRs muss also be considered in the RMA calculations. An ISR with a deadline
longer than a task with hard-time constraints results in a violation of the RMA
principle.

##  Synchronization mechanisms

Each method name is specific to the RTOS.

### semaphore

Semaphore is a up-to-down counter, whose count value means how many tasks may
make use of the shared resource simultaneously. There are two methods related
to semaphores:

- take / post: decrease the counter number, marking the use of a resource
- release / give: increase the counter number, untangling the resource from a
  specific task

Note that these methods will be used by multiple tasks on the same semaphore.
If the count is zero and a task asks to take, it can either wait indefinitely or
timeout.

Semaphores are best used as a signaling from one task to another about a
resource. Example is a FIFO with multiple producers and multiple consumers.
Another example is when a task needs to wait for the user to press a button.

### mutex

Mutex = mutual exclusion. It implies an ownership of a shared resource.

Locks the resource to a single thread. Two methods are related to mutexes:

- take / lock: take absolute control of the shared resource
- give / release: release control of the shared resource

The release method can only be used by the task that locked the semaphore.
Mutexes use [priority inheritance](#priority-inversion) mechanism.

## Designing software with RTOS

Recommendations are:

- encapsulate RTOS functions into an Operating System Abstraction Layer (OSAL)
  for ease of RTOS change later on
- use the outside-in technique[^outside-in-technique] to define initial tasks:
  look at the system from outside, beginning from the physical features
- create a data flow diagram: helps in getting a feel for the major RTOS
  components in the system
- define the task and message interfaces upfront: examine each message queue
  upfront and then build out the structure for those messages

[^outside-in-technique]: not to be confused with the TDD "top-bottom" / "School
of London" technique.

In summary, just apply SOLID to RTOS: keep RTOS agnostic; define the tasks;
define their interfaces.
