
# Lua

- [Lua](https://www.lua.org/)
- [Programming in Lua](https://www.lua.org/pil/contents.html): first edition,
  maybe outdated
- [Lua Missions](https://github.com/kikito/lua_missions): lua koan

- it is dynamically typed
- it does not have classes
- it has only one data structure, a table
- variables and functions are global by default, use the `local` specifier

```lua
hello = "Hi Mom"
print(hello)

function doMath(n)
    return x*2
end

local lambda = function(n) return n end
local a, b = "b"

doMath(2)
doMath '2'
lambda(3)

callMeLater(doMath)

array = {"Landing", "on", "Lua"}
-- array[1] = Landing

dict = {
    ['moon'] = "lua",
    ['cheese'] = "queijo"
}

-- dict.moon is the same as dict['moon']

for k, v in pairs(dict) do
    print(k, v)
end

--[[
multiline
comment
--]]

function()

assert(type(type) == "function", "type is a function + type returns the type as a string")
```

## strings

- [manual](https://www.lua.org/manual/5.3/manual.html)

```lua
a = 'alo\n123"'
a = "alo\n123\""
a = '\97lo\10\04923"'
a = [[alo
123"]]
a = [==[
alo
123"]==]
```

To concatenate strings, use the `..` operator. Type conversion is handled automatically.

## functions

The following are equivalent:

```lua
function lol(var1, var2) end
lol = function(var1, var2) end
```

If you do not give an argument, the interpreter automatically makes it `nil`.

Functions may also use the `local` keyword. If a function only has one argument, you may give it
without the parenthesis.

Functions may also be declared within a table:

```lua
local mymath =  {} -- note the local keyword
function mymath.add(a,b) print(a+b) end
function mymath.sub(a,b) print(a-b) end
function mymath.mul(a,b) print(a*b) end
function mymath.div(a,b) print(a/b) end
return mymath
```

## module

A module is a `require`ble file that contains a table (equivalent to a C++ class file).

## package

A folder containing modules and a `init.lua` file. A package may contain more packages.

For example:

```
package/
 |
 |- init.lua
 |- moduleA.lua
 |- moduleB.lua
```

When you use `require("package")` it automatically calls `package/init.lua`. To make it load the
`package/moduleA.lua`, call `require("package.moduleA")`.

## ipairs vs pairs

```lua
> u={}
> u[1]="a"
> u[3]="b"
> u[2]="c"
> u[4]="d"
> u["hello"]="world"
> for key,value in ipairs(u) do print(key,value) end
1   a
2   c
3   b
4   d
> for key,value in pairs(u) do print(key,value) end
1   a
hello   world
3   b
2   c
4   d
```

## "switch case"

```lua
local switch = {
    [1] = function()
        print "case 1"
    end,
    [2] = function()
        print "case 2"
    end,
    [3] = function()
        print "case 3"
    end
}

local a = 4
local f = switch[a]
if(f) then
    f()
else
    print "default"
end
```

## loops

```lua
while i <= 10 do
    i = i + 1
end

while true do
if i > 10 then break end
    i = i + 1
end

repeat
    i = i + 1
until i==11

local t = { 'fish', 'and', 'chips' }
local result = {}
for i=1, #t do
    table.insert(result, t[i])
end

local t = { 1,2,3,4,5,6 }
local result = {}
for i=2, #t, 2 do -- notice the two here
    table.insert(result, t[i])
end

local t = { 'fish', 'and', 'chips' }
local result = {}
for key,value in ipairs(t) do
    table.insert(result, value)
end
local t = { a = 1, b = 2 }
local result = {}
for k,v in pairs(t) do
    result[k] = v
end
```

## require from parent folder

- [Is there a better way to require file from relative path in
  lua](https://stackoverflow.com/questions/5761229/is-there-a-better-way-to-require-file-from-relative-path-in-lua)

```
ball.lua
- ball_test.lua
```

```lua
-- ball_test.lua
package.path = package.path .. ";../?.lua"
require('ball')
```

## OOP

- [METABLE | What are they and how can we use them?](https://www.youtube.com/watch?v=mtjCgqyZ5wk)

- Doing `my_table.func(my_table)` is the same as `my_table:func()`
- Declaring a method with colon is the same concept, `self` parameter will be
  the table

```lua
local person = {} -- namespace
--[[
person.prototype = default values
--]]
person.prototype = { speed = 5, strengh = 100 }
person.metatable = {
    __index = person.prototype,
    __call = function(table)
                 print('functable')
             end
 }

function person:new(o)
    o = o or {}
    setmetatable(o, person.metatable)
    return o
end

local John = person:new({ speed = 4 })

print(John.speed) -- 4
print(John.strengh) -- 100
John() -- 'functable'
```

### Metatable

- [lua metatable methods
  cheatsheet](https://gist.github.com/oatmealine/655c9e64599d0f0dd47687c1186de99f)

A metatable is a table attached to another table with the `setmetatable`
function. Its goal is to act as a "helper", defining what should happen when
specific situations occur.

A metamethod is a value (usually a function) attached to _reserved keys_ in a
metatable that are used during specific situations[^metamethods].

[^metamethods]: think is as a Python dunder.

## Documenting function

- [Annotations](https://github.com/LuaLS/lua-language-server/wiki/Annotations)

There is no standard. But the Lua language server is recommended.

Example:

```lua
--- @module M 'utils'
local M = {}

--- Dumps a Lua table or value to a string.
--- @param o integer The table or value to be dumped.
--- @return string table A string representation of the table or value.
function M.dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. M.dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

return M
```

## print table

- [How to Dump a Table to
  Console](https://stackoverflow.com/questions/9168058/how-to-dump-a-table-to-console)

There is no standard function to print a table. This is a quick and dirty way:

```lua
function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end
```
