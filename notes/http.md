
# Hypertext Transfer Protocol

## URL anatomy

- host URL: `https://api.web.com/`
- endpoint path: `end/point/path`

## automatically typing in

Many alternatives:

- [hoppscotch](https://hoppscotch.io/)
- postman
- [insomnia](https://insomnia.rest/)

## manually typing in

- [How to send an HTTP request using
  telnet](https://stackoverflow.com/questions/15772355/how-to-send-an-http-request-using-telnet)

You may use telnet to manually type in HTTP methods:

```sh
telnet stackoverflow.com 80
```

This in tun opens up the "HTTP shell", then you may type as the example (with
the two empty lines):

```http
GET /questions HTTP/1.0
Host: stackoverflow.com


```

You can also do it with `openssl` or `nc` (netcat), both allow for SSL, whereas
`telnet` does not:

```sh
openssl s_client -connect stackoverflow.com:443 # or
printf "%s\r\n" \
    "GET /questions HTTP/1.1" \
    "Host: stackoverflow.com" \
    "" |
nc --ssl stackoverflow.com 443
```

## request methods

- [HTTP request methods
  explained](https://www.freecodecamp.org/news/http-request-methods-explained/)

- `GET`: request data from a specified resource
  `/test/demo_form.php?key1=value1&key2=value2`
  - cacheable
  - remains in browser history
  - bookmarkable
  - should not be used when dealing with sensitive data: information is sent via
    URL
  - has length restriction
- `POST`: send data to a server to create/update a resource
  - never cached
  - does not remain in browser history
  - not bookmarkable
  - no data length restriction

```http
POST /test/demo_form.php HTTP/1.1
Host: w3schools.com

key1=value1&key2=value2
```

- `PUT`: send data to a server to create/update a resource
- `OPTIONS`
- `HEAD`: almost identical to GET, but without the response body
- `DELETE`
- `TRACE`: perform a message loop-back test that tests the path for the target
  resource
- `CONNECT`: start a two-way communications (a tunnel) with the requested
  resource
- `PATCH`

## Response status codes

[HTTP response status
codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

- [100-199] informational responses
- [200-299] successful responses
- [300-399] redirection messages
- [400-499] client error responses
- [500-599] server error responses
