
# TRACE32

Lauterbach software is called TRACE32.

There are some Lauterbach hardware: Power Debugger is one of them. Power
Debugger versions USB and Ethernet are available.

GDB frontend is called "Run Mode debug".

## Breakpoints

- [In 10 minutes - Learn how to use breakpoints in TRACE32](https://www.youtube.com/watch?v=sX27TqkKHlI)

Types are:

- Program: every time the instruction is executed.
- Watch or read/write: whenever the program reads or writes to certain memory
  location (everything that is memory mapped). Not possible if variable on the
  register or stack.
  - In most CPU architectures, watch breakpoints are not synchronous: they stop
    after the event has occurred (eg. Cortex-M).

Methods are:

- Software: for code running inside RAM. Implemented by replacing the code with
  a breakpoint or a trap instruction.
- On chip:

## Symbol resolution

Resolution = get the symbol address.

TRACE32 can only resolve symbols if they are static.

I guess it resolves by looking at the ELF file. This means that it is not
according to the debugger bus mapping.

## Ranges

- `0x00000000--0xFFFFFFFF` or `0x00000000..0xFFFFFFFF`
