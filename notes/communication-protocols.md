
# Communication Protocols

See also

- [For wireless communication](radio.md)
- [For cellular communication (\*G)](cellular-network.md)

## Code Rate

- Code rate / information rate: ratio between information bits and total
  transmitted bits (information + redundant bits)

## Open Systems Interconnection (OPI)

Each layer is responsible for:

1. physical layer (L1): transmission of bits over physical data link, groupping the
   bitstreams aka channels into symbols, coded and convert to physical signal to
   be transmitted over a medium
2. data link layer (L2)
3. network layer (L3)
4. transport layer (L4)
5. session layer (L5)
6. presentation layer (L6)
7. application layer (L7)
