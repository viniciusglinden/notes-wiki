
# Rust

Rust is an expression-oriented language.

- [The Rust Programming Language](https://doc.rust-lang.org/book/)

- New application in current directory: `cargo init`
- New library: `cargo new --lib <name>`
- New application: `cargo new --bin <name>`
- Run tests: `cargo test`


- build and run a project in one step using cargo run `cargo run`
- create a project using cargo new: `cargo new`
- build a project using cargo build: `cargo build`
  - with optimizations: `cargo build --release`
- build a project without producing a binary to check for errors using cargo
  check `cargo check`

## Exercises

- [rustlings](https://github.com/rust-lang/rustlings)
- [exercism](https://exercism.org)
- [Rust-101](https://www.ralfj.de/projects/rust-101/)

## Trait

- [Derive](https://doc.rust-lang.org/rust-by-example/trait/derive.html)

Think of it as a class interface, that is, it defines methods that must be
present in a `struct` that `impl`ements a `trait`.
It can also be passed as a type.

```rust
impl Summary {
    fn summerize(&self) -> String;
    fn default_text(&self) -> String {
        String::from("this will be the function implemented by default.")
    }
}
struct Text;
impl Text for Summary {
    fn summerize(&self) -> String {
        String::from("example")
    }
}

fn return_text() -> impl Summary {
    Text {}
}
```


```rust
fn notify(item: &impl Summary) {
    println!("breaking news! {}", item.summerize());
}
// or bounding a generic type to implement the `Summary` trait
fn notify<T: Summary>(item: &T) {
    println!("breaking news! {}", item.summerize());
}
// alternatively
fn notify<T>(item: &T)
where
    T: Summary
{
    println!("breaking news! {}", item.summerize());
}
```

The compiler is also able to derive some traits, use the preprocessing
`#[derive(TRAIT)]`:

- `Eq`, `PartialEq`, `Ord`, `PartialOrd`
- `Clone`
- `Copy`
- `Hash`
- `Default`
- `Debug`

Operators are implemented as traits. The default operators are found under
`std::ops`.

## Ownership

- [What is
  Ownership?](https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html)

1. Each value in Rust has an owner
2. There can only be one owner at a time
3. When the owner goes out of scope, the value will be dropped


Function parameters also take ownership, thus making values invalid. You can
return the value or just **borrow** it.

```rust
fn main() {
    let foo = 42_i32;
    borrowed_i32(foo);
    println!("foo {} is still accessible", foo);
}

fn borrow_i32(borrowed_i32: &i32) {
    println!("This int {} is borrowed", borrow_i32);
}
```

## Lifetime

- [Rust Lifetimes Finally
  Explained!](https://www.youtube.com/watch?v=juIINGuZyBc)

Lifetimes are annotated with an apostrophe.

The following displays the problem where a lifetime of a referenced value (x) is
shorter than the reference (r). As in any language, Rust will complain that you
cannot reference a local variable that no longer exists.

```rust
fn main() {
    let r;

    {
        let x = 5;
        r = &x;
    }
    println!("r: {}", r); // Problem
}
```

The following example has been fixed with **generic lifetime annotation**. These
annotations don't change the lifetime, rather specify the relationship between
them. Here lifetime `a` is used to specify that `x`, `y` and the return value
will have the same lifetime as the smallest lifetime of the parameters. This
means: if it will have the lifetime of `x` if its lifetime is smaller than that
of `y`.

```rust
fn main() {
    let string1 = String::from("abcd");
    let string2 = String::from("xyz");

    let result = longest(string1.as_str(), string2.as_str());
    println!("The longest string is {}", result);
}

fn longest<'a>(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

The lifetime declaration inside a function always has to be tied to a lifetime
of a parameter.

We don't actually always need to specify the lifetimes, for that, Rust uses the
following rules:

1. Each parameter that is a reference gets its own lifetime parameter
2. If there is exactly one input lifetime parameter, that lifetime is assigned
   to all output lifetime parameters
3. If there are multiple input lifetime parameters, but one of them is `&self`
   or `&mut self`, the lifetime of `self` is assigned to all output lifetime
   parameters

The reserved `static` lifetime name specifies that the object will remain valid
for the whole program lifetime.
