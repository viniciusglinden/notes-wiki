
# ld

ld provides explicit control over the link process, allowing complete
specification of the mapping between the linker's input files and its output/

- [Command
  language](https://ftp.gnu.org/old-gnu/Manuals/ld-2.9.1/html_chapter/ld_3.html)
- [Using ld](https://ftp.gnu.org/old-gnu/Manuals/ld-2.9.1/html_mono/ld.html)
- [ELF specification](https://refspecs.linuxbase.org/elf/elf.pdf)
- [How to write linker scripts for
  firmware](https://interrupt.memfault.com/blog/how-to-write-linker-scripts-for-firmware)
- [The most thoroughly commented linker script
  (probably)](https://blog.thea.codes/the-most-thoroughly-commented-linker-script)

A linker script contains four things:

- Memory layout: what memory is available where
- Section definitions: what part of a program should go where
- Options: [optional] commands to specify architecture, entry point, ...etc
- Symbols: variables to inject into the program at link time

## Entry

Sets the entry point address. It is not mandatory, but required for gdb.

```ld
Entry(Reset_Handler)
```

## Memory

Specifies the memory layout.

```ld
MEMORY
  {
    name (attribute) : ORIGIN = origin, LENGTH = len
  }
```

- attribute: xrwai!
  - x: section contains executable code
  - r: read-only
  - w: read and write
  - a: allocated
  - i: initialized
  - !: invert the sense of any of the following attributes

Marking a region as non-writable does not mean that it is write protected:
attributes are meant to describe the properties of the memory, not set it.

## Sections

- [Writing linker scripts and section
  placement](https://www.youtube.com/watch?v=B7oKdUvRhQQ)

> **WARNING**: section may contain wrong information

Section controls:

- where input sections are placed into output sections
- their order in the output file
- to which output sections they are allocated

You can have as many statements within it as you wish.

```ld
/*
 Simplest example
 */
SECTIONS {
  secname : {
    contents
  }
}
```

The special secname `/DISCARD/' may be used to discard input sections.

Sections are typically named:

- `.text`: code & constants
- `.bss`: uninitialized data
- `.stack`: stack
- `.data`: initialized data
- `.rodata`: read-only data
- `.comment`: added by the compiler (information read by the `file` command)

Every section in the linker script has two addresses: [LMA,
VMA](./elf.md#memory-addresses).

```ld
.my_section : {
    /*
    ...
    */
}>(VMA) AT>(LMA)
```

If VMA = LMA, just write `}> FLASH`.

Alternatively:

```ld
.my_section VMA : AT(LMA) {
    /*
    ...
    */
}>(VMA)
```

Usually you want to merge different input object files:

```
main.o { .text, .data, .bss, .rodata }
led.o  { .text, .data, .bss, .rodata }
final.elf = {
    .text    => main.o(.text),   led.o(.text)
    .data    => main.o(.data),   led.o(.data)
    .bss     => main.o(.bss),    led.o(.bss)
    .rodata  => main.o(.rodata), led.o(.rodata)
}
```

```ld
SECTIONS
{
    .text : {
        *(.isr_vector)
        *(.text)
        *(.rodata)
    }> FLASH

/* .data will load values from flash, but keep them in SRAM */
    .data : {
        *(.data)
    }> SRAM AT> FLASH

    .bss : {
        *(.bss)
    }> SRAM
}
```

Available "tags" are:

- NOLOAD: do not fill the LMA

## Symbols

Keeps a table that translates symbol name to address in memory.

```ld
<name> = <address>;
```

The `.` is known as the location counter, it always tracks VMA - not LMA.

Compile with `-Wl,-Map=symbols.map` to generate a file that describes the table.
Or use `nm`.

Available "tags" are:

- `ALIGN(#)`: byte alignment, to align on every four bytes: `. = ALIGN(4)`
- `KEEP(?)`: don't exclude symbol during linker time garbage collection
  (`--gc-sections`)
- `LENGTH(memory)`: length of memory region
- `ORIGIN(memory)`: origina of memory region
- `SIZEOF(section)`: size in bytes of section
- `LOADADDR(section)`: absolute LMA of section

Symbols that are not contained in any specific section are automatically
contained in the `COMMON` section.

### Access symbol from C

In the linker, just declare: `<name> = <address>`, the in the C code:

```c
extern uint8_t <name>;
// &<name> == <address>
```

## debugging symbols

```sh
nm file.elf
```

## place named sention at given address

- [How to Create a Super Simple Bootloader](https://youtu.be/OkUQ3iMmiYQ?t=330)

```ld
/* .name address flags */
.myBufBlockRAM 0x20000100 (NOLOAD):
{
    KEEP(*(.myBufSectionRAM)) /* keep variable even if not referenced */
} > RAM
```

Note that address is not relative to LMA.

If there was no `NOLOAD` written, the binary would be generated without a
problem, but it would also create a continuous region representing the variable.
In more tecnical

Then in code:

```c
unsigned char __attribute__((section(".myBufSectionRAM"))) buffer[128];
```
