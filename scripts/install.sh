#!/bin/sh
# Author: Vinícius Gabriel LINDEN
# Date: 2020-06-10
# Description: Install notes-wiki related files

name=$(basename "$0")
echo "This is $name script"

# exit script at first error
set -e

# get script directory
DIR=$(dirname "$(readlink -f "$0")")
PREFIX="$HOME/.local/bin"

help() {
	echo ""
	echo "OPTIONAL:"
	echo "    --help: displays this help"
	echo "    --prefix: export notes-wiki directory [~/note-wiki]"
	echo ""
}

for i in "$@"
do
	case $i in
		--help)
			help
			exit 0
			;;
		--prefix=*)
			PREFIX="${i#*=}"
			;;
		*)
			echo "Unknown option: $i"
			exit 126
			;;
	esac
done

for i in "$DIR"/*; do
	[ "$(basename "$i")" != "$name" ] && ln -sfv "$i" "$PREFIX"
done

echo "Installation completed"

