
Personal wiki for quick consultation.

Why not just use [vimwiki](https://github.com/vimwiki/vimwiki)? Because:

- non standard syntax
- I prefer to have control over my personal wiki
- I prefer to use shell script over ViML
- not tied to ViM (even if it is great)
- I don't need all of that functionality, just a simple way to organize my
  markdown files

# notes

Some content I wrote on my own, some is just copy \& paste from the Internet.
Don't expect to be well written: I wrote it for my own understanding.

# Scripts

- `install` copies content of this folder to the specified path
- `wgit` short for wiki git, just a wrapper for git
- `wiki` calls the main page

[License](LICENSE.md) for the scripts
